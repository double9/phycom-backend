<?php
$params = array_merge(
    require(PHYCOM_PATH . '/base/config/params.php'),
    require(__DIR__ . '/params.php'),
	require(ROOT_PATH . '/common/config/params.php'),
	require(ROOT_PATH . '/backend/config/params.php')
);

return [
    'id' => 'app-backend',
    'aliases' => [
        '@Phycom/Backend' => '@phycom/backend/src'
    ],
    'basePath' => ROOT_PATH . '/backend',
    'controllerNamespace' => 'Backend\Controllers',
    'bootstrap' => ['log'],
    'modules' => [],
    'components' => [
        'modelFactory' => [
            'class' => \Phycom\Backend\Components\ModelFactory::class
        ],
        'request' => [
            'csrfParam' => '_csrf_phycom_backend',
        ],
        'user' => [
        	'class' => \Phycom\Base\Components\User::class,
            'identityClass' => \Phycom\Base\Models\User::class,
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity_phycom_backend'],
        ],
        'session' => [
            // this is the name of the session cookie used for login on the backend
            'name' => 'advanced-backend',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => \yii\log\FileTarget::class,
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'statistics' => [
            'class'   => \Phycom\Backend\Components\Statistics::class,
            'enabled' => true
        ],
        'dashboard' => [
            'class'   => \Phycom\Backend\Components\Dashboard::class,
            'enabled' => true
        ],
        'landingPage' => [
            'class'   => \Phycom\Backend\Components\LandingPage::class,
            'enabled' => true
        ],
        'commerce' => [
            'components' => [
                'sale' => [
                    'class'   => \Phycom\Backend\Components\Commerce\Sale::class,
                    'enabled' => true
                ],
                'shop' => [
                    'backendShopListColumns' => [
                        'id',
                        'name',
                        'address',
                        'phone',
                        'status',
                        'created_at',
                        'updated_at'
                    ],
                    'backendVendorListColumns' => [
                        'id',
                        'name',
                        'address',
                        'phone',
                        'status',
                        'created_at',
                        'updated_at'
                    ]
                ]
            ]
        ],
        'urlManager' => require(__DIR__ . '/url-manager.php'),
	    'assetManager' => [
		    'bundles' => [
			    'dmstr\web\AdminLteAsset' => [
				    'skin' => false,
			    ],
		    ],
	    ],
        'i18n' => [
            'translations' => [
                'backend*' => [
                    'class'    => \Phycom\Base\Components\MessageSource::class,
                    'basePath' => '@translations',
                    'catalog'  => 'backend'
                ],
                'phycom/backend*' => [
                    'class'    => \Phycom\Base\Components\MessageSource::class,
                    'basePath' => '@phycom/backend/translations',
                    'catalog'  => 'backend'
                ]
            ]
        ]
    ],
    'params' => $params,
];
