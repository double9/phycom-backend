<?php

/**
 * @var $this yii\web\View
 * @var \Phycom\Backend\Models\ShopForm $model
 */

use Phycom\Backend\Widgets\Box;

$this->title = Yii::t('phycom/backend/vendor', 'Add vendor');
$this->params['breadcrumbs'][] = ['label' => Yii::t('phycom/backend/shop', 'Vendors'), 'url' => ['/vendor/list']];

?>

<div class="row">

    <div class="col-md-12">

        <?= Box::begin([
            'showHeader' => false,
            'options' => ['class' => 'box box-default'],
            'bodyOptions' => ['class' => 'box-body no-padding']
        ]);
        ?>

        <div id="vendor-tabs" class="nav-pills-custom">
            <ul class="nav nav-pills nav-stacked col-md-2">
                <li class="active"><a href="#tab1" data-toggle="pill"><?= Yii::t('phycom/backend/vendor', 'Vendor info'); ?></a></li>
            </ul>
            <div class="tab-content col-md-10">
                <div class="tab-pane active" id="tab1">
                    <?= $this->render('partials/info', ['model' => $model]); ?>
                </div>
            </div><!-- tab content -->
        </div>

        <?= Box::end(); ?>

    </div>
</div>


<div class="row">
    <div class="col-md-12">
        <?= Box::begin([
            'showHeader' => false,
            'options' => ['class' => 'box box-default'],
            'bodyOptions' => ['class' => 'box-body']
        ]);
        ?>
        <div class="clearfix">
            <div class="pull-left"><?= $this->render('/partials/back-btn', ['size' => 'lg']); ?></div>
            <?= $this->render('/partials/save-btn-2', [
                'id'           => 'save-vendor-data',
                'class'        => 'pull-right btn-lg',
                'formSelector' => '#vendor-tabs .tab-content > .tab-pane.active'
            ]);
            ?>
        </div>
        <?= Box::end(); ?>
    </div>
</div>
