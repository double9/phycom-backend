<?php

/**
 * @var $this yii\web\View
 * @var $model \Phycom\Base\Models\Attributes\ShopSetting
 */

use Phycom\Backend\Widgets\ActiveForm;

?>


<div class="row">
    <div class="col-md-8">

        <?php $form = ActiveForm::begin(['id' => 'vendor-settings-form']); ?>


        <?= $form->field($model, 'closedMessage')->textarea(['rows' => 5]); ?>
        <?= $form->field($model, 'defaultClosedMessage')->textarea(['rows' => 5]); ?>



        <?php ActiveForm::end(); ?>

    </div>
</div>

