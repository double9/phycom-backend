<?php


use Phycom\Backend\Models\Vendor\Search as SearchVendor;
use Phycom\Backend\Widgets\Box;
use Phycom\Backend\Widgets\DataGrid;

use Phycom\Base\Helpers\Filter;

use yii\data\ActiveDataProvider;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;

/**
 * @var View $this
 * @var SearchVendor $model
 * @var ActiveDataProvider $dataProvider
 */

$this->title = Yii::t('phycom/backend/shop', 'Vendors');
$this->params['breadcrumbs'][] = $this->title
?>
<div class="row">
	<div class="col-md-12">
		<?= Box::begin([
			'options' => ['class' => 'box box-default box-wide'],
			'showHeader' => false,
			'bodyOptions' => ['class' => 'box-body']
		]);
		?>

		<?= DataGrid::widget([
			'id' => 'user-grid',
			'dataProvider' => $dataProvider,
			'filterModel' => $model,
			'rowOptions' => function ($model) {
                return [
                    'class'    => 'row-link',
                    'data-url' => Url::toRoute(['vendor/edit', 'id' => $model->id])
                ];
			},
			'actions' => [
                [
                    'label'   => '<span class="fas fa-plus"></span>&nbsp;&nbsp;' . Yii::t('phycom/backend/shop', 'Add vendor'),
                    'url'     => ['vendor/add'],
                    'options' => ['class' => 'btn btn-success']
                ]
			],
			'columns' => [
                [
                    'attribute'      => 'id',
                    'format'         => 'integer',
                    'options'        => ['width' => '120'],
                    'contentOptions' => ['class' => 'hidden-sm hidden-xs'],
                    'filterOptions'  => ['class' => 'hidden-sm hidden-xs'],
                    'headerOptions'  => ['class' => 'hidden-sm hidden-xs'],
                    'visible'        => Yii::$app->commerce->shop->vendorListHasColumn('id')
                ],
                [
                    'label'          => $model->options->getAttributeLabel('logo'),
                    'format'         => 'raw',
                    'value'          => function ($model) {
                        /**
                         * @var SearchVendor $model
                         */
                        return Html::img($model->options->getLogoUrl()->medium, ['style' => 'max-width: 80px; max-height: 45px;']);
                    },
                    'options'        => ['width' => 80],
                    'contentOptions' => ['style' => 'text-align: center; padding: 0;']
                ],
                [
                    'attribute' => 'name',
                    'format'    => 'text',
                    'visible'   => Yii::$app->commerce->shop->vendorListHasColumn('name')
                ],
                [
                    'attribute'      => 'address',
                    'format'         => 'address',
                    'contentOptions' => ['class' => 'hidden-sm hidden-xs'],
                    'filterOptions'  => ['class' => 'hidden-sm hidden-xs'],
                    'headerOptions'  => ['class' => 'hidden-sm hidden-xs'],
                    'visible'        => Yii::$app->commerce->shop->vendorListHasColumn('address')
                ],
                [
                    'attribute'      => 'phone',
                    'format'         => 'phone',
                    'contentOptions' => ['class' => 'hidden-sm hidden-xs'],
                    'filterOptions'  => ['class' => 'hidden-sm hidden-xs'],
                    'headerOptions'  => ['class' => 'hidden-sm hidden-xs'],
                    'visible'        => Yii::$app->commerce->shop->vendorListHasColumn('phone')
                ],
                [
                    'attribute' => 'status',
                    'filter'    => Filter::dropDown($model, 'status', \Phycom\Base\Models\Attributes\VendorStatus::displayValues()),
                    'format'    => 'raw',
                    'value'     => function ($model) {
                        return '<span class="label ' . $model->status->labelClass . '">' . $model->status->label . '</span>';
                    },
                    'options'   => ['width' => '120'],
                    'visible'   => Yii::$app->commerce->shop->vendorListHasColumn('status')
                ],
                [
                    'attribute'      => 'created_at',
                    'filter'         => Filter::daterangepicker($model, 'createdFrom', 'createdTo'),
                    'format'         => 'datetime',
                    'options'        => ['width' => '220'],
                    'contentOptions' => ['class' => 'hidden-sm hidden-xs'],
                    'filterOptions'  => ['class' => 'hidden-sm hidden-xs'],
                    'headerOptions'  => ['class' => 'hidden-sm hidden-xs'],
                    'visible'        => Yii::$app->commerce->shop->vendorListHasColumn('created_at')
                ],
                [
                    'attribute'      => 'updated_at',
                    'filter'         => Filter::daterangepicker($model, 'updatedFrom', 'updatedTo'),
                    'format'         => 'datetime',
                    'options'        => ['width' => '220'],
                    'contentOptions' => ['class' => 'hidden-sm hidden-xs'],
                    'filterOptions'  => ['class' => 'hidden-sm hidden-xs'],
                    'headerOptions'  => ['class' => 'hidden-sm hidden-xs'],
                    'visible'        => Yii::$app->commerce->shop->vendorListHasColumn('updated_at')
                ],
                [
                    'label'          => false,
                    'format'         => 'raw',
                    'content'        => function ($model) {

                        return $this->render('/partials/delete-dropdown', [
                            'label'   => '<span class="far fa-trash-alt"></span>',
                            'url'     => ['vendor/delete', 'id' => $model->id],
                            'options' => ['class' => 'toggle-hover'],
                            'side'    => 'right'
                        ]);
                    },
                    'options'        => ['width' => 40],
                    'contentOptions' => ['style' => 'padding: 2px 0 0 0;', 'class' => 'clickable'],
                    'visible'        => Yii::$app->user->can('delete_vendor')
                ]
			]
		]);
		?>

		<?= Box::end(); ?>
	</div>
</div>
