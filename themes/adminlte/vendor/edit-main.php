<?php

/**
 * @var $this yii\web\View
 * @var VendorForm $model
 */

use Phycom\Backend\Widgets\Modal;
use Phycom\Backend\Widgets\ActiveForm;
use Phycom\Backend\Models\Vendor\VendorForm;

use Phycom\Base\Widgets\Map;

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\DetailView;

$fieldOpt = ['template' => '{input}{error}', 'options' => ['class' => 'no-margin']];
$fieldOptMd = ArrayHelper::merge($fieldOpt, ['options' => ['style' => 'max-width: 300px;']]);
$fieldOptSm = ArrayHelper::merge($fieldOpt, ['options' => ['style' => 'max-width: 200px;']]);

?>

<div class="row">
    <div class="col-md-8">
        <?php $form = ActiveForm::begin(['id' => 'vendor-form']); ?>

        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [

                [
                    'attribute' => 'name',
                    'format' => 'raw',
                    'value' => $form->field($model, 'name', $fieldOpt)->label(false)->textInput(['class' => 'form-control no-margin'])
                ],
                [
                    'attribute' => 'legalName',
                    'format' => 'raw',
                    'value' => $form->field($model, 'legalName', $fieldOpt)->label(false)->textInput(['class' => 'form-control no-margin'])
                ],
                [
                    'attribute' => 'regNumber',
                    'format' => 'raw',
                    'value' => $form->field($model, 'regNumber', $fieldOpt)->label(false)->textInput(['class' => 'form-control no-margin'])
                ],
                [
                    'attribute' => 'vatNumber',
                    'format' => 'raw',
                    'value' => $form->field($model, 'vatNumber', $fieldOpt)->label(false)->textInput(['class' => 'form-control no-margin'])
                ],
                [
                    'attribute' => 'email',
                    'format' => 'raw',
                    'value' => $form->field($model, 'email', $fieldOpt)->label(false)->textInput(['class' => 'form-control no-margin'])
                ],
                [
                    'attribute' => 'address',
                    'format' => 'raw',
                    'value' => function ($model) use ($form, $fieldOpt) {
                        return $form->field($model, 'address', ['options' => ['class' => 'no-margin']])->addressField('vendor-address-modal', $model->addressForm);
                    }
                ],
                [
                    'attribute' => 'phone',
                    'format' => 'raw',
                    'value' => $form->field($model, 'phone', $fieldOptMd)->label(false)->phoneInput2()
                ],
                [
                    'attribute' => 'logo',
                    'format'    => 'raw',
                    'value'     => function ($model) use ($form, $fieldOptMd) {
                        /**
                         * @var VendorForm $model
                         */
                        $fileInput = $form->field($model, 'logo', $fieldOptMd)->label(false)->fileInput();
                        $image = $model->vendor->options->logo
                            ? $model->vendor->options->getLogoUrl()->medium->url
                            : null;

                        return $image ? Html::img($image, ['class' => 'vendor-logo']) . $fileInput : $fileInput;
                    }
                ],
                [
                    'attribute' => 'status',
                    'format' => 'raw',
                    'value' => $form->field($model, 'status', $fieldOptSm)->dropDownList($model->statuses)
                ],
                [
                    'label' => Yii::t('phycom/backend/vendor','VAT Rate'),
                    'format' => 'percent',
                    'value' => function() {
                        if (Yii::$app->commerce->isEnabled()) {
                            return Yii::$app->commerce->getVatRate();
                        }
                        return null;
                    }
                ],
                [
                    'label' => $model->vendor->getAttributeLabel('created_at'),
                    'format' => 'datetime',
                    'value' => $model->vendor->created_at,
                    'visible' => !$model->vendor->isNewRecord
                ],
                [
                    'label' => $model->vendor->getAttributeLabel('updated_at'),
                    'format' => 'datetime',
                    'value' => $model->vendor->updated_at,
                    'visible' => !$model->vendor->isNewRecord
                ]
            ],
        ]);
        ?>
        <?php ActiveForm::end(); ?>


        <?= Modal::widget([
            'id' => 'vendor-address-modal',
            'title' => $model->vendor->address
                ? Yii::t('phycom/backend/vendor','Update address')
                : Yii::t('phycom/backend/vendor','Add address'),
            'content' => $this->render('/partials/address-form', ['model' => $model->addressForm])
        ]);
        ?>

    </div>

    <div class="col-md-4">
        <?php if (!$model->vendor->isNewRecord): ?>
            <?= Map::widget(['address' => $model->vendor->address]); ?>
        <?php endif; ?>
    </div>
</div>

