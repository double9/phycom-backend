<?php

/**
 * @var $this yii\web\View
 * @var VendorForm $model
 */

use Phycom\Backend\Widgets\Modal;
use Phycom\Backend\Widgets\ActiveForm;
use Phycom\Backend\Models\Vendor\VendorForm;

use Phycom\Base\Widgets\Map;
use Phycom\Base\Models\Attributes\VendorStatus;
use Phycom\Base\Models\Attributes\VendorType;
use Phycom\Base\Models\Address;

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\DetailView;

$tab = $tab ?? '';

$fieldOpt = ['template' => '{input}{error}', 'options' => ['class' => 'no-margin']];
$fieldOptMd = ArrayHelper::merge($fieldOpt, ['options' => ['style' => 'max-width: 300px;']]);
$fieldOptSm = ArrayHelper::merge($fieldOpt, ['options' => ['style' => 'max-width: 200px;']]);

?>


<div class="row">
    <div class="col-md-9 col-lg-8">

        <?php $form = ActiveForm::begin(['id' => 'vendor-form']); ?>

        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [

                [
                    'attribute' => 'name',
                    'format'    => 'raw',
                    'value'     => $form->field($model, 'name', $fieldOpt)->label(false)->textInput(['class' => 'form-control no-margin']),
                    'visible'   => Yii::$app->commerce->shop->vendorHasField('name')
                ],
                [
                    'attribute' => 'type',
                    'format'    => 'raw',
                    'value'     => $form->field($model, 'type', $fieldOpt)->label(false)->select2(ArrayHelper::filter(VendorType::displayValues(), Yii::$app->commerce->shop->vendorTypes)),
                    'visible'   => Yii::$app->commerce->shop->vendorHasField('type')
                ],
                [
                    'attribute' => 'legalName',
                    'format'    => 'raw',
                    'value'     => $form->field($model, 'legalName', $fieldOpt)->label(false)->textInput(['class' => 'form-control no-margin']),
                    'visible'   => Yii::$app->commerce->shop->vendorHasField('legalName')
                ],
                [
                    'attribute' => 'regNumber',
                    'format'    => 'raw',
                    'value'     => $form->field($model, 'regNumber', $fieldOpt)->label(false)->textInput(['class' => 'form-control no-margin']),
                    'visible'   => Yii::$app->commerce->shop->vendorHasField('regNumber')
                ],
                [
                    'attribute' => 'vatNumber',
                    'format'    => 'raw',
                    'value'     => $form->field($model, 'vatNumber', $fieldOpt)->label(false)->textInput(['class' => 'form-control no-margin']),
                    'visible'   => Yii::$app->commerce->shop->vendorHasField('vatNumber')
                ],
                [
                    'attribute' => 'website',
                    'format'    => 'raw',
                    'value'     => $form->field($model, 'website', $fieldOpt)->label(false)->input('url', ['class' => 'form-control no-margin']),
                    'visible'   => Yii::$app->commerce->shop->vendorHasField('website')
                ],
                [
                    'attribute' => 'email',
                    'format'    => 'raw',
                    'value'     => $form->field($model, 'email', $fieldOpt)->label(false)->textInput(['class' => 'form-control no-margin']),
                    'visible'   => Yii::$app->commerce->shop->vendorHasField('email')
                ],
                [
                    'attribute' => 'address',
                    'format'    => 'raw',
                    'value'     => function ($model) use ($form, $fieldOpt) {
                        return $form->field($model, 'address', ['options' => ['class' => 'no-margin']])->addressField('vendor-address-modal', $model->addressForm);
                    },
                    'visible'   => Yii::$app->commerce->shop->vendorHasField('address')
                ],
                [
                    'attribute' => 'phone',
                    'format'    => 'raw',
                    'value'     => $form->field($model, 'phone', $fieldOptMd)->label(false)->phoneInput2(),
                    'visible'   => Yii::$app->commerce->shop->vendorHasField('phone')
                ],
                [
                    'attribute' => 'logo',
                    'format'    => 'raw',
                    'value'     => function ($model) use ($form, $fieldOptMd) {
                        /**
                         * @var VendorForm $model
                         */
                        $fileInput = $form->field($model, 'logo', $fieldOptMd)->label(false)->fileInput();
                        $image = $model->vendor->options->logo
                            ? $model->vendor->options->getLogoUrl()->medium->url
                            : null;

                        return $image ? Html::img($image, ['class' => 'vendor-logo']) . $fileInput : $fileInput;
                    },
                    'visible'   => Yii::$app->commerce->shop->vendorHasField('logo')
                ],
                [
                    'attribute' => 'status',
                    'format'    => 'raw',
                    'value'     => $form->field($model, 'status', $fieldOptSm)->dropDownList(VendorStatus::displayValues()),
                    'visible'   => Yii::$app->commerce->shop->vendorHasField('status')
                ],
                [
                    'label'   => $model->vendor->getAttributeLabel('created_at'),
                    'format'  => 'datetime',
                    'value'   => $model->vendor->created_at,
                    'visible' => !$model->vendor->isNewRecord
                ],
                [
                    'label'   => $model->vendor->getAttributeLabel('updated_at'),
                    'format'  => 'datetime',
                    'value'   => $model->vendor->updated_at,
                    'visible' => !$model->vendor->isNewRecord
                ]
            ],
        ]);
        ?>
        <input type="hidden" name="tab" value="<?= $tab; ?>" />

        <?php ActiveForm::end(); ?>

        <?= Modal::widget([
            'id' => 'vendor-address-modal',
            'title' => $model->vendor->address
                ? Yii::t('phycom/backend/vendor','Update address')
                : Yii::t('phycom/backend/vendor','Add address'),
            'content' => $this->render('/partials/address-form', ['model' => $model->addressForm])
        ]);
        ?>

    </div>

    <div class="col-lg-4 col-md-3">
        <?php if (!$model->vendor->isNewRecord && $model->address): ?>
            <?= Map::widget(['address' => Address::create($model->address)]); ?>
        <?php endif; ?>
    </div>

</div>

