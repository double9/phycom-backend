<?php

use Phycom\Backend\Widgets\Box;
use Phycom\Backend\Widgets\Tabs2;
use Phycom\Backend\Widgets\FormCollection;
use Phycom\Backend\Widgets\FormSubmitBtn;
use Phycom\Base\Helpers\Url;

use yii\helpers\Html;


$items = $items ?? [];
$active = $active ?? null;

/**
 * @var $this yii\web\View
 * @var \Phycom\Backend\Models\Product\ProductForm $productForm
 * @var \Phycom\Backend\Models\Product\ProductTranslationForm $translationForm
 * @var \Phycom\Backend\Models\Product\ProductAttachmentForm $attachmentForm
 * @var \Phycom\Backend\Models\Product\SearchProductOption $options
 */
$this->title = $productForm->product->isNewRecord ? Yii::t('phycom/backend/product', 'Add product') : $productForm->product->translation->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('phycom/backend/product', 'Product catalog'), 'url' => ['product/catalog']];
$this->params['breadcrumbs'][] = $productForm->product->isNewRecord ? $this->title : $productForm->product->id;
$this->params['titleLabel'] = $productForm->product->isNewRecord ? null : '<span class="label label-primary">' . Html::encode($productForm->product->sku) . '</span>';

$formCollectionForms = [
    '#product-form'              => Yii::t('phycom/backend/product', 'Content'),
    '#product-translation-form'  => Yii::t('phycom/backend/product', 'Content'),
    '#product-pricing-options'   => Yii::t('phycom/backend/product', 'Pricing'),
    '#product-price-grid-form'   => Yii::t('phycom/backend/product', 'Pricing'),
    '#product-attachment-form'   => Yii::t('phycom/backend/product', 'Attachments')
];
if (Yii::$app->commerce->params->isEnabled()) {
    $formCollectionForms['#product-param-grid-form'] = Yii::t('phycom/backend/product', 'Params');
}
if (Yii::$app->commerce->variants->isEnabled()) {
    $formCollectionForms['#product-variant-grid-form'] = Yii::t('phycom/backend/product', 'Variants');
}

$formCollection = FormCollection::begin([
    'id'              => 'product-form-container',
    'submitUrl'       => $productForm->product->isNewRecord ? Url::toRoute(['/product/add']) : Url::toRoute(['/product/edit', 'id' => $productForm->product->id]),
    'forms'           => $formCollectionForms,
    'attachmentForms' => ['#productattachmentform-file' => Yii::t('phycom/backend/product', 'Attachments')],
    'tabs'            => true
]);

?>
    <div class="row">
        <div class="col-md-12">
            <div class="system-messages"></div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <?= Box::begin([
                'showHeader'  => false,
                'options'     => ['class' => 'box box-default'],
                'bodyOptions' => ['class' => 'box-body no-padding']
            ]);
            ?>
            <?= Tabs2::widget([
                'id'               => 'product-tabs',
                'contentBeforeNav' => '<div class="img-center h-200">
                    <img src="' . $productForm->attachmentForm->getImageUrl()->medium . '" />
                </div>',
                'items'            => $items,
                'activeTab'        => $active,
                'contentOptions'   => ['class' => 'tab-content col-md-10']
            ]);
            ?>
            <?= Box::end(); ?>
        </div>
    </div>


    <div class="row">
        <div class="col-md-12">
            <?= Box::begin([
                'showHeader'  => false,
                'options'     => ['class' => 'box box-default'],
                'bodyOptions' => ['class' => 'box-body']
            ]);
            ?>
            <div class="clearfix">
                <div class="pull-left"><?= $this->render('/partials/back-btn', ['size' => 'lg']); ?></div>
                <?= FormSubmitBtn::widget(['formCollection' => $formCollection]) ?>
            </div>
            <?= Box::end(); ?>
        </div>
    </div>

<?php $formCollection::end(); ?>
