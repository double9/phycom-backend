<?php

use Phycom\Base\Helpers\f;
use Phycom\Backend\Models\Product\ProductCategoryForm;
use Phycom\Backend\Widgets\Tree;
use Phycom\Backend\Widgets\Tabs;
use yii\helpers\Url;
use yii\helpers\Html;

/**
 * @var $this yii\web\View
 * @var \Phycom\Backend\Models\Product\SearchProductCategory $model
 * @var ProductCategoryForm $formModel
 * @var \yii\data\ActiveDataProvider $dataProvider
 * @var \Phycom\Base\Models\Language $activeLanguage
 */
$this->title = Yii::t('phycom/backend/product', 'Product Categories');

$this->params['breadcrumbs'][] = ['label' => Yii::t('phycom/backend/product', 'Product categories')];
if ($formModel) {
	$this->params['breadcrumbs'][] = $model->category->isNewRecord ? Yii::t('phycom/backend/product', 'Add category') : $model->category->id;
}


$activeLanguage = $model->language;

$tabItems = [];
foreach (Yii::$app->lang->enabled as $language) {
    $tabItems[] = [
        'id'    => 'content-language-' . $language->code,
        'label' => f::html('<b>' . strtoupper($language->code) . '</b> (' . $language->native . ')'),
        'url'   => Url::current(['lang' => $language->code])
    ];
}

Tabs::begin([
    'id'           => 'product-category-language-tabs',
    'items'        => $tabItems,
    'active'       => 'content-language-' . $activeLanguage->code,
    'ajax'         => false,
    'pajaxOptions' => [
        'id'                 => 'product-category-pajax',
        'enablePushState'    => false,
        'enableReplaceState' => true
    ]
]);
?>

    <div class="row">
        <div class="col-md-6">

            <br />

			<?= Tree::widget([
                    'pajaxId' => 'product-category-pajax',
                    'dataProvider' => $dataProvider,
                    'initialNode' => $model->category,
				    'moveAction' => ['move'],
                    'rowOptions' => function ($model, $key, $index, $grid) {

	                    /**
	                     * @var $model \Phycom\Backend\Models\Product\SearchProductCategory
	                     */
                        $options = [];

                        $currentRoute = [
	                        '/' . Yii::$app->controller->getRoute(),
                            'id' => Yii::$app->getRequest()->getQueryParam('id', null)
                        ];

                        if ($model->status->isHidden) {
                            Html::addCssClass($options, 'hidden-category');
                        }

	                    if (Url::toRoute($currentRoute) === Url::toRoute(['product-category/edit', 'id' => $model->id])) {
		                    Html::addCssClass($options, 'current');
                        }
                        return $options;
                    },
                    'columns' => [
                        [
                            'attribute' => 'title',
                            'format' => 'raw',
                            'content' => function($model) use ($activeLanguage) {
                                return Html::a($model->title, ['product-category/edit', 'id' => $model->id, 'lang' => $activeLanguage->code]);
                            }
                        ],
                        [
                            'attribute' => 'status',
                            'format' => 'text',
                            'content' => function($model) {
                                return Html::tag('span', $model->status->label, ['class' => 'label ' . $model->status->labelClass]);
                            }
                        ],
                        [
                            'label' => false,
                            'format' => 'raw',
                            'content' => function($model) {

                                return $this->render('/partials/delete-dropdown', [
                                    'label' => '<span class="far fa-trash-alt"></span>',
                                    'url' => ['product-category/delete', 'id' => $model->id],
                                    'options' => ['class' => 'toggle-hover']
                                ]);
                            },
                            'options' => ['width' => 40],
                            'contentOptions' => ['style' => 'padding: 2px 0 0 0;']
                        ],
                    ],
                ]);
			?>
        </div>
        <div class="col-md-6">

            <br />
            <div class="frame">

				<?php if ($formModel): ?>
					<?= $this->render('partials/category-form', [
						'model' => $formModel,
						'action' => $model->category->isNewRecord ?
							Url::toRoute(['product-category/create', 'lang' => $activeLanguage->code]) :
							Url::toRoute(['product-category/edit', 'id' => $model->category->id, 'lang' => $activeLanguage->code])
					]);
					?>
				<?php else: ?>
                    <table class="table table-striped">
                        <tr>
                            <th><?= Yii::t('phycom/backend/product', 'Total number of categories'); ?>: </th>
                            <td><?= f::integer($model->numTotal); ?></td>
                        </tr>
                        <tr>
                            <th><?= Yii::t('phycom/backend/product', 'Visible'); ?>: </th>
                            <td><?= f::integer($model->numVisible); ?></td>
                        </tr>
                        <tr>
                            <th><?= Yii::t('phycom/backend/product', 'Hidden'); ?>: </th>
                            <td><?= f::integer($model->numHidden); ?></td>
                        </tr>
                        <tr>
                            <th><?= Yii::t('phycom/backend/product', 'Translated'); ?>: </th>
                            <td><?= f::integer($model->numTranslated); ?></td>
                        </tr>
                        <tr>
                            <th><?= Yii::t('phycom/backend/product', 'Translation pending'); ?>: </th>
                            <td><?= f::integer($model->numTranslationPending); ?></td>
                        </tr>

                    </table>
				<?php endif; ?>
            </div>

            <div class="clearfix">
                <hr />
		        <?php if ($formModel): ?>
			        <?= $this->render('/partials/save-btn', ['class' => 'pull-right save-btn']); ?>
		        <?php endif; ?>

		        <?= Html::a(
			        '<span class="fas fa-plus"></span>&nbsp;&nbsp;' . Yii::t('phycom/backend/product', 'New'),
			        ['product-category/create', 'lang' => $activeLanguage->code],
			        [
				        'class' => 'btn btn-success btn-lg pull-right',
				        'style' => $formModel ? 'margin-right: 10px;' : ''
			        ]
		        );
		        ?>
            </div>

        </div>
    </div>

<?php Tabs::end(); ?>


<?php

$this->registerJs(
        <<<JS
    $(function () {
        $('.save-btn').on('click', '.save', function (e) {
            e.preventDefault();
            $('.product-category-form').yiiActiveForm('submitForm');
        });
    });
JS
	, yii\web\View::POS_END);
?>
