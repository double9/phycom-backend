<?php

/**
 * @var $this yii\web\View
 * @var $model \Phycom\Backend\Models\Product\ParamCollectionForm
 */

use Phycom\Backend\Widgets\ActiveForm;

$paramForm = Yii::$app->modelFactory->getParamForm();
?>

<div class="row">
    <div class="col-md-12">

        <?php $form = ActiveForm::begin(['id' => 'product-params']); ?>

        <table class="table table-striped form-table 2-col-form-table" style="vertical-align: middle;">
            <thead>
                <tr>
                    <td>
                        <?= $paramForm->getAttributeLabel('param') ?>
                    </td>
                    <td>
                        <?= $paramForm->getAttributeLabel('isEnabled') ?>
                    </td>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($model->models as $paramForm): ?>

                    <tr>
                        <td>
                            <?= $paramForm->getParam()->label; ?>
                        </td>
                        <td>
                            <?= $form->mField($model, $paramForm, 'isEnabled')->toggleSwitch() ?>
                        </td>
                    </tr>

                <?php endforeach; ?>
            </tbody>
        </table>

        <?php ActiveForm::end(); ?>
    </div>
</div>
