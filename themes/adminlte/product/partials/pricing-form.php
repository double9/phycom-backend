<?php
/**
 * @var \yii\web\View $this
 * @var \Phycom\Backend\Models\Product\ProductPricingForm $formModel
 * @var \Phycom\Backend\Models\Product\ProductPricingOptionsForm $optionsForm
 */
?>

<div class="row">
    <div class="col-md-12">
        <?= $this->render('pricing-options', ['model' => $optionsForm]) ?>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <?= $this->render('pricing-grid', ['model' => $formModel]) ?>
    </div>
</div>




