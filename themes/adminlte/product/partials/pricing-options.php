<?php
/**
 * @var $this yii\web\View
 * @var $model ProductPricingOptionsForm
 */

use Phycom\Backend\Widgets\ActiveForm;
use Phycom\Backend\Models\Product\ProductPricingOptionsForm;

use Phycom\Base\Models\Attributes\UnitType;
use Phycom\Base\Models\Attributes\PriceUnitMode;

$form = ActiveForm::begin([
    'id' => 'product-pricing-options',
    'options' => ['class' => 'clearfix']
]);
?>

<div class="col-sm-4 col-md-3 col-lg-2">
    <?= $form->field($model, 'priceUnit')->dropDownList(UnitType::displayValues()); ?>
</div>
<div class="col-sm-4 col-md-3 col-lg-2">
    <?= $form->field($model, 'priceUnitMode')->dropDownList(PriceUnitMode::displayValues()); ?>
</div>
<div class="col-sm-4 col-md-3 col-lg-2">
    <?= $form->field($model, 'discount')->boolean(); ?>
</div>

<?php ActiveForm::end(); ?>



