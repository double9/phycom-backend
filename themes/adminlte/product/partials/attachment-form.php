<?php
/**
 * @var $this yii\web\View
 * @var $model Phycom\Backend\Models\Product\ProductAttachmentForm
 */
use Phycom\Backend\Widgets\ActiveForm;
use Phycom\Backend\Assets\FileInputAsset;
use Phycom\Backend\Widgets\Modal;

use rmrevin\yii\fontawesome\FAS;

use yii\helpers\Url;
use yii\helpers\Html;

FileInputAsset::register($this);

$modelName = (new \ReflectionClass($model))->getShortName();

echo Html::beginTag('div', ['class' => 'attachments-modal-container']);
foreach ($model->getAttachments() as $attachment) {
    echo Modal::widget([
        'id'      => 'attachment_' . $attachment->file_id . '_modal',
        'title'   => $attachment->file->filename,
        'content' => $this->render('attachment-item-options', ['attachment' => $attachment]),
        'actions' => [
            [
                'title' => FAS::i(FAS::_CHECK) . '&nbsp;&nbsp;' . Yii::t('phycom/backend/main', 'Save'),
                'class' => 'btn-primary btn-save',
                'url'   => ['product/update-attachment', 'id' => $attachment->product_id, 'fileId' => $attachment->file_id]
            ],
            [
                'title' => FAS::i(FAS::_TIMES) . '&nbsp;&nbsp;' . Yii::t('phycom/backend/main', 'Close'),
                'class' => 'btn-default btn-close'
            ]
        ],
        'options' => ['size' => 'lg']
    ]);
}
echo Html::endTag('div');


$form = ActiveForm::begin(['id' => 'product-attachment-form', 'options' => ['class' => 'file-input-form']]);

$initialPreview = [];
$initialPreviewConfig = [];

foreach ($model->getAttachments() as $key => $attachment) {
    $initialPreview[] = $this->render('attachment-item', [
        'model'      => $model,
        'form'       => $form,
        'attachment' => $attachment
    ]);
    $initialPreviewConfig[] = [
        'caption'       => $attachment->file->name,
        'width'         => '120px',
        'key'           => $attachment->file->id,
        'previewAsData' => false
    ];

    if ($attachment->isNewRecord) {
        echo $form->field($attachment, 'file_id')->hiddenInput([
            'id'   => "attachments-$key-file-id",
            'name' => $modelName . "[attachments][$key][file_id]"
        ])->label(false);
        echo $form->field($attachment, 'order')->hiddenInput([
            'id'   => "attachments-$key-order",
            'name' => $modelName . "[attachments][$key][order]"
        ])->label(false);
    }
}

echo $form->field($model, 'file')->fileInputArea([
		'options' => [
		    'multiple' => true,
            'class' => 'attachment-form-input',
            'accept' => 'image/*',
        ],
		'pluginOptions' => [
			'theme' => 'fa',
			'browseOnZoneClick' => true,
			'uploadAsync' => true,
            'enableResumableUpload' => false,
			'autoReplace' => false,
			'overwriteInitial' => false,
			'maxFileCount' => $model->maxFiles,
			'allowedFileExtensions' => $model->allowedExtensions,
			'uploadUrl' => Url::toRoute(['product/upload-attachment', 'id' => $model->product->id]),
			'deleteUrl' => Url::toRoute(['product/delete-attachment', 'id' => $model->product->id]),
			'showClose'  => false,
			'showRemove'  => false,
			'showCaption' =>  false,
			'showBrowse' => false,
			'preferIconicPreview' => false,
			'browseClass' => 'btn btn-sm btn-primary',
			'browseIcon' => '<span class="far fa-image"></span> ',
			'browseLabel' => Yii::t('phycom/backend/main','Browse'),
			'uploadClass' => 'btn btn-sm btn-default file-upload-btn',
			'uploadLabel' => Yii::t('phycom/backend/main','Save'),
			'initialPreviewShowDelete' => true,
			'initialPreviewAsData' => true,
			'initialPreview' => $initialPreview,
            'initialPreviewConfig' => $initialPreviewConfig,
			'previewTemplates' => [
				'image' => $this->render('attachment-item-preview')
			],
			'previewSettings' => [
			    'image' => ['width' => "auto", 'height' => "auto", 'style' => 'max-height: 200px;'],
                'key' => 1
            ],
			'layoutTemplates' => [
				'main2' => '{preview}',
				'actions' => '<div class="file-footer-buttons">{delete}</div>{drag}',
				'progress' => '',
				'footer' => $this->render('attachment-item-footer')
			],
			'fileActionSettings' => [
				'showDrag' => true,
				'dragIcon' => '<span class="fas fa-bars"></span> ',
                'removeIcon' => '<span class="fas fa-minus-circle"></span> ',
				'dragClass' => 'drag',
				'dragTitle' => Yii::t('phycom/backend/main','Drag'),
//				'dragSettings' => []
			],
			'customPreviewTags' => [],
//			'uploadExtraData' => \Phycom\Base\Helpers\JsExpression::create(<<<JS
//				function (previewId, index) {
//                    var obj = {};
//                    $("#"+previewId).find(".attachment-extra-param").each(function() {
//						var key = $(this).attr("data-name"),
//                            val = $(this).val();
//                        obj[key] = val;
//                    });
//                    return obj;
//				}
//JS
//			)
		]
	]);

ActiveForm::end();


$this->registerJs(<<<JS

[].forEach.call(document.querySelectorAll('img[data-src]'),    function(img) {
    img.setAttribute('src', img.getAttribute('data-src'));
    img.onload = function() {
        img.removeAttribute('data-src');
    };
});
JS
, \yii\web\View::POS_READY);

