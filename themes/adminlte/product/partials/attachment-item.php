<?php

/**
 * @var $this yii\web\View
 * @var $model Phycom\Backend\Models\Product\ProductAttachmentForm
 * @var $form \Phycom\Backend\Widgets\ActiveForm
 * @var $attachment \Phycom\Base\Models\Product\ProductAttachment
 */

use Phycom\Base\Models\File;

use yii\helpers\Html;
use yii\helpers\Url;

?>

<?= Html::beginTag('div', [
    'class'           => 'attachment-item initial-item',
    'data-update-url' => Url::toRoute(['/product/update-attachment', 'id' => $attachment->product_id, 'fileId' => $attachment->file_id]),
    'data-toggle'     => 'tooltip',
    'title'           => $attachment->file->name,
    'data-placement'  => 'top',
    'data-container'  => 'body',
    'data-file-id'    => $attachment->file_id,
])
?>
    <div class="item-content">
	    <?= Html::img(null, ['data-src' => $attachment->file->getThumbUrl(File::THUMB_SIZE_SMALL), 'class' => 'file-preview-image']); ?>
    </div>
    <div class="item-options">
        <?php if (!$attachment->is_visible): ?>
            <span class="label label-default"><?= Yii::t('phycom/backend/main', 'Hidden'); ?></span>
        <?php endif; ?>
    </div>

<?= Html::endTag('div'); ?>
