<?php

/**
 * @var \yii\web\View $this
 * @var \Phycom\Backend\Models\Product\ProductForm $model
 */
use Phycom\Backend\Widgets\ActiveForm;
use Phycom\Base\Models\Attributes\ProductStatus;
use Phycom\Base\Helpers\f;

?>

<div class="row">
    <div class="col-md-7 col-lg-8">
        <?= $this->render('translation-form', ['model' => $model->translationForm, 'box' => false]) ?>
    </div>
    <div class="col-md-5 col-lg-4" style="padding-top: 54px;">

        <?php $form = ActiveForm::begin(['id' => 'product-form']); ?>

        <?= $form->field($model, 'sku')->textInput(); ?>
        <?= $form->field($model, 'categories')->multiSelect($model->allCategories); ?>
        <?= $form->field($model, 'tags')->multiSelect($model->allTags); ?>

        <?php if (count($model->allVendors) > 1): ?>
            <?= $form->field($model, 'vendors')->multiSelect($model->allVendors); ?>
        <?php endif; ?>

        <?= $form->field($model, 'status')->dropDownList(ProductStatus::displayValues()); ?>


        <table class="table table-striped" style="margin-top: 50px;">
            <colgroup>
                <col width="200">
                <col>
            </colgroup>
            <tbody>
                <tr>
                    <td><strong><?= $model->getProduct()->getAttributeLabel('created_at'); ?></strong></td>
                    <td><?= f::datetime($model->getProduct()->created_at ?? null); ?></td>
                </tr>
                <tr>
                    <td><strong><?= $model->getProduct()->getAttributeLabel('updated_at'); ?></strong></td>
                    <td><?= f::datetime($model->getProduct()->updated_at ?? null); ?></td>
                </tr>
                <tr>
                    <td><strong><?= $model->getProduct()->getAttributeLabel('created_by'); ?></strong></td>
                    <td><?= f::text($model->getProduct()->createdBy ? $model->getProduct()->createdBy->fullName : null); ?></td>
                </tr>
            </tbody>
        </table>


        <?php ActiveForm::end(); ?>

    </div>
</div>




