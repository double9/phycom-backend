<?php

/**
 * @var $this yii\web\View
 * @var $model Phycom\Backend\Models\Product\ProductForm
 */
use Phycom\Backend\Widgets\ActiveForm;
use Phycom\Base\Models\Attributes\ProductStatus;
?>

<?php $form = ActiveForm::begin(['id' => 'product-form']); ?>

	<?= $form->field($model, 'sku')->textInput(); ?>
	<?= $form->field($model, 'categories')->multiSelect($model->allCategories); ?>
    <?= $form->field($model, 'tags')->multiSelect($model->allTags); ?>
    <?= $form->field($model, 'vendors')->multiSelect($model->allVendors); ?>

	<div class="row">
		<div class="col-md-4">
			<?= $form->field($model, 'status')->dropDownList(ProductStatus::displayValues()); ?>
		</div>
        <div class="col-md-8">

        </div>
	</div>

<?php ActiveForm::end(); ?>
