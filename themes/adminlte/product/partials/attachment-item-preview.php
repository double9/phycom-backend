<?php

/**
 * @var $this yii\web\View
 * @var $model Phycom\Backend\Models\Product\ProductAttachmentForm
 * @var $form \Phycom\Backend\Widgets\ActiveForm
 * @var $attachment \Phycom\Base\Models\Product\ProductAttachment
 */

?>

<div class="file-preview-frame {frameClass}" id="{previewId}" data-fileid="{fileid}" data-fileindex="{fileindex}" data-template="{template}">
    <div class="kv-file-content">
        <div class="attachment-item">
            <img src="{data}" class="kv-preview-data file-preview-image" title="{caption}" alt="{caption}" {style} />
            <div class="item-options"></div>
        </div>
    </div>
    {footer}
</div>
