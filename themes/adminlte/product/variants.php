<?php

/**
 * @var $this yii\web\View
 * @var $model \Phycom\Backend\Models\Product\VariantCollectionForm
 */

use Phycom\Backend\Widgets\ActiveForm;

$variantForm = Yii::$app->modelFactory->getVariantForm();
?>

<div class="row">
    <div class="col-md-12">

        <?php $form = ActiveForm::begin(['id' => 'product-variants']); ?>

        <table class="table table-striped form-table 2-col-form-table" style="vertical-align: middle;">
            <thead>
                <tr>
                    <td>
                        <?= $variantForm->getAttributeLabel('variant') ?>
                    </td>
                    <td>
                        <?= $variantForm->getAttributeLabel('isEnabled') ?>
                    </td>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($model->models as $variantForm): ?>

                    <tr>
                        <td>
                            <?= $variantForm->getVariant()->label; ?>
                        </td>
                        <td>
                            <?= $form->mField($model, $variantForm, 'isEnabled')->toggleSwitch() ?>
                        </td>
                    </tr>

                <?php endforeach; ?>
            </tbody>
        </table>

        <?php ActiveForm::end(); ?>
    </div>
</div>
