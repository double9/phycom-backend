<?php

use Phycom\Base\Models\Attributes\DiscountRuleStatus;
use Phycom\Backend\Widgets\Box;
use Phycom\Backend\Widgets\DataGrid;
use Phycom\Base\Helpers\Filter;
use yii\helpers\Url;

/**
 * @var $this yii\web\View
 * @var \Phycom\Backend\Models\SearchDiscountRule $model
 * @var \yii\data\ActiveDataProvider $dataProvider
 */

$this->title = Yii::t('phycom/backend/marketing', 'Promotion codes');
$this->params['breadcrumbs'][] = Yii::t('phycom/backend/marketing', 'Marketing');
$this->params['breadcrumbs'][] = $this->title
?>
<div class="row">
	<div class="col-md-12">
		<?= Box::begin([
			'options' => ['class' => 'box box-default box-wide'],
			'showHeader' => false,
			'bodyOptions' => ['class' => 'box-body']
		]);
		?>

		<?= DataGrid::widget([
			'id' => 'user-grid',
			'dataProvider' => $dataProvider,
			'filterModel' => $model,
            'bulkEditForm' => 'bulk-edit',
			'rowOptions' => function ($model) {
				return [
					'class' => 'row-link',
					'data-url' => Url::toRoute(['promotion-code/edit','id' => $model->id])
				];
			},
            'actions' => [
                [
                    'label' => '<span class="fas fa-plus"></span>&nbsp;&nbsp;' . Yii::t('phycom/backend/user', 'Add new promotion code'),
                    'url' => ['promotion-code/add'],
                    'options' => ['class' => 'btn btn-success']
                ]
            ],
			'columns' => [
				[
					'attribute' => 'code',
					'format' => 'text',
				],
				[
					'attribute' => 'discount_rate',
					'format' => 'percent',
					'options' => ['width' => '200']
				],
				[
					'attribute' => 'used',
					'format' => 'integer',
					'options' => ['width' => '120']
				],
				[
					'attribute' => 'expires_at',
					'filter' => Filter::daterangepicker($model, 'expiresFrom', 'expiresTo'),
					'format' => 'datetime',
					'options' => ['width' => '220']
				],
				[
					'attribute' => 'status',
					'filter' => Filter::dropDown($model, 'status', DiscountRuleStatus::displayValues()),
					'format' => 'raw',
					'value' => function ($model) {
						return '<span class="label '.$model->status->labelClass.'">' . $model->status->label . '</span>';
					},
					'options' => ['width' => '120']
				],
				[
					'attribute' => 'created_at',
					'filter' => Filter::daterangepicker($model, 'createdFrom', 'createdTo'),
					'format' => 'datetime',
					'options' => ['width' => '220']
				]
			]
		]);
		?>

		<?= Box::end(); ?>
	</div>
</div>
