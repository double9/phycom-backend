<?php

/**
 * @var $this yii\web\View
 * @var \Phycom\Backend\Models\InvoiceForm $model
 */

use Phycom\Backend\Widgets\Box;
use Phycom\Backend\Widgets\AjaxGrid;

use Phycom\Base\Helpers\f;

use rmrevin\yii\fontawesome\FAS;

use yii\widgets\DetailView;
use yii\helpers\Url;
use yii\helpers\Html;

$this->title = Yii::t('phycom/backend/invoice', 'Invoice # {no}', ['no' => $model->invoice->number]);
$this->params['breadcrumbs'][] = Yii::t('phycom/backend/main', 'Sale');
$this->params['breadcrumbs'][] = ['label' => Yii::t('phycom/backend/main', 'Invoices'), 'url' => ['/invoice']];
$this->params['breadcrumbs'][] = $this->title
?>
<div class="row">

    <div class="col-lg-8 col-md-12">

		<?= Box::begin([
			'showHeader' => false,
			'options' => ['class' => 'box box-default'],
			'bodyOptions' => ['class' => 'box-body']
		]);
		?>

        <div class="row">
            <div class="col-md-8">
				<?= DetailView::widget([
					'id' => 'order-detail',
					'model' => $model->invoice,
					'attributes' => [
						'number:text',
						[
							'label' => $model->invoice->getAttributeLabel('status'),
							'format' => 'raw',
							'value' => '<span class="label '.$model->invoice->status->labelClass.'">' . $model->invoice->status->label . '</span>',
                            'captionOptions' => ['style' => 'width: 200px;']
						],
                        [
                            'label' => $model->invoice->getAttributeLabel('customer_type'),
                            'format' => 'raw',
                            'value' => function ($model) {
	                            /**
	                             * @var $model \Phycom\Base\Models\Invoice
	                             */
				                return \yii\helpers\Html::tag('span', $model->customer_type->label, ['class' => 'label label-primary']);
                            }
                        ],
						[
							'label' => Yii::t('phycom/backend/invoice', 'Customer'),
							'value' => function ($model) {
								/**
								 * @var $model \Phycom\Base\Models\Invoice
								 */
								return f::titleCase($model->customer);
							}
						],
						[
						    'label' => Yii::t('phycom/backend/invoice', 'Registration number'),
                            'format' => 'text',
                            'attribute' => 'reg_no',
                            'visible' => $model->invoice->reg_no !== null
                        ],
						[
							'label' => $model->invoice->getAttributeLabel('address'),
                            'format' => 'addressLink',
							'value' => $model->invoice->address
						],
                        [
	                        'label' => $model->invoice->getAttributeLabel('file'),
                            'format' => 'raw',
	                        'value' => function ($model) {
		                        /**
		                         * @var $model \Phycom\Base\Models\Invoice
		                         */
		                        return \yii\helpers\Html::a('<span class="far fa-file-pdf"></span>&nbsp;&nbsp;' . '...' . substr($model->file, -15), $model->fileUrl, ['target' => '_blank']);
	                        },
                            'visible' => $model->invoice->file !== null
                        ],
						'created_at:datetime',
					],
				]);

				?>
            </div>
            <div class="col-md-4 clearfix">
                <?= Html::a(
                    Yii::t('phycom/backend/order', 'Order # {no}', ['no' => $model->invoice->order->number]),
                    ['/order/edit', 'id' => $model->invoice->order->id],
                    ['class' => 'btn btn-default pull-right'])
                ?>
            </div>
        </div>

        <br />
        <br />
		<?= $this->render('partials/invoice-items', ['invoice' => $model->invoice]); ?>
        <br />
        <br />
        <br />

		<?= Box::end(); ?>
    </div>

    <div class="col-lg-4 col-md-12">
	    <?php

        echo Box::begin([
		    'title' => Yii::t('phycom/backend/invoice', 'Payments'),
		    'options' => ['class' => 'box box-default'],
		    'bodyOptions' => ['class' => 'box-body']
	    ]);

        $dataProvider = (new \Phycom\Backend\Models\SearchPayment(['invoice_id' => $model->invoice->id]))->search();
        $dataProvider->pagination = false;

	    echo AjaxGrid::widget([
		    'id' => 'payments-grid',
		    'dataProvider' => $dataProvider,
		    'route' => 'invoice/payments',
		    'rowOptions' => function ($model) {
			    return [
				    'class' => 'row-link',
				    'data-url' => Url::toRoute(['payment-list/edit','id' => $model->id])
			    ];
		    },
		    'columns' => [
			    [
				    'attribute' => 'amount',
				    'format' => 'currency',
				    'enableSorting' => false,
				    'options' => ['width' => '100']
			    ],
			    [
				    'attribute' => 'status',
				    'format' => 'raw',
				    'enableSorting' => false,
				    'value' => function ($model) {
					    /**
					     * @var \Phycom\Backend\Models\SearchPayment $model
					     */
	                    return Html::tag('span', $model->status->label, ['class' => 'label ' . $model->status->labelClass]);
                    },
				    'options' => ['width' => '180']
			    ],
			    [
				    'attribute' => 'transaction_time',
				    'format' => 'datetime',
				    'enableSorting' => false,
				    'options' => ['width' => '180']
			    ],
			    [
				    'attribute' => 'created_at',
				    'format' => 'datetime',
				    'enableSorting' => false,
				    'options' => ['width' => '180']
			    ]
		    ]
	    ]);
	    ?>

        <div class="clearfix">
            <div class="pull-right">
			    <?php if (!$model->invoice->isPaid): ?>
                    <a href="<?= Url::toRoute(['/invoice/add-payment', 'id' => $model->invoice->id]); ?>" class="btn btn-lg btn-success">
					    <?= FAS::i(FAS::_PLUS); ?>&nbsp;&nbsp;
					    <?= Yii::t('phycom/backend/invoice', 'Create payment'); ?>
                    </a>
			    <?php endif; ?>
            </div>
        </div>

	    <?= Box::end(); ?>
    </div>
</div>
<div class="row">
    <div class="col-lg-8 col-md-12">
        <?= Box::begin([
            'showHeader' => false,
            'options' => ['class' => 'box box-default'],
            'bodyOptions' => ['class' => 'box-body']
        ]);
        ?>
        <div class="clearfix">
            <div class="pull-left"><?= $this->render('/partials/back-btn', ['size' => 'lg']); ?></div>
        </div>
        <?= Box::end(); ?>
    </div>
</div>

