<?php


use Phycom\Base\Helpers\f;

use yii\helpers\Html;

/**
 * @var \Phycom\Base\Models\Invoice $invoice
 */

$orderItem = new \Phycom\Base\Models\OrderItem();

/**
 * @var $deliveryModule \Phycom\Base\Modules\Delivery\Module;
 */
$deliveryModule = Yii::$app->getModule('delivery');
/**
 * @var $couriers \Phycom\Base\Modules\Delivery\Interfaces\DeliveryMethodInterface[]
 */
$couriers = $deliveryModule->allMethods;

?>

<table class="order-table table table-striped">
	<colgroup>
		<col width="120">
        <col width="120">
		<col>
		<col width="120">
		<col width="120">
		<col width="120">
		<col width="120">
		<col width="50">
	</colgroup>
	<thead>
		<tr>
			<th><?= $orderItem->getAttributeLabel('code'); ?></th>
			<th><?= $orderItem->getAttributeLabel('title'); ?></th>
            <th></th>
			<th><?= $orderItem->getAttributeLabel('quantity'); ?></th>
			<th><?= $orderItem->getAttributeLabel('price'); ?></th>
			<th><?= $orderItem->getAttributeLabel('discount'); ?></th>
			<th><?= $orderItem->getAttributeLabel('total'); ?></th>
			<th></th>
		</tr>
	</thead>
	<tbody>
		<?php foreach ($invoice->items as $model): ?>

			<tr>
				<td><?= f::text($model->orderItem->code); ?></td>
				<td><?= f::text($model->orderItem->title); ?></td>
                <td>
                    <?php
                    $title = '';
                    if (!$model->orderItem->product) {

                        $attributes = $model->orderItem->product_attributes;
                        $Option = Yii::$app->modelFactory->getVariantOption();

                        if (!empty($attributes)) {

                            foreach ($attributes as $attributeName => $attributeValue) {

                                $label = '';
                                $value = '';
                                // render attribute label
                                if ($variant = Yii::$app->modelFactory->getVariant()::findByName($attributeName)) {
                                    $label = Html::tag('span', $variant->label, ['class' => 'line-attribute']);
                                } else if (filter_var($attributeValue, FILTER_VALIDATE_URL)) {
                                    $label = Html::a($attributeValue, $attributeValue, ['target' => '_blank']);
                                }

                                // render attribute value
                                if ($option = $Option::findByKey($attributeName, $attributeValue)) {
                                    $value = Html::tag('span', $option->label, ['class' => 'label label-default', 'data-attribute' => $attributeName]);
                                } else {
                                    $value = Html::tag('span', $attributeValue, ['class' => 'label label-default', 'data-attribute' => $attributeName]);
                                }

                                $title .= $label . $value;
                            }
                        }

//	                        foreach ($couriers as $courier) {
//	                            foreach ($courier->getAreas() as $area) {
//	                                if ($area->getUniqueId() === $model->code) {
//	                                    if ($courier->getType()->value === \Phycom\Base\Modules\Delivery\Models\CourierType::HOME_DELIVERY) {
//                                            $title .= ' - <span class="delivery-address">' . (string) \Phycom\Base\Models\Address::create($model->order->delivery_address) . '</span>';
//                                        }
//	                                    break 2;
//                                    }
//                                }
//                            }
                    }

                    echo $title;
                    ?>
                </td>
				<td><?= f::integer($model->quantity); ?></td>
				<td><?= f::currency($model->orderItem->price); ?></td>
				<td><?= f::percent($model->orderItem->discount); ?></td>
				<td><?= f::currency($model->orderItem->total); ?></td>
				<td>
                </td>

			</tr>

		<?php endforeach; ?>
	</tbody>
	<tfoot>
		<tr>
			<td colspan="5"></td>
			<td><strong><?= Yii::t('phycom/backend/order', 'Total amount'); ?></strong></td>
            <td><h4 class="no-margin"><strong><?= f::currency($invoice->total); ?></strong></h4></td>
			<td></td>
		</tr>
	</tfoot>
</table>

