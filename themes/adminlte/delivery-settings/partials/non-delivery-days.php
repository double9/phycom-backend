<?php

/**
 * @var $this yii\web\View
 * @var DeliverySettingsForm $model
 */

use Phycom\Backend\Models\DeliverySettings\DeliverySettingsForm;
use Phycom\Backend\Widgets\ActiveForm;
use Phycom\Backend\Widgets\DatePicker;

$tab = $tab ?? '';

?>

<div class="row">
    <div class="col-md-12">
        <div id="non-delivery-days"></div>

        <?php $form = ActiveForm::begin(['id' => 'non-delivery-days-form']); ?>

            <input type="hidden" name="tab" value="<?= $tab; ?>" />

            <?= $form->field($model, 'nonDeliveryDays')->widget(DatePicker::class, [
                'mode'           => DatePicker::MODE_MULTIPLE,
                'showInputField' => false
            ])->label(false); ?>

        <?php ActiveForm::end(); ?>

    </div>
</div>
