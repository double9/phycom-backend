<?php

/**
 * @var $this yii\web\View
 * @var $model \Phycom\Base\Modules\Delivery\Models\DeliveryMethodSettings
 */
use Phycom\Backend\Widgets\ModuleSettings;

?>
<div id="delivery-method-settings">

    <div class="row">
        <ul class="col-md-12">

            <?php if ($model): ?>
                <li id="<?= $model->module->id; ?>" class="delivery" style="max-width: 915px; display: block;">
                    <table class="table table-striped">
                        <thead>
                        <tr style="background-color: #f6f6f6; border-bottom: 2px solid #ddd;">
                            <td colspan="2">
                                <h3><?= Yii::t('phycom/backend/settings', 'Common delivery options') ?></h3>
                            </td>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td colspan="2">
                                <?= ModuleSettings::widget(['model' => $model, 'exclude' => ['nonDeliveryDays']]); ?>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </li>
            <?php endif; ?>

        </ul>
    </div>
</div>
