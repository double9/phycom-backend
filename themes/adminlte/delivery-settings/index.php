<?php

/**
 * @var $this yii\web\View
 * @var DeliverySettingsForm $model
 */

use Phycom\Backend\Models\DeliverySettings\DeliverySettingsForm;
use Phycom\Backend\Widgets\Box;
use Phycom\Backend\Widgets\Tabs2;

$this->title = Yii::t('phycom/backend/delivery', 'Delivery settings');

$items = $items ?? [];
$active = $active ?? null;

?>


<div class="row">

    <div class="col-md-12">

        <?= Box::begin([
            'showHeader'  => false,
            'options'     => ['class' => 'box box-default'],
            'bodyOptions' => ['class' => 'box-body no-padding']
        ]);
        ?>
        <?= Tabs2::widget([
            'id'             => 'delivery-settings-tabs',
            'items'          => $items,
            'activeTab'      => $active,
            'contentOptions' => ['class' => 'tab-content col-md-10'],
        ]);
        ?>
        <?= Box::end(); ?>

    </div>
</div>


<div class="row">
    <div class="col-md-12">
        <?= Box::begin([
            'showHeader'  => false,
            'options'     => ['class' => 'box box-default'],
            'bodyOptions' => ['class' => 'box-body']
        ]);
        ?>
        <div class="clearfix">
            <div class="pull-left">
                <?= $this->render('/partials/back-btn', ['size' => 'lg']); ?>
            </div>

            <?= $this->render('/partials/save-btn-2', [
                'id'           => 'save-delivery-settings',
                'class'        => 'pull-right btn-lg',
                'formSelector' => '#delivery-settings-tabs .tab-content > .tab-pane.active'
            ]);
            ?>
        </div>
        <?= Box::end(); ?>
    </div>
</div>




