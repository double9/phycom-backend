<?php
/**
 * @var $this yii\web\View
 * @var $model Phycom\Backend\Models\PostAttachmentForm
 */

use Phycom\Backend\Widgets\ActiveForm;
use Phycom\Backend\Assets\FileInputAsset;
use Phycom\Backend\Widgets\Modal;

use rmrevin\yii\fontawesome\FAS;

use yii\helpers\Url;
use yii\helpers\Html;

FileInputAsset::register($this);


echo Html::beginTag('div', ['class' => 'attachments-modal-container']);
foreach ($model->post->attachments as $attachment) {
    echo Modal::widget([
        'id' => 'attachment_' . $attachment->file_id . '_modal',
        'title' => $attachment->file->filename,
        'content' => $this->render('attachment-item-options', ['attachment' => $attachment]),
        'actions' => [
            [
                'title' => FAS::i(FAS::_CHECK) . '&nbsp;&nbsp;' . Yii::t('phycom/backend/main', 'Save'),
                'class' => 'btn-primary btn-save',
                'url' => ['post/update-attachment', 'id' => $attachment->post_id, 'fileId' => $attachment->file_id]
            ],
            [
                'title' => FAS::i(FAS::_TIMES) . '&nbsp;&nbsp;' .Yii::t('phycom/backend/main', 'Close'),
                'class' => 'btn-default btn-close'
            ]
        ],
        'options' => ['size' => '2xl']
    ]);
}
echo Html::endTag('div');


$form = ActiveForm::begin(['id' => 'post-attachment-form', 'options' => ['class' => 'file-input-form']]);

$initialPreview = [];
$initialPreviewConfig = [];

foreach ($model->post->attachments as $attachment) {
    $initialPreview[] = $this->render('attachment-item', [
        'model'      => $model,
        'form'       => $form,
        'attachment' => $attachment
    ]);
    $initialPreviewConfig[] = [
        'caption'       => $attachment->file->name,
        'width'         => '120px',
        'key'           => $attachment->file->id,
        'previewAsData' => false
    ];
}

echo $form->field($model, 'file')->fileInputArea([
		'options' => [
            'multiple' => true,
            'class' => 'attachment-form-input',
            'accept' => 'image/*'
        ],
		'pluginOptions' => [
			'theme' => 'fa',
			'browseOnZoneClick' => true,
			'uploadAsync' => true,
            'enableResumableUpload' => false,
			'autoReplace' => false,
			'overwriteInitial' => false,
			'maxFileCount' => $model->maxFiles,
			'allowedFileExtensions' => $model->allowedExtensions,
			'uploadUrl' => Url::toRoute(['post/upload-attachment', 'id' => $model->post->id]),
			'deleteUrl' => Url::toRoute(['post/delete-attachment', 'id' => $model->post->id]),
			'showClose'  => false,
			'showRemove'  => false,
			'showCaption' =>  false,
			'showBrowse' => false,
			'preferIconicPreview' => false,
			'browseClass' => 'btn btn-sm btn-primary',
			'browseIcon' => '<span class="far fa-image"></span> ',
			'browseLabel' => Yii::t('phycom/backend/main','Browse'),
			'uploadClass' => 'btn btn-sm btn-default file-upload-btn',
			'uploadLabel' => Yii::t('phycom/backend/main','Save'),
			'initialPreviewShowDelete' => true,
			'initialPreviewAsData' => true,
			'initialPreview' => $initialPreview,
            'initialPreviewConfig' => $initialPreviewConfig,
			'previewTemplates' => [
				'image' => $this->render('attachment-item-preview')
			],
			'previewSettings' => [
			    'image' => ['width' => "auto", 'height' => "auto", 'style' => 'max-height: 200px;'],
                'key' => 1
            ],
			'layoutTemplates' => [
				'main2' => '{preview}',
				'actions' => '<div class="file-footer-buttons">{delete}</div>{drag}',
				'progress' => '',
				'footer' => $this->render('attachment-item-footer')
			],
			'fileActionSettings' => [
				'showDrag' => true,
				'dragIcon' => '<span class="fas fa-bars"></span> ',
                'removeIcon' => '<span class="fas fa-minus-circle"></span> ',
				'dragClass' => 'drag',
				'dragTitle' => Yii::t('phycom/backend/main','Drag')
			],
			'customPreviewTags' => [],
		]
	]);

ActiveForm::end();


$this->registerJs(<<<JS

[].forEach.call(document.querySelectorAll('img[data-src]'),    function(img) {
    img.setAttribute('src', img.getAttribute('data-src'));
    img.onload = function() {
        img.removeAttribute('data-src');
    };
});
JS
, \yii\web\View::POS_READY);

