<?php

/**
 * @var $this yii\web\View
 */

?>

<div class="file-preview-frame krajee-default kv-preview-thumb {frameClass}" id="{previewId}" data-fileid="{fileid}" data-fileindex="{fileindex}" data-template="{template}">
    <div class="kv-file-content">
        <div class="attachment-item">
            <img src="{data}" class="kv-preview-data file-preview-image" title="{caption}" alt="{caption}" {style} />
            <div class="item-options"></div>
        </div>
    </div>
    {footer}
</div>
