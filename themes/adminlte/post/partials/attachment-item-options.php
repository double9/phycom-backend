<?php

/**
 * @var $this yii\web\View
 * @var $attachment \Phycom\Base\Models\PostAttachment
 */

use Phycom\Backend\Widgets\ActiveForm;

use Phycom\Base\Models\File;

use yii\helpers\Html;


$options = Yii::$app->modelFactory->getPostAttachmentOptionsForm($attachment);

$form = ActiveForm::begin([
    'id'      => 'attachment_' . $attachment->file_id . '_form',
    'action'  => ['/post/update-attachment', 'id' => $attachment->post_id, 'fileId' => $attachment->file_id],
    'options' => ['class' => 'product-attachment-options']
]);
?>
<div class="row">
    <div class="col-md-12">
        <div class="messages"></div>
    </div>
</div>
<div class="row">
    <div class="col-md-7">
        <?= Html::img(null, ['data-src' => $attachment->file->getThumbUrl(File::THUMB_SIZE_LARGE)]); ?>
    </div>
    <div class="col-md-5">

        <table class="table table-striped no-margin">
            <colgroup>
                <col width="200">
                <col>
            </colgroup>

            <tr>
                <td><?= Yii::t('phycom/backend/main', 'Imported filename') ?>:</td>
                <td><?= $attachment->file->name; ?></td>
            </tr>
            <tr>
                <td><?= $options->getAttributeLabel('title') ?>:</td>
                <td><?= $form->field($options, 'title')->textInput(['placeholder' => $options->getAttributeLabel('title')])->label(false); ?></td>
            </tr>
            <tr>
                <td><?= $options->getAttributeLabel('alt') ?>:</td>
                <td><?= $form->field($options, 'alt')->textInput(['placeholder' => $options->getAttributeLabel('alt')])->label(false); ?></td>
            </tr>
            <tr>
                <td><?= $options->getAttributeLabel('caption') ?>:</td>
                <td><?= $form->field($options, 'caption')->textInput(['placeholder' => $options->getAttributeLabel('caption')])->label(false); ?></td>
            </tr>
            <tr>
                <td><?= $options->getAttributeLabel('description') ?>:</td>
                <td><?= $form->field($options, 'description')->textarea(['rows' => 5, 'placeholder' => $options->getAttributeLabel('description')])->label(false); ?></td>
            </tr>
            <tr>
                <td><?= $options->getAttributeLabel('link') ?>:</td>
                <td><?= $form->field($options, 'link')->input('url', ['placeholder' => $options->getAttributeLabel('link')])->label(false); ?></td>
            </tr>
            <tr>
                <td><?= $options->getAttributeLabel('smallScreen') ?>:</td>
                <td><?= $form->field($options, 'smallScreen')->boolean()->label(false); ?></td>
            </tr>


        </table>
    </div>
</div>
<?php ActiveForm::end(); ?>
