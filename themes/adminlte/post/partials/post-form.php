<?php

/**
 * @var $this yii\web\View
 * @var $model Phycom\Backend\Models\PostForm
 */

use Phycom\Backend\Helpers\Html;
use Phycom\Backend\Widgets\ActiveForm;
use Phycom\Base\Models\Attributes\PostStatus;
use Phycom\Base\Helpers\f;

$defaultAttributes = [
    'categories',
    'tags',
    'status',
    'created_by',
    'created_at',
    'updated_at',
    'published_at'
];
$attributes = $attributes ?? $defaultAttributes;


?>
<?php $form = ActiveForm::begin(['id' => 'post-form']); ?>

<?= $form->field($model, 'shopId')->hiddenInput()->label(false); ?>

<div class="row">

    <div class="col-md-12">

        <table class="table table-striped">
            <colgroup>
                <col width="200">
                <col>
            </colgroup>
            <tbody>
            <?php if (in_array('created_by', $attributes)): ?>
                <tr>
                    <td><strong><?= $model->post->getAttributeLabel('created_by'); ?></strong></td>
                    <td><?= f::text($model->post->createdBy ? $model->post->createdBy->fullName : null); ?></td>
                </tr>
            <?php endif; ?>
            <?php if (in_array('created_at', $attributes)): ?>
                <tr>
                    <td><strong><?= $model->post->getAttributeLabel('created_at'); ?></strong></td>
                    <td><?= f::datetime($model->post->created_at ?? null); ?></td>
                </tr>
            <?php endif; ?>
            <?php if (in_array('updated_at', $attributes)): ?>
                <tr>
                    <td><strong><?= $model->post->getAttributeLabel('updated_at'); ?></strong></td>
                    <td><?= f::datetime($model->post->updated_at ?? null); ?></td>
                </tr>
            <?php endif; ?>
            <?php if (in_array('published_at', $attributes)): ?>
                <tr>
                    <td><strong><?= $model->post->getAttributeLabel('published_at'); ?></strong></td>
                    <td><?= f::datetime($model->post->published_at ?? null); ?></td>
                </tr>
            <?php endif; ?>
            </tbody>
        </table>

        <?php if (in_array('categories', $attributes)): ?>
            <div class="row">
                <div class="col-md-8">
                    <?= $form->field($model, 'categories')->multiSelect($model->allCategories); ?>
                </div>
            </div>
        <?php endif; ?>
        <?php if (in_array('tags', $attributes)): ?>
            <div class="row">
                <div class="col-md-8">
                    <?= $form->field($model, 'tags')->tags($model->getPostTags()); ?>
                </div>
            </div>
        <?php endif; ?>
        <?php if (in_array('status', $attributes)): ?>
            <div class="row">
                <div class="col-md-8">
                    <?= $form->field($model, 'status')->dropDownList(PostStatus::displayValues()); ?>
                </div>
            </div>
        <?php endif; ?>

        <?php if (in_array('identifier', $attributes)): ?>
            <div class="row">
                <div class="col-md-8">
                    <?= $form->field($model, 'identifier')->dropDownList($model->getIdentifiers(), [
                            'prompt' => Yii::t('phycom/backend/main', 'Select {attribute}', ['attribute' => $model->getAttributeLabel('identifier')])
                        ]);
                    ?>
                </div>
            </div>
        <?php endif; ?>
    </div>

</div>
<?php ActiveForm::end(); ?>
