<?php

use Phycom\Backend\Widgets\Box;
use Phycom\Backend\Widgets\DataGrid;
use Phycom\Base\Helpers\Filter;

use yii\helpers\ArrayHelper;

/**
 * @var $this yii\web\View
 * @var \Phycom\Backend\Models\SearchSubscription $model
 * @var \yii\data\ActiveDataProvider $dataProvider
 */

$this->title = Yii::t('phycom/backend/marketing', 'Newsletter subscriptions');
$this->params['breadcrumbs'][] = Yii::t('phycom/backend/marketing', 'Marketing');
$this->params['breadcrumbs'][] = $this->title
?>
<div class="row">
	<div class="col-md-12">
		<?= Box::begin([
			'options' => ['class' => 'box box-default'],
			'showHeader' => false,
			'bodyOptions' => ['class' => 'box-body']
		]);
		?>

		<?= DataGrid::widget([
			'id' => 'user-grid',
			'dataProvider' => $dataProvider,
			'filterModel' => $model,
			'actions' => [
                [
                    'label' => '<span class="far fa-save"></span>&nbsp;&nbsp;' . Yii::t('phycom/backend/product', 'Export'),
                    'url' => ArrayHelper::merge(['/subscription'], Yii::$app->request->getQueryParams(), ['export' => 1]),
                    'options' => ['class' => 'btn btn-default']
                ]
            ],
			'columns' => [
                [
	                'attribute' => 'name',
	                'format' => 'text',
                ],
				[
					'attribute' => 'email',
					'format' => 'text'
				],
//				[
//					'attribute' => 'registeredUser',
//					'format' => 'boolean',
//					'options' => ['width' => '120']
//				],
				[
					'attribute' => 'emails_sent',
					'format' => 'integer',
					'options' => ['width' => '160']
				],
				[
					'attribute' => 'status',
					'filter' => Filter::dropDown($model, 'status', \Phycom\Base\Models\Attributes\SubscriptionStatus::displayValues()),
					'format' => 'raw',
					'value' => function ($model) {
						return '<span class="label '.$model->status->labelClass.'">' . $model->status->label . '</span>';
					},
					'options' => ['width' => '120']
				],
				[
					'attribute' => 'created_at',
					'filter' => Filter::daterangepicker($model, 'createdFrom', 'createdTo'),
					'format' => 'datetime',
					'options' => ['width' => '220']
				],
				[
					'attribute' => 'updated_at',
					'filter' => Filter::daterangepicker($model, 'updatedFrom', 'updatedTo'),
					'format' => 'datetime',
					'options' => ['width' => '220']
				]
			]
		]);
		?>

		<?= Box::end(); ?>
	</div>
</div>
