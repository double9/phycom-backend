<aside class="main-sidebar">

    <section class="sidebar">

        <?= Phycom\Backend\Widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu', 'data-widget' => 'tree', 'data-animation-speed' => '100'],
                'items' => [
                    [
                        'label'   => Yii::t('phycom/backend/main', 'Dashboard'),
                        'icon'    => 'home',
                        'url'     => ['/site/index'],
                        'visible' => Yii::$app->dashboard->isEnabled()
                    ],
	                [
                        'label' => Yii::t('phycom/backend/main', 'Products'),
                        'icon' => 'cube',
                        'items' => [
                            [
                                'label'         => Yii::t('phycom/backend/main', 'Catalog'),
                                'icon'          => 'tags',
                                'url'           => ['/product/'],
                                'exclude_child' => ['/product/add'],
                                'visible'       => Yii::$app->user->can('search_products')
                            ],
                            [
                                'label'   => Yii::t('phycom/backend/main', 'Categories'),
                                'icon'    => 'th-list',
                                'url'     => ['/product-category/'],
                                'visible' => Yii::$app->user->can('search_product_categories')
                            ]
                        ],
                        'visible' => (
                            Yii::$app->commerce->isEnabled() &&
                            (
                                Yii::$app->user->can('search_products')
                                || Yii::$app->user->can('search_product_categories')
                                || Yii::$app->user->can('create_product')
                            )
                        )
                    ],
	                [
		                'label' => Yii::t('phycom/backend/main', 'Sale'),
		                'icon' => 'cart-arrow-down',
		                'items' => [
                            [
                                'label'   => Yii::t('phycom/backend/main', 'Orders'),
                                'icon'    => 'cart-plus',
                                'url'     => ['/order'],
                                'visible' => Yii::$app->user->can('search_orders')
                            ],
                            [
                                'label'   => Yii::t('phycom/backend/main', 'Invoices'),
                                'icon'    => 'file-alt',
                                'url'     => ['/invoice'],
                                'visible' => Yii::$app->user->can('search_invoices')
                            ],
                            [
                                'label'   => Yii::t('phycom/backend/main', 'Payments'),
                                'icon'    => 'euro-sign',
                                'url'     => ['/payment-list'],
                                'visible' => Yii::$app->user->can('search_payments')
                            ],
                            [
                                'label'   => Yii::t('phycom/backend/main', 'Statistics'),
                                'icon'    => 'chart-bar',
                                'url'     => ['/site/statistics'],
                                'visible' => Yii::$app->statistics->isEnabled() && Yii::$app->user->can('search_statistics')
                            ],
                            [
                                'label'   => Yii::t('phycom/backend/main', 'Reports'),
                                'icon'    => 'calculator',
                                'url'     => ['/report'],
                                'visible' => (bool)Yii::$app->getModule('report') && Yii::$app->user->can('search_reports')
                            ],
		                ],
                        'visible' => (
                            Yii::$app->commerce->isEnabled() && Yii::$app->commerce->sale->isEnabled() &&
                            (
                                Yii::$app->user->can('search_orders')
                                || Yii::$app->user->can('search_invoices')
                                || Yii::$app->user->can('search_payments')
                                || (Yii::$app->params['showStatistics'] && Yii::$app->user->can('search_statistics'))
                                || ((bool) Yii::$app->getModule('report') && Yii::$app->user->can('search_reports'))
                            )
                        ),
	                ],
                    [
                        'label' => Yii::t('phycom/backend/main', 'Delivery'),
                        'icon' => 'mdi-local-shipping',
                        'items' => [
                            [
                                'label'   => Yii::t('phycom/backend/main', 'Shipments'),
                                'icon'    => 'truck',
                                'url'     => ['/shipment'],
                                'visible' => Yii::$app->user->can('search_shipments')
                            ],
                            [
                                'label'   => Yii::t('phycom/backend/main', 'Delivery areas'),
                                'icon'    => 'mdi-place',
                                'url'     => ['/delivery-area'],
                                'visible' => Yii::$app->user->can('search_delivery_areas')
                            ],
                            [
                                'label'   => Yii::t('phycom/backend/delivery', 'Delivery settings'),
                                'icon'    => 'mdi-settings',
                                'url'     => ['/delivery-settings'],
                                'visible' => Yii::$app->user->can('update_delivery_settings')
                            ]
                        ],
                        'visible' => (
                            Yii::$app->commerce->isEnabled() && Yii::$app->commerce->delivery->isEnabled() &&
                            (
                                Yii::$app->user->can('search_shipments')
                                || Yii::$app->user->can('search_delivery_areas')
                            )
                        )
                    ],
	                [
		                'label' => Yii::t('phycom/backend/main', 'Marketing'),
		                'icon' => 'bullhorn',
		                'items' => [
                            [
                                'label'   => Yii::t('phycom/backend/main', 'Promotion codes'),
                                'icon'    => 'qrcode',
                                'url'     => ['/promotion-code/'],
                                'visible' => Yii::$app->commerce->isEnabled() && Yii::$app->commerce->promoCodes->isEnabled() && Yii::$app->user->can('search_promotion_codes')
                            ],
                            [
                                'label'   => Yii::t('phycom/backend/main', 'Client cards'),
                                'icon'    => 'sticky-note',
                                'url'     => ['/client-card/'],
                                'visible' => Yii::$app->commerce->isEnabled() && Yii::$app->commerce->clientCards->isEnabled() && Yii::$app->user->can('search_client_cards')
                            ],
                            [
                                'label'   => Yii::t('phycom/backend/main', 'Subscriptions'),
                                'icon'    => 'far fa-bookmark',
                                'url'     => ['/subscription/'],
                                'visible' => Yii::$app->subscription->isEnabled() && Yii::$app->user->can('search_newsletter_subscriptions')
                            ],
		                ],
                        'visible' => (
                            Yii::$app->commerce->isEnabled() &&
                            (
                                (Yii::$app->commerce->promoCodes->isEnabled() && Yii::$app->user->can('search_promotion_codes'))
                                || (Yii::$app->commerce->clientCards->isEnabled() && Yii::$app->user->can('search_client_cards'))
                            )
                        ) || (Yii::$app->subscription->isEnabled() && Yii::$app->user->can('search_newsletter_subscriptions'))

                    ],
	                [
		                'label' => Yii::t('phycom/backend/shop', 'Shops'),
		                'icon' => 'mdi-store',
		                'url' => ['/shop'],
                        'visible' => Yii::$app->commerce->isEnabled()
                            && Yii::$app->commerce->shop->isEnabled()
                            && Yii::$app->user->can('search_shops')
                            && !Yii::$app->commerce->shop->multipleVendors
	                ],
                    [
                        'label' => Yii::t('phycom/backend/shop', 'Shops'),
                        'icon' => 'mdi-store',
                        'visible' => Yii::$app->commerce->isEnabled()
                            && Yii::$app->commerce->shop->isEnabled()
                            && Yii::$app->user->can('search_shops')
                            && Yii::$app->commerce->shop->multipleVendors,
                        'items' => [
                            [
                                'label'  => Yii::t('phycom/backend/shop', 'Shops'),
                                'icon'   => 'map-marker-alt',
                                'url'    => ['/shop']
                            ],
                            [
                                'label'  => Yii::t('phycom/backend/shop', 'Vendors'),
                                'icon'   => 'address-card',
                                'url'    => ['/vendor']
                            ],
                        ]

                    ],
	                [
		                'label' => Yii::t('phycom/backend/main', 'Content'),
		                'icon' => 'mdi-description',
		                'items' => [
                            [
                                'label'  => Yii::t('phycom/backend/main', 'Landing pages'),
                                'icon'   => 'desktop',
                                'url'    => ['/post/landing-pages'],
                                'active' => function ($item, $hasActiveChild, $isItemActive, $widget) {
                                    return $isItemActive || $hasActiveChild || in_array($widget->route, ['post/add-landing-page']);
                                },
                                'visible' => Yii::$app->landingPage->isEnabled()
                            ],
                            [
                                'label'  => Yii::t('phycom/backend/main', 'Web Pages'),
                                'icon'   => 'edit',
                                'url'    => ['/post/pages'],
                                'active' => function ($item, $hasActiveChild, $isItemActive, $widget) {
                                    return $isItemActive || $hasActiveChild || in_array($widget->route, ['post/add-page']);
                                }
                            ],
                            [
                                'label'   => Yii::t('phycom/backend/main', 'Blog posts'),
                                'icon'    => 'quote-right',
                                'url'     => ['/post/index'],
                                'active'  => function ($item, $hasActiveChild, $isItemActive, $widget) {
                                    return $isItemActive || $hasActiveChild || in_array($widget->route, ['post/add']);
                                },
                                'visible' => Yii::$app->blog->isEnabled()
                            ],
                            [
                                'label'   => Yii::t('phycom/backend/main', 'Blog Categories'),
                                'icon'    => 'th-list',
                                'url'     => ['/post-category/index'],
                                'visible' => Yii::$app->blog->isEnabled()
                            ],
		                ],
                        'active' => function ($item, $hasActiveChild, $isItemActive, $widget) {
                            return $isItemActive || $hasActiveChild || in_array($widget->route, [
                                    'post/add',
                                    'post/add-page',
                                    'post/add-landing-page',
                                    'post/landing-pages',
                                    'post/pages',
                                    'post/index'
                                ]);
                        }
	                ],
	                [
		                'label' => Yii::t('phycom/backend/main', 'Users'),
		                'icon' => 'users',
		                'url' => ['/user']
	                ],
                    [
                        'label' => Yii::t('phycom/backend/main', 'Other'),
                        'icon' => 'mdi-all-inclusive',
                        'items' => [
                            [
                                'label'   => Yii::t('phycom/backend/main', 'Comments'),
                                'icon'    => 'mdi-comment',
                                'url'     => ['/comment'],
                                'visible' => Yii::$app->comments->isEnabled()
                            ],
                            [
                                'label'   => Yii::t('phycom/backend/main', 'Reviews'),
                                'icon'    => 'star',
                                'url'     => ['/review'],
                                'visible' => Yii::$app->reviews->isEnabled()
                            ],
                            [
                                'label'   => Yii::t('phycom/backend/main', 'Partner contracts'),
                                'icon'    => 'mdi-accessibility',
                                'url'     => ['/gdpr'],
                                'visible' => Yii::$app->partnerContracts->isEnabled()
                            ],
                        ],
                    ],
	                [
		                'label' => Yii::t('phycom/backend/main', 'Settings'),
		                'icon' => 'cogs',
		                'url' => ['/settings'],
                        'visible' => Yii::$app->user->can('update_global_settings')
	                ]
                ],
            ]
        ) ?>

    </section>

</aside>
