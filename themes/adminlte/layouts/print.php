<?php
use yii\helpers\Html;
use yii\helpers\Json;

/* @var $this \yii\web\View */
/* @var $content string */

Phycom\Backend\Assets\AppAsset::register($this);

$messages = Json::encode(['error' => Yii::t('phycom/backend/main', 'Error')]);

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <script type="text/javascript">
        var baseUrl = '<?= Yii::$app->urlManager->baseUrl; ?>';
        var i18n = JSON.parse('<?= $messages ?>');
    </script>
</head>
<body class="print">
    <?php $this->beginBody() ?>
            <?= $content ?>
    <?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
