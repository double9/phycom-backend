<?php

/**
 * @var $this yii\web\View
 * @var \Phycom\Backend\Models\Shop\ShopForm $model
 */

use Phycom\Backend\Widgets\Box;
use Phycom\Backend\Widgets\Tabs2;

use yii\web\JsExpression;

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('phycom/backend/shop', 'Shops'), 'url' => ['/shop/index']];

$items = $items ?? [];
$active = $active ?? null;

?>

<div class="row">

	<div class="col-md-12">

        <?= Box::begin([
            'showHeader'  => false,
            'options'     => ['class' => 'box box-default'],
            'bodyOptions' => ['class' => 'box-body no-padding']
        ]);
        ?>
        <?= Tabs2::widget([
            'id'             => 'shop-tabs',
            'items'          => $items,
            'activeTab'      => $active,
            'contentOptions' => ['class' => 'tab-content col-md-10'],
            'onShowTab'      => new JsExpression("function(tab, e) { if (tab.id === 't2-closed-schedule') {multiCalendarDatePicker.reloadCalendar();} }")
        ]);
        ?>
        <?= Box::end(); ?>

	</div>
</div>


<div class="row">
    <div class="col-md-12">
        <?= Box::begin([
            'showHeader'  => false,
            'options'     => ['class' => 'box box-default'],
            'bodyOptions' => ['class' => 'box-body']
        ]);
        ?>
        <div class="clearfix">
            <div class="pull-left"><?= $this->render('/partials/back-btn', ['size' => 'lg']); ?></div>
            <?= $this->render('/partials/save-btn-2', [
                'id'           => 'save-shop-data',
                'class'        => 'pull-right btn-lg',
                'formSelector' => '#shop-tabs .tab-content > .tab-pane.active'
            ]);
            ?>
        </div>
		<?= Box::end(); ?>
    </div>
</div>



