<?php


use Phycom\Backend\Widgets\Box;
use Phycom\Backend\Widgets\DataGrid;
use Phycom\Backend\Models\Shop\Search as SearchShop;

use Phycom\Base\Helpers\Filter;
use Phycom\Base\Models\Attributes\ShopStatus;
use Phycom\Base\Models\Attributes\ShopType;

use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

/**
 * @var $this yii\web\View
 * @var SearchShop $model
 * @var ActiveDataProvider $dataProvider
 */

$this->title = Yii::t('phycom/backend/shop', 'Shops');
$this->params['breadcrumbs'][] = $this->title
?>
<div class="row">
	<div class="col-md-12">
		<?= Box::begin([
			'options' => ['class' => 'box box-default box-wide'],
			'showHeader' => false,
			'bodyOptions' => ['class' => 'box-body']
		]);
		?>

		<?= DataGrid::widget([
			'id' => 'user-grid',
			'dataProvider' => $dataProvider,
			'filterModel' => $model,
			'rowOptions' => function ($model) {
				return [
					'class' => 'row-link',
					'data-url' => Url::toRoute(['shop/edit','id' => $model->id])
				];
			},
			'actions' => [
				[
					'label' => '<span class="fas fa-plus"></span>&nbsp;&nbsp;' . Yii::t('phycom/backend/shop', 'Add shop'),
					'url' => ['shop/add'],
                    'options' => ['class' => 'btn btn-success']
                ]
			],
			'columns' => [
                [
                    'attribute'      => 'id',
                    'format'         => 'integer',
                    'options'        => ['width' => '60'],
                    'contentOptions' => ['class' => 'hidden-sm hidden-xs'],
                    'filterOptions'  => ['class' => 'hidden-sm hidden-xs'],
                    'headerOptions'  => ['class' => 'hidden-sm hidden-xs'],
                    'visible'        => Yii::$app->commerce->shop->shopListHasColumn('id')
                ],
                [
                    'attribute' => 'type',
                    'filter'    => Filter::dropDown($model, 'type', ArrayHelper::filter(ShopType::displayValues(), Yii::$app->commerce->shop->shopTypes)),
                    'format'    => 'raw',
                    'value'     => fn($model) => '<span class="label ' . $model->type->labelClass . '">' . $model->type->label . '</span>',
                    'options'   => ['width' => '140'],
                    'visible'   => Yii::$app->commerce->shop->shopListHasColumn('type')
                ],
                [
                    'attribute' => 'name',
                    'format'    => 'text',
                    'visible'   => Yii::$app->commerce->shop->shopListHasColumn('name')
                ],
                [
                    'attribute'      => 'address',
                    'format'         => 'address',
                    'contentOptions' => ['class' => 'hidden-sm hidden-xs'],
                    'filterOptions'  => ['class' => 'hidden-sm hidden-xs'],
                    'headerOptions'  => ['class' => 'hidden-sm hidden-xs'],
                    'visible'        => Yii::$app->commerce->shop->shopListHasColumn('address')
                ],
                [
                    'attribute'      => 'phone',
                    'format'         => 'phone',
                    'contentOptions' => ['class' => 'hidden-sm hidden-xs'],
                    'filterOptions'  => ['class' => 'hidden-sm hidden-xs'],
                    'headerOptions'  => ['class' => 'hidden-sm hidden-xs'],
                    'options'        => ['width' => '140'],
                    'visible'        => Yii::$app->commerce->shop->shopListHasColumn('phone')
                ],
                [
                    'attribute' => 'status',
                    'filter'    => Filter::dropDown($model, 'status', ShopStatus::displayValues()),
                    'format'    => 'raw',
                    'value'     => fn($model) => '<span class="label ' . $model->status->labelClass . '">' . $model->status->label . '</span>',
                    'options'   => ['width' => '120'],
                    'visible'   => Yii::$app->commerce->shop->shopListHasColumn('status')
                ],
                [
                    'attribute' => 'vendor',
                    'format'    => 'text',
                    'value'     => fn($model) => $model->vendor ? $model->vendor->name : null,
                    'options'   => ['width' => '220'],
                    'visible'   => Yii::$app->commerce->shop->shopListHasColumn('vendor')
                ],
                [
                    'attribute'      => 'created_at',
                    'filter'         => Filter::daterangepicker($model, 'createdFrom', 'createdTo'),
                    'format'         => 'datetime',
                    'options'        => ['width' => '220'],
                    'contentOptions' => ['class' => 'hidden-sm hidden-xs'],
                    'filterOptions'  => ['class' => 'hidden-sm hidden-xs'],
                    'headerOptions'  => ['class' => 'hidden-sm hidden-xs'],
                    'visible'        => Yii::$app->commerce->shop->shopListHasColumn('created_at')
                ],
                [
                    'attribute'      => 'updated_at',
                    'filter'         => Filter::daterangepicker($model, 'updatedFrom', 'updatedTo'),
                    'format'         => 'datetime',
                    'options'        => ['width' => '220'],
                    'contentOptions' => ['class' => 'hidden-sm hidden-xs'],
                    'filterOptions'  => ['class' => 'hidden-sm hidden-xs'],
                    'headerOptions'  => ['class' => 'hidden-sm hidden-xs'],
                    'visible'        => Yii::$app->commerce->shop->shopListHasColumn('updated_at')
                ],
                [
                    'label'          => false,
                    'format'         => 'raw',
                    'content'        => function ($model) {

                        return $this->render('/partials/delete-dropdown', [
                            'label'   => '<span class="far fa-trash-alt"></span>',
                            'url'     => ['shop/delete', 'id' => $model->id],
                            'options' => ['class' => 'toggle-hover'],
                            'side'    => 'right'
                        ]);
                    },
                    'options'        => ['width' => 40],
                    'contentOptions' => ['style' => 'padding: 2px 0 0 0;', 'class' => 'clickable'],
                    'visible'        => Yii::$app->user->can('delete_shop')
                ]
			]
		]);
		?>

		<?= Box::end(); ?>
	</div>
</div>
