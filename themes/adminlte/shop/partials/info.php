<?php

/**
 * @var $this yii\web\View
 * @var ShopForm $model
 */

use Phycom\Backend\Widgets\Modal;
use Phycom\Backend\Widgets\ActiveForm;
use Phycom\Backend\Models\Shop\ShopForm;

use Phycom\Base\Widgets\Map;
use Phycom\Base\Models\Attributes\ShopStatus;
use Phycom\Base\Models\Attributes\ShopType;
use Phycom\Base\Models\Address;

use yii\helpers\ArrayHelper;
use yii\widgets\DetailView;

$tab = $tab ?? '';

$fieldOpt = ['template' => '{input}{error}', 'options' => ['class' => 'no-margin']];
$fieldOptMd = ArrayHelper::merge($fieldOpt, ['options' => ['style' => 'max-width: 300px;']]);
$fieldOptSm = ArrayHelper::merge($fieldOpt, ['options' => ['style' => 'max-width: 200px;']]);

?>


<div class="row">
    <div class="col-md-9 col-lg-8">

        <?php $form = ActiveForm::begin(['id' => 'shop-form']); ?>

        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [

                [
                    'attribute' => 'name',
                    'format'    => 'raw',
                    'value'     => $form->field($model, 'name', $fieldOpt)->label(false)->textInput(['class' => 'form-control no-margin'])
                ],
                [
                    'attribute' => 'type',
                    'format'    => 'raw',
                    'value'     => $form->field($model, 'type', $fieldOpt)->label(false)->select2(ArrayHelper::filter(ShopType::displayValues(), Yii::$app->commerce->shop->shopTypes))
                ],
                [
                    'attribute' => 'vendorId',
                    'format'    => 'raw',
                    'value'     => $form->field($model, 'vendorId', $fieldOpt)->label(false)->select2($model->getAllVendors()),
                    'visible'   => count($model->getAllVendors()) > 1
                ],
                [
                    'attribute' => 'email',
                    'format'    => 'raw',
                    'value'     => $form->field($model, 'email', $fieldOpt)->label(false)->textInput(['class' => 'form-control no-margin'])
                ],
                [
                    'attribute' => 'address',
                    'format'    => 'raw',
                    'value'     => function ($model) use ($form, $fieldOpt) {
                        return $form->field($model, 'address', ['options' => ['class' => 'no-margin']])->addressField('shop-address-modal', $model->addressForm);
                    }
                ],
                [
                    'attribute' => 'deliveryArea',
                    'format'    => 'raw',
                    'value'     => $form->field($model, 'deliveryArea', $fieldOptMd)->label(false)->boolean(),
                    'visible'   => Yii::$app->commerce->delivery->isEnabled()
                ],
                [
                    'attribute' => 'phone',
                    'format'    => 'raw',
                    'value'     => $form->field($model, 'phone', $fieldOptMd)->label(false)->phoneInput2()
                ],
                [
                    'attribute' => 'status',
                    'format'    => 'raw',
                    'value'     => $form->field($model, 'status', $fieldOptSm)->dropDownList(ShopStatus::displayValues())
                ],
                [
                    'label'   => $model->shop->getAttributeLabel('created_at'),
                    'format'  => 'datetime',
                    'value'   => $model->shop->created_at,
                    'visible' => !$model->shop->isNewRecord
                ],
                [
                    'label'   => $model->shop->getAttributeLabel('updated_at'),
                    'format'  => 'datetime',
                    'value'   => $model->shop->updated_at,
                    'visible' => !$model->shop->isNewRecord
                ]
            ],
        ]);
        ?>
        <input type="hidden" name="tab" value="<?= $tab; ?>" />

        <?php ActiveForm::end(); ?>

        <?= Modal::widget([
            'id' => 'shop-address-modal',
            'title' => $model->shop->address
                ? Yii::t('phycom/backend/shop','Update address')
                : Yii::t('phycom/backend/shop','Add address'),
            'content' => $this->render('/partials/address-form', ['model' => $model->addressForm])
        ]);
        ?>

    </div>

    <div class="col-lg-4 col-md-3">
        <?php if (!$model->shop->isNewRecord): ?>
            <?= Map::widget(['address' => Address::create($model->address)]); ?>
        <?php endif; ?>
    </div>

</div>

