<?php

/**
 * @var $this yii\web\View
 * @var $formModel \Phycom\Backend\Models\Shop\SupplyScheduleCollectionForm
 */

use Phycom\Backend\Widgets\ActiveForm;
use Phycom\Base\Helpers\f;

$m = new \Phycom\Base\Models\ShopSupply();
$tab = $tab ?? '';
?>


    <div class="row">
        <div class="col-md-12" style="max-width: 660px;">

            <?php $form = ActiveForm::begin(['id' => 'supply-weekdays-form']); ?>

            <table class="table table-striped warning">
                <thead>
                    <tr>
                        <th><?= $m->getAttributeLabel('day_of_week'); ?></th>
                        <th><?= $m->getAttributeLabel('delivery'); ?></th>
                        <th><?= $m->getAttributeLabel('dispatched_at'); ?></th>
                        <th><?= $m->getAttributeLabel('delivery_at'); ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($formModel->models as $key => $model): ?>
                        <tr>
                            <td>
                                <strong><?= f::weekday($model->day_of_week, '%A'); ?></strong>
                            </td>
                            <td>
                                <?= $form->mField($formModel, $model, 'delivery')->boolean(false);
                                ?>
                            </td>
                            <td style="width: 140px;">
                                <?= $form->mField($formModel, $model, 'dispatched_at')->textInput([
                                        'type' => 'time',
                                        'value' => $model->dispatched_at instanceof \DateTime ? $model->dispatched_at->format('H:i') : $model->dispatched_at
                                ]); ?>
                            </td>
                            <td style="width: 140px;">
                                <?= $form->mField($formModel, $model, 'delivery_at')->textInput([
                                        'type' => 'time',
                                        'value' => $model->delivery_at instanceof \DateTime ? $model->delivery_at->format('H:i') : $model->delivery_at
                                ]); ?>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>

            <input type="hidden" name="tab" value="<?= $tab; ?>" />

            <?php ActiveForm::end(); ?>

        </div>
    </div>

