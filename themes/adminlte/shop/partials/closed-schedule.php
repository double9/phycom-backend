<?php

/**
 * @var $this yii\web\View
 * @var $model ClosedScheduleForm
 */

use Phycom\Backend\Models\Shop\ClosedScheduleForm;
use Phycom\Backend\Widgets\ActiveForm;
use Phycom\Backend\Widgets\DatePicker;

$tab = $tab ?? '';
?>

<div class="row">
    <div class="col-md-12">
        <?php $form = ActiveForm::begin(['id' => 'closed-schedule-form']); ?>

            <input type="hidden" name="tab" value="<?= $tab; ?>" />
            <?= $form->field($model, 'events')->widget(DatePicker::class, [
                'mode'           => DatePicker::MODE_MULTIPLE,
                'showInputField' => false
            ])->label(false); ?>

        <?php ActiveForm::end(); ?>
    </div>
</div>
