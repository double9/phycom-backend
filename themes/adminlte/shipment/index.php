<?php

use Phycom\Backend\Widgets\Box;
use Phycom\Backend\Widgets\DataGrid;
use Phycom\Base\Helpers\Filter;
use yii\helpers\Url;
use yii\helpers\Html;

/**
 * @var $this yii\web\View
 * @var \Phycom\Backend\Models\SearchShipment $model
 * @var \yii\data\ActiveDataProvider $dataProvider
 */

$this->title = Yii::t('phycom/backend/main', 'Shipments');
$this->params['breadcrumbs'][] = Yii::t('phycom/backend/main', 'Sale');
$this->params['breadcrumbs'][] = $this->title
?>
<div class="row">
	<div class="col-md-12">
		<?= Box::begin([
			'options' => ['class' => 'box box-default box-wide'],
			'showHeader' => false,
			'bodyOptions' => ['class' => 'box-body']
		]);
		?>

		<?= DataGrid::widget([
			'id' => 'user-grid',
			'dataProvider' => $dataProvider,
			'filterModel' => $model,
			'actions' => [],
            'rowOptions' => function ($model) {
                return [
                    'class' => 'row-link',
                    'data-url' => Url::toRoute(['shipment/edit','id' => $model->id])
                ];
            },
			'columns' => [
				[
					'attribute' => 'tracking_number',
					'format' => 'text',
				],
				[
					'attribute' => 'carrier_name',
					'format' => 'raw',
                    'value' => function ($model) {
		                /**
                         * @var \Phycom\Backend\Models\SearchShipment $model
                         */
		                return '<span class="text-muted">' . $model->carrier_name . '</span>';
                    },
					'contentOptions' => ['class' => 'hidden-sm hidden-xs'],
					'filterOptions' => ['class' => 'hidden-sm hidden-xs'],
					'headerOptions' => ['class' => 'hidden-sm hidden-xs']
				],
				[
					'attribute' => 'carrier_area',
					'format' => 'raw',
					'value' => function ($model) {
						/**
						 * @var \Phycom\Backend\Models\SearchShipment $model
						 */
						return '<span class="label label-default">' . $model->carrier_area . '</span>';
					}
				],
				[
					'attribute' => 'recipientName',
					'format' => 'text',
					'contentOptions' => ['class' => 'hidden-sm hidden-xs', 'style' => 'min-width: 160px;'],
					'filterOptions' => ['class' => 'hidden-sm hidden-xs'],
					'headerOptions' => ['class' => 'hidden-sm hidden-xs'],
				],
				[
					'attribute' => 'status',
					'filter' => Filter::dropDown($model, 'status', \Phycom\Base\Models\Attributes\ShipmentStatus::displayValues()),
					'format' => 'raw',
					'value' => function ($model) {
						return '<span class="label '.$model->status->labelClass.'">' . $model->status->label . '</span>';
					},
					'options' => ['width' => '200']
				],
				[
					'attribute' => 'created_at',
					'filter' => Filter::daterangepicker($model, 'createdFrom', 'createdTo'),
					'format' => 'datetime',
					'options' => ['width' => '220'],
					'contentOptions' => ['class' => 'hidden-sm hidden-xs'],
					'filterOptions' => ['class' => 'hidden-sm hidden-xs'],
					'headerOptions' => ['class' => 'hidden-sm hidden-xs']
				],
				[
					'attribute' => 'updated_at',
					'filter' => Filter::daterangepicker($model, 'updatedFrom', 'updatedTo'),
					'format' => 'datetime',
					'options' => ['width' => '220'],
					'contentOptions' => ['class' => 'hidden-sm hidden-xs'],
					'filterOptions' => ['class' => 'hidden-sm hidden-xs'],
					'headerOptions' => ['class' => 'hidden-sm hidden-xs']
				],
				[
					'attribute' => 'shipped_at',
					'filter' => Filter::daterangepicker($model, 'shippedFrom', 'shippedTo'),
					'format' => 'datetime',
					'options' => ['width' => '220'],
					'contentOptions' => ['class' => 'hidden-sm hidden-xs'],
					'filterOptions' => ['class' => 'hidden-sm hidden-xs'],
					'headerOptions' => ['class' => 'hidden-sm hidden-xs']
				]
			]
		]);
		?>

		<?= Box::end(); ?>
	</div>
</div>
