<?php

/**
 * @var $this yii\web\View
 * @var \Phycom\Base\Models\Comment $model
 */

use Phycom\Backend\Widgets\Box;
use Phycom\Base\Models\Attributes\CommentStatus;
use Phycom\Backend\Widgets\BtnAction;

use rmrevin\yii\fontawesome\FAR;

use yii\widgets\DetailView;
use yii\helpers\Html;

$this->title = Yii::t('phycom/backend/comment', 'Comment {id}', ['id' => $model->id]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('phycom/backend/comment', 'Comments'), 'url' => ['/comment/index']];

?>

<div class="row">

	<div class="col-md-12">


		<?= Box::begin([
			'showHeader' => false,
			'options' => ['class' => 'box box-default'],
			'bodyOptions' => ['class' => 'box-body']
		]);
		?>

        <div class="row">
            <div class="col-md-12">

                <?= DetailView::widget([
                    'model' => $model,
                    'attributes' => [
                        [
                            'label' => 	Yii::t('phycom/backend/comment', 'Target'),
                            'format' => 'html',
                            'value' => function ($model) {
                                /**
                                 * @var \Phycom\Base\Models\Comment $model
                                 */
                                if ($model->product) {
                                    return Html::a(
                                            '<span class="fas fa-link"></span>&nbsp;&nbsp;' . Yii::t('phycom/backend/product', 'Product {id}', ['id' => $model->product->id]) . ' - ' . $model->product->translation->title,
                                            ['product/edit', 'id' => $model->product->id]
                                    );
                                } else if ($model->post) {
                                    return Html::a(
                                        '<span class="fas fa-link"></span>&nbsp;&nbsp;' . Yii::t('phycom/backend/post', 'Post {id}', ['id' => $model->post->id]) . ' - ' . $model->post->translation->title,
                                        ['post/edit', 'id' => $model->post->id]
                                    );
                                }

                                return '';
                            },
                            'captionOptions' => ['style' => 'width: 200px;']
                        ],
                        [
                            'attribute' => 'status',
                            'format' => 'html',
                            'value' => '<span class="label ' . $model->status->labelClass . '">' . $model->status->label . '</span>'
                        ],
                        [
                            'attribute' => 'approved_by',
                            'format' => 'html',
                            'value' => function ($model) {
                                if ($model->approved_by) {
                                    return $model->approvedBy->fullName . '&nbsp;&nbsp;' .
                                        Html::a(FAR::i(FAR::_USER),
                                            ['/user/profile', 'id' => $model->approved_by],
                                            ['class' => 'btn btn-default btn-sm']
                                        );
                                }
                                return null;
                            },
                            'visible' => (bool) $model->approved_by
                        ],
                        'author_name:titleCase',
                        'author_email',
                        'author_ip',
                        'author_agent',
                        'content',    // description attribute in HTML
                        'created_at:datetime',
                        'updated_at:datetime'// creation date formatted as datetime
                    ],
                ]);

                ?>

            </div>
        </div>


        <div class="row">
            <div class="col-md-4">
                <?= $this->render('/partials/back-btn', ['size' => 'lg']); ?>
            </div>
            <div class="col-md-8">
                <div class="pull-right" style="display: inline-block">
                    <?php

                    if ($model->status->is(CommentStatus::PENDING)) {

                        echo BtnAction::widget(['model' => $model, 'attributes' => ['status' => CommentStatus::REJECTED],
                            'label' => '<i class="fas fa-ban" style="margin-right: 10px;"></i>' . Yii::t('phycom/backend/comment', 'Reject'),
                            'options' => ['style' => 'display: inline-block; margin-left: 10px;']
                        ]);

                        echo BtnAction::widget(['model' => $model, 'attributes' => ['status' => CommentStatus::APPROVED],
                            'label' => '<i class="fas fa-check" style="margin-right: 10px;"></i>' . Yii::t('phycom/backend/comment', 'Approve'),
                            'options' => ['style' => 'display: inline-block; margin-left: 10px;'],
                            'btnOptions' => ['class' => 'btn bg-maroon btn-lg']
                        ]);
                    }

//                    echo DeleteAction::widget(['model' => $model, 'options' => ['style' => 'display: inline-block; margin-left: 10px;']]);

                    ?>
                </div>
            </div>
        </div>

		<?= Box::end(); ?>

	</div>

</div>



