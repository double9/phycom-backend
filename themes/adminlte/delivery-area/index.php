<?php

use Phycom\Backend\Widgets\Box;
use Phycom\Backend\Widgets\DataGrid;
use Phycom\Base\Helpers\Filter;

use Phycom\Base\Modules\Delivery\Module as DeliveryModule;
use Phycom\Base\Modules\Delivery\Models\DeliveryAreaStatus;

use yii\helpers\ArrayHelper;
use yii\helpers\Url;

/**
 * @var $this yii\web\View
 * @var \Phycom\Backend\Models\DeliveryArea\Search $model
 * @var \yii\data\ActiveDataProvider $dataProvider
 */

$this->title = Yii::t('phycom/backend/main', 'Delivery areas');
$this->params['breadcrumbs'][] = $this->title;

/**
 * @var DeliveryModule $deliveryModule
 */
$deliveryModule = Yii::$app->getModule('delivery');
$deliveryMethods = ArrayHelper::map($deliveryModule->getAllMethods(), 'id', 'name');

?>
<div class="row">
	<div class="col-md-12">
		<?= Box::begin([
			'options' => ['class' => 'box box-default box-wide'],
			'showHeader' => false,
			'bodyOptions' => ['class' => 'box-body']
		]);
		?>

		<?= DataGrid::widget([
			'id' => 'user-grid',
			'dataProvider' => $dataProvider,
			'filterModel' => $model,
			'bulkEditForm' => 'bulk-edit',
			'rowOptions' => function ($model) {
				return [
					'class' => 'row-link',
					'data-url' => Url::toRoute(['delivery-area/edit','id' => $model->id])
				];
			},
			'actions' => [
				[
					'label' => '<span class="fas fa-plus"></span>&nbsp;&nbsp;' . Yii::t('phycom/backend/delivery-area', 'Add delivery area'),
					'url' => ['delivery-area/add'],
                    'options' => ['class' => 'btn btn-success']
				]
			],
			'columns' => [
                [
                    'attribute' => 'code',
                    'format'    => 'html',
                    'value'     => fn($model) => '<span class="label label-primary">' . $model->code . '</span>',
                    'options'   => ['width' => '100']
                ],
                [
                    'attribute' => 'method',
                    'filter'    => Filter::dropDown($model, 'method', $deliveryMethods),
                    'format'    => 'text',
                    'value'     => function ($model) use ($deliveryModule) {
		                $deliveryMethod = $deliveryModule->getDeliveryMethod($model->method);
		                return $deliveryMethod ? $deliveryMethod->getName() : null;
                    },
                    'options'   => ['width' => '200']
                ],
                [
                    'attribute' => 'carrier',
                    'format'    => 'text',
                    'options'   => ['width' => '200']
                ],
                [
                    'attribute'      => 'service',
                    'format'         => 'text',
                    'contentOptions' => ['class' => 'hidden-sm hidden-xs'],
                    'filterOptions'  => ['class' => 'hidden-sm hidden-xs'],
                    'headerOptions'  => ['class' => 'hidden-sm hidden-xs'],
                    'options'        => ['width' => '200']
                ],
                [
                    'attribute' => 'area_code',
                    'format'    => 'html',
                    'value'     => fn($model) => '<span class="label label-default">' . $model->area_code . '</span>',
                    'options'   => ['width' => '120']
                ],
                [
                    'attribute' => 'area',
                    'format'    => 'shortArea',
                ],
                [
                    'attribute' => 'price',
                    'format'    => 'currency',
                    'options'   => ['width' => '120']
                ],
                [
                    'attribute' => 'status',
                    'filter'    => Filter::dropDown($model, 'status', DeliveryAreaStatus::displayValues()),
                    'format'    => 'raw',
                    'value'     => fn($model) => '<span class="label ' . $model->status->labelClass . '">' . $model->status->label . '</span>',
                    'options'   => ['width' => '120']
                ],
                [
                    'attribute'      => 'created_at',
                    'filter'         => Filter::daterangepicker($model, 'createdFrom', 'createdTo'),
                    'format'         => 'datetime',
                    'options'        => ['width' => '220'],
                    'contentOptions' => ['class' => 'hidden-sm hidden-xs'],
                    'filterOptions'  => ['class' => 'hidden-sm hidden-xs'],
                    'headerOptions'  => ['class' => 'hidden-sm hidden-xs']
                ],
                [
                    'attribute'      => 'updated_at',
                    'filter'         => Filter::daterangepicker($model, 'updatedFrom', 'updatedTo'),
                    'format'         => 'datetime',
                    'options'        => ['width' => '220'],
                    'contentOptions' => ['class' => 'hidden-sm hidden-xs'],
                    'filterOptions'  => ['class' => 'hidden-sm hidden-xs'],
                    'headerOptions'  => ['class' => 'hidden-sm hidden-xs']
                ],
                [
                    'label'          => false,
                    'format'         => 'raw',
                    'content'        => function ($model) {

                        return $this->render('/partials/delete-dropdown', [
                            'label'   => '<span class="far fa-trash-alt"></span>',
                            'url'     => ['delivery-area/delete', 'id' => $model->id],
                            'options' => ['class' => 'toggle-hover'],
                            'side'    => 'right'
                        ]);
                    },
                    'options'        => ['width' => 40],
                    'contentOptions' => ['style' => 'padding: 2px 0 0 0;', 'class' => 'clickable']
                ]
			]
		]);
		?>

		<?= Box::end(); ?>
	</div>
</div>
