<?php

use Phycom\Backend\Widgets\ActiveForm;
use Phycom\Backend\Widgets\CalendarJsonEditor;
use Phycom\Backend\Widgets\JsonEditor;

use Phycom\Base\Modules\Delivery\Models\DeliveryAreaStatus;

use yii\helpers\Html;

$currencySymbol = Yii::$app->locale->getCurrencySymbol(Yii::$app->formatter->currencyCode);

$model = Yii::$app->modelFactory->getDeliveryAreaBulkUpdateForm();
$modelName = (new \ReflectionClass($model))->getShortName();

$form = ActiveForm::begin();
?>
    <div class="keys" data-name="<?= $modelName . "[keys][]" ?>"></div>


    <div class="row">
        <div class="col-md-3 col-lg-2">
            <?= $form->field($model, 'price')
                    ->textInput(['placeholder' => $model->getAttributeLabel('price')])
                    ->inputAppend($currencySymbol)
                    ->label(false)
            ?>
        </div>
        <div class="col-md-3 col-lg-2">
            <?= $form->field($model, 'focThresholdAmount')
                ->textInput(['placeholder' => $model->getAttributeLabel('focThresholdAmount')])
                ->inputAppend($currencySymbol)
                ->label(false)
            ?>
        </div>
        <div class="col-sm-3 col-md-2">
            <?= $form->field($model, 'status')
                    ->dropDownList(DeliveryAreaStatus::displayValues(), ['prompt' => Yii::t('phycom/backend/delivery-area', 'Select delivery area status')])
                    ->label(false)
            ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-9">
            <div class="checkbox-option">
                <h6 class="collapse-title">
                    <?= $form->field($model, 'updateDailySchedule')->checkboxCollapse([
                        'class' => 'custom-control-label checkbox-option-title',
                        'collapse' => [
                            'data-target'   => '#opt-collapse-daily-schedule',
                            'aria-controls' => 'opt-collapse-daily-schedule'
                        ]
                    ]) ?>
                </h6>
                <div id="opt-collapse-daily-schedule" class="collapse">
                    <?= $form->field($model, 'dailySchedule')->widget(CalendarJsonEditor::class, ['schema' => $model->getDailyScheduleSchema()])->label(false); ?>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 col-lg-9">
            <div class="checkbox-option">
                <h6 class="collapse-title">
                    <?= $form->field($model, 'updateWeeklySchedule')->checkboxCollapse([
                        'class' => 'custom-control-label checkbox-option-title',
                        'collapse' => [
                            'data-target'   => '#opt-collapse-weekly-schedule',
                            'aria-controls' => 'opt-collapse-weekly-schedule'
                        ]
                    ]) ?>
                </h6>
                <div id="opt-collapse-weekly-schedule" class="collapse">
                    <?= $form->field($model, 'weeklySchedule')->widget(JsonEditor::class, [
                            'schema' => $model->getWeeklyScheduleSchema(),
                            'editorHolderOptions' => ['class' => 'bordered']
                    ])->label(false); ?>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">
            <?= Html::submitButton(
                    '<span class="fas fa-exclamation-circle"></span>&nbsp;&nbsp;' . Yii::t('phycom/backend/main', 'Bulk update selected rows'),
                    [
                        'class' => 'btn btn-md btn-primary',
                        'style' => 'margin-bottom: 15px; margin-top: 10px;'
                    ]
            ) ?>
        </div>
    </div>


<?php ActiveForm::end(); ?>
