<?php

/**
 * @var $this yii\web\View
 * @var DeliveryAreaForm $model
 */

use Phycom\Backend\Models\DeliveryArea\DeliveryAreaForm;
use Phycom\Backend\Widgets\Box;
use Phycom\Backend\Widgets\Tabs2;
use Phycom\Backend\Widgets\DeleteAction;

use yii\web\JsExpression;

$this->title = Yii::t('phycom/backend/main', 'Delivery area {id}', ['id' => $model->deliveryArea->id]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('phycom/backend/main', 'Delivery areas'), 'url' => ['/delivery-area/index']];

$items = $items ?? [];
$active = $active ?? null;

?>


<div class="row">

    <div class="col-md-12">

        <?= Box::begin([
            'showHeader'  => false,
            'options'     => ['class' => 'box box-default'],
            'bodyOptions' => ['class' => 'box-body no-padding']
        ]);
        ?>
        <?= Tabs2::widget([
            'id'             => 'delivery-area-tabs',
            'items'          => $items,
            'activeTab'      => $active,
            'contentOptions' => ['class' => 'tab-content col-md-10'],
        ]);
        ?>
        <?= Box::end(); ?>

    </div>
</div>


<div class="row">
    <div class="col-md-12">
        <?= Box::begin([
            'showHeader'  => false,
            'options'     => ['class' => 'box box-default'],
            'bodyOptions' => ['class' => 'box-body']
        ]);
        ?>
        <div class="clearfix">
            <div class="pull-left">
                <?= $this->render('/partials/back-btn', ['size' => 'lg']); ?>
            </div>

            <?php
            if (Yii::$app->user->can('delete_delivery_area')) {
                echo DeleteAction::widget([
                    'model' => $model->deliveryArea,
                    'options' => [
                        'class' => 'pull-right',
                        'style' => 'display: inline-block; margin-left: 10px;'
                    ]
                ]);
            }
            ?>

            <?= $this->render('/partials/save-btn-2', [
                'id'           => 'save-delivery-area',
                'class'        => 'pull-right btn-lg',
                'formSelector' => '#delivery-area-tabs .tab-content > .tab-pane.active'
            ]);
            ?>
        </div>
        <?= Box::end(); ?>
    </div>
</div>




