<?php

/**
 * @var $this yii\web\View
 * @var DeliveryAreaForm $model
 */
use Phycom\Backend\Widgets\Modal;
use Phycom\Backend\Widgets\ActiveForm;
use Phycom\Backend\Models\DeliveryArea\DeliveryAreaForm;

use Phycom\Base\Widgets\Map;
use Phycom\Base\Models\Address;
use Phycom\Base\Modules\Delivery\Models\DeliveryAreaStatus;

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\DetailView;

$tab = $tab ?? '';

$fieldOpt = ['template' => '{input}{error}', 'options' => ['class' => 'custom-data-field']];
$fieldOptMd = ArrayHelper::merge($fieldOpt, ['options' => ['class' => 'custom-data-field medium']]);
$fieldOptSm = ArrayHelper::merge($fieldOpt, ['options' => ['class' => 'custom-data-field small']]);

$currencySymbol = Yii::$app->locale->getCurrencySymbol(Yii::$app->formatter->currencyCode);
?>

<div class="row">

    <div class="col-md-9 col-lg-8">

        <div class="row">
            <div class="col-md-12">

                <?php $form = ActiveForm::begin(['id' => 'delivery-area-form']); ?>

                <?= DetailView::widget([
                    'model'      => $model,
                    'attributes' => [
                        [
                            'attribute' => 'name',
                            'format'    => 'raw',
                            'value'     => $form->field($model, 'name', $fieldOptSm)->label(false)->textInput(['class' => 'form-control no-margin'])
                        ],
                        [
                            'attribute' => 'code',
                            'format'    => 'raw',
                            'value'     => function ($model) use ($form, $fieldOptMd) {

                                return $form
                                    ->field($model, 'code', $fieldOptMd)
                                    ->label(false)
                                    ->textInput(['class' => 'form-control no-margin'])
                                    ->customAppend(
                                        Html::button('<i class="fas fa-retweet"></i>', [
                                            'class' => 'btn btn-default field-btn generate-code'
                                        ])
                                    );
                            }
                        ],
                        [
                            'attribute' => 'status',
                            'format'    => 'raw',
                            'value'     => $form->field($model, 'status', $fieldOptSm)->dropDownList(DeliveryAreaStatus::displayValues())
                        ],
                        [
                            'attribute' => 'price',
                            'format'    => 'raw',
                            'value'     => $form
                                ->field($model, 'price', $fieldOptSm)
                                ->label(false)
                                ->textInput(['class' => 'form-control no-margin'])
                                ->inputAppend($currencySymbol)
                        ],
                        [
                            'attribute' => 'focThresholdAmount',
                            'format'    => 'raw',
                            'value'     => $form
                                ->field($model, 'focThresholdAmount', $fieldOptSm)
                                ->label(false)
                                ->textInput(['class' => 'form-control no-margin'])
                                ->inputAppend($currencySymbol)
                        ],
                        [
                            'attribute' => 'method',
                            'format'    => 'raw',
                            'value'     => $form->field($model, 'method', $fieldOptSm)->dropDownList($model->getDeliveryMethods())
                        ],
                        [
                            'attribute' => 'carrier',
                            'format'    => 'raw',
                            'value'     => $form->field($model, 'carrier', $fieldOptSm)->label(false)->textInput(['class' => 'form-control no-margin'])
                        ],
                        [
                            'attribute' => 'service',
                            'format'    => 'html',
                            'value'     => '<span class="label label-primary">' . $model->deliveryArea->service . '</span>',
                            'visible'   => !$model->deliveryArea->isNewRecord
                        ],
                        [
                            'label'   => $model->deliveryArea->getAttributeLabel('area_code'),
                            'format'  => 'raw',
                            'value'   => function ($model) {
                                $html = $model->deliveryArea->area_code;
//                                $html .= Html::button('<i class="fas fa-retweet"></i>', [
//                                    'class' => 'btn btn-default field-btn update-area-code'
//                                ]);

                                return $html;
                            },
                            'visible' => !$model->deliveryArea->isNewRecord
                        ],
                        [
                            'attribute' => 'area',
                            'format'    => 'raw',
                            'value'     => function ($model) use ($form, $fieldOpt) {
                                /**
                                 * @var DeliveryAreaForm $model
                                 */
                                return $form->field($model, 'area', ['options' => ['class' => 'no-margin']])
                                    ->addressField('delivery-area-address-modal', $model->getAddressForm())
                                    ->label(false);
                            }
                        ],
                        [
                            'attribute' => 'deliveryTime',
                            'format'    => 'text',
                            'value'     => $model->deliveryArea->delivery_time,
                            'visible'   => (bool)$model->deliveryArea->delivery_time
                        ],
                        [
                            'label'   => $model->deliveryArea->getAttributeLabel('created_at'),
                            'format'  => 'datetime',
                            'value'   => $model->deliveryArea->created_at,
                            'visible' => !$model->deliveryArea->isNewRecord
                        ],
                        [
                            'label'   => $model->deliveryArea->getAttributeLabel('updated_at'),
                            'format'  => 'datetime',
                            'value'   => $model->deliveryArea->updated_at,
                            'visible' => !$model->deliveryArea->isNewRecord
                        ]
                    ],
                ]);

                ?>

                <input type="hidden" name="tab" value="<?= $tab; ?>" />

                <?php ActiveForm::end(); ?>

                <?= Modal::widget([
                    'id' => 'delivery-area-address-modal',
                    'title' => $model->deliveryArea
                        ? Yii::t('phycom/backend/delivery-area', 'Update area')
                        : Yii::t('phycom/backend/delivery-address', 'Add area'),
                    'content' => $this->render('/partials/address-form-extended', ['model' => $model->getAddressForm()])
                ]);
                ?>

            </div>
        </div>

    </div>

    <div class="col-md-3 col-lg-4">
        <?php if (!$model->deliveryArea->isNewRecord): ?>
            <?= Map::widget(['address' => Address::create($model->deliveryArea->area)]); ?>
        <?php endif; ?>
    </div>

</div>
