<?php

/**
 * @var $this yii\web\View
 * @var WeeklyScheduleForm $model
 */
use Phycom\Backend\Widgets\JsonEditor;

use Phycom\Backend\Widgets\ActiveForm;
use Phycom\Backend\Models\DeliveryArea\WeeklyScheduleForm;

$tab = $tab ?? '';
?>


<div class="row">
    <div class="col-xl-8 col-md-12">

        <?php $form = ActiveForm::begin(['id' => 'weekly-schedule-form']); ?>

            <input type="hidden" name="tab" value="<?= $tab; ?>" />

            <?= $form->field($model, 'deliveryTimes')->widget(JsonEditor::class, [
                    'schema' => $model->getSchema()
                ])->label(false);
            ?>

        <?php ActiveForm::end(); ?>

    </div>
</div>
