<?php

/**
 * @var $this yii\web\View
 * @var DailyScheduleForm $model
 */

use Phycom\Backend\Models\DeliveryArea\DailyScheduleForm;
use Phycom\Backend\Widgets\ActiveForm;
use Phycom\Backend\Widgets\CalendarJsonEditor;

$tab = $tab ?? '';

?>

<div class="row">
    <div class="col-md-12">
        <div id="daily-schedule-calendar"></div>

        <?php $form = ActiveForm::begin(['id' => 'daily-schedule-form']); ?>

            <input type="hidden" name="tab" value="<?= $tab; ?>" />

            <?= $form->field($model, 'dates')
                    ->widget(CalendarJsonEditor::class, ['schema' => $model->getSchema()])
                    ->label(false);
            ?>

        <?php ActiveForm::end(); ?>
    </div>
</div>
