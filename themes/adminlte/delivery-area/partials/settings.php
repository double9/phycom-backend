<?php

/**
 * @var $this yii\web\View
 * @var SettingsForm $model
 */

use Phycom\Backend\Models\DeliveryArea\SettingsForm;
use Phycom\Backend\Widgets\ActiveForm;

$tab = $tab ?? '';

?>

<div class="row">
    <div class="col-md-9 col-lg-8">

        <div class="row">
            <div class="col-md-12">

                <?php $form = ActiveForm::begin(['id' => 'delivery-area-settings-form']); ?>

                    <div class="row">
                        <div class="col-md-4">
                            <?= $form->field($model, 'defaultFirstDeliveryThreshold')->textInput(['type' => 'time']) ?>
                        </div>
                        <div class="col-md-4">
                            <?= $form->field($model, 'defaultFirstDeliveryRule')->textInput() ?>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-4">
                            <?= $form->field($model, 'delayedFirstDeliveryThreshold')->textInput(['type' => 'time']) ?>
                        </div>
                        <div class="col-md-4">
                            <?= $form->field($model, 'delayedFirstDeliveryRule')->textInput() ?>
                        </div>
                    </div>

                <input type="hidden" name="tab" value="<?= $tab; ?>" />

                <?php ActiveForm::end(); ?>

            </div>
        </div>

    </div>

    <div class="col-md-3 col-lg-4">
    </div>
</div>
