<?php

/**
 * @var string $content
 * @var array $bodyOptions
 */

use yii\helpers\Html;

$id = $id ?? 'modal-' . md5(time());
$type = $type ?? 'info';
$size = $size ?? 'md';
$footer = $footer ?? false;
$bodyOptions = $bodyOptions ?? ['class' => 'panel-body'];

?>

<div id="<?= $id; ?>" class="modal fade <?= $type; ?>">
	<div class="modal-dialog modal-<?= $size; ?>">
		<div class="modal-content">
			<div class="panel panel-<?= $type; ?>">
				<div class="panel-heading">
					<button type="button" class="close" data-dismiss="modal">
						<span aria-hidden="true">&times;</span>
						<span class="sr-only">Close</span>
					</button>
					<strong><?= ($title ?? '&nbsp;'); ?></strong>
				</div>
				<div class="<?= $bodyOptions['class'] ?>">
					<?= $content; ?>
				</div>
                <?php if ($footer): ?>
                    <div class="panel-footer clearfix">
                        <?= $footer; ?>
                    </div>
                <?php endif; ?>
			</div>
		</div>
	</div>
</div>