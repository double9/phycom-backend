<?php
/**
 * @var $this yii\web\View
 * @var $model Phycom\Base\Models\AddressForm
 */

use Phycom\Backend\Widgets\ActiveForm;


use yii\helpers\Url;
use yii\helpers\Html;

$reflect = new \ReflectionClass($model);
$modelId = strtolower($reflect->getShortName());

$id = $id ?? $modelId;
?>

<?php $form = ActiveForm::begin(['id' => $id]); ?>


<div class="row">
    <div class="col-md-6">
        <?= $form->field($model, 'country')->dropDownList($model->countries, ['data-url' => Url::toRoute('/address/divisions')]); ?>
    </div>
    <div class="col-md-6">
        <?= $form->field($model, 'province')->dropDownList($model->divisions); ?>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <?= $form->field($model, 'locality')->textInput(); ?>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <?= $form->field($model, 'city')->textInput(); ?>
    </div>
    <div class="col-md-6">
        <?= $form->field($model, 'district')->textInput(); ?>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <?= $form->field($model, 'street')->textInput(); ?>
    </div>
    <div class="col-md-6">
        <?= $form->field($model, 'postcode')->textInput(); ?>
    </div>
</div>


<div class="row">
	<div class="col-md-12">
		<?= Html::submitButton(Yii::t('phycom/backend/user', 'Submit'), ['class' => 'btn btn-primary submit']); ?>
	</div>
</div>

<?php ActiveForm::end(); ?>
