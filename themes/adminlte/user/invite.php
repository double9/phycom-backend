<?php

use Phycom\Backend\Widgets\Box;

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

/**
 * @var $this yii\web\View
 * @var \Phycom\Backend\Models\UserInvitationForm $model
 * @var \Phycom\Backend\Models\SearchUserInvitation $searchModel
 */
$this->title = Yii::t('phycom/backend/user', 'Create a new backend user');
$this->params['breadcrumbs'][] = ['label' => Yii::t('phycom/backend/user', 'Users'), 'url' => ['user/index']];
$this->params['breadcrumbs'][] = $this->title
?>

<div class="row">
	<div class="col-md-6">
		<?= Box::begin([
				'options' => ['class' => 'box box-default'],
				'title' => Yii::t('phycom/backend/user', 'Send registration request'),
				'bodyOptions' => ['class' => 'box-body']
			]);
		?>


		<?php $form = ActiveForm::begin(['id' => 'invite-form']); ?>

        <div class="row">
            <div class="col-md-12">

                <?php if ($model->vendorCount > 1): ?>
	                <?= $form->field($model, 'vendorId')->dropDownList($model->vendorList); ?>
                <?php endif; ?>

	            <?= $form->field($model, 'firstName')->textInput(); ?>
				<?= $form->field($model, 'lastName')->textInput(); ?>
				<?= $form->field($model, 'email')->textInput(); ?>

                <div class="row">
                    <div class="col-md-4">
                        <?= $form->field($model, 'role')
                            ->dropDownList(ArrayHelper::map($model->rolesList, 'name', 'displayName'), [
                                'prompt' => Yii::t('phycom/backend/main', 'Select {attribute}', ['attribute' => lcfirst($model->getAttributeLabel('role'))])
                            ]);
                        ?>
                    </div>
                </div>
                <br>


            </div>

        </div>

        <div class="row">
            <div class="col-md-12">
				<?= Html::submitButton(Yii::t('phycom/backend/user', 'Send'), ['class' => 'btn btn-primary']); ?>
            </div>
        </div>

		<?php ActiveForm::end(); ?>

		<?= Box::end(); ?>
	</div>
    <div class="col-md-6">

	    <?= Box::begin([
		    'options' => ['class' => 'box box-default'],
		    'title' => Yii::t('phycom/backend/user', 'Notes'),
		    'bodyOptions' => ['class' => 'box-body', 'style' => 'min-height: 370px;']
	    ]);
	    ?>

        <p><?=  Yii::t('phycom/backend/user', 'When pressing "Send" an email is sent to the address containing the registration link.'); ?></p>
        <p><?=  Yii::t('phycom/backend/user', 'When user clicks on the link he is redirected to the registration form where he can complete the registration process'); ?></p>
        <br>
        <p><?=  Yii::t('phycom/backend/user', 'The link expires in 3 days and a new request must be sent after this.'); ?></p>
        <br>
        <h4><?= Yii::t('phycom/backend/user', 'Roles'); ?></h4>

        <table class="table table-striped table-condensed">
            <thead>
                <tr>
                    <th><?= Yii::t('phycom/backend/user', 'Role'); ?></th>
                    <th><?= Yii::t('phycom/backend/user', 'Description'); ?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($model->rolesList as $role): ?>
                    <tr>
                        <td><b><?= $role->displayName; ?></b></td>
                        <td><i><?= $role->displayDescription; ?></i></td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>

        <?= Box::end(); ?>
    </div>
</div>
<div class="row">
    <div class="col-md-12">

		<?= Box::begin(['title' => Yii::t('phycom/backend/user', 'Registration pending')]); ?>
		<?= $this->render('partials/invitations', ['dataProvider' => $searchModel->search([])]); ?>
		<?= Box::end(); ?>

    </div>
</div>
