<?php

use Phycom\Backend\Widgets\Box;
/**
 * @var $this yii\web\View
 * @var $profileForm Phycom\Backend\Models\UserProfileForm
 * @var $profileImageForm Phycom\Backend\Models\UserProfileImageForm
 * @var $addressForm \Phycom\Backend\Models\UserAddressForm
 * @var \Phycom\Base\Models\User $user
 * @var \Phycom\Backend\Models\SearchUserActivity $activity
 */
$this->title = Yii::$app->user->id === $user->id ? Yii::t('phycom/backend/user', 'My profile') : Yii::t('phycom/backend/user', 'User {user_id} profile', ['user_id' => $user->id]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('phycom/backend/user', 'Users'), 'url' => ['user/index']];
$this->params['breadcrumbs'][] = $this->title
?>
<div class="row">

    <div class="col-md-8">

        <div class="row">
            <div class="col-md-5">
		        <?= $this->render('partials/profile-overview', ['user' => $user, 'model' => $profileImageForm]); ?>
            </div>
            <div class="col-md-7">
		        <?= $this->render('partials/profile-form', ['user' => $user, 'model' => $profileForm]); ?>
            </div>
        </div>

        <?php if ($user->type->isAdmin): ?>
            <div class="row">
                <div class="col-md-12">
                    <?= $this->render('partials/profile-extra', ['user' => $user, 'model' => $user->meta]); ?>
                </div>
            </div>
        <?php endif; ?>


        <div class="row">
            <div class="col-md-12">
		        <?= $this->render('partials/address-box', ['user' => $user, 'model' => $addressForm]); ?>
            </div>
        </div>

        <?php if(Yii::$app->user->can('search_user_activity')): ?>
            <div class="row">
                <div class="col-md-12">

                    <?= Box::begin(['title' => Yii::t('phycom/backend/user', 'User activity log')]); ?>
                    <?= $this->render('partials/user-activity', ['dataProvider' => $activity->search([])]); ?>
                    <?= Box::end(); ?>

                </div>
            </div>
        <?php endif; ?>
    </div>

    <div class="col-md-4">
	    <?= $this->render('partials/notes', ['user' => $user]); ?>
	    <?= $this->render('/order/widget', ['user' => $user, 'title' => Yii::t('phycom/backend/user', 'Order history')]); ?>
    </div>
</div>
