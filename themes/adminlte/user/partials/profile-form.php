<?php
/**
 * @var $this yii\web\View
 * @var $model Phycom\Backend\Models\UserProfileForm
 */

use Phycom\Backend\Widgets\ActiveForm;
?>

<div class="box box-primary">
	<?php $form = ActiveForm::begin(['id' => 'profile-info']); ?>

	<div class="box-body">

        <div class="row">
            <div class="col-md-6">
	            <?= $form->field($model, 'firstName')->textInput(); ?>
            </div>
            <div class="col-md-6">
	            <?= $form->field($model, 'lastName')->textInput(); ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
	            <?= $form->field($model, 'companyName')->textInput(); ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
				<?= $form->field($model, 'email')->textInput(); ?>
            </div>
            <div class="col-md-6">
	            <?= $form->field($model, 'phone')->phoneInput(); ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
	            <?= $form->field($model, 'language')->dropDownList($model->languages); ?>
            </div>
            <div class="col-md-6">
	            <?= $form->field($model, 'timezone')->dropDownList($model->timezones); ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <?= $form->field($model, 'birthday')->datePicker(); ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'advertise')->boolean(true); ?>
            </div>
        </div>

        <?php if ($model->user->type->isAdmin): ?>

        <div class="row">
            <div class="col-md-6">
                <?= $form->field($model, 'receiveOrderNotifications')->boolean(true); ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'receiveErrorNotifications')->boolean(true); ?>
            </div>
        </div>

        <?php endif; ?>

	</div>

	<div class="box-footer">
        <button type="submit" class="btn btn-primary"><?= Yii::t('phycom/backend/user', 'Update'); ?></button>
	</div>

	<?php ActiveForm::end(); ?>
</div>
