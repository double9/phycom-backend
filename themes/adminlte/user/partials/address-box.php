<?php
/**
 * @var $this yii\web\View
 * @var \Phycom\Base\Models\User $user
 */
use Phycom\Base\Helpers\f;
use yii\helpers\Html;
use Phycom\Backend\Widgets\Modal;
?>


<div class="box box-default">

	<div class="box-header with-border">
		<h3 class="box-title"><?= Yii::t('phycom/backend/user', 'Addresses'); ?></h3>
	</div>

	<div class="box-body no-padding">
        <table class="table table-striped">
            <tbody>
                <?php foreach ($user->addresses as $address): ?>
                    <tr>
                        <td width="60"><?= f::integer($address->id); ?></td>
                        <td><?= f::address($address); ?></td>
                        <td><span class="label label-default"><?= f::text($address->type->label); ?></span></td>
                        <td width="200"><?= f::datetime($address->created_at); ?></td>
                        <td width="60">
                            <?= $this->render('/partials/delete-dropdown', [
                                'label' => '<span class="far fa-trash-alt"></span>',
                                'url' => ['user/delete-address', 'id' => $address->id],
                                'options' => ['class' => 'toggle-hover'],
                                'size' => 'md'
                            ]);
                            ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
	</div>

	<div class="box-footer with-border">

        <?= Modal::widget([
                'id' => 'address-modal',
                'title' => Yii::t('phycom/backend/user','Add address'),
                'content' => $this->render('address-form', ['model' => $model])
            ]);
        ?>
        <?= Html::a(
	        '<span class="fas fa-map-marker-alt"></span>&nbsp;&nbsp;' . Yii::t('phycom/backend/user', 'Create new'),
	        ['user/create-address', 'id' => $user->id],
	        ['class' => 'btn btn-primary', 'data-toggle' => 'modal', 'data-target' => '#address-modal']
        ); ?>
	</div>
</div>
