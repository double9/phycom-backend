<?php

/**
 * @var $this yii\web\View
 * @var $model Phycom\Backend\Models\UserProfileImageForm
 * @var \Phycom\Base\Models\User $user
 */
use Phycom\Base\Helpers\f;
use Phycom\Backend\Widgets\ActiveForm;

use yii\helpers\Inflector;
use yii\helpers\Url;

?>
<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title"><?= f::text($user->fullName); ?></h3>
    </div>
    <div class="box-body">
	<?php $form = ActiveForm::begin(['id' => 'profile-image']); ?>

		<?= $form->field($model, 'avatar')->fileInputArea(['pluginOptions' => [
			    'theme' => 'fa',
		        'uploadAsync' => true,
		        'autoReplace' => true,
		        'overwriteInitial' => true,
		        'maxFileCount' => $model->maxFiles,
			    'allowedFileExtensions' => $model->allowedExtensions,
		        'uploadUrl' => Url::toRoute(['user/upload-avatar', 'id' => $user->id]),
			    'showClose'  => false,
			    'showRemove'  => false,
                'showCaption' =>  false,
			    'browseClass' => 'btn btn-sm btn-primary',
			    'browseIcon' => '<span class="far fa-image"></span> ',
			    'browseLabel' => Yii::t('phycom/backend/main','Browse'),
			    'uploadClass' => 'btn btn-sm btn-default file-upload-btn',
                'uploadLabel' => Yii::t('phycom/backend/main','Save'),
			    'defaultPreviewContent' => '<img src="'.$user->avatar.'" alt="'.$user->fullName.' Avatar">',
                'initialPreviewShowDelete' => false,
			    'previewSettings' => ['image' => ['width' => "auto", 'height' => "auto"]],
                'layoutTemplates' => [
	                'main2' => '{preview}<div class="kv-upload-progress hide"></div>{browse}{upload}',
                    'actions' => '', 'progress' => ''
                ]
        ]])->label(false); ?>

	<?php ActiveForm::end(); ?>

        <table class="table">
            <tr>
                <td><strong><?= Yii::t('phycom/backend/main','Status') ?>:</strong></td>
                <td><span class="label label-default"><?= f::text($user->status->label); ?></span></td>
            </tr>
            <tr>
                <td><strong><?= Yii::t('phycom/backend/main','Roles') ?>:</strong></td>
                <td>
                    <?php foreach ($user->roles as $role): ?>
                        <span class="label label-default"><?= Inflector::titleize($role->name); ?></span>
                    <?php endforeach; ?>
                </td>
            </tr>
            <tr>
                <td><strong><?= Yii::t('phycom/backend/main','Username') ?>:</strong></td><td><?= f::text($user->username); ?></td>
            </tr>
            <tr>
                <td><strong><?= Yii::t('phycom/backend/main','Member since') ?>:</strong></td><td><?= $user->created_at->format('M\. Y'); ?></td>
            </tr>
        </table>

    </div>
</div>

<?php
$this->registerJs(
        <<<JS
    $('#userprofileimageform-avatar').on('change', function(event) {
        $('.file-upload-btn').css({display: 'inline-block'});
    });
    $('#userprofileimageform-avatar').on('fileuploaded', function(event, data, id, index) {
        $('.file-upload-btn').hide();
    });
    $('#userprofileimageform-avatar').on('fileuploaderror', function(event, data, msg) {
        $('.file-upload-btn').hide();
    });
JS
, \yii\web\View::POS_END);
