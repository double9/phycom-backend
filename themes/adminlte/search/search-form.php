<?php

use yii\helpers\Url;

?>
<!-- search form -->
<form action="<?= Url::toRoute('/search'); ?>" method="get" class="main-search-form">
    <div class="input-group">
        <input type="text" name="q" class="form-control" placeholder="<?= Yii::t('phycom/backend/main', 'Search...') ?>"/>
        <span class="input-group-btn">
            <button type='submit' name='search' id='search-btn' class="btn">
                <i class="fas fa-search"></i>
            </button>
        </span>
    </div>
</form>
