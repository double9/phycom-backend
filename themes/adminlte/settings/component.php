<?php

/**
 * @var \yii\web\View $this
 * @var \Phycom\Backend\Models\ComponentForm $model
 */

use Phycom\Backend\Widgets\ActiveForm;
use Phycom\Backend\Widgets\JsonEditor;
use yii\helpers\Html;

$form = ActiveForm::begin();
?>

    <div class="row">
        <div class="col-md-12 col-lg-9">
            <?= $form->field($model, 'config')->widget(JsonEditor::class, [
                    'schema' => $model->getComponent()->getJsonSchema(),
                    'editorHolderOptions' => ['class' => 'bordered']
            ])->label(false); ?>
        </div>
    </div>

<?php ActiveForm::end(); ?>