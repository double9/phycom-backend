<?php

/**
 * @var $this yii\web\View
 * @var $model \Phycom\Backend\Models\SiteSettingForm
 */

use Phycom\Backend\Widgets\ActiveForm;

?>

<div class="row">
	<div class="col-md-8">

		<?php $form = ActiveForm::begin(['id' => 'site-settings']); ?>

		<table class="table table-striped" style="vertical-align: middle;">
            <tbody>
                <tr>
                    <td style="width: 220px;"><strong><?= $model->getAttributeLabel('saleState'); ?>:</strong></td>
                    <td><?= $form->field($model, 'saleState')->dropDownList(\Phycom\Base\Models\Attributes\SaleState::displayValues())->label(false); ?></td>
                </tr>
                <tr>
                    <td><strong><?= $model->getAttributeLabel('closedMessage'); ?>:</strong></td>
                    <td><?= $form->field($model, 'closedMessage')->textarea()->label(false); ?></td>
                </tr>
                <tr>
                    <td><strong><?= $model->getAttributeLabel('frontMessage'); ?>:</strong></td>
                    <td><?= $form->field($model, 'frontMessage')->textarea()->label(false); ?></td>
                </tr>
            </tbody>
		</table>

		<?php ActiveForm::end(); ?>
	</div>
</div>







