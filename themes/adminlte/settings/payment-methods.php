<?php

/**
 * @var $this yii\web\View
 * @var $models \Phycom\Base\Modules\Payment\Settings[]
 */

use Phycom\Backend\Widgets\ModuleSettings;

use yii\bootstrap\Alert;
use yii\helpers\Html;

?>
<div id="payment-method-settings">

    <?php if (empty($models)): ?>
        <div class="row">
            <div class="col-md-12">
                <?= Alert::widget([
                    'body'        => '<i class="icon fas fa-info"></i>' . Yii::t('phycom/backend/main', 'No payment methods were found'),
                    'closeButton' => false,
                    'options'     => ['class' => 'alert-info']
                ]) ?>
            </div>
        </div>
    <?php endif; ?>
    <div class="row">
        <ul class="col-md-12">
            <?php foreach ($models as $form): ?>
                <li id="<?= $form->module->id; ?>" class="payment-method">
                    <table class="table table-striped">
                        <thead>
                            <tr style="background-color: #f6f6f6; border-bottom: 2px solid #ddd;">
                                <td>
                                    <h3><?= $form->module->getLabel(); ?></h3>
                                </td>
                                <td class="clearfix">
                                    <?= Html::img($form->module->getLogo(), ['style' => 'height: 57px; float: right']) ?>
                                </td>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td colspan="2">
                                    <?= ModuleSettings::widget(['model' => $form]); ?>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </li>
            <?php endforeach; ?>
        </ul>
    </div>
</div>
