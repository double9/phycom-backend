<?php

/**
 * @var $this yii\web\View
 * @var $module \Phycom\Base\Interfaces\ModuleInterface|\Phycom\Base\Modules\BaseModule
 * @var $models \Phycom\Base\Models\ModuleSettingsForm[]
 */

use Phycom\Backend\Widgets\ModuleSettings;

use yii\bootstrap\Alert;
use yii\helpers\Html;

?>
<div id="payment-method-settings">

    <?php if (empty($models)): ?>
        <div class="row">
            <div class="col-md-12">
                <?= Alert::widget([
                    'body'        => '<i class="icon fas fa-info"></i>' . Yii::t('phycom/backend/main', 'No settings is available for {module} module', ['module' => $module->getLabel()]),
                    'closeButton' => false,
                    'options'     => ['class' => 'alert-info']
                ]) ?>
            </div>
        </div>
    <?php endif; ?>
    <div class="row">
        <ul class="col-md-12">
            <?php foreach ($models as $model): ?>
                <li id="<?= $model->module->id; ?>" class="settings-block">
                    <table class="table table-striped">
                        <thead>
                            <tr style="background-color: #f6f6f6; border-bottom: 2px solid #ddd;">
                                <td>
                                    <h3><?= $model->module->getLabel(); ?></h3>
                                </td>
                                <td class="clearfix">
                                    <?php
                                        if ($logo = $model->module->getLogo()) {
                                            echo Html::img($logo, ['style' => 'height: 57px; float: right']);
                                        }
                                    ?>
                                </td>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td colspan="2">
                                    <?= ModuleSettings::widget(['model' => $model]); ?>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </li>
            <?php endforeach; ?>
        </ul>
    </div>
</div>
