<?php

use Phycom\Backend\Widgets\Box;
use Phycom\Backend\Widgets\DataGrid;

use Phycom\Base\Helpers\Filter;
use Phycom\Base\Models\PartnerContract;

use yii\helpers\Url;

/**
 * @var $this yii\web\View
 * @var \Phycom\Backend\Models\SearchPartnerContract $model
 * @var \yii\data\ActiveDataProvider $dataProvider
 */

$this->title = Yii::t('phycom/backend/main', 'Partner contracts');
$this->params['breadcrumbs'][] = Yii::t('phycom/backend/main', 'Other');
$this->params['breadcrumbs'][] = $this->title
?>

<div class="row">
    <div class="col-md-12">
        <?= Box::begin([
            'options' => ['class' => 'box box-default box-wide'],
            'showHeader' => false,
            'bodyOptions' => ['class' => 'box-body']
        ]);
        ?>

        <?= DataGrid::widget([
            'id' => 'gdpr-grid',
            'dataProvider' => $dataProvider,
            'filterModel' => $model,
            'rowOptions' => function ($model) {
                return [
                    'class' => 'row-link',
                    'data-url' => Url::toRoute(['gdpr/edit','id' => $model->id])
                ];
            },
            'actions' => [
                [
                    'label' => '<span class="fas fa-plus"></span>&nbsp;&nbsp;' . Yii::t('phycom/backend/gdpr', 'Add new contract'),
                    'url' => ['gdpr/add'],
                    'options' => ['class' => 'btn btn-success']
                ]
            ],
            'columns' => [
                [
                    'attribute' => 'id',
                    'format' => 'integer',
                    'options' => ['width' => '120'],
                    'contentOptions' => ['class' => 'hidden-sm hidden-xs'],
                    'filterOptions' => ['class' => 'hidden-sm hidden-xs'],
                    'headerOptions' => ['class' => 'hidden-sm hidden-xs']
                ],
                [
                    'attribute' => 'company_name',
                    'format' => 'text',
                    'options' => [
                        'width' => '250'
                    ]
                ],
                [
                    'attribute' => 'contract_start',
                    'filter' => Filter::daterangepicker($model, 'contractStartFrom', 'contractStartTo'),
                    'format' => 'date',
                    'options' => [
                        'width' => '250'
                    ]
                ],
                [
                    'attribute' => 'contract_end',
                    'filter' => Filter::daterangepicker($model, 'contractEndFrom', 'contractEndTo'),
                    'format' => 'date',
                    'options' => [
                        'width' => '250'
                    ]
                ],
                [
                    'attribute' => 'max_consent_days',
                    'format' => 'integer',
                ],
                [
                    'attribute' => 'mandatory',
                    'format' => 'boolean',
                ],
                [
                    'attribute' => 'category',
                    'filter' => PartnerContract::getCategories(),
                    'format' => 'text',
                    'value' => function ($model) {
                        if (array_key_exists($model['category'], PartnerContract::getCategories())) {
                            return PartnerContract::getCategories()[$model['category']];
                        }
                    }
                ],
                [
                    'attribute' => 'consent_required_on',
                    'filter' => PartnerContract::getRequiredForFields(),
                    'format' => 'text',
                    'value' => function ($model) {
                        return PartnerContract::getRequiredForFields()[$model['consent_required_on']];
                    }
                ],
                [
                    'attribute' => 'created_at',
                    'filter' => Filter::daterangepicker($model, 'createdFrom', 'createdTo'),
                    'format' => 'datetime',
                    'options' => ['width' => '220'],
                    'contentOptions' => ['class' => 'hidden-sm hidden-xs'],
                    'filterOptions' => ['class' => 'hidden-sm hidden-xs'],
                    'headerOptions' => ['class' => 'hidden-sm hidden-xs']
                ],
                [
                    'attribute' => 'updated_at',
                    'filter' => Filter::daterangepicker($model, 'updatedFrom', 'updatedTo'),
                    'format' => 'datetime',
                    'options' => ['width' => '220'],
                    'contentOptions' => ['class' => 'hidden-sm hidden-xs'],
                    'filterOptions' => ['class' => 'hidden-sm hidden-xs'],
                    'headerOptions' => ['class' => 'hidden-sm hidden-xs']
                ]
            ]
        ]);
        ?>

        <?= Box::end(); ?>
    </div>
</div>
