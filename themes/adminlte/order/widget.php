<?php

/**
 * @var \Phycom\Backend\Models\SearchShipment $dataProvider
 * @var \Phycom\Base\Models\User $user
 * @var array $exclude
 */
use yii\helpers\Html;
use yii\helpers\Url;
use Phycom\Backend\Widgets\AjaxGrid;
use Phycom\Backend\Widgets\Box;
use Phycom\Base\Models\Attributes\OrderStatus;

$title = $title ?? Yii::t('phycom/backend/order', 'Orders');
$exclude = $exclude ?? [];
$dataProvider = (new \Phycom\Backend\Models\SearchOrder(['user_id' => $user->id]))->search();
$dataProvider->pagination = false;
$dataProvider->query->andWhere(['not', ['o.status' => [OrderStatus::DELETED, OrderStatus::NEW, OrderStatus::PENDING_PAYMENT]]]);

if (!empty($exclude)) {
	$dataProvider->query->andWhere(['not', ['o.id' => $exclude]]);
}

echo Box::begin([
	'title' => $title,
	'options' => ['class' => 'box box-default'],
	'bodyOptions' => ['class' => 'box-body']
]);

	echo AjaxGrid::widget([
		'id' => 'shipments-grid',
		'dataProvider' => $dataProvider,
		'route' => 'order/shipments',
		'rowOptions' => function ($model) {
			return [
				'class' => 'row-link',
				'data-url' => Url::toRoute(['order/edit','id' => $model->id])
			];
		},
		'columns' => [
			[
				'attribute' => 'number',
				'format' => 'text',
				'enableSorting' => false,
			],
			[
				'attribute' => 'status',
				'format' => 'raw',
				'enableSorting' => false,
				'value' => function ($model) {
					/**
					 * @var \Phycom\Backend\Models\SearchOrder $model
					 */
					return Html::tag('span', $model->status->label, ['class' => 'label ' . $model->status->labelClass]);
				},
				'options' => ['width' => '180']
			],
			[
				'attribute' => 'created_at',
				'format' => 'datetime',
				'enableSorting' => false,
				'options' => ['width' => '180']
			]
		]
	]);

echo Box::end();
