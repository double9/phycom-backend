<?php

use Phycom\Backend\Widgets\Box;
use Phycom\Backend\Widgets\DataGrid;
use Phycom\Base\Helpers\Filter;
use yii\helpers\Url;

/**
 * @var $this yii\web\View
 * @var \Phycom\Backend\Models\SearchOrder $model
 * @var \yii\data\ActiveDataProvider $dataProvider
 */

$this->title = Yii::t('phycom/backend/main', 'Orders');
$this->params['breadcrumbs'][] = Yii::t('phycom/backend/main', 'Sale');
$this->params['breadcrumbs'][] = $this->title
?>
<div class="row">
	<div class="col-md-12">
		<?= Box::begin([
			'options' => ['class' => 'box box-default box-wide'],
			'showHeader' => false,
			'bodyOptions' => ['class' => 'box-body']
		]);
		?>

		<?= DataGrid::widget([
			'id' => 'user-grid',
			'dataProvider' => $dataProvider,
			'filterModel' => $model,
			'rowOptions' => function ($model) {
				return [
					'class' => 'row-link',
					'data-url' => Url::toRoute(['order/edit','id' => $model->id])
				];
			},
			'actions' => [

			],
			'columns' => [
				[
					'attribute' => 'number',
                    'label' => '#',
					'format' => 'text',
					'options' => ['width' => '200']
				],
                [
	                'attribute' => 'customer',
	                'format' => 'text',
	                'contentOptions' => ['class' => 'hidden-xs'],
	                'filterOptions' => ['class' => 'hidden-xs'],
	                'headerOptions' => ['class' => 'hidden-xs']
                ],
                [
	                'attribute' => 'total',
	                'format' => 'currencyDisplay',
	                'options' => ['width' => '200']
                ],
				[
					'attribute' => 'status',
					'filter' => Filter::dropDown($model, 'status', \Phycom\Base\Models\Attributes\OrderStatus::displayValues()),
					'format' => 'raw',
					'value' => function ($model) {
						return '<span class="label '.$model->status->labelClass.'">' . $model->status->label . '</span>';
					},
					'options' => ['width' => '200']
				],
				[
					'attribute' => 'created_at',
					'filter' => Filter::daterangepicker($model, 'createdFrom', 'createdTo'),
					'format' => 'datetime',
					'options' => ['width' => '220'],
					'contentOptions' => ['class' => 'hidden-sm hidden-xs'],
					'filterOptions' => ['class' => 'hidden-sm hidden-xs'],
					'headerOptions' => ['class' => 'hidden-sm hidden-xs']
				],
				[
					'attribute' => 'updated_at',
					'filter' => Filter::daterangepicker($model, 'updatedFrom', 'updatedTo'),
					'format' => 'datetime',
					'options' => ['width' => '220'],
                    'contentOptions' => ['class' => 'hidden-sm hidden-xs'],
					'filterOptions' => ['class' => 'hidden-sm hidden-xs'],
					'headerOptions' => ['class' => 'hidden-sm hidden-xs']
				],
				[
					'attribute' => 'paid_at',
					'filter' => Filter::daterangepicker($model, 'paidFrom', 'paidTo'),
					'format' => 'datetime',
					'options' => ['width' => '220'],
					'contentOptions' => ['class' => 'hidden-sm hidden-xs'],
					'filterOptions' => ['class' => 'hidden-sm hidden-xs'],
					'headerOptions' => ['class' => 'hidden-sm hidden-xs']
				]
			]
		]);
		?>

		<?= Box::end(); ?>
	</div>
</div>
