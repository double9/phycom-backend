<?php

use Phycom\Backend\Widgets\PopoverConfirmDialog;
use Phycom\Backend\Widgets\Box;

use Phycom\Base\Helpers\f;

use rmrevin\yii\fontawesome\FAS;
use rmrevin\yii\fontawesome\FAR;

use yii\widgets\DetailView;
use yii\helpers\Html;

/**
 * @var $this yii\web\View
 * @var \Phycom\Backend\Models\OrderForm $model
 * @var \Phycom\Base\Models\Order $order
 */

$this->title = Yii::t('phycom/backend/main', 'Order #{no} - {customer}', ['no' => $order->number, 'customer' => $order->getFullName()]);
$this->params['breadcrumbs'][] = Yii::t('phycom/backend/main', 'Sale');
$this->params['breadcrumbs'][] = ['label' => Yii::t('phycom/backend/main', 'Orders'), 'url' => ['/order']];
$this->params['breadcrumbs'][] = $order->number;

$updatableStatuses = $model->getUpdatableStatuses();

?>

<?php $this->beginBlock('order-status-dialog') ?>
    <h5><?= Yii::t('phycom/backend/order', 'Change order status to') ?>:</h5>
    <?= Html::dropDownList(
            'OrderForm[status]',
            (string) $model->order->status,
            $updatableStatuses,
            ['class' => 'form-control']
    ) ?>

<?php $this->endBlock(); ?>



<?php $this->beginBlock('order-control-bar') ?>

    <div class="order-control-bar col-md-12">
        <?= Box::begin([
            'showHeader' => false,
            'options' => ['class' => 'box box-default'],
            'bodyOptions' => ['class' => 'box-body']
        ]);
        ?>
        <div class="clearfix">
            <div class="pull-left"><?= $this->render('/partials/back-btn', ['size' => 'lg']); ?></div>
            <div class="order-actions pull-right">
                <?= Html::a(
                    FAS::i(FAS::_PRINT) . '&nbsp;&nbsp;' .Yii::t('phycom/backend/order', 'Print label'),
                    ['/order/label', 'id' => $order->id],
                    ['class' => 'btn btn-lg btn-default', 'target' => '_blank']
                ); ?>

                <?php

                    if (!empty($updatableStatuses)) {
                        echo PopoverConfirmDialog::widget([
                            'buttonLabel'        => FAS::i(FAS::_EDIT) . '&nbsp;&nbsp;' . Yii::t('phycom/backend/order', 'Update status'),
                            'successButtonLabel' => Yii::t('phycom/backend/main', 'Confirm'),
                            'dismissButtonLabel' => Yii::t('phycom/backend/main', 'Cancel'),
                            'buttonOptions'      => [
                                'class'          => 'btn btn-lg btn-primary',
                                'data-container' => 'body',
                                'data-placement' => 'top'
                            ],
                            'route'              => ['/order/edit', 'id' => $order->id],
                            'content'            => $this->blocks['order-status-dialog'],
                            'formOptions'        => ['class' => 'popover-dialog-form']
                        ]);
                    }
                ?>
            </div>
        </div>
        <?= Box::end(); ?>
    </div>

<?php $this->endBlock(); ?>



<div class="row">
    <div class="col-lg-8 col-md-12">

		<?= Box::begin([
			'showHeader' => false,
			'options' => ['class' => 'box box-default'],
			'bodyOptions' => ['class' => 'box-body']
		]);
		?>

        <div class="row">
            <div class="col-md-8">
	            <?= DetailView::widget([
		            'id' => 'order-detail',
		            'model' => $order,
		            'attributes' => [
			            'number:text',
			            [
				            'label' => Yii::t('phycom/backend/order', 'Customer'),
				            'format' => 'html',
				            'value' => function ($model) {
                                /**
                                 * @var $model \Phycom\Base\Models\Order
                                 */
                                $html = '<span class="name">' . f::titleCase($model->getFullName()) .  '</span>';

	                            if ($model->user_id) {
                                    $html .= Html::a(
                                        FAR::i(FAR::_USER),
                                        ['/user/profile', 'id' => $model->user_id],
                                        ['class' => 'btn btn-default btn-sm pull-right']
                                    );
                                }

					            return $html;
				            },
                            'captionOptions' => ['style' => 'width: 200px;']
			            ],
                        [
                            'attribute' => 'company_name',
                            'visible' => (bool) $order->company_name
                        ],
			            'promotion_code:text',
			            [
				            'label' => $order->getAttributeLabel('status'),
				            'format' => 'raw',
				            'value' => '<span class="label ' . $order->status->labelClass . '">' . $order->status->label . '</span>'
			            ],
                        [
                            'label' => Yii::t('phycom/backend/order', 'Phone number'),
                            'format' => ['phone', true],
                            'value' => $order->phone_code . $order->phone_number,
                            'visible' => (bool) $order->phone_number
                        ],
			            [
				            'label' => Yii::t('phycom/backend/order', 'Email'),
				            'format' => 'email',
				            'value' => $order->email
			            ],
                        [
                            'attribute' => 'comment',
                            'format' => 'text',
                            'visible' => (bool)$order->comment
                        ],
                        [
                            'label' => Yii::t('phycom/backend/order', 'Shipment type'),
                            'format' => 'raw',
                            'value' => $order->shipment ? '<span class="label ' . $order->shipment->type->labelClass . '">' . $order->shipment->type->label . '</span>' : null,
                            'visible' => (bool)$order->shipment
                        ],
                        [
                            'label' => Yii::t('phycom/backend/order', 'Address'),
                            'format' => 'addressLink',
                            'value' => $order->shipment ? $order->shipment->to_address : null,
                            'visible' => $order->shipment && $order->shipment->isDelivery
                        ],
                        [
                            'attribute' => 'paid_at',
                            'format' => 'raw',
                            'value' => $order->paid_at ? f::datetime($order->paid_at) : Html::tag('span', Yii::t('phycom/backend/order', 'Payment not received'), ['class' => 'label label-danger'])
                        ],
			            'created_at:datetime',
			            'updated_at:datetime'// creation date formatted as datetime
		            ],
	            ]);

	            ?>
            </div>
            <div class="col-md-4 clearfix">
                <div class="btn-group pull-right">
                <?php foreach ($model->order->invoices as $invoice): ?>
	                <?= Html::a(
	                        Yii::t('phycom/backend/order', 'Invoice # {no}', ['no' => $invoice->number]),
                            ['/invoice/edit', 'id' => $invoice->id],
                            ['class' => 'btn ' . $invoice->status->getCssClass('btn-')])
                    ?>
                <?php endforeach; ?>
                </div>
            </div>
        </div>



        <br />
        <br />
	    <?= $this->render('/order/partials/order-items', ['order' => $order]); ?>
        <br />
        <br />
        <br />

		<?= Box::end(); ?>

        <div class="row visible-lg">
            <?= $this->blocks['order-control-bar'] ?>
        </div>
    </div>

    <div class="col-lg-4 col-md-12">
        <?php
            $dataProvider = (new \Phycom\Backend\Models\SearchShipment(['order_id' => $order->id]))->search();
            $dataProvider->pagination = false;
            echo $this->render('/shipment/widget', ['dataProvider' => $dataProvider]);
        ?>
    </div>
    <?php if ($order->user_id): ?>
        <div class="col-lg-4 col-md-12">
            <?= $this->render('widget', [
                    'user' => $order->user,
                    'exclude' => [$order->id],
                    'title' => Yii::t('phycom/backend/order', 'Previous orders')
            ]); ?>
        </div>
    <?php endif; ?>
</div>
<div class="row hidden-lg">
    <?= $this->blocks['order-control-bar'] ?>
</div>

