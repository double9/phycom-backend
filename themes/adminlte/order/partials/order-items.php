<?php

use Phycom\Base\Components\FileStorage;
use yii\data\ActiveDataProvider;
use Phycom\Base\Helpers\f;

use yii\helpers\Url;
use yii\helpers\Html;

/**
 * @var \Phycom\Base\Models\Order $order
 */

$orderItem = new \Phycom\Base\Models\OrderItem();

/**
 * @var $deliveryModule \Phycom\Base\Modules\Delivery\Module;
 */
$deliveryModule = Yii::$app->getModule('delivery');
/**
 * @var $couriers \Phycom\Base\Modules\Delivery\Interfaces\DeliveryMethodInterface[]
 */
$couriers = $deliveryModule->allMethods;

?>

<table class="order-table table table-striped">
	<colgroup>
		<col width="200">
		<col>
		<col width="200">
		<col width="200">
		<col width="200">
		<col width="200">
		<col width="50">
	</colgroup>
	<thead>
		<tr>
			<th><?= $orderItem->getAttributeLabel('code'); ?></th>
			<th><?= $orderItem->getAttributeLabel('label'); ?></th>
			<th><?= $orderItem->getAttributeLabel('quantity'); ?></th>
			<th><?= $orderItem->getAttributeLabel('price'); ?></th>
			<th><?= $orderItem->getAttributeLabel('discount'); ?></th>
			<th><?= $orderItem->getAttributeLabel('total'); ?></th>
			<th></th>
		</tr>
	</thead>
	<tbody>
		<?php foreach ($order->orderItems as $model): ?>

			<tr>
				<td><?= f::text($model->code); ?></td>
				<td>
                    <?php
                        $title = f::text($model->label);

                        if ($model->product) {
                            $attributes = $model->product_attributes ?: [];
                            $Option = Yii::$app->modelFactory->getVariantOption();

	                        if (!empty($attributes)) {
		                        $title .= ' - ';
	                        }
	                        $sp = false;
	                        foreach ($attributes as $attributeName => $attributeValue) {
	                            if (empty($attributeValue)) {
                                    continue;
                                }
		                        if ($option = $Option::findByKey($attributeName, $attributeValue)) {
			                        $attributeValue = Html::tag('span', $option->label, ['class' => 'label label-default']);
		                        } else if (filter_var($attributeValue, FILTER_VALIDATE_URL)) {
		                            $attributeValue = f::url($attributeValue, ['target' => '_blank']);
                                } else if ('image' === $attributeName) {

                                    $bucket = Yii::$app->fileStorage->getBucket(FileStorage::BUCKET_ORDER);
                                    if (!$bucket->fileExists($attributeValue)) {
                                        $bucket = Yii::$app->fileStorage->getBucket(FileStorage::BUCKET_TEMP);
                                    }
                                    if ($bucket->fileExists($attributeValue)) {
                                        $url = Url::to($bucket->getFileUrl($attributeValue), true);
                                        $attributeValue = f::url($url, ['target' => '_blank']);
                                    }
                                } else {

		                            if ($variant = Yii::$app->modelFactory->getVariant()::findByName($attributeName)) {
		                                $attributeValue = mb_strtoupper($variant->label) . ': ' . $attributeValue;
                                    }

	                                $attributeValue = Html::tag('span', $attributeValue, ['class' => 'label label-default']);
                                }
		                        $title .= Html::tag('span', $attributeValue, ['class' => 'line-attribute', 'data-attribute' => $attributeName]);
	                        }


//	                        foreach ($couriers as $courier) {
//	                            foreach ($courier->getAreas() as $area) {
//	                                if ($area->getUniqueId() === $model->code) {
//	                                    if ($courier->getType()->value === \Phycom\Base\Modules\Delivery\Models\CourierType::HOME_DELIVERY) {
//                                            $title .= ' - <span class="delivery-address">' . (string) \Phycom\Base\Models\Address::create($model->order->delivery_address) . '</span>';
//                                        }
//	                                    break 2;
//                                    }
//                                }
//                            }
                        }

                        echo $title;
                    ?>
                </td>
				<td><?= f::integer($model->quantity); ?></td>
				<td><?= f::currency($model->price); ?></td>
				<td><?= f::percent($model->discount); ?></td>
				<td><?= f::currency($model->total); ?></td>
				<td>
                </td>

			</tr>

		<?php endforeach; ?>
	</tbody>
	<tfoot>
		<tr>
			<td colspan="4"></td>
			<td><strong><?= Yii::t('phycom/backend/order', 'Total amount'); ?></strong></td>
            <td><h4 class="no-margin"><strong><?= f::currency($order->total); ?></strong></h4></td>
			<td></td>
		</tr>
	</tfoot>
</table>

