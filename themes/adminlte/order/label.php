<?php

use Phycom\Base\Helpers\f;

use yii\widgets\DetailView;

/**
 * @var $this yii\web\View
 * @var \Phycom\Base\Models\Order $order
 */

$this->title = Yii::t('phycom/backend/main', 'Order #{no} - {customer}', ['no' => $order->number, 'customer' => $order->getFullName()]);

$orderItem = Yii::$app->modelFactory->getOrderItem();

$f = clone Yii::$app->formatter;

?>

<div id="order-label">
    <div class="row">
        <div id="order-information" class="col-md-12">
            <div class="row">

                <div class="col-sm-6 col-xs-6">
                    <?= DetailView::widget([
                        'id'        => 'order-detail-col-1',
                        'options'   => ['class' => 'table table-striped detail-view'],
                        'model'     => $order,
                        'formatter' => $f,
                        'attributes' => [
                            'number:text',
                            [
                                'label' => $order->getAttributeLabel('status'),
                                'format' => 'text',
                                'value' => $order->status->label
                            ],
                            'created_at:datetime',
                            [
                                'label' => Yii::t('phycom/backend/order', 'Customer'),
                                'format' => 'titleCase',
                                'value' => function ($model) use ($f) {
                                    /**
                                     * @var $model \Phycom\Base\Models\Order
                                     */
                                    return $model->getFullName();
                                },
                                'captionOptions' => ['style' => 'width: 200px;']
                            ],
                            [
                                'attribute' => 'company_name',
                                'visible' => (bool) $order->company_name
                            ],
                            [
                                'label' => Yii::t('phycom/backend/order', 'Phone number'),
                                'format' => ['phone', true],
                                'value' => $order->phone_code . $order->phone_number,
                                'visible' => (bool) $order->phone_number
                            ],
                            [
                                'label' => Yii::t('phycom/backend/order', 'Email'),
                                'format' => 'email',
                                'value' => $order->email
                            ]
                        ],
                    ]);

                    ?>
                </div>
                <div class="col-sm-6 col-xs-6">
                    <?= DetailView::widget([
                        'id'         => 'order-detail-col-2',
                        'options'    => ['class' => 'table table-striped detail-view'],
                        'model'      => $order->shipment,
                        'attributes' => [
                            [
                                'label'  => Yii::t('phycom/backend/order', 'Invoice number'),
                                'format' => 'text',
                                'value'  => $order->invoice->number
                            ],
                            [
                                'label'  => $order->getAttributeLabel('paid_at'),
                                'format' => 'raw',
                                'value'  => $order->paid_at ? $f->asDatetime($order->paid_at) : Yii::t('phycom/backend/order', 'Payment not received')
                            ],
                            [
                                'label'     => Yii::t('phycom/backend/order', 'Shipment type'),
                                'attribute' => 'type',
                                'format'    => 'text',
                                'value'     => $order->shipment->type->label
                            ],
                            [
                                'attribute' => 'to_address',
                                'format'    => 'plainAddress',
                            ],
                            [
                                'attribute' => 'delivery_date',
                                'format'    => 'date',
                                'value'     => $order->shipment->delivery_date,
                                'visible'   => (bool)$order->shipment->delivery_date,
                            ],
                            [
                                'attribute' => 'delivery_time',
                                'format'    => 'text',
                                'value'     => $order->shipment->delivery_time,
                                'visible'   => (bool)$order->shipment->delivery_time,
                            ]
                        ]
                    ]);
                    ?>
                </div>
            </div>


            <?php if ($order->comment): ?>
            <div class="row">
                <div class="col-md-12">
                    <strong><?= $order->getAttributeLabel('comment') ?></strong>: <?= $f->asText($order->comment) ?>
                </div>
            </div>
            <?php endif; ?>


            <br />
            <br />

            <table class="order-table table table-striped">
                <colgroup>
                    <col width="100">
                    <col>
                    <col width="100">
                    <col width="120">
                    <col width="120">
                    <col width="140">
                    <col width="160">
                </colgroup>
                <thead>
                <tr>
                    <th><?= $orderItem->getAttributeLabel('code'); ?></th>
                    <th><?= $orderItem->getAttributeLabel('label'); ?></th>
                    <th><?= $orderItem->getAttributeLabel('quantity'); ?></th>
                    <th><?= $orderItem->getAttributeLabel('num_units'); ?></th>
                    <th><?= $orderItem->getAttributeLabel('price'); ?></th>
                    <th><?= $orderItem->getAttributeLabel('discount'); ?></th>
                    <th><?= $orderItem->getAttributeLabel('total'); ?></th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($order->orderItems as $model): ?>

                    <?php if ($model->product): ?>

                        <tr>
                            <td><?= f::text($model->code); ?></td>
                            <td class="line-label"><h3><?= f::text($model->label); ?></h3></td>
                            <td><?= f::integer($model->quantity); ?></td>
                            <td><?= f::units($model->num_units, $model->product->price_unit); ?></td>
                            <td><?= f::currency($model->price); ?></td>
                            <td><?= f::percent($model->discount); ?></td>
                            <td><?= f::currency($model->total); ?></td>
                        </tr>

                    <?php else: ?>

                        <tr>
                            <td><?= f::text($model->code); ?></td>
                            <td class="line-label"><h3><?= f::text($model->label); ?></h3></td>
                            <td><?= f::integer($model->quantity); ?></td>
                            <td></td>
                            <td><?= f::currency($model->price); ?></td>
                            <td><?= f::percent($model->discount); ?></td>
                            <td><?= f::currency($model->total); ?></td>
                        </tr>


                    <?php endif; ?>


                <?php endforeach; ?>
                </tbody>
                <tfoot>
                <tr>
                    <td colspan="4"></td>
                    <td colspan="2" style="text-align: right;"><strong><?= Yii::t('phycom/backend/order', 'Total amount'); ?></strong></td>
                    <td><h4 class="no-margin"><strong><?= f::currency($order->total); ?></strong></h4></td>
                </tr>
                </tfoot>
            </table>

            <br />
            <br />
            <br />

        </div>
    </div>
</div>

<?php

$this->registerJs('window.print();', \yii\web\View::POS_END);
