<?php

/**
 * @var $this yii\web\View
 * @var \Phycom\Backend\Models\PaymentForm $model
 */

use Phycom\Backend\Widgets\Box;
use Phycom\Backend\Widgets\ActiveForm;
use yii\helpers\Html;


$this->title = Yii::t('phycom/backend/main', 'Create new payment');
$this->params['breadcrumbs'][] = Yii::t('phycom/backend/main', 'Sale');
$this->params['breadcrumbs'][] = ['label' => Yii::t('phycom/backend/main', 'Payments'), 'url' => ['/payment']];
$this->params['breadcrumbs'][] = $this->title
?>

<div class="row">

    <div class="col-md-8">


		<?= Box::begin([
			'showHeader' => false,
			'options' => ['class' => 'box box-default'],
			'bodyOptions' => ['class' => 'box-body']
		]);
		?>

	    <?php $form = ActiveForm::begin(['id' => 'product-form']); ?>


        <div class="row">
            <div class="col-md-6">


                <div class="row">
                    <div class="col-md-6">
	                    <?= $form->field($model, 'amount')->textInput(); ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
			            <?= $form->field($model, 'method')->dropDownList($model->paymentMethods); ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
			            <?= $form->field($model, 'transactionDate')->datePicker(); ?>
                    </div>
                </div>
	            <?= $form->field($model, 'remitter')->textInput(); ?>
	            <?= $form->field($model, 'referenceNumber')->textInput(); ?>
	            <?= $form->field($model, 'explanation')->textarea(['rows' => 5]); ?>
	            <?= $form->field($model, 'transactionId')->textInput(); ?>

            </div>
            <div class="col-md-4">

            </div>

        </div>




        <div class="clearfix">
            <div class="pull-left"><?= $this->render('/partials/back-btn', ['size' => 'lg']); ?></div>
            <div class="pull-right">
                <?= Html::submitButton(Yii::t('phycom/backend/main', 'Create payment'), ['class' => 'btn btn-lg btn-success']); ?>
            </div>
        </div>

	    <?php ActiveForm::end(); ?>

	    <?= Box::end(); ?>
    </div>

</div>
