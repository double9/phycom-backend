<?php

use Phycom\Backend\Widgets\ActiveForm;
use Phycom\Base\Models\Attributes\DiscountRuleStatus;

use yii\helpers\Html;

$model = new \Phycom\Backend\Models\DiscountRuleBulkUpdateForm();
$modelName = (new \ReflectionClass($model))->getShortName();

$form = ActiveForm::begin();
?>
    <div class="keys" data-name="<?= $modelName . "[keys][]" ?>"></div>

    <div class="row">
        <div class="col-md-2">
            <?= $form->field($model, 'discountRate')
                ->numberInput(['placeholder' => $model->getAttributeLabel('discountRate')])
                ->inputAppend('%')
                ->label(false)
            ?>
        </div>
        <div class="col-md-2">
            <?= $form->field($model, 'birthdayDiscountRate')
                ->numberInput(['placeholder' => $model->getAttributeLabel('birthdayDiscountRate')])
                ->inputAppend('%')
                ->label(false)
            ?>
        </div>
        <div class="col-md-2">
            <?= $form->field($model, 'minPurchase')
                ->numberInput(['placeholder' => $model->getAttributeLabel('minPurchase')])
                ->label(false)
            ?>
        </div>
        <div class="col-md-2">
            <?= $form->field($model, 'status')
                ->dropDownList(DiscountRuleStatus::displayValues(), [
                    'prompt' => Yii::t('phycom/backend/main', 'Select {attribute}', [
                        'attribute' => $model->getAttributeLabel('status')
                    ])
                ])
                ->label(false)
            ?>
        </div>
        <div class="col-md-3">
            <?= Html::submitButton('<span class="fas fa-exclamation-circle"></span>&nbsp;&nbsp;' . Yii::t('phycom/backend/main', 'Bulk update selected rows'), ['class' => 'btn btn-md btn-primary']) ?>
        </div>
    </div>


<?php ActiveForm::end(); ?>
