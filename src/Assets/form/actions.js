'use strict';

var FormCollectionActions = Object.freeze({
    SAVE:           'save',
    SAVE_NEW:       'new',
    SAVE_DUPLICATE: 'duplicate',
    SAVE_CLOSE:     'close'
});
