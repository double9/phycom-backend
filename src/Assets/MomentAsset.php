<?php

namespace Phycom\Backend\Assets;

use yii\web\AssetBundle;

/**
 * moment js asset bundle.
 */
class MomentAsset extends AssetBundle
{
    public $sourcePath = '@vendor/npm-asset/moment/min';
    public $css = [];
    public $js = [
        'moment-with-locales.min.js',
    ];
    public $publishOptions = ['except' => ['*.less']];
    public $depends = [];
}
