multiFormGrid = (function ($) {
    return {
        init: function (el, options) {
                var $grid = $(el, '.ajax-grid'),
                    $form = $(el);

            $form.yiiActiveForm('data').attributes = [];
            for (let i=0; i<options.formAttributes.length; i++) {
                let attribute = options.formAttributes[i];
                if (attribute.id.includes(options.templateKey)) {
                    continue;
                }
                $form.yiiActiveForm('add', attribute);
            }

            $grid.on('click', '.add-row', function(e) {
                e.preventDefault();

                let $btn = $(this);
                let key = multiFormGrid.generateKey($grid);
                let $select = $(this).closest('.new-row-form').find('select');
                let value = $select.length ? $select.val() : null;


                if (!$select.length || !value || !value.length || value === 'custom') {

                    $grid.find('.multi-form-table > tbody')
                        .append('<tr data-key="'+ key +'">' + $grid.find('.multi-form-table > tbody > tr[data-key="__tpl__"]').html().replace(/__tpl__/g, key) + '</tr>');

                    $grid.find('tr[data-key="'+ key +'"] .form-group')
                        .removeClass('has-success')
                        .removeClass('has-error')
                        .removeClass('has-warning');

                    $grid.find('tr[data-key="'+ key +'"] input, tr[data-key="'+ key +'"] select').each(function(i, el) {

                        let id = $(el).attr('id');
                        if (!id) {
                            return;
                        }

                        let name = id.split('-').reverse()[0];
                        let attribute = $.extend(true, {}, options.formAttributesTemplate[name]);

                        attribute.id        = el.id;
                        attribute.container = ".field-" + el.id;
                        attribute.input     = "#" + el.id;
                        attribute.value     = $("#" + el.id).val();
                        attribute.status    = 0;
                        $form.yiiActiveForm('add', attribute);
                    });

                } else {

                    let selected = $select.find('option:selected');
                    let url = $select.data('url') + '?' + $.param({key: key, name: value});

                    let rowContent = $grid.find('.multi-form-table > tbody > tr[data-key="__tpl__"]').html().replace(/__tpl__/g, key);
                    $grid.find('.multi-form-table > tbody').append('<tr data-key="' + key + '_loading" class="loading">' + rowContent + '</tr>');

                    let $row = $grid.find('tr[data-key="' + key + '_loading"]');
                    let numCells = $('td', $row).length;
                    let height = $('td', $row).eq(1).innerHeight();

                    $row.html('<td colspan="' + numCells + '" style="height: ' + height + 'px;"><div class="spinner"><i class="fas fa-circle-notch fa-spin"></i></div></td>');

                    $.get(url, function (html) {

                        $grid.find('tr[data-key="' + key + '_loading"]').remove();
                        $grid.find('.multi-form-table > tbody').append(html);

                        $grid.find('tr[data-key="' + key + '"] .form-group')
                            .removeClass('has-success')
                            .removeClass('has-error')
                            .removeClass('has-warning');

                        $grid.find('tr[data-key="' + key + '"] input, tr[data-key="' + key + '"] select').each(function(i, el) {

                            let id = $(el).attr('id');
                            if (!id) {
                                return;
                            }

                            let name = id.split('-').reverse()[0];
                            let attribute = $.extend(true, {}, options.formAttributesTemplate[name]);

                                attribute.id = el.id;
                                attribute.container = ".field-" + el.id;
                                attribute.input = "#" + el.id;
                                attribute.value = $("#" + el.id).val();
                                attribute.status = 0;
                                $form.yiiActiveForm('add', attribute);
                        });

                        selected.remove();
                        value = $select.val();
                        if (!value || !value.length) {
                            $select.attr('disabled', true);
                            $btn.addClass('disabled');
                        }
                    });

                }
            });

            $grid.on('click', '.remove-row', function(e) {
                e.preventDefault();
                let $btn = $('.add-row', $grid);
                let $row = $(this).closest('tbody tr');

                $row.find('input, select').each(function(i, el) {
                    $form.yiiActiveForm('remove', el.id);
                });

                let $select = $grid.find('.new-row-form select');
                if ($select.length) {
                    let field = $row.data('fieldName');
                    let label = $row.data('fieldLabel');

                    if (field && label) {
                        if ($select.attr('disabled')) {
                            $select.removeAttr('disabled');
                            $btn.removeClass('disabled');
                        }
                        $select.append($("<option></option>")
                            .attr('data-label', label)
                            .attr('value', field)
                            .text(label));
                    }
                }
                $row.remove();
            });

            // let rowCount = $grid.find('.multi-form-table tbody tr').length;
            //
            // if (rowCount <= 1) {
            //     $('.add-row', $grid).click();
            // }
        },

        generateKey: function($el) {
            var id = 0;
            $('.multi-form-table > tbody > tr', $el).each(function(i, row) {
                var key = $(row).attr('data-key').replace('new', '');
                if (!isNaN(key)) {
                    id = parseInt(key);
                }
            });
            return 'new' + (++id);
        }
    };
})(jQuery);
