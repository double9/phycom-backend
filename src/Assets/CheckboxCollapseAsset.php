<?php

namespace Phycom\Backend\Assets;

use yii\bootstrap\BootstrapAsset;
use yii\web\AssetBundle;
use yii\web\JqueryAsset;

/**
 * Checkbox collapse asset bundle.
 */
class CheckboxCollapseAsset extends AssetBundle
{
    public $sourcePath = '@phycom/backend/src/Assets/checkbox-collapse';
    public $css = [
        'main.css',
    ];
    public $js = [
        'main.js',
    ];
    public $publishOptions = ['except' => ['*.less', '*.scss']];
    public $depends = [
        BootstrapAsset::class,
        JqueryAsset::class
    ];
}
