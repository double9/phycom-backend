<?php

namespace Phycom\Backend\Assets;

use yii\web\AssetBundle;

/**
 * Json editor calendar asset bundle.
 */
class CalendarJsonEditorAsset extends AssetBundle
{
	public $sourcePath = '@phycom/backend/src/Assets/calendar';
	public $css = [
	    'calendar.css',
        'calendar-json-editor.css'
	];
	public $js = [
		'calendar-json-editor.js',
	];
	public $publishOptions = ['except' => ['*.less', '*.scss']];
	public $depends = [
	    LodashAsset::class,
        MomentAsset::class,
        FlatpickrAsset::class,
        JsonEditorCustomAsset::class,
	];
}
