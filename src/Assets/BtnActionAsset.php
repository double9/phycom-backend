<?php

namespace Phycom\Backend\Assets;

use yii\web\AssetBundle;
use yii\web\JqueryAsset;

/**
 * Class BtnActionAsset
 * @package Phycom\Backend\Assets
 */
class BtnActionAsset extends AssetBundle
{
	public $sourcePath = '@phycom/backend/src/Assets/btn-action';
	public $js = [
		'main.js'
	];
	public $publishOptions = ['except' => ['*.less']];
	public $depends = [
        JqueryAsset::class
	];
}
