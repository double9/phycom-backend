<?php

namespace Phycom\Backend\Assets;

use Phycom\Backend\Assets\MomentAsset;

use yii\bootstrap\BootstrapAsset;
use yii\web\AssetBundle;
use yii\web\JqueryAsset;
use yii;

/**
 * FullCalendar asset bundle.
 */
class FullCalendarAsset extends AssetBundle
{
    public $sourcePath = '@vendor/npm-asset/fullcalendar';
    public $css = [
        'main.min.css',
//        'fullcalendar.print.min.css',
    ];
    public $js = [
        'main.min.js',
//        'locales-all.js',
//        'gcal.js'
    ];
    public $publishOptions = ['except' => ['*.less']];
    public $depends = [
        MomentAsset::class,
        JqueryAsset::class,
        BootstrapAsset::class
    ];
}
