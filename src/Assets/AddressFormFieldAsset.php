<?php

namespace Phycom\Backend\Assets;

use yii\web\AssetBundle;
use yii\web\JqueryAsset;

/**
 * Class AddressFormFieldAsset
 * @package Phycom\Backend\Assets
 */
class AddressFormFieldAsset extends AssetBundle
{
    public $sourcePath = '@phycom/backend/src/Assets/address-form-field';
    public $js = [
        'main.js'
    ];
    public $publishOptions = ['except' => ['*.less', '*.scss']];
    public $depends = [
        JqueryAsset::class
    ];
}
