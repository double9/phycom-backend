<?php

namespace Phycom\Backend\Assets;

use yii\web\AssetBundle;

/**
 * Grid widget asset bundle.
 */
class GridAsset extends AssetBundle
{
	public $sourcePath = '@phycom/backend/src/Assets/grid';
	public $css = [
		'main.css'
	];
	public $js = [
		'main.js'
	];
	public $publishOptions = ['except' => ['*.less']];
	public $depends = [
		'yii\grid\GridViewAsset'
	];
}
