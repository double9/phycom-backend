<?php

namespace Phycom\Backend\Assets;

use yii\web\AssetBundle;
use yii;

/**
 * Calendar asset bundle.
 */
class CalendarAsset extends AssetBundle
{
    public $sourcePath = '@phycom/backend/src/Assets/calendar';
    public $css = [
        'calendar.css'
    ];
    public $js = [
        'multicalendar.js'
    ];
    public $publishOptions = ['except' => ['*.less']];
    public $depends = [
        FullCalendarAsset::class
    ];
}
