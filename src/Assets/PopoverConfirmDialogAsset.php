<?php

namespace Phycom\Backend\Assets;


use yii\bootstrap\BootstrapAsset;
use yii\web\AssetBundle;

/**
 * Class PopoverConfirmDialogAsset
 *
 * @package Phycom\Backend\Assets
 */
class PopoverConfirmDialogAsset extends AssetBundle
{
    public $sourcePath = '@phycom/backend/src/Assets/form';
    public $js = [

    ];
    public $css = [
        'popover-confirm-dialog.css'
    ];
    public $publishOptions = ['except' => ['*.less']];
    public $depends = [
        BootstrapAsset::class
    ];
}
