jQuery( function($) {
    "use strict";

    let getHashParam = function (key) {
        let url = document.location.toString();
        if ( url.match('#') ) {
            let hash = url.split('#')[1];
            let params = new URLSearchParams(hash);
            return params.get(key);
        }
        return null;
    };

    let updateHash = function (key, value) {
        let url = document.location.toString();
        if ( url.match('#') ) {
            let hash = url.split('#')[1];
            let params = new URLSearchParams(hash);
            if (value === null) {
                params.delete(key);
            } else {
                params.set(key, value);
            }
            document.location.hash = params.toString();
            return;
        }
        if (value === null) {
            history.replaceState('', document.title, window.location.pathname + window.location.search);
            return;
        }
        document.location.hash = key + '=' + value;
    };

    $('#product-form-container').on('hide.bs.collapse', '.collapse', function (e) {
        $(this).closest('.attribute-grid-view').find('.caret').addClass('caret-right');
        updateHash('show', null);
    });
    $('#product-form-container').on('show.bs.collapse', '.collapse', function (e) {
        $(this).closest('.attribute-grid-view').find('.caret').removeClass('caret-right');
        updateHash('show', 'attribute-grid');
    });

    let showTarget = getHashParam('show');
    if (showTarget) {
        let $target = $('#' + showTarget);
        if ($target.length && $target.hasClass('collapse')) {
            $target.collapse('show');
        }
    }


    $('#product-form-container').on('click', '.save-btn .save', function (e) {
        e.preventDefault();
        // validate but not submit

        $('#product-form').data('yiiActiveForm').submitting = true;
        $('#product-form').yiiActiveForm('validate');
        $('#product-form').on('submit', function(e){
            e.preventDefault();
            return false;
        });

        $('#product-translation-form').data('yiiActiveForm').submitting = true;
        $('#product-translation-form').yiiActiveForm('validate');
        $('#product-translation-form').on('submit', function(e){
            e.preventDefault();
            return false;
        });

        // $('#product-price-grid-form').data('yiiActiveForm').submitting = true;
        // $('#product-price-grid-form').yiiActiveForm('validate');
        // $('#product-price-grid-form').on('submit', function(e){
        //    e.preventDefault();
        //    return false;
        // });

        $('#product-translation-form, #product-price-grid-form, #attribute-grid-form')
            .find('input[name="_csrf-backend"]').attr('disabled','disabled');

        let data = $('#product-form, #product-translation-form, #product-param-grid-form, #product-price-grid-form, #attribute-grid-form, #product-pricing-options').serialize();

        let showAlert = function (msg) {
            let alert = '<div class="alert alert-error alert-dismissible" role="alert">' +
                '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' +
                '<strong>Error:</strong> ' + msg + '</div>';
            $('#system-messages').append(alert);
        };

        // submit all forms by ajax
        $.ajax({
            url: $('#product-form').attr('action'),
            type: 'POST',
            data: data,
            success: function (data) {

                if (data.error) {
                    showAlert(data.error);
                    return;
                }

                // handle success
                let el = $('#productattachmentform-file'),
                    fileInput = el.data("fileinput"),
                    totalFiles = Object.keys(el.fileinput('getFileStack')).length,
                    numFilesUploaded = 0;

                if (totalFiles === 0) {
                    window.location.href = data.url;
                    return location.reload();
                }

                el.on('fileuploaded', function (e, uploadedData, previewId, index) {
                    numFilesUploaded++;
                    if (numFilesUploaded >= totalFiles) {
                        console.log('url ' + data.url);
                        window.location.href = data.url;
                        return location.reload();
                    }
                });

                fileInput.uploadUrl = data.uploadUrl;
                el.fileinput('upload');
            },
            error: function(jqXHR, errMsg) {
                console.log(jqXHR);
                showAlert(jqXHR.status + ' ' + jqXHR.statusText + '<br />' + '<span><pre>' + JSON.stringify(jqXHR.responseJSON, null, 2) + '</pre></span>');
            }
        });
    });
});
