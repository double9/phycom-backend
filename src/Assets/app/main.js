"use strict";
(function($) {

    /**
     * replace dropdown to dropup in case close to bottom of a screen
     */
    $('.dropdown, .dropup').on('shown.bs.dropdown', function() {
        let c = $(this).find('.dropdown-menu').offset();
        if (($(window).height()-c.top) < 30) {
            $(this).addClass('dropup');
        }
    });

    /**
     * close success alert messages automatically
     */
    window.setTimeout(function () {
        $('#system-messages .alert-success').each(function(i, el) {
            let $el = $(el);
            $el.fadeOut(2000, function () {
                $el.alert('close');
            });
        });
    }, 10000);

    let updateUserSettings = async function (data) {
        const csrfToken = document.querySelector('meta[name="csrf-token"]').content;
        const response = await fetch(baseUrl + '/user/update-settings', {
            method: 'POST',
            headers: {
                'X-Requested-With': 'XMLHttpRequest',
                'X-CSRF-Token': csrfToken,
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(data)
        });
        return response.json();
    };

    $(document).on('expanded.pushMenu', function (e) {
        updateUserSettings({sidebarCollapse: false});
    });


    $(document).on('collapsed.pushMenu', function (e) {
        updateUserSettings({sidebarCollapse: true});
    });


})(jQuery);

