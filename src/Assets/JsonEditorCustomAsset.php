<?php

namespace Phycom\Backend\Assets;

use yii\web\AssetBundle;

/**
 * Json editor styling and translation overloads asset bundle.
 */
class JsonEditorCustomAsset extends AssetBundle
{
	public $sourcePath = '@phycom/backend/src/Assets/json-editor';
	public $css = [
	    'style.css'
	];
	public $js = [
        'et.js'
	];
	public $publishOptions = ['except' => ['*.less']];
	public $depends = [
        JsonEditorAsset::class,
        SpectreIconsAsset::class
	];
}
