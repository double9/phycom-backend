<?php

namespace Phycom\Backend\Assets;

use dmstr\web\AdminLteAsset;
use Phycom\Base\Assets\Md\AssetBundle as MdAsset;

use yii\web\YiiAsset;
use yii\bootstrap\BootstrapAsset;
use yii\web\AssetBundle;

/**
 * Main backend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $sourcePath = '@phycom/backend/src/Assets/app';
    public $css = [
        'admin.css',
	    'adminlte-skin-custom.css',
        'lazy-load-images.css'
    ];
    public $js = [
        'lazy-load-images.js',
    	'main.js',

	    //'product.js',
    ];
	public $publishOptions = ['except' => ['*.less']];
    public $depends = [
        YiiAsset::class,
        BootstrapAsset::class,
        AdminLteOptionsAsset::class,
	    AdminLteAsset::class,
        AdminLtePluginsAsset::class,
        MdAsset::class
    ];
}
