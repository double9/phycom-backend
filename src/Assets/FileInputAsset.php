<?php

namespace Phycom\Backend\Assets;

use yii\web\AssetBundle;
use yii;

/**
 * AttributeGrid widget asset bundle.
 */
class FileInputAsset extends AssetBundle
{
    public $sourcePath = '@phycom/backend/src/Assets/file-input';
    public $css = [
//        'main.css'
    ];
    public $js = [
        'main.js'
    ];
    public $publishOptions = ['except' => ['*.less']];
    public $depends = [
        'yii\web\JqueryAsset',
        'yii\bootstrap\BootstrapAsset'
    ];
}
