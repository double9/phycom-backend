jQuery(function($) {
    'use strict';

    $('#select-delivery-method').on('change', function () {

        var value = $('option:selected', this).val();

        if (value === 'self-pickup') {
            $('#street-details').slideDown(100);
            $('#street-details input').each(function (i, el) {
                $(el).val('');
            });
        } else {
            $('#street-details').slideUp(200);
        }

    });

    var counter = 0;
    $('#deliveryareaform-country').on('change', function () {

        var provinceListUrl = $(this).data('url');
        var countryCode = $('option:selected', this).val();
        $('#deliveryareaform-province').attr('disabled','disabled');
        counter++;
        $.post(provinceListUrl, {country: countryCode}, function (data) {
            var options = '<option value="">-</option>',
                $area = $('#deliveryareaform-province');

            for (var key in data) {
                options += '<option value="'+key+'">' + data[key] + '</option>';
            }

            $area.html(options);
            $area[0].selectedIndex = 0;
            if (--counter <= 0) {
                $area.removeAttr('disabled');
                counter = 0;
            }
        });

    });

});