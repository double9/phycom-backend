<?php

namespace Phycom\Backend\Assets;

use dmstr\web\AdminLteAsset;
use yii\web\AssetBundle;

/**
 * Class AdminLtePluginsAsset
 * @package Phycom\Backend\Assets
 */
class AdminLtePluginsAsset extends AssetBundle
{
	public $sourcePath = '@vendor/almasaeed2010/adminlte/plugins';
	public $css = [
		'pace/pace.css'
	];
	public $js = [
		'pace/pace.js'
	];
	public $depends = [
        AdminLteAsset::class,
	];
}
