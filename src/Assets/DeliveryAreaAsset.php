<?php

namespace Phycom\Backend\Assets;

use yii\web\AssetBundle;
use yii\web\JqueryAsset;
use yii\bootstrap\BootstrapAsset;

/**
 * DeliveryArea form related assets
 */
class DeliveryAreaAsset extends AssetBundle
{
    public $sourcePath = '@phycom/backend/src/Assets/delivery-area';
    public $js = [
        'main.js'
    ];
    public $publishOptions = ['except' => ['*.less']];
    public $depends = [
        JqueryAsset::class,
        BootstrapAsset::class
    ];
}
