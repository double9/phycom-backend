<?php

namespace Phycom\Backend\Assets;

use yii\web\AssetBundle;

/**
 * Lodash js asset bundle.
 */
class LodashAsset extends AssetBundle
{
    public $sourcePath = null;
    public $js = [
        'https://cdn.jsdelivr.net/npm/lodash@4.17.20/lodash.min.js'
    ];
    public $publishOptions = ['except' => ['*.less']];
    public $depends = [];
}
