<?php

namespace Phycom\Backend\Assets;

use yii\web\AssetBundle;
use yii\web\JqueryAsset;

/**
 * Class AdminLteOptionsAsset
 * @package Phycom\Backend\Assets
 */
class AdminLteOptionsAsset extends AssetBundle
{
	public $sourcePath = '@phycom/backend/src/Assets/app';
	public $css = [];
	public $js = [
		'adminlte.options.js'
	];
	public $depends = [
        JqueryAsset::class
	];
}
