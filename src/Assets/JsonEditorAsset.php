<?php

namespace Phycom\Backend\Assets;

use yii\web\AssetBundle;

/**
 * Class JsonEditorAsset
 *
 * @package Phycom\Backend\Assets
 */
class JsonEditorAsset extends AssetBundle
{
    public $sourcePath = null;
    public $js = [
        'https://cdn.jsdelivr.net/npm/@json-editor/json-editor@latest/dist/jsoneditor.min.js',
    ];
}
