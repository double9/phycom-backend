<?php

namespace Phycom\Backend\Assets;

use yii\web\AssetBundle;

/**
 * Class FormActionsAsset
 * @package Phycom\Backend\Assets
 */
class FormActionsAsset extends AssetBundle
{
	public $sourcePath = '@phycom/backend/src/Assets/form';
	public $js = [
		'actions.js'
	];
}
