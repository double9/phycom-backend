"use strict";

class ChartWidget extends StatisticsReport {

    constructor(conf) {
        super(conf);
        this.context = document.getElementById(conf.id);
        this.locale = conf.locale || 'en-US';
        this.chartType = 'line';
    }

    init(data) {
        this.createChart(this.parseData(data));
    }

    parseData(data) {
        return {
            labels: [],
            datasets: []
        };
    }

    getChartOptions() {
        let locale = this.locale;

        return {
            tooltips: {
                callbacks: {
                    title: function (tooltipItems) {
                        return tooltipItems.map(tooltipItem => new Date(tooltipItem.label).toLocaleDateString(locale, {day: 'numeric', month:'short', year: 'numeric'}));
                    }
                }
            },
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true
                    }
                }],
                xAxes: [{
                    type: 'time',
                    ticks: {
                        autoSkip: true,
                        maxTicksLimit: 10,
                        callback: function(time, index, data) {
                            return new Date(data[index].value).toLocaleDateString(locale, {day: 'numeric', month:'short', hour12: false});
                        },
                    }
                }]
            }
        };
    }

    createChart(chartData) {
        return new Chart(this.context, {
            type: this.chartType,
            data: chartData,
            options: this.getChartOptions()
        });
    }
}
