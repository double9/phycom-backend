"use strict";

class DailyOrdersChart extends ChartWidget {

    constructor(props) {
        super(props);
        this.countLabel = props.countLabel || 'Num. orders';
        this.abandonedLabel = props.abandonedLabel || 'Abandoned orders';
    }

    parseData(data) {
        let labels = Array.from(data, x => x.date);
        let count = Array.from(data, x => x.count);
        let abandoned = Array.from(data, x => x.abandoned);

        return {
            labels: labels,
            datasets: [
                {
                    label: this.countLabel,
                    data: count,
                    backgroundColor: 'rgba(54, 162, 235, 0.2)',
                    borderColor: 'rgba(54, 162, 235, 1)',
                    lineTension: 0.2,
                    fill: true,
                    borderWidth: 1
                },
                {
                    label: this.abandonedLabel,
                    data: abandoned,
                    backgroundColor: 'rgba(153, 102, 255, 0.2)',
                    borderColor: 'rgba(153, 102, 255, 1)',
                    lineTension: 0.2,
                    fill: true,
                    borderWidth: 1
                },
            ]
        };
    }

}