<?php

namespace Phycom\Backend\Assets;


use yii\web\AssetBundle;


/**
 * Toggle switch asset bundle.
 */
class ToggleSwitchAsset extends AssetBundle
{
    public $sourcePath = '@phycom/backend/src/Assets/toggle-switch';
    public $css = [
        'main.css'
    ];
    public $js = [];
    public $publishOptions = ['except' => ['*.less', '*.scss']];
    public $depends = [];
}
