<?php

namespace Phycom\Backend\Assets;

use yii\web\AssetBundle;
use yii\web\JqueryAsset;

/**
 * Class FormSubmitBtnAsset
 * @package Phycom\Backend\Assets
 */
class FormSubmitBtnAsset extends AssetBundle
{
	public $sourcePath = '@phycom/backend/src/Assets/form';
	public $js = [
		'btn.js'
	];
	public $publishOptions = ['except' => ['*.less']];
	public $depends = [
	    FormActionsAsset::class,
        JqueryAsset::class
	];
}
