var CalendarJsonEditor = (function ($) {

    let instance = null;

    const CalendarJsonEditorWidget = function (id, config) {
        this.id = id;
        this.opt = config;
        this.input = document.getElementById(this.id);
        this.calendarHolder = document.getElementById(this.id + '-calendar-holder');
        this.calendarInput = this.calendarHolder.querySelector('input');
        this.editorHolder = document.getElementById(this.id + '-editor-holder');
        this.selectedDate = null;
        this.selectedDateStr = null;
        this.init();
    }

    CalendarJsonEditorWidget.prototype.init = function() {
        this.loadInputData();
        this.createCalendar();
    }

    CalendarJsonEditorWidget.prototype.loadInputData = function() {
        if (this.input && this.input.value && this.input.value !== '[]') {
            return this.data = JSON.parse(this.input.value);
        }
        return this.data = {};
    }


    CalendarJsonEditorWidget.prototype.createEditor = function(dateStr, config = {}) {
        config = _.extend(this.opt.jsonEditor, config);

        if (this.editor) {
            this.editor.destroy();
        }

        this.editorHolder.innerHTML = '';
        this.editor = new JSONEditor(this.editorHolder, config);

        this.editor.on('change', () => {

            const v = this.editor.getValue();
            const closed = v.closed;
            const times = v.times || [];

            let i = times.length;
            if (i) {
                while (i--) {
                    if (
                        _.isEmpty(times[i])
                        || typeof times[i].from === 'undefined'
                        || typeof times[i].to === 'undefined'
                    ) {
                        times.splice(i, 1);
                        continue;
                    }
                }
            }

            let updateInput = false;
            let updateCalendar = false;

            if (false === closed) {

                if (!times.length && typeof this.data[dateStr] !== 'undefined') {
                    delete this.data[dateStr];
                    updateInput = true;
                    updateCalendar = true;
                } else if (times.length) {
                    if (typeof this.data[dateStr] === 'undefined') {
                        updateCalendar = true;
                    }
                    this.data[dateStr] = {closed: false, times: times};
                    updateInput = true;
                }

            } else {
                if (typeof this.data[dateStr] === 'undefined') {
                    updateCalendar = true;
                }
                this.data[dateStr] = {closed: true, times: []};
                updateInput = true;
            }


            if (updateInput) {
                this.input.value = JSON.stringify(this.data);
            }
            if (updateCalendar) {
                this.calendar.redraw();
            }
        });
    }

    CalendarJsonEditorWidget.prototype.createCalendar = function() {

        if (this.calendar) {
            this.calendar.destroy();
        }

        let config = _.extend(this.opt.calendar, {
            onChange: (selectedDates, dateStr, instance) => {

                this.selectedDate = selectedDates[0];
                this.selecteDateStr = dateStr;

                this.createEditor(dateStr, {startval: this.data[dateStr] || {}});
            },
            onDayCreate: (dObj, dStr, fp, dayElem) => {
                let dateStr = moment(dayElem.dateObj).format('YYYY-MM-DD');
                if (typeof this.data[dateStr] !== 'undefined') {
                    let className = 'configured-date';
                    if (dayElem.className.indexOf(className) === -1) {
                        dayElem.className += ' ' + className;
                    }
                }
            }
        });

        this.calendar = flatpickr(this.calendarInput, config);

        let input = this.calendarInput.closest('.flatpickr-wrapper').querySelector('input[type="text"]');
        if (input) {
            input.style.display = 'none';
        }
    }




    CalendarJsonEditorWidget.prototype.importDates = function () {
        // var dates = JSON.parse(this.$field.val());
        var events = [];
        //
        // for (var i=0; i<dates.length; i++) {
        //     events.push({
        //         id: '_' + Math.random().toString(36).substr(2, 9),
        //         title: '',
        //         start: moment(dates[i]),
        //         end: moment(dates[i]).add(1, 'day'),
        //         rendering: 'background'
        //     });
        // }
        return events;
    };


    return {
        init: function (id, config) {
            instance = new CalendarJsonEditorWidget(id, config);
        },
    }

})(jQuery);
