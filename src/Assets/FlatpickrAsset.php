<?php

namespace Phycom\Backend\Assets;


use yii\web\AssetBundle;
use Yii;

/**
 * Flatpickr asset bundle.
 */
class FlatpickrAsset extends AssetBundle
{
    public $sourcePath = '@vendor/npm-asset/flatpickr/dist';
    public $css = [
        'flatpickr.min.css',
        'themes/material_blue.css'
    ];
    public $js = [
        'flatpickr.min.js',
    ];

    public $publishOptions = ['except' => ['*.less', '*.scss']];
    public $depends = [];

    public function init()
    {
        parent::init();

        foreach (Yii::$app->lang->enabled as $language) {
            $relPath = 'l10n/' . $language->code . '.js';
            $fullPath = Yii::getAlias($this->sourcePath . '/' . $relPath);
            if (file_exists($fullPath)) {
                $this->js[] = $relPath;
            }
        }
    }
}
