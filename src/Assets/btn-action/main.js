var BtnAction = (function ($) {
    'use strict';
    var BtnActionForm = function (el) {
        this.$form = $(el);
        if (!this.$form.length) {
            throw 'Element ' + el + ' not found';
        }
        this.form = this.$form[0];
        this.init();
    };

    BtnActionForm.prototype.init = function () {
        this.$form.on('click', 'button[type="submit"]', function (e) {
            $(this).button('loading');
        });
    };


    return {
        init: function (el) {
            return new BtnActionForm(el);
        }
    };

})(jQuery);