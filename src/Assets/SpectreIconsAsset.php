<?php

namespace Phycom\Backend\Assets;

use yii\web\AssetBundle;

/**
 * Class SpectreAsset
 *
 * @package Phycom\Backend\Assets
 */
class SpectreIconsAsset extends AssetBundle
{
    public $sourcePath = null;
    public $css = [
//        'https://unpkg.com/spectre.css/dist/spectre.min.css',
//        'https://unpkg.com/spectre.css/dist/spectre-exp.min.css',
        'https://unpkg.com/spectre.css/dist/spectre-icons.min.css'
    ];
}
