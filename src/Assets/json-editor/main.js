const JsonEditor = (function () {

    const JsonEditorWidget = function (id, config) {
        this.id = id;
        this.config = config;
        this.holder = document.getElementById(this.id + '-editor-holder');
        this.editor = new JSONEditor(this.holder, this.config);
        this.init();
    };

    JsonEditorWidget.prototype.init = function() {
        let inputField = document.getElementById(this.id);
        this.editor.on('change', () => {
            const json = JSON.stringify(this.editor.getValue());
            inputField.value = json;
        });
    };


    return {
        init: function (id, config) {
            return new JsonEditorWidget(id, config);
        }
    };

})();
