<?php

namespace Phycom\Backend\Assets;

use yii\web\AssetBundle;

/**
 * Json editor widget asset bundle.
 */
class JsonEditorWidgetAsset extends AssetBundle
{
	public $sourcePath = '@phycom/backend/src/Assets/json-editor';
	public $js = [
		'main.js',
	];
	public $publishOptions = ['except' => ['*.less']];
	public $depends = [
        JsonEditorCustomAsset::class
	];
}
