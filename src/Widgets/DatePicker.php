<?php

namespace Phycom\Backend\Widgets;


use Phycom\Backend\Assets\FlatpickrAsset;

use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\widgets\InputWidget;
use Yii;


/**
 * Class DatePicker
 *
 * @package Phycom\Backend\Widgets
 */
class DatePicker extends InputWidget
{
    const MODE_SINGLE = 'single';
    const MODE_MULTIPLE = 'multiple';
    const MODE_RANGE = 'range';

    /**
     * @var string|null
     */
    public ?string $language = null;

    /**
     * @var string
     */
    public string $mode = self::MODE_SINGLE;

    /**
     * @var bool
     */
    public bool $showInputField = true;

    /**
     * @var array
     */
    public array $widgetOptions = [
        'allowInput' => false,
        'inline'     => true,
        'altInput'   => true
    ];


    public function init()
    {
        parent::init();

        if (!$this->language) {
            $this->language = substr(Yii::$app->language, 0, 2);
        }

        if (!isset($this->widgetOptions['locale'])) {
            $this->widgetOptions['locale'] = $this->language;
        }

        if (!isset($this->widgetOptions['minDate'])) {
            $this->widgetOptions['minDate'] = (new \DateTime('today'))->format('Y-m-d');
        }

        if (isset($this->widgetOptions['altInput']) && !isset($this->widgetOptions['altFormat'])) {
            $this->widgetOptions['altFormat'] = 'd.m.Y';
        }
    }

    public function run()
    {
        $id = $this->options['id'];
        echo $this->renderInputHtml('hidden');

        $options = ArrayHelper::merge($this->widgetOptions, [
            'mode' => $this->mode
        ]);
        $jsConfig = Json::encode($options);
        $view = $this->getView();

        FlatpickrAsset::register($view);
        $view->registerJs(<<<JS
            (function() {
                const datePicker = flatpickr('#$id', $jsConfig);
            })();
JS
);
        if (!$this->showInputField) {
            $view->registerJs(<<<JS
                let input = document.getElementById('$id').parentNode.querySelector('input[type="text"]');
                if (input) {
                    input.style.display = 'none';
                }
JS
            );
        }
    }
}
