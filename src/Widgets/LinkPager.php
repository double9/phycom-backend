<?php

namespace Phycom\Backend\Widgets;

use yii;

/**
 * Class LinkPager
 * @package Phycom\Backend\Widgets
 */
class LinkPager extends \yii\widgets\LinkPager
{
    public $linkOptions = ['class' => 'paginate-link'];

	public static function small($pagination = false)
	{
		return static::widget([
			'pagination' => $pagination,
			'nextPageLabel' => Yii::t('phycom/backend/main', 'Next'),
			'prevPageLabel' => Yii::t('phycom/backend/main', 'Previous'),
			'options' => [
				'class' => 'no-margin pagination pagination-sm pull-right'
			]
		]);
	}

    public static function medium($pagination = false)
    {
        return static::widget([
            'pagination' => $pagination,
            'nextPageLabel' => Yii::t('phycom/backend/main', 'Next'),
            'prevPageLabel' => Yii::t('phycom/backend/main', 'Previous'),
            'options' => [
                'class' => 'no-margin pagination pagination-md pull-right'
            ]
        ]);
    }
}
