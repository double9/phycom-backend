<?php

namespace Phycom\Backend\Widgets;


use Phycom\Backend\Assets\PopoverConfirmDialogAsset;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\web\View;
use yii\base\Widget;
use yii\helpers\Html;

/**
 * Class PopoverConfirmDialog
 *
 * @package Phycom\Backend\Widgets
 */
class PopoverConfirmDialog extends Widget
{
    /**
     * @var string
     */
    public string $buttonLabel;

    /**
     * @var array|string[]
     */
    public array $buttonOptions = [
        'class'          => 'btn btn-sm btn-primary',
        'data-container' => 'body',
        'data-placement' => 'bottom'
    ];

    /**
     * @var string
     */
    public string $content = 'Are you sure?';

    /**
     * @var bool
     */
    public bool $sanitizeContent = false;

    /**
     * @var string|array
     */
    public $route;

    /**
     * @var string
     */
    public string $method = 'post';

    /**
     * @var array
     */
    public array $formOptions = [];

    /**
     * @var string
     */
    public string $successButtonLabel = 'Yes';

    /**
     * @var array|string[]
     */
    public array $successButtonOptions = ['class' => 'btn btn-success', 'style' => 'width: 100%;'];

    /**
     * @var string
     */
    public string $dismissButtonLabel = 'No';

    /**
     * @var array|string[]
     */
    public array $dismissButtonOptions = ['class' => 'btn btn-primary', 'style' => 'width: 100%;'];

    /**
     * @var string
     */
    public string $popoverPlacement = 'bottom';

    /**
     * @return string
     */
    public function run()
    {
        $id = $this->getId();

        $options = ArrayHelper::merge([
            'id'             => $id,
            'data-toggle'    => 'popover',
            'data-html'      => '1',
        ], $this->buttonOptions);

        $html = Html::button($this->buttonLabel, $options);

        $this->registerAssets($this->renderDialogContent());

        return $html;
    }

    /**
     * @return string
     */
    protected function renderDialogContent(): string
    {
        return Html::beginForm(Url::toRoute($this->route), $this->method, $this->formOptions) .
                '<div class="dialog-description">' . $this->content . '</div>' .
                '<div class="row">' .
                    '<div class="col-md-6">' . $this->renderSuccessButton() . '</div>' .
                    '<div class="col-md-6">' . $this->renderDismissButton() . '</div>' .
                '</div>' .
            Html::endForm();
    }

    /**
     * @return string
     */
    protected function renderSuccessButton(): string
    {
        $options = ArrayHelper::merge($this->successButtonOptions, [
            'type' => 'submit'
        ]);

        return Html::button($this->successButtonLabel, $options);
    }

    /**
     * @return string
     */
    protected function renderDismissButton(): string
    {
        $options = ArrayHelper::merge($this->dismissButtonOptions, [
            'data-dismiss' => 'popover',
            'onclick'      => '$(this).closest(\'div.popover\').popover(\'hide\');'
        ]);
        Html::addCssClass($options, 'close-dialog');

        return Html::a($this->dismissButtonLabel, null, $options);
    }

    protected function registerAssets(string $popoverContent)
    {
        $id = $this->getId();
        $view = $this->getView();

        PopoverConfirmDialogAsset::register($view);

        $config = [
            'content' => $popoverContent,
            'sanitize' => $this->sanitizeContent
        ];
        $jsConfig = Json::encode($config);
        $view->registerJs('$(function () { $("#' . $id . '").popover(' . $jsConfig . ');})', View::POS_END);

        // fixes a popover bug where manual close requires two clicks to reopen a popover
        $view->registerJs('$(function () { 
            $("body").on("hidden.bs.popover", function (e) {
                $(e.target).data("bs.popover").inState.click = false;
            });
        })', View::POS_END);
    }
}
