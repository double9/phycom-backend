<?php

namespace Phycom\Backend\Widgets;

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii;

/**
 * Class Menu
 * @package Phycom\Backend\Widgets
 */
class Menu extends \dmstr\widgets\Menu
{
    /**
     * @var string
     */
    public $linkTemplate = '<a href="{url}" class="menu-link">{icon} {label}</a>';

    /**
     * @var string
     */
    public $labelTemplate = '<span class="menu-label">{label}</span>';

    /**
     * @var string[]
     */
    public $options = ['data-widget' => 'tree'];

    public static $iconClassPrefix = 'fas fa-';
    /**
     * @inheritdoc
     */
    protected function renderItem($item)
    {
        $iconClassPrefix = self::$iconClassPrefix;
        if (!empty($item['icon'])) {
            if (substr($item['icon'], 0, 4) === 'mdi-') {
                self::$iconClassPrefix = 'mdi mdi-36px ';
            }
            if (in_array(substr($item['icon'], 0, 3), ['far', 'fab', 'fas', 'fal'])) {
                self::$iconClassPrefix = '';
            }
        }

        if (isset($item['items'])) {
            $labelTemplate = '<a href="{url}" class="menu-link">{icon} {label} <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span></a>';
            $linkTemplate = '<a href="{url}" class="menu-link">{icon} {label} <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span></a>';
        } else {
            $labelTemplate = $this->labelTemplate;
            $linkTemplate = $this->linkTemplate;
        }

        $replacements = [
            '{label}' => strtr($this->labelTemplate, ['{label}' => $item['label'],]),
            '{icon}' => empty($item['icon']) ? $this->defaultIconHtml
                : '<i class="' . static::$iconClassPrefix . $item['icon'] . '"></i> ',
            '{url}' => isset($item['url']) ? Url::to($item['url']) : 'javascript:void(0);',
        ];

        $template = ArrayHelper::getValue($item, 'template', isset($item['url']) ? $linkTemplate : $labelTemplate);

        $result = strtr($template, $replacements);

//        $result = parent::renderItem($item);
        self::$iconClassPrefix = $iconClassPrefix;
        return $result;
    }


	/**
	 * @inheritdoc
	 */
	protected function isItemActive($item)
	{
		$isActive = parent::isItemActive($item);

		if (!$isActive && isset($item['exclude_child']) && isset($item['url']) && isset($item['url'][0])) {
			$route = $this->parseRoute($item['url']);
			foreach ($item['exclude_child'] as $excluded) {
				$excludedRoute = $this->parseRoute($excluded);
				if ($this->route === $excludedRoute && 0 === strpos($excludedRoute, $route)) {
					return $isActive;
				}
			}
		}
		if (!$isActive && isset($item['url']) && is_array($item['url']) && isset($item['url'][0])) {
			$route = $this->parseRoute($item['url']);
			return 0 === strpos($this->route, $route);
		}
		return $isActive;
	}

    /**
     * @inheritdoc
     */
    protected function normalizeItems($items, &$active)
    {
        foreach ($items as $i => $item) {
            if (isset($item['visible']) && !$item['visible']) {
                unset($items[$i]);
                continue;
            }
            if (!isset($item['label'])) {
                $item['label'] = '';
            }
            $encodeLabel = isset($item['encode']) ? $item['encode'] : $this->encodeLabels;
            $items[$i]['label'] = $encodeLabel ? Html::encode($item['label']) : $item['label'];
            $items[$i]['icon'] = isset($item['icon']) ? $item['icon'] : '';
            $hasActiveChild = false;
            if (isset($item['items'])) {
                $items[$i]['items'] = $this->normalizeItems($item['items'], $hasActiveChild);
                if (empty($items[$i]['items']) && $this->hideEmptyItems) {
                    unset($items[$i]['items']);
                    if (!isset($item['url'])) {
                        unset($items[$i]);
                        continue;
                    }
                }
            }
            if (!isset($item['active'])) {
                if ($this->activateParents && $hasActiveChild || $this->activateItems && $this->isItemActive($item)) {
                    $active = $items[$i]['active'] = true;
                } else {
                    $items[$i]['active'] = false;
                }
            } elseif ($item['active'] instanceof \Closure) {
                $active = $items[$i]['active'] = call_user_func($item['active'], $item, $hasActiveChild, $this->isItemActive($item), $this);
            } elseif ($item['active']) {
                $active = true;
            }
        }
        return array_values($items);
    }


	protected function parseRoute($url)
	{
		$route = is_array($url) ? $url[0] : $url;
		if ($route[0] !== '/' && Yii::$app->controller) {
			$route = ltrim(Yii::$app->controller->module->getUniqueId() . '/' . $route, '/');
		}
		return ltrim($route,'/');
	}

}
