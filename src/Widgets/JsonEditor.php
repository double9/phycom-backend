<?php

namespace Phycom\Backend\Widgets;


use Phycom\Backend\Assets\JsonEditorWidgetAsset;

use yii\helpers\Html;
use yii\helpers\Json;
use yii\widgets\InputWidget;
use Yii;

/**
 * Class JsonEditor
 *
 * @package Phycom\Backend\Widgets
 */
class JsonEditor extends InputWidget
{
    /**
     * @var array
     */
    public array $schema = [];

    /**
     * @var string|null
     */
    public ?string $jsonSchema = null;

    /**
     * @var string|null
     */
    public ?string $language = null;

    /**
     * @var array
     */
    public array $editorHolderOptions = [];

    /**
     * @var array
     */
    public array $jsonEditorOptions = [
        'theme'                         => "bootstrap3",
        'iconlib'                       => "spectre", //"fontawesome5",
        'object_layout'                 => "grid",
        'show_errors'                   => "interaction",
//        'remove_button_labels'          => true,
        'remove_empty_properties'       => true,
        'keep_oneof_values'             => true,
        'disable_edit_json'             => true,
        'array_controls_top'            => true,
        'disable_array_delete_last_row' => true,
        'disable_array_reorder'         => true,
        'prompt_before_delete'          => false,
        'disable_collapse'              => true,
        'disable_properties'            => true
    ];

    public function init()
    {
        parent::init();

        if (!$this->language) {
            $this->language = substr(Yii::$app->language, 0, 2);
        }
    }

    public function run()
    {
        $id = $this->options['id'];

        echo $this->renderInputHtml('hidden');

        $holderOptions = $this->editorHolderOptions;
        Html::addCssClass($holderOptions, 'json-editor-widget');
        if (!isset($holderOptions['id'])) {
            $holderOptions['id'] = $id . '-editor-holder';
        }

        echo Html::tag('div', '', $holderOptions);

        $value = $this->hasModel()
            ? $this->model->{$this->attribute}
            : $this->value;

        $options = $this->jsonEditorOptions;
        $options['schema'] = $this->loadSchema();
        $options['startval'] = Json::decode($value);

        $jsConfig = Json::encode($options);
        $view = $this->getView();

        JsonEditorWidgetAsset::register($view);
        $language = $this->language;
        $view->registerJs("JSONEditor.defaults.language = '$language';");
        $view->registerJs("JsonEditor.init('$id', $jsConfig);");
    }

    /**
     * @return array
     */
    protected function loadSchema(): array
    {
        return $this->jsonSchema ? Json::decode($this->jsonSchema) : $this->schema;
    }
}
