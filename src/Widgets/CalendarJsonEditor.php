<?php

namespace Phycom\Backend\Widgets;


use Phycom\Backend\Assets\CalendarJsonEditorAsset;

use yii\helpers\Html;
use yii\helpers\Json;

use Yii;

/**
 * Class CalendarJsonEditor
 *
 * @package Phycom\Backend\Widgets
 */
class CalendarJsonEditor extends JsonEditor
{
    public array $calendarOptions = [
        'allowInput' => false,
        'static'     => true,
        'inline'     => true,
    ];

    public array $calendarInputOptions = [];

    public function init()
    {
        parent::init();

        if (!isset($this->calendarOptions['locale'])) {
            $this->calendarOptions['locale'] = $this->language;
        }
        if (!isset($this->calendarOptions['minDate'])) {
            $this->calendarOptions['minDate'] = (new \DateTime('today'))->format('Y-m-d');
        }
    }

    /**
     * @return string|void
     */
    public function run()
    {
        $id = $this->options['id'];

        echo Html::beginTag('div', ['class' => 'calendar-json-editor']);
        echo Html::tag('div', $this->renderCalendarInputHtml('hidden'), [
            'id'    => $id . '-calendar-holder',
            'class' => 'calendar-holder'
        ]);

        $editorHolderOptions = $this->editorHolderOptions;
        Html::addCssClass($editorHolderOptions, ['json-editor-widget', 'editor-holder']);
        if (!isset($editorHolderOptions['id'])) {
            $editorHolderOptions['id'] = $id . '-editor-holder';
        }

        echo Html::tag('div', $this->renderEmptyEditorHtml(), $editorHolderOptions);
        echo Html::endTag('div');

        echo $this->renderInputHtml('hidden');

        $value = $this->hasModel()
            ? $this->model->{$this->attribute}
            : $this->value;

        $jsonEditorConfig = $this->jsonEditorOptions;
        $jsonEditorConfig['schema'] = $this->loadSchema();
        $jsonEditorConfig['startval'] = Json::decode($value);

        $options = [
            'jsonEditor' => $jsonEditorConfig,
            'calendar'   => $this->calendarOptions
        ];

        $jsConfig = Json::encode($options);
        $view = $this->getView();

        CalendarJsonEditorAsset::register($view);
        $language = $this->language;
        $view->registerJs("JSONEditor.defaults.language = '$language';");
        $view->registerJs("CalendarJsonEditor.init('$id', $jsConfig);");
    }

    /**
     * @param string $type
     * @return string
     */
    protected function renderCalendarInputHtml(string $type): string
    {
        $this->calendarInputOptions['id'] = $this->options['id'] . '-calendar-input';
        return Html::input($type, $this->name . 'calendar', '', $this->calendarInputOptions);
    }

    /**
     * @return string
     */
    protected function renderEmptyEditorHtml(): string
    {
        return Html::tag('span', Yii::t('phycom/backend/schedule', 'No date selected'), ['class' => 'select-date']);
    }
}
