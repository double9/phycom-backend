<?php

namespace Phycom\Backend\Helpers;

use Phycom\Base\Models\User;

use yii\base\BaseObject;
use Yii;

/**
 * Class UserLanguage
 *
 * @package Phycom\Backend\Helpers
 */
class UserLanguage extends BaseObject
{
    protected $user;

    public function __construct(User $user, $config = [])
    {
        $this->user = $user;
        parent::__construct($config);
    }

    public function check()
    {
        if (!empty($this->user->settings->language) && Yii::$app->language !== $this->user->settings->language) {
            Yii::$app->language = $this->user->settings->language;
        }
    }
}
