<?php

namespace Phycom\Backend\Helpers;


use Phycom\Backend\Models\Product\ProductParamForm;
use Phycom\Backend\Models\Product\SearchProductParam;
use Phycom\Backend\Widgets\MultiFormGrid;

use Phycom\Base\Helpers\Url;
use Phycom\Base\Models\Product\ProductParam;

use yii\base\BaseObject;
use yii\data\ArrayDataProvider;
use yii\data\DataProviderInterface;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use Yii;
use yii\helpers\ReplaceArrayValue;
use yii\web\JsExpression;
use yii\widgets\MaskedInput;

/**
 * Class ProductParamGrid
 *
 * @package Phycom\Backend\Helpers
 */
class ProductParamGrid extends BaseObject
{
    /**
     * @var ProductParamForm
     */
    protected $form;

    /**
     * @param ProductParamForm $productParamForm
     * @param array $config
     * @return static|object
     * @throws \yii\base\InvalidConfigException
     */
    public static function create(ProductParamForm $productParamForm, array $config = [])
    {
        return Yii::createObject(static::class, [$productParamForm, $config]);
    }

    public function __construct(ProductParamForm $productParamForm, $config = [])
    {
        $this->form = $productParamForm;
        parent::__construct($config);
    }

    /**
     * @return string
     * @throws \Exception
     */
    public function render()
    {
        return MultiFormGrid::widget($this->createConfiguration());
    }

    /**
     * @return MultiFormGrid
     * @throws \yii\base\Exception
     * @throws \yii\base\InvalidConfigException
     */
    public function instance()
    {
        return new MultiFormGrid($this->createConfiguration());
    }

    /**
     * @return array
     * @throws \yii\base\Exception
     * @throws \yii\base\InvalidConfigException
     */
    protected function createConfiguration()
    {
        $dataProvider = $this->getDataProvider();

        return [
            'id'                     => 'product-param-grid',
            'dataProvider'           => $dataProvider,
            'formModel'              => $this->form,
            'formOptions'            => [
                'validateOnBlur'   => false,
                'validateOnChange' => false
            ],
            'rowOptions'        => function ($model, $key, $index, $grid) {
                /**
                 * @var ProductParam $model
                 * @var mixed $key
                 * @var int $index
                 * @var MultiFormGrid $grid
                 */
                if ($param = Yii::$app->modelFactory->getParam()::findByName($model->name)) {
                    return [
                        'data-field-name'  => $param->name,
                        'data-field-label' => $param->label
                    ];
                }
            },
            'columns'                => $this->createColumns(),
            'newRow'                 => $this->newRow($dataProvider)
        ];
    }

    /**
     * @return DataProviderInterface
     * @throws \yii\base\Exception
     * @throws \yii\base\InvalidConfigException
     */
    protected function getDataProvider()
    {
        if ($this->form->product->isNewRecord) {
            return new ArrayDataProvider([
                'allModels'  => $this->form->getModels(),
                'modelClass' => ProductParam::class
            ]);
        } else {
            return SearchProductParam::create($this->form->product)->search();
        }
    }

    /**
     * @return array
     */
    protected function createColumns()
    {
        return [
            [
                'attribute'     => 'name',
                'format'        => 'raw',
                'enableSorting' => false,
                'inputField'    => function ($field, $model, $key) {
                    /**
                     * @var \Phycom\Base\Models\Product\ProductParam $model
                     * @var \Phycom\Backend\Widgets\ActiveField $field
                     */
                    if ($param = Yii::$app->modelFactory->getParam()::findByName($model->name)) {
                        return $field->hiddenInput() . Html::tag('span', $model->label, ['class' => 'field-label label label-default']);
                    } else {
                        return $field->textInput();
                    }
                },
                'options'       => ['width' => 200]
            ],
            [
                'attribute'     => 'value',
                'format'        => 'raw',
                'enableSorting' => false,
                'inputField'    => function ($field, $model, $key) {
                    /**
                     * @var \Phycom\Base\Models\Product\ProductParam $model
                     * @var \Phycom\Backend\Widgets\ActiveField $field
                     */
                    if ($param = Yii::$app->modelFactory->getParam()::findByName($model->name)) {
                        switch ($param->type) {

                            case $param::TYPE_OPTION:
                                return $field->dropDownList(ArrayHelper::map($param->options, 'key', 'label'), [
                                    'class' => 'form-control',
//                                    'style' => 'max-width: 500px;'
                                ]);
                            case $param::TYPE_SELECT:
                                $allOptions = ArrayHelper::map($param->options, 'key', 'label');
                                $value = $field->model->{$field->attribute};
                                $value = is_array($value) ? $value : [];
                                return $field->multiSelect($allOptions, ['value' => new ReplaceArrayValue($value)]);

//                                return $field->checkboxList(ArrayHelper::map($param->options, 'key', 'label'), [
//                                    'id'    => 'product-params-' . $key . '-value',
//                                    'class' => 'checkbox-list',
//                                    'name'  => 'ProductParamForm[models][' . $key . '][value]',
//                                    'value' => $model->value
//                                ]);
                            case $param::TYPE_TAGS:
                                return $field->tags(ArrayHelper::map($param->options, 'key', 'label'));
                            case $param::TYPE_BOOL:
                                return $field->boolean();
                            default:
                                $options = [];
                                switch ($param->type) {
                                    case $param::TYPE_NUMBER:
                                        $options['type'] = 'number';
                                        break;
                                }
                                $input = $field->textInput($options);
                                if ($param->unit) {
                                    $input->inputAppend($param->unit, ['class' => 'type-' . $param->type]);
                                }
//                                if ($param->inputMask) {
//                                    return $input->widget(MaskedInput::class, [
//                                        'mask' => new JsExpression($param->inputMask)
//                                    ]);
//                                }
                                return $input;
                        }
                    } else {
                        return $field->textInput();
                    }
                }
            ],
            [
                'attribute'     => 'is_public',
                'format'        => 'boolean',
                'enableSorting' => false,
                'inputField'    => function ($field, $model, $key) {
                    /**
                     * @var \Phycom\Base\Models\Product\ProductParam $model
                     * @var \Phycom\Backend\Widgets\ActiveField $field
                     */
                    return $field->boolean();
                },
                'options'       => ['width' => 140]
            ],
            [
                'attribute'     => 'created_at',
                'format'        => 'datetime',
                'inputField'    => false,
                'enableSorting' => false,
                'options'       => ['width' => 200]
            ],
            [
                'attribute'     => 'updated_at',
                'format'        => 'datetime',
                'inputField'    => false,
                'enableSorting' => false,
                'options'       => ['width' => 200]
            ]
        ];
    }

    /**
     * @param DataProviderInterface $dataProvider
     * @return \Closure
     */
    protected function newRow(DataProviderInterface $dataProvider)
    {
        return function() use ($dataProvider) {

            $customKey = 'custom';
            $customLabel = Yii::t('phycom/backend/product', 'Custom Param');
            $items = [$customKey => $customLabel];
            $options = [$customKey => ['data-label' => $customLabel]];

            $selected = ArrayHelper::getColumn($dataProvider->models, 'name');
            foreach (Yii::$app->modelFactory->getParam()::findAll() as $param) {
                if (!in_array($param->name, $selected)) {
                    $items[$param->name] = $param->label;
                    $options[$param->name] = [
                        'data-label' => $param->label,
                    ];
                }
            }

            $dropDown = Html::tag('div', Html::dropDownList('name', null, $items, [
                'class'    => 'field-name form-control',
                'data-url' => Url::toBeRoute(['/product/get-param-row']),
                'options'  => $options
            ]), ['class' => 'form-group pull-left', 'style' => 'width: 260px; margin-bottom: 0;']);

            $btn = Html::button('<span class="fas fa-plus"></span>&nbsp;&nbsp;' . Yii::t('phycom/backend/product', 'Add Param'), [
                'class' => 'add-row btn btn-flat btn-default pull-left',
                'style' => 'margin-right: 10px;'
            ]);

            return Html::tag('div', $btn . $dropDown, ['class' => 'new-row-form clearfix']);
        };
    }
}
