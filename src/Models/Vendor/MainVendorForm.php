<?php

namespace Phycom\Backend\Models\Vendor;

use Phycom\Base\Models\Attributes\VendorType;
use Phycom\Base\Validators\PhoneInputValidator;

/**
 * Class MainVendorForm
 *
 * @package Phycom\Backend\Models\Vendor
 */
class MainVendorForm extends VendorForm
{
    public ?bool $main = true;
    public ?string $type = VendorType::SITE_OWNER;

    public ?string $defaultClosedMessage = null;
    public ?string $closedMessage = null;
    public ?string $courierStart = null;
    public ?string $courierEnd = null;

    public function rules()
    {
        return [
            [['name', 'legalName', 'regNumber', 'vatNumber'], 'trim'],

            [['name', 'status', 'email','phone', 'type'], 'required'],
            [['name', 'status', 'type', 'email','phone', 'legalName', 'regNumber', 'vatNumber'], 'string', 'max' => 255],

            [['email'], 'trim'],
            [['email'], 'email'],

            [['phone'], 'trim'],
            [['phone'], PhoneInputValidator::class],
            [['phoneNumber'], 'string'],

            ['address', 'safe'],
            ['main', 'boolean'],

            [['closedMessage', 'defaultClosedMessage'], 'trim'],
            [['closedMessage', 'defaultClosedMessage', 'courierStart', 'courierEnd'], 'string'],
        ];
    }
}
