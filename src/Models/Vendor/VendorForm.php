<?php

namespace Phycom\Backend\Models\Vendor;

use Phycom\Base\Helpers\ThumbnailGenerator;
use Phycom\Base\Models\Attributes\VendorOption;
use Phycom\Base\Models\Attributes\VendorType;
use Phycom\Base\Models\Traits\ModelTrait;
use Phycom\Base\Models\Address;
use Phycom\Base\Models\Attributes\AddressType;
use Phycom\Base\Models\Attributes\ContactAttributeStatus;
use Phycom\Base\Models\Attributes\VendorStatus;
use Phycom\Base\Models\Email;
use Phycom\Base\Helpers\PhoneHelper as PhoneHelper;
use Phycom\Base\Models\Phone;
use Phycom\Base\Models\Vendor;
use Phycom\Base\Validators\PhoneInputValidator;

use yii\base\Model;
use Yii;
use yii\web\JsExpression;
use yii\web\UploadedFile;

/**
 * Class VendorForm
 *
 * @package Phycom\Backend\Models
 *
 * @property-read Vendor $vendor
 * @property-read AddressForm $addressForm
 * @property-read array $statuses
 */
class VendorForm extends Model
{
    const LOGO_FIELD = 'logo';

	use ModelTrait;

	public ?string $name;
	public ?string $type;
    public ?string $legalName;
    public ?string $regNumber;
    public ?string $vatNumber;
	public ?string $status;
	public ?string $email;
	public ?string $phone;
	public ?string $phoneNumber;
	public ?string $address;
	public ?string $website;

    /**
     * @var bool|null
     */
	public ?bool $main = false;

    /**
     * @var UploadedFile|mixed
     */
    public $logo;

    /**
     * @var Vendor
     */
	protected Vendor $vendor;

    /**
     * @var int
     */
	protected int $maxImageSize = 8 * 1024 * 1024;

	/**
     * @var array|string[]
     */
	protected array $allowedImageExtensions = ['png', 'jpg', 'jpeg', 'gif', 'pdf'];


	public function __construct(Vendor $vendor = null, array $config = [])
	{
        $this->vendor = $vendor ?: Yii::$app->modelFactory->getVendor([
            'status'  => VendorStatus::ACTIVE,
            'options' => Yii::createObject(VendorOption::class)
        ]);
		parent::__construct($config);
	}

	public function init()
	{
		parent::init();
		$this->loadModelValues();
	}

    public function loadModelValues()
    {
        $this->name = $this->vendor->name;
        $this->legalName = $this->vendor->legal_name;
        $this->regNumber = $this->vendor->reg_number;
        $this->vatNumber = $this->vendor->options->vatNumber;
        $this->website = $this->vendor->options->website;

        if (!isset($this->type)) {
            $this->type = $this->vendor->type
                ? (string)$this->vendor->type
                : Yii::$app->commerce->shop->defaultVendorType;
        }

        $this->status = (string) $this->vendor->status;
        $this->email = $this->vendor->email->email ?? null;
        $this->phone = $this->vendor->phone->phone_nr ?? null;
        $this->phoneNumber = $this->vendor->phone->fullNumber ?? null;
        $this->address = $this->vendor->address
            ? (string) $this->vendor->address->export()
            : null;
        $this->main = $this->vendor->main;
    }

	public function getVendor()
	{
		return $this->vendor;
	}


	public function rules()
	{
		return [
			[['name', 'legalName', 'regNumber', 'vatNumber'], 'trim'],

			[['name', 'status'], 'required'],
            [['type'], 'required', 'when' => function() {
		        return Yii::$app->commerce->shop->multipleVendors;
            }],
			[['name', 'status', 'type', 'email', 'phone', 'legalName', 'regNumber', 'vatNumber', 'website'], 'string', 'max' => 255],

			[['email'], 'trim'],
			[['email'], 'email'],

			[['phone'], 'trim'],
			[['phone'], PhoneInputValidator::class],
			[['phoneNumber'], 'string'],

            [[static::LOGO_FIELD], 'file',
                'skipOnEmpty'            => true,
                'extensions'             => $this->allowedImageExtensions,
                'maxSize'                => $this->maxImageSize,
                'maxFiles'               => 1,
                'enableClientValidation' => true
            ],
            [[static::LOGO_FIELD], 'required',
                'when' => fn() => !$this->vendor->options->logo,
                'whenClient' => new JsExpression(
                    "function (attribute, value) {
                        let logo = document.getElementById(attribute.id).closest('td').querySelector('img.vendor-logo');
                        return !Boolean(logo);
                    }"
                ),
            ],

            ['address', 'safe'],
            ['main', 'boolean'],
		];
	}

	public function attributeLabels()
	{
        return [
            'name'             => Yii::t('phycom/backend/vendor', 'Name'),
            'type'             => Yii::t('phycom/backend/vendor', 'Type'),
            static::LOGO_FIELD => Yii::t('phycom/backend/vendor', 'Logo'),
            'website'          => $this->vendor->options->getAttributeLabel('website'),
            'legalName'        => Yii::t('phycom/backend/vendor', 'Legal name'),
            'regNumber'        => Yii::t('phycom/backend/vendor', 'Reg Number'),
            'vatNumber'        => $this->vendor->options->getAttributeLabel('vatNumber'),
            'status'           => Yii::t('phycom/backend/vendor', 'Status'),
            'settings'         => Yii::t('phycom/backend/vendor', 'Settings'),
            'email'            => Yii::t('phycom/backend/vendor', 'Email'),
            'phone'            => Yii::t('phycom/backend/vendor', 'Phone'),
            'address'          => Yii::t('phycom/backend/vendor', 'Address'),
        ];
	}

	public function getStatuses()
    {
        return VendorStatus::displayValues([VendorStatus::DELETED]);
    }

	public function getAddressForm(): AddressForm
    {
        $addressForm = Yii::$app->modelFactory->getVendorAddressForm($this->vendor);

        if ($this->vendor->address) {
            $addressForm->populate($this->vendor->address);
        }

        return $addressForm;
    }

    /**
     * @param array $data
     * @param null $formName
     * @return bool
     */
    public function load($data, $formName = null)
    {
        if ($loaded = parent::load($data, $formName)) {
            $this->logo = UploadedFile::getInstance($this, static::LOGO_FIELD);
        }
        return $loaded;
    }

    public function update()
	{
		if ($this->validate()) {

		    $transaction = Yii::$app->db->beginTransaction();
		    try {

                $this->vendor->name = $this->name;
                $this->vendor->legal_name = $this->legalName;
                $this->vendor->reg_number = $this->regNumber;
                $this->vendor->options->vatNumber = $this->vatNumber;
                $this->vendor->options->website = $this->website;
                $this->vendor->status = VendorStatus::create($this->status);
                $this->vendor->main = (bool) $this->main;

                if (Yii::$app->commerce->shop->multipleVendors) {
                    $this->vendor->type = VendorType::create($this->type);
                }

                if (!$this->vendor->save()) {
                    return $this->rollback($transaction, $this->vendor->errors);
                }

                if ($this->logo instanceof UploadedFile && !$this->saveLogo()) {
                    return $this->rollback($transaction, [static::LOGO_FIELD => [Yii::t('phycom/backend/vendor', 'Error saving logo')]]);
                }

                /**
                 * Update vendor address
                 */
                if ($this->address) {
                    if ($address = $this->vendor->address) {
                        $address->updateJson($this->address);
                    } else {
                        $address = Address::create($this->address);
                        $address->type = AddressType::MAIN;
                        $address->status = ContactAttributeStatus::UNVERIFIED;
                        $address->vendor_id = $this->vendor->id;
                    }
                    if (!$address->save()) {
                        return $this->rollback($transaction, $address->errors, 'address');
                    }
                }

                /**
                 *  Update vendor email
                 */
                if ($this->email) {
                    if (!$email = $this->vendor->email) {
                        $email = new Email();
                        $email->status = ContactAttributeStatus::UNVERIFIED;
                        $email->vendor_id = $this->vendor->id;
                    }
                    $email->email = $this->email;
                    if (!$email->save()) {
                        return $this->rollback($transaction, $email->errors, 'email');
                    }
                }

                /**
                 *  Update vendor phone
                 */
                if ($this->phone) {
                    if (!$phone = $this->vendor->phone) {
                        $phone = new Phone();
                        $phone->status = ContactAttributeStatus::UNVERIFIED;
                        $phone->vendor_id = $this->vendor->id;
                    }
                    $phone->phone_nr = PhoneHelper::getNationalPhoneNumber($this->phoneNumber);
                    $phone->country_code = PhoneHelper::getPhoneCode($this->phoneNumber);
                    if (!$phone->save()) {
                        return $this->rollback($transaction, $phone->errors, 'phone');
                    }
                }

                $transaction->commit();
                return true;

		    } catch (\Exception $e) {
		        $transaction->rollBack();
		        throw $e;
            }
		}
		return false;
	}

    /**
     * @return bool
     * @throws \yii\base\Exception
     */
	protected function saveLogo(): bool
    {
        $filename = time() . '_' . Yii::$app->security->generateRandomString(32) . '.' . $this->logo->extension;
        $bucket = Yii::$app->fileStorage->getBucket($this->vendor->options->getFileBucket());

        if (!$bucket->copyFileIn($this->logo->tempName, $filename)) {
            $this->addError(static::LOGO_FIELD, Yii::t('phycom/frontend/main', 'Error saving uploaded file'));
            return false;
        }
        /**
         * @var ThumbnailGenerator|object $thumbnailGenerator
         */
        $thumbnailGenerator = Yii::createObject(ThumbnailGenerator::class, [$bucket->getName()]);
        $thumbnailGenerator->generate($filename);

        $this->vendor->options->logo = $filename;
        return $this->vendor->save();
    }
}
