<?php

namespace Phycom\Backend\Models\Vendor;

use Phycom\Base\Models\Vendor;
use Phycom\Base\Models\AddressForm as BaseAddressForm;

/**
 * Class AddressForm
 *
 * @package Phycom\Backend\Models\Vendor
 */
class AddressForm extends BaseAddressForm
{
    /**
     * @var Vendor
     */
	protected Vendor $vendor;

    /**
     * @return Vendor
     */
	public function getModel()
	{
		return $this->vendor;
	}

	public function __construct(Vendor $vendor, array $config = [])
	{
		$this->vendor = $vendor;
		parent::__construct($config);
	}
}
