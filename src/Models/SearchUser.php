<?php

namespace Phycom\Backend\Models;

use Phycom\Base\Components\ActiveQuery;
use Phycom\Base\Models\Attributes\UserStatus;
use Phycom\Base\Models\Traits\SearchQueryFilter;
use Phycom\Base\Interfaces\SearchModelInterface;
use Phycom\Base\Models\Attributes\ContactAttributeStatus;
use Phycom\Base\Models\Email;
use Phycom\Base\Models\User;

use yii\data\ActiveDataProvider;
use yii\data\Sort;
use yii\helpers\ArrayHelper;
use Yii;

/**
 * Class SearchUser
 * @package Phycom\Backend\Models
 */
class SearchUser extends User implements SearchModelInterface
{
	use SearchQueryFilter;

	public $name;
	public $email;

	public $createdFrom;
	public $createdTo;
	public $updatedFrom;
	public $updatedTo;

	public $showUnregistered = false;

	public function rules()
	{
		return [
			['id', 'integer'],
			[[
				'first_name',
				'last_name',
				'status',
				'type',
				'created_at',
				'updated_at',
				'createdFrom',
				'createdTo',
				'updatedFrom',
				'updatedTo'
			], 'safe'],
			[['name'], 'string'],
			[['company_name', 'display_name', 'username', 'email'], 'string', 'max' => 255]
		];
	}

	public function attributeLabels()
    {
        return ArrayHelper::merge(parent::attributeLabels(), [
            'name'  => Yii::t('phycom/backend/user', 'Name'),
            'email' => Yii::t('phycom/backend/user', 'Email'),
        ]);
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     * @return ActiveDataProvider
     * @throws \yii\base\Exception
     */
	public function search(array $params = [])
	{
		$query = $this->createSearchQuery();
		$dataProvider = new ActiveDataProvider([
			'query' => $query,
			'sort' => ['defaultOrder' => ['created_at' => SORT_DESC]],
			'pagination' => [
				'pageSize' => 20
			],
		]);

		$this->sort($dataProvider->sort);

		if (!($this->load($params) && $this->validate())) {
			return $dataProvider;
		}

		$query->andFilterWhere([
			'u.id' => $this->id,
			'u.type' => (string)$this->type,
			'u.status' => (string)$this->status
		]);

		$query->filterText('u.username', $this->username);
		$query->filterText('e.email', $this->email);
		$query->filterText('u.company_name', $this->company_name);
		$query->filterFullName(['u.first_name', 'u.last_name'], $this->name);
		$query->filterDateRange('u.created_at', $this->createdFrom, $this->createdTo);

		return $dataProvider;
	}

	protected function sort(Sort $sort)
	{
		$sort->attributes['name'] = [
			'asc' => ['u.first_name' => SORT_ASC, 'u.last_name' => SORT_ASC],
			'desc' => ['u.first_name' => SORT_DESC, 'u.last_name' => SORT_DESC],
		];
		$sort->attributes['email'] = [
			'asc' => ['e.email' => SORT_ASC],
			'desc' => ['e.email' => SORT_DESC],
		];
	}

    /**
     * @return ActiveQuery
     * @throws \yii\base\Exception
     */
	protected function createSearchQuery()
	{
		$query = static::find()
            ->select([
                'e.email as email',
                'TRIM(CONCAT_WS(\' \', u.first_name, u.last_name)) as name',
                'u.*'
            ])
            ->from(['u' => User::tableName()])
            ->leftJoin(['e' => Email::tableName()], [
                'and',
                'e.user_id = u.id',
                'e.status != :status_deleted',
            ], ['status_deleted' => ContactAttributeStatus::DELETED])
            ->where(['not', ['u.status' => UserStatus::DELETED]])
            ->groupBy(['u.id', 'e.email']);


//		$query->leftJoin(['e' => 'LATERAL('.
//			(new Query())
//				->select('e2.email')
//				->from(['e2' => Email::tableName()])
//				->where('e2.user_id = u.id')
//				->andWhere('e2.status != :email_status_deleted')
//				->orderBy(['e2.id' => SORT_DESC . ' NULLS LAST','e2.updated_at' => SORT_DESC])
//				->limit(1)
//				->createCommand()
//				->sql
//			.')'
//		], 'true', ['email_status_deleted' => ContactAttributeStatus::DELETED]);

		return $query;
	}
}
