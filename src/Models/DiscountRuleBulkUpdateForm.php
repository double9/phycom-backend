<?php

namespace Phycom\Backend\Models;


use Phycom\Base\Helpers\Currency;
use Phycom\Base\Models\DiscountRule;
use Phycom\Base\Models\Traits\ModelTrait;
use Phycom\Base\Validators\RequiredOneValidator;

use yii\base\Model;
use Yii;

/**
 * Class DiscountRuleBulkUpdateForm
 * @package Phycom\Backend\Models
 */
class DiscountRuleBulkUpdateForm extends Model
{
    use ModelTrait;

    public $discountRate;
    public $discountAmount;
    public $birthdayDiscountRate;
    public $minPurchase;
    public $startDate;
    public $expirationDate;
    public $maxUsage;
    public $status;
    public $keys = [];

    public function rules()
    {
        return [
            [['keys'], 'required'],
            [[
                'discountRate',
                'discountAmount',
                'birthdayDiscountRate',
                'minPurchase',
                'expirationDate',
                'startDate',
                'maxUsage',
                'status'
            ], RequiredOneValidator::class],
            [['keys'], 'each', 'rule' => ['integer']],
            [['discountRate', 'discountAmount', 'birthdayDiscountRate', 'minPurchase'], 'number'],
            [['maxUsage'], 'integer', 'min' => 1],
            [['expirationDate', 'startDate'], 'date', 'format' => 'php:Y-m-d'],
            [['status'], 'string']
        ];
    }

    public function attributeLabels()
    {
        $model = Yii::$app->modelFactory->getDiscountRule();
        return [
            'discountRate'         => $model->getAttributeLabel('discount_rate'),
            'discountAmount'       => $model->getAttributeLabel('discount_amount'),
            'birthdayDiscountRate' => $model->getAttributeLabel('birthday_discount_rate'),
            'minPurchase'          => $model->getAttributeLabel('min_purchase'),
            'expirationDate'       => $model->getAttributeLabel('expires_at'),
            'startDate'            => $model->getAttributeLabel('starts_at'),
            'maxUsage'             => $model->getAttributeLabel('max_usage'),
            'status'               => $model->getAttributeLabel('status'),
        ];
    }

    public function update()
    {
        if ($this->validate()) {
            $count = 0;
            foreach (Yii::$app->modelFactory->getDiscountRule()::find()->where(['id' => $this->keys])->batch() as $discountRules) {
                /**
                 * @var DiscountRule[] $discountRules
                 */
                foreach ($discountRules as $discountRule) {
                    if (!empty($this->discountRate)) {
                        $discountRule->discount_rate = bcdiv($this->discountRate, '100', 2);
                    }
                    if (!empty($this->birthdayDiscountRate)) {
                        $discountRule->birthday_discount_rate = bcdiv($this->birthdayDiscountRate, '100', 2);
                    }
                    if (!empty($this->discountAmount)) {
                        $discountRule->discount_amount = Currency::toInteger($this->discountAmount);
                    }
                    if (!empty($this->minPurchase)) {
                        $discountRule->min_purchase = Currency::toInteger($this->minPurchase);
                    }
                    if (!empty($this->expirationDate)) {
                        $discountRule->expires_at = new \DateTime($this->expirationDate);
                    }
                    if (!empty($this->startDate)) {
                        $discountRule->starts_at = new \DateTime($this->startDate);
                    }
                    if (!empty($this->maxUsage)) {
                        $discountRule->max_usage = $this->maxUsage;
                    }
                    if (!empty($this->status)) {
                        $discountRule->status = $this->status;
                    }

                    if ($discountRule->save()) {
                        $count++;
                    } else {
                        $errors = $discountRule->getFirstErrors();
                        foreach ($errors as $attribute => $error) {
                            $this->addError('keys', $error);
                        }
                    }
                }
            }
            return $count;
        }
        return false;
    }
}
