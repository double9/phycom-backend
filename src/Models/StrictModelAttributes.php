<?php

namespace Phycom\Backend\Models;

trait StrictModelAttributes
{
    /**
     * Returns the list of attribute names.
     * By default, this method returns all public non-static properties of the class.
     * You may override this method to change the default behavior.
     * @return array list of attribute names.
     */
    abstract public function attributes();

    /**
     * Returns the attribute names that are safe to be massively assigned in the current scenario.
     * @return string[] safe attribute names
     */
    abstract public function safeAttributes();

    /**
     * This method is invoked when an unsafe attribute is being massively assigned.
     * The default implementation will log a warning message if YII_DEBUG is on.
     * It does nothing otherwise.
     * @param string $name the unsafe attribute name
     * @param mixed $value the attribute value
     */
    abstract public function onUnsafeAttribute($name, $value);

    /**
     * Sets the attribute values in a massive way.
     *
     * @param array $values attribute values (name => value) to be assigned to the model.
     * @param bool $safeOnly whether the assignments should only be done to the safe attributes.
     * A safe attribute is one that is associated with a validation rule in the current [[scenario]].
     * @throws \ReflectionException
     * @see attributes()
     * @see safeAttributes()
     */
    public function setAttributes($values, $safeOnly = true)
    {
        if (is_array($values)) {
            $attributes = array_flip($safeOnly ? $this->safeAttributes() : $this->attributes());
            foreach ($values as $name => $value) {
                if (isset($attributes[$name])) {
                    $this->$name = $this->castAttributeValue($name, $value);
                } elseif ($safeOnly) {
                    $this->onUnsafeAttribute($name, $value);
                }
            }
        }
    }

    /**
     * @param string $name
     * @param mixed $value
     * @return mixed
     * @throws \ReflectionException
     */
    protected function castAttributeValue(string $name, mixed $value): mixed
    {
        $reflect = new \ReflectionObject($this);
        $property = $reflect->getProperty($name);
        if ($type = $property->getType()) {
            switch ($type->getName()) {
                case 'bool':
                case 'boolean':
                    return (bool) $value;
                case 'int':
                    return (int) $value;
                default:
                    return $value;
            }
        }
        return $value;
    }
}
