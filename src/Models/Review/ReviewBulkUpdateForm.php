<?php

namespace Phycom\Backend\Models\Review;


use Phycom\Base\Models\Review;
use Phycom\Base\Models\Traits\ModelTrait;
use Phycom\Base\Validators\RequiredOneValidator;

use yii\base\Model;

/**
 * Class ReviewBulkUpdateForm
 * @package Phycom\Backend\Models\Review
 */
class ReviewBulkUpdateForm extends Model
{
    use ModelTrait;

    public $status;
    public $keys = [];

    public function rules()
    {
        return [
            [['keys'], 'required'],
            [['status'], RequiredOneValidator::class],
            [['keys'], 'each', 'rule' => ['integer']],
            [['status'], 'string']
        ];
    }

    public function attributeLabels()
    {
        $model = new Review();
        return [
            'status' => $model->getAttributeLabel('status'),
        ];
    }

    public function update()
    {
        if ($this->validate()) {
            $count = 0;
            foreach (Review::find()->where(['id' => $this->keys])->batch() as $reviews) {
                /**
                 * @var Review[] $reviews
                 */
                foreach ($reviews as $review) {

                    if (!empty($this->status)) {
                        $review->status = $this->status;
                    }

                    if ($review->save()) {
                        $count++;
                    } else {
                        $errors = $review->getFirstErrors();
                        foreach ($errors as $attribute => $error) {
                            $this->addError('keys', $error);
                        }
                    }
                }
            }
            return $count;
        }
        return false;
    }
}
