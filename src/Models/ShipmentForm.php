<?php

namespace Phycom\Backend\Models;

use Phycom\Base\Models\Traits\ModelTrait;
use Phycom\Base\Models\Shipment;
use yii\base\Model;

/**
 * Class ShipmentForm
 * @package Phycom\Backend\Models
 *
 * @property-read Shipment $shipment
 */
class ShipmentForm extends Model
{
	use ModelTrait;

	protected $shipment;

	public function __construct(Shipment $shipment = null, array $config = [])
	{
		$this->shipment = $shipment ?: new Shipment();
		parent::__construct($config);
	}

	public function getShipment()
	{
		return $this->shipment;
	}
}
