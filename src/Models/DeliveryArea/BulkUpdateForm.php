<?php

namespace Phycom\Backend\Models\DeliveryArea;


use Phycom\Base\Helpers\Currency;
use Phycom\Base\Models\Traits\ModelTrait;
use Phycom\Base\Validators\RequiredOneValidator;
use Phycom\Base\Modules\Delivery\Module as DeliveryModule;
use Phycom\Base\Modules\Delivery\Models\DeliveryArea;

use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\base\InvalidConfigException;
use Yii;
use yii\helpers\Json;

/**
 * Class BulkUpdateForm
 * @package Phycom\Backend\Models
 */
class BulkUpdateForm extends Model
{
    use ModelTrait;

    public $price;
    public $status;
    public $focThresholdAmount;
    public $updateDailySchedule = false;
    public $dailySchedule;
    public $updateWeeklySchedule = false;
    public $weeklySchedule;
    public $keys = [];

    public function rules()
    {
        return [
            [['keys'], 'required'],
            [['price', 'status', 'focThresholdAmount', 'updateDailySchedule', 'updateWeeklySchedule'], RequiredOneValidator::class],
            [['keys'], 'each', 'rule' => ['integer']],
            [['price', 'focThresholdAmount'], 'number'],
            [['status'], 'string'],
            [['updateDailySchedule', 'updateWeeklySchedule'], 'boolean'],
            [['dailySchedule', 'weeklySchedule'], 'safe']
        ];
    }

    public function attributeLabels()
    {
        return [
            'status'               => Yii::t('phycom/backend/delivery-area', 'Status'),
            'price'                => Yii::t('phycom/backend/delivery-area', 'Price'),
            'focThresholdAmount'   => Yii::t('phycom/backend/delivery-area', 'Free of charge threshold amount'),
            'updateDailySchedule'  => Yii::t('phycom/backend/delivery-area', 'Update daily schedule'),
            'dailySchedule'        => Yii::t('phycom/backend/delivery-area', 'Daily schedule'),
            'updateWeeklySchedule' => Yii::t('phycom/backend/delivery-area', 'Update weekly schedule'),
            'weeklySchedule'       => Yii::t('phycom/backend/delivery-area', 'Weekly schedule')
        ];
    }

    /**
     * @throws InvalidConfigException
     */
    public function init()
    {
        parent::init();
        $this->weeklySchedule = Json::encode($this->createWeeklyScheduleForm()->getDefaultDeliveryTimes());
    }

    /**
     * @return array
     */
    public function getDeliveryMethods(): array
    {
        /**
         * @var DeliveryModule $delivery
         */
        $delivery = Yii::$app->getModule('delivery');
        return ArrayHelper::map($delivery->getAllMethods(), 'id', 'name');
    }

    /**
     * @return array
     * @throws InvalidConfigException
     */
    public function getDailyScheduleSchema(): array
    {
        return $this->createDailyScheduleForm()->getSchema();
    }


    /**
     * @return array
     * @throws InvalidConfigException
     */
    public function getWeeklyScheduleSchema(): array
    {
        return $this->createWeeklyScheduleForm()->getSchema();
    }

    /**
     * @return false|int
     * @throws InvalidConfigException
     */
    public function update()
    {
        if ($this->validate()) {
            $count = 0;
            foreach (DeliveryArea::find()->where(['id' => $this->keys])->batch() as $deliveryAreas) {
                /**
                 * @var DeliveryArea[] $deliveryAreas
                 */
                foreach ($deliveryAreas as $deliveryArea) {
                    if ($this->updateDeliveryArea($deliveryArea)) {
                        $count++;
                    } else {
                        $errors = $deliveryArea->getFirstErrors();
                        foreach ($errors as $attribute => $error) {
                            $this->addError('keys', $error);
                        }
                    }
                }
            }
            return $count;
        }
        return false;
    }

    /**
     * @param DeliveryArea $deliveryArea
     * @return bool
     * @throws InvalidConfigException
     */
    protected function updateDeliveryArea(DeliveryArea $deliveryArea): bool
    {
        if (!empty($this->price)) {
            $deliveryArea->price = Currency::toInteger($this->price);
        }
        if (!empty($this->focThresholdAmount)) {
            $deliveryArea->foc_threshold_amount = Currency::toInteger($this->focThresholdAmount);
        }
        if (!empty($this->status)) {
            $deliveryArea->status = $this->status;
        }
        if ($this->updateDailySchedule && !empty($this->dailySchedule)) {
            $dailyScheduleForm = Yii::$app->modelFactory->getDeliveryAreaDailyScheduleForm($deliveryArea);
            $dailyScheduleForm->dates = $this->dailySchedule;
            $dailyScheduleForm->updateSettings();
        }
        if ($this->updateWeeklySchedule && !empty($this->weeklySchedule)) {
            $weeklyScheduleForm = Yii::$app->modelFactory->getDeliveryAreaWeeklyScheduleForm($deliveryArea);
            $weeklyScheduleForm->deliveryTimes = $this->weeklySchedule;
            $weeklyScheduleForm->updateSettings();
        }

        return $deliveryArea->save();
    }

    /**
     * @return WeeklyScheduleForm
     * @throws InvalidConfigException
     */
    protected function createWeeklyScheduleForm(): WeeklyScheduleForm
    {
        $deliveryArea = Yii::createObject(DeliveryArea::class);
        return Yii::$app->modelFactory->getDeliveryAreaWeeklyScheduleForm($deliveryArea);
    }

    /**
     * @return DailyScheduleForm
     * @throws InvalidConfigException
     */
    protected function createDailyScheduleForm(): DailyScheduleForm
    {
        $deliveryArea = Yii::createObject(DeliveryArea::class);
        return Yii::$app->modelFactory->getDeliveryAreaDailyScheduleForm($deliveryArea);
    }
}
