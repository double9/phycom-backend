<?php

namespace Phycom\Backend\Models\DeliveryArea;


use Phycom\Base\Helpers\Currency;
use Phycom\Base\Models\Attributes\AddressField;
use Phycom\Base\Models\Attributes\ShopStatus;
use Phycom\Base\Models\Shop;
use Phycom\Base\Models\Traits\ModelTrait;

use Phycom\Base\Modules\Delivery\Module as DeliveryModule;
use Phycom\Base\Modules\Delivery\Models\DeliveryArea;

use yii\base\InvalidConfigException;
use yii\helpers\ArrayHelper;
use yii\base\Model;
use yii\helpers\Json;
use Yii;

/**
 * Class DeliveryAreaForm
 * @package Phycom\Backend\Models\DeliveryArea
 *
 * @property-read DeliveryArea $deliveryArea
 */
class DeliveryAreaForm extends Model
{
	use ModelTrait;

    public $name;
	public $code;
	public $method;
	public $carrier;
	public $service = 'default';
	public $price;
	public $focThresholdAmount;
	public $deliveryTime;
	public $status;
	public $area;

    /**
     * @var WeeklyScheduleForm
     */
	protected WeeklyScheduleForm $weeklyScheduleForm;

    /**
     * @var DailyScheduleForm
     */
    protected DailyScheduleForm $dailyScheduleForm;

    /**
     * @var SettingsForm
     */
    protected SettingsForm $settingsForm;

    /**
     * @var DeliveryArea
     */
	protected DeliveryArea $deliveryArea;

    /**
     * DeliveryAreaForm constructor.
     *
     * @param DeliveryArea|null $area
     * @param array $config
     * @throws InvalidConfigException
     */
	public function __construct(DeliveryArea $area = null, array $config = [])
	{
		$this->deliveryArea = $area ?: Yii::createObject(DeliveryArea::class);
		parent::__construct($config);
	}

	public function init()
    {
        parent::init();
        $this->loadModelValues();
    }

    public function loadModelValues()
    {
        $formatter = Yii::$app->formatter;

        $this->name = $this->deliveryArea->name;
        $this->code = $this->deliveryArea->code;
        $this->method = $this->deliveryArea->method;
        $this->carrier = $this->deliveryArea->carrier;
        if ($this->deliveryArea->service) {
            $this->service = $this->deliveryArea->service;
        }
        $this->price = $formatter->asCurrencyValue($this->deliveryArea->price);
        $this->focThresholdAmount = $formatter->asCurrencyValue($this->deliveryArea->foc_threshold_amount);
        $this->deliveryTime = $this->deliveryArea->delivery_time;
        $this->status = (string) $this->deliveryArea->status;
        $this->area = $this->deliveryArea->area ? Json::encode($this->deliveryArea->area) : null;
    }

	public function rules()
    {
        return [
            [['method', 'price', 'status', 'area', 'name'], 'required'],
            [['price', 'focThresholdAmount', 'deliveryTime'], 'number'],
            [[
                'name',
                'method',
                'carrier',
                'service',
                'status',
                'code'
            ], 'string'],
            [['area'], 'safe']
        ];
    }

    public function attributeLabels()
    {
        return [
            'name'               => Yii::t('phycom/backend/delivery-area', 'Name'),
            'code'               => Yii::t('phycom/backend/delivery-area', 'Code'),
            'method'             => Yii::t('phycom/backend/delivery-area', 'Method'),
            'carrier'            => Yii::t('phycom/backend/delivery-area', 'Carrier'),
            'service'            => Yii::t('phycom/backend/delivery-area', 'Service'),
            'status'             => Yii::t('phycom/backend/delivery-area', 'Status'),
            'price'              => Yii::t('phycom/backend/delivery-area', 'Price'),
            'focThresholdAmount' => Yii::t('phycom/backend/delivery-area', 'Free of charge threshold amount'),
            'deliveryTime'       => Yii::t('phycom/backend/delivery-area', 'Delivery Time'),
            'area'               => Yii::t('phycom/backend/delivery-area', 'Area')
        ];
    }

    public function attributeHints()
    {
        return [];
    }

    /**
     * @return DeliveryArea
     */
    public function getDeliveryArea(): DeliveryArea
	{
		return $this->deliveryArea;
	}

    /**
     * @return AddressForm
     */
    public function getAddressForm(): AddressForm
    {
        $addressForm = Yii::$app->modelFactory->getDeliveryAreaAddressForm($this->deliveryArea);
        if ($this->deliveryArea->area) {
            $addressForm->populateArray($this->deliveryArea->area);
            return $addressForm;
        }
        return $addressForm;
    }

    /**
     * @return WeeklyScheduleForm
     */
    public function getWeeklyScheduleForm(): WeeklyScheduleForm
    {
        if (!isset($this->weeklyScheduleForm)) {
            $this->weeklyScheduleForm = Yii::$app->modelFactory->getDeliveryAreaWeeklyScheduleForm($this->deliveryArea);
        }
        return $this->weeklyScheduleForm;
    }

    /**
     * @return DailyScheduleForm
     */
    public function getDailyScheduleForm(): DailyScheduleForm
    {
        if (!isset($this->dailyScheduleForm)) {
            $this->dailyScheduleForm = Yii::$app->modelFactory->getDeliveryAreaDailyScheduleForm($this->deliveryArea);
        }
        return $this->dailyScheduleForm;
    }

    /**
     * @return SettingsForm
     */
    public function getSettingsForm(): SettingsForm
    {
        if (!isset($this->settingsForm)) {
            $this->settingsForm = Yii::$app->modelFactory->getDeliveryAreaSettingsForm($this->deliveryArea);
        }
        return $this->settingsForm;
    }

    /**
     * @return array
     */
	public function getDeliveryMethods(): array
    {
        /**
         * @var DeliveryModule $delivery
         */
        $delivery = Yii::$app->getModule('delivery');
        return ArrayHelper::map($delivery->getAllMethods(), 'id', 'name');
    }



    /**
     * @return Shop[]
     */
    public function getShops(): array
    {
        $Shop = Yii::$app->modelFactory->getShop();
        return $Shop::find()->where(['not', 'status', [ShopStatus::DELETED]])->all();
    }

    /**
     * @return bool
     */
    public function save(): bool
    {
        if ($this->validate()) {
            $da = $this->deliveryArea;

            $da->name = $this->name;
            $da->method = $this->method;
            $da->carrier = $this->carrier ?: null;
            $da->service = $this->service ?: null;

            $previousArea = !$da->isNewRecord ? $da->area : null;
            $da->area = (new AddressField($this->area))->toArray();

            if (!$previousArea || $previousArea != $da->area || !$da->area_code) {
                $da->area_code = $da->generateAreaCode();
            }
            $da->code = $this->code ?: $da->generateCode();

//            if ($shop = $this->checkIsShop($area)) {
//                $da->shop_id = $shop->id;
//            }
            $da->price = Currency::toInteger($this->price);
            $da->foc_threshold_amount = Currency::toInteger($this->focThresholdAmount);
            $da->delivery_time = $this->deliveryTime;
            $da->status = $this->status;

            if ($da->save()) {
                return true;
            } else {
                Yii::error('Error saving delivery area: ' . Json::encode($da->errors), __METHOD__);
                foreach ($da->errors as $attribute => $errors) {
                    foreach ($errors as $error) {
                        $this->addError('error', $error);
                    }
                }
            }
        }
        return false;
    }

    /**
     * Checks if the particular delivery area has exactly the same address as a shop.
     *
     * @param AddressField $area
     * @return Shop|null
     * @throws InvalidConfigException
     */
    protected function checkIsShop(AddressField $area): ?Shop
    {
        $areaJson = $area->toJson();
        foreach ($this->getShops() as $shop) {
            $shopAddressJson = $shop->address->export()->toJson([
                'country',
                'province',
                'city',
                'locality',
                'district',
                'street',
                'postcode'
            ]);

            if ($areaJson === $shopAddressJson) {
                return $shop;
            }
        }
        return null;
    }
}
