<?php

namespace Phycom\Backend\Models\DeliveryArea;

use Phycom\Backend\Models\Schedule\TimeRange;
use Phycom\Base\Modules\Delivery\Models\DeliveryArea;

use yii\base\InvalidConfigException;
use yii\base\Model;
use yii\helpers\Json;
use Yii;

/**
 * Class DailyScheduleForm
 *
 * @package Phycom\Backend\Models\DeliveryArea
 */
class DailyScheduleForm extends Model
{
    const STORAGE_KEY = 'deliveryDTimes';

    /**
     * @var string|null
     */
    public ?string $dates = null;

    /**
     * @var DeliveryArea
     */
    protected DeliveryArea $deliveryArea;

    /**
     * WeeklyScheduleForm constructor.
     *
     * @param DeliveryArea $deliveryArea
     * @param array $config
     */
    public function __construct(DeliveryArea $deliveryArea, array $config = [])
    {
        $this->deliveryArea = $deliveryArea;
        parent::__construct($config);
    }

    /**
     * @throws InvalidConfigException
     */
    public function init()
    {
        parent::init();
        $this->loadDates();
    }

    /**
     * @return \string[][]
     */
    public function rules()
    {
        return [
            ['dates', 'string']
        ];
    }

    /**
     * @return bool
     * @throws InvalidConfigException
     */
    public function save(): bool
    {
        if ($this->validate()) {

            $this->updateSettings();

            if (!$result = $this->deliveryArea->save()) {
                foreach ($this->deliveryArea->getErrors('settings') as $errors) {
                    $this->addErrors(['dates' => $errors]);
                }
            }
            return $result;
        }
        return false;
    }

    /**
     * @throws InvalidConfigException
     */
    public function updateSettings()
    {
        $settings = $this->deliveryArea->settings;
        $settings[static::STORAGE_KEY] = $this->parseDates();
        $this->deliveryArea->settings = $settings;
    }

    /**
     * @return array
     */
    public function getSchema(): array
    {
        return [
            'title'       => Yii::t('phycom/backend/delivery-area', 'Daily schedule'),
            '$ref'        => '#/definitions/date',
            'definitions' => [
                'date' => [
                    'type'       => 'object',
                    'format'     => 'table',
                    'required'   => ['closed', 'times'],
                    'properties' => [
                        'closed' => [
                            'title'   => Yii::t('phycom/backend/schedule', 'Closed'),
                            'type'    => 'boolean',
                            'options' => [
                                'enum_titles' => [
                                    Yii::t('phycom/backend/main', 'Yes'),
                                    Yii::t('phycom/backend/main', 'No')
                                ]
                            ]
                        ],
                        'times'  => [
                            'title'  => Yii::t('phycom/backend/delivery-area', 'Times'),
                            'type'   => 'array',
                            'format' => 'table',
                            'items'  => [
                                'title' => Yii::t('phycom/backend/delivery-area', 'Time range'),
                                '$ref'  => '#/definitions/time',
                            ],
                        ]
                    ]
                ],
                'time' => [
                    'type'       => 'object',
                    'required'   => ['from', 'to'],
                    'properties' => [
                        'from' => [
                            'title'  => Yii::t('phycom/backend/schedule', 'From'),
                            'type'   => 'string',
                            'format' => 'time',
                        ],
                        'to' => [
                            'title'  => Yii::t('phycom/backend/schedule', 'To'),
                            'type'   => 'string',
                            'format' => 'time',
                        ]
                    ]
                ]
            ]
        ];
    }


    /**
     * @throws InvalidConfigException
     */
    protected function loadDates(): void
    {
        $dates = [];
        $rawDates = $this->deliveryArea->settings[static::STORAGE_KEY] ?? [];
        foreach ($rawDates as $dateStr => $times) {
            if (false === $times) {
                $dates[$dateStr] = ['closed' => true, 'times' => []];
            } else if (is_array($times)) {
                $timeRanges = [];
                foreach ($times as $timeRangeKey) {
                    $timeRange = TimeRange::create($timeRangeKey);
                    $timeRanges[] = ['from' => $timeRange->from, 'to' => $timeRange->to];
                }
                $dates[$dateStr] = ['closed' => false, 'times' => $timeRanges];
            }
        }
        $this->dates = Json::encode($dates);
    }

    /**
     * @return array
     * @throws InvalidConfigException
     */
    protected function parseDates(): array
    {
        Yii::info('Parse dates: ' . $this->dates, 'delivery-area');
        $dates = Json::decode($this->dates);
        $out = [];
        if (is_array($dates)) {
            foreach ($dates as $date => $value) {
                if ($value['closed'] === true) {
                    $out[$date] = false;
                } else {
                    $times = [];
                    foreach ($value['times'] as $timeRangeConfig) {
                        $timeRangeConfig['class'] = TimeRange::class;
                        /**
                         * @var TimeRange|object $timeRange
                         */
                        $timeRange = Yii::createObject($timeRangeConfig);
                        $times[] = (string)$timeRange;
                    }
                    $out[$date] = $times;
                }
            }
        }
        Yii::info('Dates pared: ' . Json::encode($out), 'delivery-area');
        return $out;
    }
}
