<?php

namespace Phycom\Backend\Models\DeliveryArea;


use Phycom\Base\Modules\Delivery\Models\DeliveryArea;

use yii\base\Model;
use Yii;

/**
 * Class SettingsForm
 *
 * @package Phycom\Backend\Models\DeliveryArea
 */
class SettingsForm extends Model
{
    /**
     * @var string|null
     */
    public ?string $defaultFirstDeliveryThreshold = null;

    /**
     * @var string|null
     */
    public ?string $defaultFirstDeliveryRule = null;

    /**
     * @var string|null
     */
    public ?string $delayedFirstDeliveryThreshold = null;

    /**
     * @var string|null
     */
    public ?string $delayedFirstDeliveryRule = null;

    /**
     * @var DeliveryArea
     */
    protected DeliveryArea $deliveryArea;

    /**
     * SettingsForm constructor.
     *
     * @param DeliveryArea $deliveryArea
     * @param array $config
     */
    public function __construct(DeliveryArea $deliveryArea, $config = [])
    {
        $this->deliveryArea = $deliveryArea;
        parent::__construct($config);
    }

    public function init()
    {
        parent::init();
        $this->loadSavedValues();
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['defaultFirstDeliveryThreshold', 'delayedFirstDeliveryThreshold'], 'string'],
            [['defaultFirstDeliveryRule', 'delayedFirstDeliveryRule'], 'string'],
            ['defaultFirstDeliveryRule', 'required', 'when' => function () {
                return !empty($this->defaultFirstDeliveryThreshold);
            }],
            ['delayedFirstDeliveryRule', 'required', 'when' => function () {
                return !empty($this->delayedFirstDeliveryThreshold);
            }],
        ];
    }

    public function attributeLabels()
    {
        return [
            'defaultFirstDeliveryThreshold' => Yii::t('phycom/backend/delivery-area', 'Default FD threshold'),
            'defaultFirstDeliveryRule'      => Yii::t('phycom/backend/delivery-area', 'Default FD rule'),
            'delayedFirstDeliveryThreshold' => Yii::t('phycom/backend/delivery-area', 'Delayed FD threshold'),
            'delayedFirstDeliveryRule'      => Yii::t('phycom/backend/delivery-area', 'Delayed FD rule'),
        ];
    }

    public function attributeHints()
    {
        return [
            'defaultFirstDeliveryThreshold' => Yii::t('phycom/backend/delivery-area', 'Last order time for next day deliveries'),
            'defaultFirstDeliveryRule'      => Yii::t('phycom/backend/delivery-area', 'Next day first delivery rule'),
            'delayedFirstDeliveryThreshold' => Yii::t('phycom/backend/delivery-area', 'Last order time for next day deliveries for delayed group'),
            'delayedFirstDeliveryRule'      => Yii::t('phycom/backend/delivery-area', 'Next day first delivery rule for delayed group'),
        ];
    }

    /**
     * @return bool
     */
    public function save(): bool
    {
        if ($this->validate()) {

            $settings = $this->deliveryArea->settings;
            $settings['FDTRules'] = [];

            if ($this->defaultFirstDeliveryThreshold && $this->defaultFirstDeliveryRule) {
                $settings['FDTRules']['default'] = [
                    $this->defaultFirstDeliveryThreshold => $this->defaultFirstDeliveryRule
                ];
            }
            if ($this->delayedFirstDeliveryThreshold && $this->delayedFirstDeliveryRule) {
                $settings['FDTRules']['delayed'] = [
                    $this->delayedFirstDeliveryThreshold => $this->delayedFirstDeliveryRule
                ];
            }

            $this->deliveryArea->settings = $settings;

            if (!$result = $this->deliveryArea->save()) {
                foreach ($this->deliveryArea->getErrors('settings') as $errors) {
                    foreach ($errors as $error) {
                        $this->addErrors('error', $error);
                    }
                }
            }
            return $result;
        }
        return false;
    }

    protected function loadSavedValues()
    {
        $fdtRules = $this->deliveryArea->settings['FDTRules'] ?? [];
        foreach ($fdtRules as $group => $condition) {
            if (empty($condition)) {
                continue;
            }
            if ('default' === $group) {
                [$this->defaultFirstDeliveryThreshold, $this->defaultFirstDeliveryRule] = array_merge(array_keys($condition), array_values($condition));
            }
            if ('delayed' === $group) {
                [$this->delayedFirstDeliveryThreshold, $this->delayedFirstDeliveryRule] = array_merge(array_keys($condition), array_values($condition));
            }
        }
    }
}
