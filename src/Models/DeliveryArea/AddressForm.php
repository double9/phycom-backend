<?php

namespace Phycom\Backend\Models\DeliveryArea;


use Phycom\Base\Models\AddressForm as BaseAddressForm;
use Phycom\Base\Models\Country;
use Phycom\Base\Modules\Delivery\Models\DeliveryArea;

use yii\helpers\ArrayHelper;
use Yii;

/**
 * Class AddressForm
 *
 * @package Phycom\Backend\Models
 */
class AddressForm extends BaseAddressForm
{
	protected DeliveryArea $deliveryArea;

	public function __construct(DeliveryArea $deliveryArea, array $config = [])
	{
		$this->deliveryArea = $deliveryArea;
		parent::__construct($config);
	}

	public function init()
    {
        parent::init();
        $this->country = Yii::$app->country->defaultCountry;
    }

    public function rules()
    {
        return [
            [['country','province'], 'required'],
            ['country', 'string', 'length' => 2],
            ['country', 'exist', 'targetClass' => Country::class, 'targetAttribute' => 'code'],
            [['province','locality','city','district','street','postcode'], 'string', 'max' => 255]
        ];
    }

    public function attributeHints()
    {
        return [
            'locality' => Yii::t('phycom/backend/address', 'Village, parish or municipality'),
            'district' => Yii::t('phycom/backend/address', 'An area of a city'),
            'street'   => Yii::t('phycom/backend/address', 'Full street name with house number and apartment number'),
        ];
    }

    /**
     * @return DeliveryArea
     */
    public function getModel()
	{
		return $this->deliveryArea;
	}

    /**
     * @return array
     */
    public function getCountries(): array
    {
        $countries = Country::find()->orderBy(['name' => SORT_ASC]);
        return ArrayHelper::map($countries->all(), 'code', 'name');
    }

    /**
     * @return array
     */
    public function getDivisions(): array
    {
        if ($this->country) {
            $country = Country::findOne(['code' => $this->country]);
            return ArrayHelper::map($country->divisions, 'name', 'name');
        }
        return [];
    }
}
