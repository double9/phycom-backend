<?php

namespace Phycom\Backend\Models\DeliveryArea;

use Phycom\Base\Components\ActiveQuery;
use Phycom\Base\Helpers\Diacritics;
use Phycom\Base\Models\Traits\SearchQueryFilter;
use Phycom\Base\Interfaces\SearchModelInterface;

use Phycom\Base\Modules\Delivery\Models\DeliveryArea;
use Phycom\Base\Modules\Delivery\Models\DeliveryAreaStatus;

use yii\base\Exception;
use yii\data\ActiveDataProvider;
use yii\data\Sort;
use yii\db\Expression;

/**
 * Class Search
 * @package Phycom\Backend\Models\DeliveryArea
 */
class Search extends DeliveryArea implements SearchModelInterface
{
	use SearchQueryFilter;

	public $address;

	public $createdFrom;
	public $createdTo;
	public $updatedFrom;
	public $updatedTo;

	public function rules()
	{
		return [
		    [['id'], 'integer'],
            [['code', 'area_code', 'carrier', 'method', 'service'], 'string'],
            [['price'], 'number'],
            [['area', 'created_at', 'updated_at','createdFrom','createdTo','updatedFrom','updatedTo', 'status'], 'safe']
		];
	}

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     * @return ActiveDataProvider
     * @throws Exception
     */
	public function search(array $params = [])
	{
		$query = $this->createSearchQuery();
		$dataProvider = new ActiveDataProvider([
			'query' => $query,
			'sort' => ['defaultOrder' => ['created_at' => SORT_DESC]],
			'pagination' => [
				'pageSize' => 20
			],
		]);

		$this->sort($dataProvider->sort);

		if (!($this->load($params) && $this->validate())) {
			return $dataProvider;
		}

        $query->andFilterWhere([
            'da.id'     => $this->id,
            'da.status' => (string)$this->status,
            'da.method' => $this->method
        ]);

		if ($this->area) {
		    $area = '%' . trim(Diacritics::remove((string)$this->area)) . '%';
		    $query->andFilterWhere([
		        'or',
                new Expression('UNACCENT(da.area #>> \'{"country"}\') ILIKE :area', ['area' => $area]),
                new Expression('UNACCENT(da.area #>> \'{"locality"}\') ILIKE :area', ['area' => $area]),
                new Expression('UNACCENT(da.area #>> \'{"province"}\') ILIKE :area', ['area' => $area]),
                new Expression('UNACCENT(da.area #>> \'{"city"}\') ILIKE :area', ['area' => $area]),
                new Expression('UNACCENT(da.area #>> \'{"district"}\') ILIKE :area', ['area' => $area]),
                new Expression('UNACCENT(da.area #>> \'{"street"}\') ILIKE :area', ['area' => $area])
            ]);
        }

		$query->filterDateRange('da.created_at', $this->createdFrom, $this->createdTo);
		$query->filterDateRange('da.updated_at', $this->updatedFrom, $this->updatedTo);
		return $dataProvider;
	}

	protected function sort(Sort $sort)
	{

	}

    /**
     * @return ActiveQuery
     * @throws Exception
     */
	protected function createSearchQuery()
	{
		$query = static::find();
		$query->select([
			'da.*'
		]);
		$query->from(['da' => DeliveryArea::tableName()]);
        $query->where(['not', ['da.status' => DeliveryAreaStatus::DELETED]]);

		return $query;
	}
}
