<?php

namespace Phycom\Backend\Models\DeliveryArea;

use Phycom\Backend\Models\Schedule\TimeRange;
use Phycom\Base\Modules\Delivery\Models\DeliveryArea;

use yii\base\InvalidConfigException;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use Yii;

/**
 * Class WeeklyScheduleForm
 *
 * @package Phycom\Backend\Models\DeliveryArea
 */
class WeeklyScheduleForm extends Model
{
    const MONDAY = 'E';
    const TUESDAY = 'T';
    const WEDNESDAY = 'K';
    const THURSDAY = 'N';
    const FRIDAY = 'R';
    const SATURDAY = 'L';
    const SUNDAY = 'P';

    const STORAGE_KEY = 'deliveryTimes';

    /**
     * @var string|null
     */
    public ?string $deliveryTimes;

    /**
     * @var DeliveryArea
     */
    protected DeliveryArea $deliveryArea;

    /**
     * WeeklyScheduleForm constructor.
     *
     * @param DeliveryArea $deliveryArea
     * @param array $config
     */
    public function __construct(DeliveryArea $deliveryArea, $config = [])
    {
        $this->deliveryArea = $deliveryArea;
        parent::__construct($config);
    }

    /**
     * @throws InvalidConfigException
     */
    public function init()
    {
        parent::init();
        $this->loadDeliveryTimes();
    }

    public function rules()
    {
        return [
            ['deliveryTimes', 'string']
        ];
    }

    /**
     * @return bool
     * @throws InvalidConfigException
     */
    public function save(): bool
    {
        if ($this->validate()) {

            $this->updateSettings();

            if (!$result = $this->deliveryArea->save()) {
                foreach ($this->deliveryArea->getErrors('settings') as $errors) {
                    foreach ($errors as $error) {
                        $this->addErrors('deliveryTimes', $error);
                    }
                }
            }
            return $result;
        }
        return false;
    }

    /**
     * @throws InvalidConfigException
     */
    public function updateSettings()
    {
        $settings = $this->deliveryArea->settings;
        $settings[static::STORAGE_KEY] = $this->parseDeliveryTimes();
        $this->deliveryArea->settings = $settings;
    }

    /**
     * @return array
     */
    public function getSchema(): array
    {
        return [
            'title'       => Yii::t('phycom/backend/delivery-area', 'Weekly schedule'),
            '$ref'        => '#/definitions/weekdays',
            'definitions' => [
                'weekdays' => [
                    'id'         => 'weekdays',
                    'type'       => 'object',
                    'properties' => [
                        static::MONDAY => [
                            'title' => Yii::t('phycom/backend/schedule', 'Monday'),
                            '$ref'  => '#/definitions/weekday',
                        ],
                        static::TUESDAY => [
                            'title' => Yii::t('phycom/backend/schedule', 'Tuesday'),
                            '$ref'  => '#/definitions/weekday',
                        ],
                        static::WEDNESDAY => [
                            'title' => Yii::t('phycom/backend/schedule', 'Wednesday'),
                            '$ref'  => '#/definitions/weekday',
                        ],
                        static::THURSDAY => [
                            'title' => Yii::t('phycom/backend/schedule', 'Thursday'),
                            '$ref'  => '#/definitions/weekday',
                        ],
                        static::FRIDAY => [
                            'title' => Yii::t('phycom/backend/schedule', 'Friday'),
                            '$ref'  => '#/definitions/weekday',
                        ],
                        static::SATURDAY => [
                            'title' => Yii::t('phycom/backend/schedule', 'Saturday'),
                            '$ref'  => '#/definitions/weekday',
                        ],
                        static::SUNDAY => [
                            'title' => Yii::t('phycom/backend/schedule', 'Sunday'),
                            '$ref'  => '#/definitions/weekday',
                        ],
                    ],
                ],
                'weekday'  => [
                    'type'   => 'array',
                    'format' => 'table',
                    'items'  => [
                        'title' => Yii::t('phycom/backend/delivery-area', 'Time range'),
                        '$ref'  => '#/definitions/time',
                    ],
                ],
                'time' => [
                    'type'       => 'object',
                    'required'   => ['from', 'to'],
                    'properties' => [
                        'from' => [
                            'title'  => Yii::t('phycom/backend/schedule', 'From'),
                            'type'   => 'string',
                            'format' => 'time',
                        ],
                        'to' => [
                            'title'  => Yii::t('phycom/backend/schedule', 'To'),
                            'type'   => 'string',
                            'format' => 'time',
                        ]
                    ]
                ]
            ]
        ];
    }

    /**
     * @throws InvalidConfigException
     */
    protected function loadDeliveryTimes()
    {
        $deliveryTimes = [];
        $rawDeliveryTimes = $this->deliveryArea->settings[static::STORAGE_KEY] ?? [];
        foreach ($rawDeliveryTimes as $weekdays => $times) {
            foreach (str_split($weekdays) as $weekday) {
                $timeRanges = [];
                foreach ($times as $timeRangeKey) {
                    $timeRange = TimeRange::create($timeRangeKey);
                    $timeRanges[] = ['from' => $timeRange->from, 'to' => $timeRange->to];
                }
                $deliveryTimes[$weekday] = $timeRanges;
            }
        }

        $deliveryTimes = ArrayHelper::merge($this->getDefaultDeliveryTimes(), $deliveryTimes);
        $this->deliveryTimes = Json::encode($deliveryTimes);
    }

    /**
     * @return array[]
     */
    public function getDefaultDeliveryTimes(): array
    {
        return array_fill_keys($this->getWeekdays(), []);
    }

    /**
     * @return string[]
     */
    protected function getWeekdays(): array
    {
        return [static::MONDAY, static::TUESDAY, static::WEDNESDAY, static::THURSDAY, static::FRIDAY, static::SATURDAY, static::SUNDAY];
    }

    /**
     * @return array
     * @throws InvalidConfigException
     */
    protected function parseDeliveryTimes(): array
    {
        $deliveryTimes = Json::decode($this->deliveryTimes);

        $out = [];
        if (is_array($deliveryTimes)) {
            foreach ($deliveryTimes as $weekday => $timeRanges) {
                $times = [];
                foreach ($timeRanges as $timeRangeConfig) {
                    $timeRangeConfig['class'] = TimeRange::class;
                    /**
                     * @var TimeRange|object $timeRange
                     */
                    $timeRange = Yii::createObject($timeRangeConfig);
                    $times[] = (string)$timeRange;
                }

                foreach ($out as $key => $comparedTimes) {
                    if ($this->compareArrays($comparedTimes, $times)) {
                        $out[$key . $weekday] = $times;
                        unset($out[$key]);
                        continue 2;
                    }
                }
                $out[$weekday] = $times;
            }

            // make sure weekday keys are ordered
            $order = $this->getWeekdays();
            foreach ($out as $key => $times) {
                $weekdays = str_split($key);

                usort($weekdays, function ($a, $b) use ($order) {
                    foreach($order as $key => $value) {
                        if ($a == $value) {
                            return 0;
                        }
                        if ($b == $value) {
                            return 1;
                        }
                    }
                });

                $key2 = implode('', $weekdays);

                if ($key !== $key2) {
                    $out[$key2] = $times;
                    unset($out[$key]);
                }
            }
        }

        return $out;
    }

    /**
     * @param array $arr1
     * @param array $arr2
     * @return bool - true if arrays are similar false otherwise
     */
    protected function compareArrays(array $arr1, array $arr2): bool
    {
        if (empty($arr1) && !empty($arr2)) {
            return false;
        }

        foreach ($arr1 as $value) {
            if (!in_array($value, $arr2)) {
                return false;
            }
        }
        return true;
    }
}
