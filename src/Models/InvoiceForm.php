<?php

namespace Phycom\Backend\Models;


use Phycom\Base\Models\Traits\ModelTrait;
use Phycom\Base\Models\Invoice;

use yii\base\Model;

/**
 * Class InvoiceForm
 * @package Phycom\Backend\Models
 *
 * @property-read Invoice $invoice
 */
class InvoiceForm extends Model
{
	use ModelTrait;

	protected $invoice;

	public function __construct(Invoice $invoice = null, array $config = [])
	{
		$this->invoice = $invoice ?: new Invoice();
		parent::__construct($config);
	}

	public function getInvoice()
	{
		return $this->invoice;
	}
}
