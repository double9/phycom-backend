<?php

namespace Phycom\Backend\Models\Shop;

use Phycom\Base\Components\ActiveQuery;
use Phycom\Base\Models\Attributes\ShopStatus;
use Phycom\Base\Models\Attributes\VendorStatus;
use Phycom\Base\Models\Traits\SearchQueryFilter;
use Phycom\Base\Interfaces\SearchModelInterface;
use Phycom\Base\Models\Address;
use Phycom\Base\Models\Attributes\ContactAttributeStatus;
use Phycom\Base\Models\Phone;
use Phycom\Base\Models\Shop;

use Phycom\Base\Models\Vendor;
use yii\base\InvalidArgumentException;
use yii\data\ActiveDataProvider;
use yii\data\Sort;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use Yii;
use yii\helpers\Json;

/**
 * Class Search
 * @package Phycom\Backend\Models\Shop
 */
class Search extends Shop implements SearchModelInterface
{
	use SearchQueryFilter;

	public $address;
	public $phone;
	public $createdFrom;
	public $createdTo;
	public $updatedFrom;
	public $updatedTo;


	protected ?string $searchVendor = null;

	public function rules()
	{
		return [
			[['id','vendor_id', 'phone'], 'integer'],
			[['address'], 'string'],
			[['created_at','updated_at','createdFrom','createdTo','updatedFrom','updatedTo','status','type'], 'safe'],
			[['name', 'vendor'], 'string', 'max' => 255]
		];
	}

	public function attributeLabels()
    {
        return ArrayHelper::merge(parent::attributeLabels(), [
            'vendor'  => Yii::t('phycom/backend/vendor', 'Vendor'),
            'address' => Yii::t('phycom/backend/main', 'Address'),
            'phone'   => Yii::t('phycom/backend/main', 'Phone'),
        ]);
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     * @return ActiveDataProvider
     * @throws \yii\base\Exception
     */
	public function search(array $params = [])
	{
		$query = $this->createSearchQuery();
        /**
         * @var ActiveDataProvider|object $dataProvider
         */
        $dataProvider = Yii::createObject([
            'class'      => ActiveDataProvider::class,
            'query'      => $query,
            'sort'       => ['defaultOrder' => ['created_at' => SORT_DESC]],
            'pagination' => [
                'pageSize' => 20
            ],
        ]);

		$this->sort($dataProvider->sort);

		if (!($this->load($params) && $this->validate())) {
			return $dataProvider;
		}

        $query->andFilterWhere([
            's.id'        => $this->id,
            's.type'      => (string) $this->type,
            's.status'    => (string) $this->status,
            's.vendor_id' => $this->vendor_id
        ]);

		$query->filterText('s.name', $this->name);
		$query->filterText('v.name', $this->searchVendor);
		$query->andFilterWhere(['like','p.phone_nr', $this->phone]);
		$query->filterFullName(['a.street', 'a.district', 'a.city', 'a.province', 'a.postcode', 'a.locality'], $this->address);
		$query->filterDateRange('c.created_at', $this->createdFrom, $this->createdTo);
		$query->filterDateRange('c.updated_at', $this->updatedFrom, $this->updatedTo);
		return $dataProvider;
	}

	protected function sort(Sort $sort)
	{
        $sort->attributes['vendor'] = [
            'asc'  => ['v.name' => SORT_ASC],
            'desc' => ['v.name' => SORT_DESC],
        ];
	}

    /**
     * @param string|null $value
     */
	public function setVendor(string $value = null)
    {
        if ($value) {
            $vendorData = null;
            try {
                $vendorData = Json::decode($value);
            } catch (InvalidArgumentException $e) {
                // not a valid json just mute the error here to assign search value
            }

            if ($vendorData) {
                $vendor = Yii::$app->modelFactory->getVendor($vendorData);
                $vendor->afterFind();

                $this->populateRelation('vendor', $vendor);
                return;
            }
        }
        $this->searchVendor = $value;
    }

    /**
     * @return string|\yii\db\ActiveQuery|null
     */
    public function getVendor()
    {
        if ($this->searchVendor) {
            return $this->searchVendor;
        }
        return parent::getVendor();
    }

    /**
     * @return ActiveQuery
     * @throws \yii\base\Exception
     */
	protected function createSearchQuery()
	{
		$query = static::find();
		$query->select([
			's.*',
//			'TRIM(CONCAT_WS(\', \', a.street, a.district, a.city)) AS address',
			'row_to_json(a) AS address',
            'row_to_json(v) AS vendor',
            'CONCAT(p.country_code, p.phone_nr) AS phone'
		]);
		$query->from(['s' => Shop::tableName()]);

		$query->leftJoin(['v' => Vendor::tableName()], [
		    'and',
		    's.vendor_id = v.id',
            ['not', ['v.status' => VendorStatus::DELETED]]
        ]);

		$query->leftJoin(['a' => 'LATERAL('.
			(new Query())
				->select('aa.*')
				->from(['aa' => Address::tableName()])
				->where('aa.shop_id = s.id')
				->andWhere('aa.status != :status_deleted')
				->orderBy(['aa.id' => SORT_DESC . ' NULLS LAST','aa.updated_at' => SORT_DESC])
				->limit(1)
				->createCommand()
				->sql
			.')'
		], 'true', ['status_deleted' => ContactAttributeStatus::DELETED]);

		$query->leftJoin(['p' => 'LATERAL('.
			(new Query())
				->select('pp.*')
				->from(['pp' => Phone::tableName()])
				->where('pp.shop_id = s.id')
				->andWhere('pp.status != :status_deleted')
				->orderBy(['pp.id' => SORT_DESC . ' NULLS LAST','pp.updated_at' => SORT_DESC])
				->limit(1)
				->createCommand()
				->sql
			.')'
		], 'true', ['status_deleted' => ContactAttributeStatus::DELETED]);

        $query->where(['not', ['s.status' => ShopStatus::DELETED]]);

		return $query;
	}
}
