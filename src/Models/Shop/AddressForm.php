<?php

namespace Phycom\Backend\Models\Shop;

use Phycom\Base\Models\Shop;
use Phycom\Base\Models\AddressForm as BaseAddressForm;

use Yii;

/**
 * Class AddressForm
 *
 * @package Phycom\Backend\Models\Shop
 */
class AddressForm extends BaseAddressForm
{
	protected $shop;

	public function __construct(Shop $shop, array $config = [])
	{
		$this->shop = $shop;
		parent::__construct($config);
	}

	public function init()
    {
        parent::init();
        $this->country = Yii::$app->country->defaultCountry;
    }

    public function getModel()
	{
		return $this->shop;
	}
}
