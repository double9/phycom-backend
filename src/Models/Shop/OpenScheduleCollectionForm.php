<?php

namespace Phycom\Backend\Models\Shop;


use Phycom\Backend\Models\ActiveRecordCollectionForm;

use Phycom\Base\Models\Traits\ModelTrait;
use Phycom\Base\Models\Shop;
use Phycom\Base\Models\ShopOpen;

use yii\db\ActiveRecordInterface;
use yii;

/**
 * Class OpenScheduleForm
 * @package Phycom\Backend\Models\Shop
 *
 * @property-read Shop $shop
 * @property-read ShopOpen[] $models
 */
class OpenScheduleCollectionForm extends ActiveRecordCollectionForm
{
    use ModelTrait;

    protected $shop;

    public function __construct(Shop $shop, array $config = [])
    {
        $this->shop = $shop;
        parent::__construct($config);
    }

    public function getWeekdays()
    {
        return [
            'mon' => Yii::t('phycom/backend/shop', 'Monday'),
            'tue' => Yii::t('phycom/backend/shop', 'Tuesday'),
            'wed' => Yii::t('phycom/backend/shop', 'Wednesday'),
            'thu' => Yii::t('phycom/backend/shop', 'Thursday'),
            'fri' => Yii::t('phycom/backend/shop', 'Friday'),
            'sat' => Yii::t('phycom/backend/shop', 'Saturday'),
            'sun' => Yii::t('phycom/backend/shop', 'Sunday')
        ];
    }

    public function getModelClassName()
    {
        return ShopOpen::class;
    }

    protected function loadSavedModels()
    {
        if ($this->shop->isNewRecord) {
            return [];
        }
        $models = [];
        for ($day=1; $day<=7; $day++) {

            $model = ShopOpen::findOne(['shop_id' => $this->shop->id, 'day_of_week' => $day]);

            if (!$model) {
                $model = $this->createModel();
                $model->day_of_week = $day;
                $models[static::NEW_MODEL_KEY . $day] = $model;
            } else {
                $models[$model->id] = $model;
            }
        }
        return $models;
    }

    /**
     * @param ShopOpen|ActiveRecordInterface $model
     * @param array $attributes
     */
    protected function afterUpdateModelAttributes(ActiveRecordInterface $model, array $attributes = [])
    {
        $this->formatTime($model, 'opened_at');
        $this->formatTime($model, 'closed_at');
        $model->open = (bool) $model->open;
    }

    /**
     * @param ShopOpen $model
     * @param $attribute
     */
    private function formatTime($model, $attribute)
    {
        $value = $model->$attribute;
        if (!$value instanceof \DateTime) {
            if (is_string($value) && strlen($value)) {
                $dateTime = \DateTime::createFromFormat('H:i', $value, new \DateTimeZone('UTC'));
                if ($dateTime) {
                    $model->$attribute = $dateTime;
                    return;
                }
            }
            if (empty($value)) {
                $model->addError($attribute, Yii::t('phycom/backend/shop', '{attribute} cannot be empty', ['attribute' => $model->getAttributeLabel($attribute)]));
            } else {
                $model->addError($attribute, Yii::t('phycom/backend/shop', 'Invalid attribute format. {format} expected. ', ['format' => 'HH:MM']));
            }
        }
    }

    /**
     * @param ShopOpen|ActiveRecordInterface $model
     * @param mixed $key
     * @return bool
     */
    protected function beforeSaveModel(ActiveRecordInterface $model, $key)
    {
        return parent::beforeSaveModel($model, $key) && $model->open;
    }

    protected function getRelationMap()
    {
        return ['shop_id' => $this->shop->id];
    }

    protected function createModel(array $attributes = [])
    {
        /**
         * @var ShopOpen $model
         */
        $model = parent::createModel($attributes);
        if (!$this->shop->isNewRecord) {
            $model->shop_id = $this->shop->id;
            $model->populateRelation('shop', $this->shop);
        }
        return $model;
    }


}
