<?php

namespace Phycom\Backend\Models\Shop;


use Phycom\Base\Helpers\Date;
use Phycom\Base\Models\Traits\ModelTrait;
use Phycom\Base\Models\Shop;
use Phycom\Base\Models\ShopClosed;

use yii\base\InvalidCallException;
use yii\base\Model;
use Yii;

/**
 * Class ClosedScheduleForm
 * @package Phycom\Backend\Models\Shop
 *
 * @property-read Shop $shop
 */
class ClosedScheduleForm extends Model
{
    use ModelTrait;

    /**
     * @var string|null
     */
    public ?string $events = null;

    /**
     * @var Shop
     */
    protected Shop $shop;

    public function __construct(Shop $shop, array $config = [])
    {
        $this->shop = $shop;
        parent::__construct($config);
    }

    public function init()
    {
        parent::init();
        $this->loadEvents();
    }

    public function rules()
    {
        return [
            [['events'], 'string']
        ];
    }

    public function getShop(): Shop
    {
        return $this->shop;
    }

    /**
     * @return bool
     * @throws \yii\db\Exception
     */
    public function save(): bool
    {
        if ($this->validate()) {
            if ($this->shop->isNewRecord) {
                throw new InvalidCallException('Shop must be saved');
            }
            $transaction = Yii::$app->db->beginTransaction();
            try {
                $events = $this->parseEvents();
                $today = (new \DateTime())->setTime(0,0);
                Yii::$app->db->createCommand()->delete(ShopClosed::tableName(), [
                    'and',
                    ['shop_id' => $this->shop->id],
                    ['>=', 'date', $today->format('Y-m-d')]
                ])->execute();

                foreach ($events as $date) {
                    if (Date::create($date, 'Y-m-d') < $today) {
                        continue;
                    }

                    $model = new ShopClosed();
                    $model->shop_id = $this->shop->id;
                    $model->date = Date::create($date, 'Y-m-d', new \DateTimeZone('UTC'))->dateTime->setTime(0,0,0);
                    if (!$model->save()) {
                        Yii::error($model->errors, __METHOD__);
                        return $this->rollback($transaction, $model->errors, 'events');
                    }
                }
                $transaction->commit();
                return true;
            } catch (\Exception $e) {
                $transaction->rollBack();
                throw $e;
            }
        }
        return false;
    }

    protected function loadEvents()
    {
        $dates = [];
        $today = (new \DateTime())->setTime(0,0,0);
        foreach ($this->shop->shopClosed as $model) {
            if ($model->date >= $today) {
                $dates[] = $model->date->format('Y-m-d');
            }
        }
        $this->events = implode(', ', $dates);
    }
    /**
     * @return array
     */
    protected function parseEvents(): array
    {
        return !empty($this->events)
            ? array_map('trim', explode(',', $this->events))
            : [];
    }
}
