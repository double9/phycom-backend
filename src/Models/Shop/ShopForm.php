<?php

namespace Phycom\Backend\Models\Shop;

use Phycom\Backend\Models\PostForm;

use Phycom\Base\Models\AddressMeta;
use Phycom\Base\Models\Attributes\PostType;
use Phycom\Base\Models\Attributes\ShopType;
use Phycom\Base\Models\Attributes\VendorStatus;
use Phycom\Base\Models\Traits\ModelTrait;
use Phycom\Base\Models\Address;
use Phycom\Base\Models\Attributes\AddressType;
use Phycom\Base\Models\Attributes\ContactAttributeStatus;
use Phycom\Base\Models\Attributes\ShopStatus;
use Phycom\Base\Models\Email;
use Phycom\Base\Helpers\PhoneHelper as PhoneHelper;
use Phycom\Base\Models\Phone;
use Phycom\Base\Models\Shop;
use Phycom\Base\Models\Vendor;
use Phycom\Base\Modules\Delivery\Models\DeliveryArea;
use Phycom\Base\Modules\Delivery\Models\DeliveryAreaStatus;
use Phycom\Base\Modules\Delivery\Module as DeliveryModule;
use Phycom\Base\Validators\PhoneInputValidator;

use yii\base\InvalidConfigException;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\base\Model;
use Yii;

/**
 * Class ShopForm
 *
 * @package Phycom\Backend\Models\Shop
 *
 * @property-read Shop $shop
 * @property-read AddressForm $addressForm
 * @property-read OpenScheduleCollectionForm $openForm
 * @property-read ClosedScheduleForm $closedForm
 * @property-read SupplyScheduleCollectionForm $supplyForm
 * @property-read PostForm $contentForm
 */
class ShopForm extends Model
{
	use ModelTrait;

	public $name;
	public $type;
	public $vendorId;
	public $status;
	public $settings;
	public $email;
	public $phone;
	public $phoneNumber;
	public $address;
	public $deliveryArea;

	public $defaultClosedMessage;
	public $closedMessage;
	public $courierStart;
	public $courierEnd;

	protected $shop;
    protected $openForm;
	protected $closedForm;
	protected $supplyForm;
	protected $contentForm;

    /**
     * @var Vendor[]|null
     */
	protected ?array $vendors = null;

	public function __construct(Shop $shop = null, array $config = [])
	{
		$this->shop = $shop ?: Yii::$app->modelFactory->getShop(['vendor_id' => Yii::$app->vendor->id, 'status' => ShopStatus::INACTIVE]);
		parent::__construct($config);
	}

	public function init()
	{
		parent::init();
		$this->loadModelValues();
	}

    public function loadModelValues()
    {
        $this->name = $this->shop->name;
        $this->type = (string) $this->shop->type;
        $this->vendorId = !$this->shop->isNewRecord ? $this->shop->vendor_id : Yii::$app->vendor->id;
        $this->status = (string) $this->shop->status;
        $this->email = $this->shop->email->email ?? null;
        $this->phone = $this->shop->phone->phone_nr ?? null;
        $this->phoneNumber = $this->shop->phone->fullNumber ?? null;
        $this->address = $this->shop->address ? (string) $this->shop->address->export() : null;
        $this->deliveryArea = $this->shop->settings && isset($this->shop->settings['deliveryArea']) ? '1' : '0';
    }

	public function getShop()
	{
		return $this->shop;
	}


	public function rules()
	{
		return [
			[['name'], 'trim'],

			[['name', 'status', 'vendorId', 'address', 'phone'], 'required'],
			[['name', 'status', 'type', 'email', 'phone'], 'string', 'max' => 255],
			[['vendorId'], 'integer'],

			[['email'], 'trim'],
			[['email'], 'email'],

			[['phone'], 'trim'],
			[['phone'], PhoneInputValidator::class],
			[['phoneNumber'], 'string'],

            [['deliveryArea'], 'boolean'],

			[['closedMessage', 'defaultClosedMessage'], 'trim'],
			[['closedMessage', 'defaultClosedMessage', 'courierStart', 'courierEnd'], 'string'],
//			['defaultClosedMessage', 'required'],
		];
	}

	public function attributeLabels()
	{
        return [
            'name'         => Yii::t('phycom/backend/shop', 'Name'),
            'type'         => Yii::t('phycom/backend/shop', 'Type'),
            'vendorId'     => Yii::t('phycom/backend/shop', 'Vendor'),
            'status'       => Yii::t('phycom/backend/shop', 'Status'),
            'settings'     => Yii::t('phycom/backend/shop', 'Settings'),
            'email'        => Yii::t('phycom/backend/shop', 'Email'),
            'phone'        => Yii::t('phycom/backend/shop', 'Phone'),
            'address'      => Yii::t('phycom/backend/shop', 'Address'),
            'deliveryArea' => Yii::t('phycom/backend/shop', 'Is delivery area'),
        ];
	}

    /**
     * @return AddressForm
     */
	public function getAddressForm(): AddressForm
    {
        $addressForm = Yii::$app->modelFactory->getShopAddressForm($this->shop);
        if ($this->shop->address) {
            $addressForm->populate($this->shop->address);
            return $addressForm;
        }
        return $addressForm;
    }

    /**
     * @return OpenScheduleCollectionForm
     */
    public function getOpenForm(): OpenScheduleCollectionForm
    {
        if (!$this->openForm) {
            $this->openForm = Yii::$app->modelFactory->getShopOpenScheduleCollectionForm($this->shop);
        }
        return $this->openForm;
    }

    /**
     * @return ClosedScheduleForm
     */
    public function getClosedForm(): ClosedScheduleForm
    {
        if (!$this->closedForm) {
            $this->closedForm = Yii::$app->modelFactory->getShopClosedScheduleForm($this->shop);
        }
        return $this->closedForm;
    }

    /**
     * @return SupplyScheduleCollectionForm
     */
    public function getSupplyForm(): SupplyScheduleCollectionForm
    {
        if (!$this->supplyForm) {
            $this->supplyForm = Yii::$app->modelFactory->getShopSupplyScheduleCollectionForm($this->shop);
        }
        return $this->supplyForm;
    }

    /**
     * @return PostForm
     */
    public function getContentForm(): PostForm
    {
        if (!$this->contentForm) {
            $this->contentForm = new PostForm(PostType::create(PostType::SHOP), $this->shop->content);
            $this->contentForm->shopId = $this->shop->id;
        }
        return $this->contentForm;
    }

    /**
     * @return array
     */
    public function getAllVendors(): array
    {
        if (!isset($this->vendors)) {
            $this->vendors = Vendor::find()->where(['not', ['status' => VendorStatus::DELETED]])->all();
        }
        return ArrayHelper::map($this->vendors, 'id', 'name');
    }

	public function update()
	{
		if ($this->validate()) {

		    $transaction = Yii::$app->db->beginTransaction();
		    try {

                $this->shop->name = $this->name;
                $this->shop->vendor_id = $this->vendorId;
                $this->shop->status = ShopStatus::create($this->status);

                if (count(Yii::$app->commerce->shop->shopTypes) > 1) {
                    $this->shop->type = ShopType::create($this->type);
                } else {
                    $this->shop->type = ShopType::create(Yii::$app->commerce->shop->shopTypes[0]);
                }

                if (!$this->shop->save()) {
                    return $this->rollback($transaction, $this->shop->errors);
                }

                /**
                 * Update shop address
                 */
                if ($address = $this->shop->address) {
                    $address->updateJson($this->address);
                } else {
                    $address = Address::create($this->address);
                    $address->type = AddressType::MAIN;
                    $address->status = ContactAttributeStatus::UNVERIFIED;
                    $address->shop_id = $this->shop->id;
                }

                $address->on(ActiveRecord::EVENT_AFTER_UPDATE, fn($e) => $e->sender->geocode(true));
                $address->on(ActiveRecord::EVENT_AFTER_INSERT, fn($e) => $e->sender->geocode(true));

                if (!$address->save()) {
                    return $this->rollback($transaction, $address->errors);
                }

                if ($address) {
                    $this->deliveryArea ?
                        $this->updateDeliveryArea($address) :
                        $this->revokeDeliveryArea($address);
                }

                if ($this->email) {
                    /**
                     *  Update shop email
                     */
                    if (!$email = $this->shop->email) {
                        $email = new Email();
                        $email->status = ContactAttributeStatus::UNVERIFIED;
                        $email->shop_id = $this->shop->id;
                    }
                    $email->email = $this->email;
                    if (!$email->save()) {
                        return $this->rollback($transaction, $email->errors);
                    }
                }

                /**
                 *  Update shop phone
                 */
                if (!$phone = $this->shop->phone) {
                    $phone = new Phone();
                    $phone->status = ContactAttributeStatus::UNVERIFIED;
                    $phone->shop_id = $this->shop->id;
                }
                $phone->phone_nr = PhoneHelper::getNationalPhoneNumber($this->phoneNumber);
                $phone->country_code = PhoneHelper::getPhoneCode($this->phoneNumber);
                if (!$phone->save()) {
                    return $this->rollback($transaction, $phone->errors);
                }

                $transaction->commit();
                return true;

		    } catch (\Exception $e) {
		        $transaction->rollBack();
		        throw $e;
            }
		}
		return false;
	}

	protected function updateDeliveryArea(Address $address)
    {
        if (!$area = $this->findDeliveryArea()) {
            $area = Yii::createObject(DeliveryArea::class);
            $area->name = $this->name;
        }

        $addressField = $address->export();
        $addressField->name = $this->shop->name;

        $area->method = DeliveryModule::METHOD_SELF_PICKUP;
        $area->area = $addressField->toArray();
        $area->service = 'default';
        $area->price = 0;
        $area->status = DeliveryAreaStatus::ACTIVE;

        $area->area_code = $area->generateAreaCode();
        $area->code = $area->generateCode();

        if (!$area->save()) {
            Yii::error('Error saving shop ' . $this->shop->id . ' delivery area: ' . json_encode($area->errors), __METHOD__);
            return false;
        }
        $this->shop->settings->deliveryArea = $area->id;
        return $this->shop->update(false, ['settings']);
    }

    protected function revokeDeliveryArea(Address $address)
    {
        if ($area = $this->findDeliveryArea()) {
            $area->status = DeliveryAreaStatus::DISABLED;
            if ($area->update()) {
                return true;
            }
            Yii::error('Error revoking shop ' . $this->shop->id . ' delivery area ' . $area->id . ': ' . json_encode($area->errors), __METHOD__);
        }
        return false;
    }

    /**
     * @return null|DeliveryArea
     */
    private function findDeliveryArea()
    {
        if ($this->shop->settings->deliveryArea) {
            return DeliveryArea::findOne(['id' => $this->shop->settings->deliveryArea]);
        }
        return null;
    }
}
