<?php

namespace Phycom\Backend\Models;

use Phycom\Base\Models\Attributes\UserStatus;
use Phycom\Base\Models\Attributes\UserTokenType;
use Phycom\Base\Models\UserToken;
use yii\db\Query;
use yii;

/**
 * Class SearchUserInvitation
 * @package Phycom\Backend\Models
 */
class SearchUserInvitation extends SearchUser
{
	public $tokenCount;

	public function rules()
	{
		$rules = parent::rules();
		$rules[] = [['tokenCount'],'safe'];
		return $rules;
	}

	public function attributeLabels()
	{
		$labels = parent::attributeLabels();
		$labels['tokenCount'] = Yii::t('phycom/backend/user', 'Invitations');
		return $labels;
	}

	protected function createSearchQuery()
	{
		$query = parent::createSearchQuery();
		$query->addSelect([
			'token.count as tokenCount'
		]);
		$query->leftJoin(['token' => 'LATERAL('.
			(new Query())
				->select('count(*) as count')
				->from(['t' => UserToken::tableName()])
				->where('t.user_id = u.id AND t.type = :token_type')
				->createCommand()
				->sql
			.')'
		], 'true', ['token_type' => UserTokenType::REGISTRATION_REQUEST]);
		$query->addGroupBy(['token.count']);
		$query->andWhere(['u.status' => UserStatus::PENDING]);
		return $query;
	}
}
