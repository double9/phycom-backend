<?php

namespace Phycom\Backend\Models;

use yii;

/**
 * Class ChangePasswordForm
 * @package Phycom\Backend\Models
 */
class ChangePasswordForm extends \Phycom\Base\Models\ChangePasswordForm
{
	public $currentPassword;

	public function rules()
	{
		$rules = parent::rules();
		$rules[] = ['currentPassword','required'];
		$rules[] = ['currentPassword','validatePassword'];
		return $rules;
	}

	public function attributeLabels()
	{
		$labels = parent::attributeLabels();
		$labels['currentPassword'] = Yii::t('phycom/backend/user', 'Current password');
 		return $labels;
	}

	/**
	 * Validates the password.
	 * This method serves as the inline validation for password.
	 *
	 * @param string $attribute the attribute currently being validated
	 * @param array $params the additional name-value pairs given in the rule
	 */
	public function validatePassword($attribute, $params)
	{
		if (!$this->hasErrors()) {
			if (!$this->user->validatePassword($this->currentPassword)) {
				$this->addError($attribute, Yii::t('phycom/backend/user', 'Incorrect password'));
			}
		}
	}
}
