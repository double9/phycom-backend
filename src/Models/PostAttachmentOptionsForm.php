<?php

namespace Phycom\Backend\Models;

use Phycom\Base\Models\Traits\ModelTrait;
use Phycom\Base\Models\PostAttachment;

use yii\base\Model;
use yii;

/**
 * Class PostAttachmentForm
 * @package Phycom\Backend\Models
 *
 * @property array $meta
 *
 * @property-read PostAttachment $attachment
 */
class PostAttachmentOptionsForm extends Model
{
	use ModelTrait;

	public string $title = '';

	public string $alt = '';

	public string $caption = '';

	public string $description = '';

    /**
     * @var bool
     */
	public bool $smallScreen = false;

    /**
     * @var string
     */
	public string $link = '';

	protected PostAttachment $attachment;

	public function __construct(PostAttachment $attachment, array $config = [])
	{
		$this->attachment = $attachment;
		parent::__construct($config);
	}

	public function init()
    {
        parent::init();
        $this->loadAttachmentMeta();
    }

    public function rules()
	{
		return [
            [['title', 'alt', 'caption', 'description'], 'string'],
            [['smallScreen'], 'boolean'],
            [['link'], 'url']
		];
	}

	public function attributeLabels()
    {
        return [
            'title'       => Yii::t('phycom/backend/attachment', 'Title'),
            'alt'         => Yii::t('phycom/backend/attachment', 'Alt text'),
            'caption'     => Yii::t('phycom/backend/attachment', 'Caption'),
            'description' => Yii::t('phycom/backend/attachment', 'Description'),
            'smallScreen' => Yii::t('phycom/backend/attachment', 'Small screens'),
            'link'        => Yii::t('phycom/backend/attachment', 'Link')
        ];
    }

    public function getAttachment(): PostAttachment
	{
		return $this->attachment;
	}

	public function save(): bool
    {
        if ($this->validate()) {

            $meta = $this->attachment->meta ?: [];
            foreach ($this->attributes as $key => $value) {
                $meta[$key] = $value;
            }
            foreach ($meta as $key => $value) {
                if (is_string($value)) {
                    $value = trim($value);
                    if (!$value) {
                        unset($meta[$key]);
                    }
                }
            }
            $this->attachment->meta = !empty($meta) ? $meta : null;
            return $this->attachment->save();
        }
        return false;
    }

    /**
     * @return $this
     */
    protected function loadAttachmentMeta()
    {
        $attributes = $this->getAttributes();
        if ($this->attachment->meta) {
            foreach ($this->attachment->meta as $key => $value) {
                if (array_key_exists($key, $attributes)) {
                    $this->$key = $value;
                }
            }
        }
        return $this;
    }
}
