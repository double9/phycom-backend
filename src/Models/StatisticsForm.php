<?php

namespace Phycom\Backend\Models;

use Phycom\Base\Helpers\Date;

use yii\base\Model;
use yii;

/**
 * Class StatisticsForm
 * @package Phycom\Backend\Models
 */
class StatisticsForm extends Model
{
    /**
     * @var string|null
     */
    public ?string $from = null;

    /**
     * @var string|null
     */
    public ?string $to = null;


    public function rules()
    {
        return [
            [['from','to'], 'date', 'format' => Yii::$app->formatter->dateFormat]
        ];
    }

    public function attributeLabels()
    {
        return [
            'from' => Yii::t('phycom/backend/statistics', 'From'),
            'to'   => Yii::t('phycom/backend/statistics', 'To')
        ];
    }

    /**
     * @return \DateTime|null
     * @throws \Exception
     */
    public function getFromDate(): ?\DateTime
    {
        return $this->parseDate($this->from);
    }

    /**
     * @return \DateTime|null
     * @throws \Exception
     */
    public function getToDate(): ?\DateTime
    {
        return $this->parseDate($this->to);
    }

    /**
     * @param string|null $dateString
     * @return \DateTime|null
     */
    protected function parseDate(string $dateString = null): ?\DateTime
    {
        return !empty($dateString)
            ? Date::create($dateString, Yii::$app->formatter->dateFormat)->dateTime
            : null;
    }
}
