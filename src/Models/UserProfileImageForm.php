<?php

namespace Phycom\Backend\Models;

use Phycom\Base\Components\FileStorage;
use Phycom\Base\Models\Traits\ModelTrait;
use Phycom\Base\Models\Attributes\FileStatus;
use Phycom\Base\Models\Attributes\FileType;
use Phycom\Base\Models\Attributes\UserFileType;
use Phycom\Base\Models\File;
use Phycom\Base\Models\User;
use Phycom\Base\Models\UserFile;
use yii\base\Model;
use yii;

/**
 * Class UserProfileImageForm
 * @package Phycom\Backend\Models
 *
 * @property-read array $allowedExtensions
 * @property-read int $maxFileSize
 * @property-read int $maxFiles
 */
class UserProfileImageForm extends Model
{
	use ModelTrait;
	/**
	 * @var yii\web\UploadedFile
	 */
	public $avatar;

	protected $user;

	public function __construct(User $user, array $config = [])
	{
		$this->user = $user;
		parent::__construct($config);
	}

	public function rules()
	{
		return [
			[
				'avatar',
				'file',
				'skipOnEmpty' => false,
				'extensions' => $this->allowedExtensions,
				'maxSize' => $this->maxFileSize,
				'maxFiles' => $this->maxFiles,
				'enableClientValidation' => false
			]
		];
	}

	public function getAllowedExtensions()
	{
		return ['png', 'jpg', 'gif'];
	}

	public function getMaxFiles()
	{
		return 1;
	}

	public function getMaxFileSize()
	{
		return 1024*1024;
	}


	public function upload()
	{
		if ($this->validate()) {

			$transaction = Yii::$app->db->beginTransaction();
			try {

				$currentAvatar = $this->user->avatarImage;

				$file = new File();
				$file->mime_type = new FileType($this->avatar->type);
				$file->name = $this->avatar->name;
				$file->filename = time() . '_' . Yii::$app->security->generateRandomString(32) . '.' . $this->avatar->extension;
				$file->bucket = FileStorage::BUCKET_AVATARS;
				$file->status = FileStatus::PROCESSING;
				$file->created_by = Yii::$app->user->id;

				if (!$file->save()) {
					$transaction->rollBack();
					$this->setErrors($file->errors);
					return false;
				}

				if ($currentAvatar && !$currentAvatar->delete()) {
					Yii::error('Unable to remove user ' . $this->user->id . ' current avatar ' . $currentAvatar->id, __METHOD__);
				}

				$file->filename = $file->id . '_' . $file->filename;
				$file->status = FileStatus::VISIBLE;
				if (!$file->save()) {
					$transaction->rollBack();
					$this->setErrors($file->errors);
					return false;
				}

				$userFile = new UserFile();
				$userFile->file_id = $file->id;
				$userFile->user_id = $this->user->id;
				$userFile->type = UserFileType::AVATAR;

				if (!$userFile->save()) {
					$transaction->rollBack();
					$this->setErrors($userFile->errors);
					return false;
				}

				unset($this->user->profileImage);

				$bucket = Yii::$app->fileStorage->getBucket($file->bucket);
				$bucket->copyFileIn($this->avatar->tempName, $file->filename);
				if (!$bucket->fileExists($file->filename)) {
					$transaction->rollBack();
					$this->addError('error', Yii::t('phycom/backend/error', 'Error saving file to bucket'));
					return false;
				}

				$transaction->commit();
				return $bucket->getFileUrl($file->filename);

			} catch (\Exception $e) {
				$transaction->rollBack();
				throw $e;
			}

		} else {
			return false;
		}
	}
}
