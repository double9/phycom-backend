<?php

namespace Phycom\Backend\Models\Product;

use Phycom\Base\Components\ActiveQuery;
use Phycom\Base\Models\Product\ProductParam;
use Phycom\Base\Models\Traits\SearchQueryFilter;
use Phycom\Base\Interfaces\SearchModelInterface;

use yii\data\ActiveDataProvider;
use yii\data\Sort;
use yii;

/**
 * Class SearchProductParam
 * @package Phycom\Backend\Models\Product
 */
class SearchProductParam extends ProductParam implements SearchModelInterface
{
	use SearchQueryFilter;

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     * @return ActiveDataProvider
     * @throws yii\base\Exception
     */
	public function search(array $params = [])
	{
		$query = $this->createSearchQuery();
        $dataProvider = new ActiveDataProvider([
            'query'      => $query,
            'sort'       => ['defaultOrder' => ['created_at' => SORT_ASC]],
            'pagination' => ['pageSize' => 40],
        ]);

		$this->sort($dataProvider->sort);
		return $dataProvider;
	}

	protected function sort(Sort $sort)
	{

	}

    /**
     * @return ActiveQuery
     * @throws yii\base\Exception
     */
	protected function createSearchQuery()
	{
		$query = static::find();
		$query->select(['pp.*']);
		$query->from(['pp' => static::tableName()]);
        $query->andWhere(['pp.product_id' => $this->product_id]);

		return $query;
	}
}
