<?php

namespace Phycom\Backend\Models\Product;

use Phycom\Backend\Models\StrictModelAttributes;
use Phycom\Base\Models\Product\Param;

use yii\base\InvalidValueException;
use yii\base\InvalidConfigException;
use yii\base\Model;
use Yii;

/**
 * @property Param $param
 */
class ParamForm extends Model
{
    use StrictModelAttributes;

    /**
     * @var bool
     */
    public bool $isEnabled = true;

    /**
     * @var string|null
     */
    public ?string $label;

    /**
     * @var Param|null
     */
    protected ?Param $param;


    public function __construct(Param $param = null, $config = [])
    {
        $this->label = $param?->label;
        $this->param = $param;
        parent::__construct($config);
    }

    /**
     * @param Param|string $value
     * @throws InvalidConfigException
     */
    public function setParam(Param|string $value): void
    {
        if ($value instanceof Param) {
            $this->param = $value;
            return;
        } else if (is_string($value) && $param = Yii::$app->modelFactory->getParam()::findByName($value)) {
            $this->param = $param;
        }
        throw new InvalidValueException('Invalid param');
    }


    public function getParam(): ?Param
    {
        return $this->param;
    }


    public function rules()
    {
        return [
            [['isEnabled'], 'boolean', 'trueValue' => true, 'falseValue' => false, 'strict' => true],
            [['param'], 'safe']
        ];
    }

    public function attributeLabels()
    {
        return [
            'isEnabled' => Yii::t('phycom/backend/settings', 'Is Enabled'),
            'param'   => Yii::t('phycom/backend/product', 'Param'),
        ];
    }
}
