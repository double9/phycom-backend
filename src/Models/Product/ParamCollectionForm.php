<?php

namespace Phycom\Backend\Models\Product;


use Phycom\Backend\Models\ModelCollectionForm;
use Phycom\Base\Models\Product\Param;
use Phycom\Base\Models\Setting;

use Yii;

/**
 * @method ParamForm createModel(array $attributes = [])
 * @method ParamForm[] getModels()
 * @property ParamForm[] $models
 */
class ParamCollectionForm extends ModelCollectionForm
{
    public array $productParamConfig = [];

    /**
     * @var Variant[]
     */
    protected array $productParams = [];

    /**
     * @var string
     */
    protected $productParamEnabledPrefix = 'productParamEnabled__';


    protected function loadProductParams(): void
    {
        $this->productParams = Yii::$app->modelFactory->getParam()::findAll();
    }

    public function getModelDefinition(): array
    {
        return [
            'class' => ParamForm::class
        ];
    }

    protected function loadSavedModels(): array
    {
        $this->loadProductParams();
        $prefix = $this->productParamEnabledPrefix;
        $paramsEnabled = Setting::findByKeyPrefix($prefix, fn($key) => explode($prefix, $key)[1]);
        $models = [];

        foreach ($this->productParams as $productParam) {

            $isEnabled = array_key_exists($productParam->name, $paramsEnabled) && $paramsEnabled[$productParam->name];

            $model = $this->createModel(['isEnabled' => $isEnabled]);
            $model->setParam($productParam);

            $models[$productParam->name] = $model;
        }

        return $models;
    }

    /**
     * @return bool
     */
    public function update(): bool
    {
        if (!$this->validate()) {
            return false;
        }
        foreach ($this->getModels() as $model) {
            $key = $this->productParamEnabledPrefix . $model->getParam()->name;
            $value = $model->isEnabled ? '1' : '0';
            Setting::set($key, $value);
        }
        return true;
    }
}
