<?php

namespace Phycom\Backend\Models\Product;

use Phycom\Base\Components\ActiveQuery;
use Phycom\Base\Models\Attributes\UnitType;
use Phycom\Base\Models\Traits\SearchQueryFilter;
use Phycom\Base\Interfaces\SearchModelInterface;

use yii\data\ActiveDataProvider;
use yii\data\Sort;
use yii\helpers\ArrayHelper;
use yii;

/**
 * Class SearchProductPrice
 * @package Phycom\Backend\Models\Product
 */
class SearchProductPrice extends ProductPrice implements SearchModelInterface
{
	use SearchQueryFilter;

    /**
     * @param array $columnOptions
     * @return array
     * @throws yii\base\InvalidConfigException
     */
	public function getOptionFieldColumns(array $columnOptions = [])
	{
		$columns = [];
		foreach ($this->getAllPriceVariations() as $priceOption) {

		    if ($priceOption->variant->unitPrice && $priceOption->variant->unit) {
		        $unit = UnitType::create($priceOption->variant->unit);
                $label = '<div class="variant">' . $priceOption->variant->label . '</div><div class="unit">1 ' . $unit->getUnitLabel() . '</div>';
                $encodeLabel = false;
            } else if ($priceOption->variant_name === $priceOption->option_key) {
                $label = $priceOption->label;
                $encodeLabel = true;
            } else {
                $label = '<div class="variant">' . $priceOption->variant->label . '</div><div class="option">' . $priceOption->label . '</div>';
                $encodeLabel = false;
            }

            $columns[] = ArrayHelper::merge([
                'label'       => $label,
                'encodeLabel' => $encodeLabel,
                'attribute'   => $priceOption->key,
                'format'      => 'currencyValue',
                'options'     => ['width' => 100]
            ], $columnOptions);
		}
		return $columns;
	}

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     * @return ActiveDataProvider
     * @throws yii\base\Exception
     */
	public function search(array $params = [])
	{
		$query = $this->createSearchQuery();
		$dataProvider = new ActiveDataProvider([
			'query' => $query,
			'sort' => [
			    'defaultOrder' => [
			        'num_units' => SORT_ASC,
			        'created_at' => SORT_ASC
                ]
            ],
			'pagination' => [
				'pageSize' => 40
			],
		]);

		$this->sort($dataProvider->sort);
		return $dataProvider;
	}

	protected function sort(Sort $sort)
	{

	}

    /**
     * @return ActiveQuery
     * @throws yii\base\Exception
     */
	protected function createSearchQuery()
	{
		$query = static::find();
		$query->select(['pp.*']);
		$query->from(['pp' => static::tableName()]);
        $query->andWhere(['pp.product_id' => $this->product_id]);

		return $query;
	}
}
