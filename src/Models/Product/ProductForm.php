<?php

namespace Phycom\Backend\Models\Product;

use Phycom\Backend\Helpers\ProductCategoryLabel;

use Phycom\Base\Models\Attributes\VendorStatus;
use Phycom\Base\Models\Product\ProductVendorRelation;
use Phycom\Base\Models\Traits\ModelTrait;
use Phycom\Base\Models\Attributes\ProductStatus;
use Phycom\Base\Models\Product\Product;
use Phycom\Base\Models\Product\ProductCategory;
use Phycom\Base\Models\Product\ProductCategoryProductRelation;
use Phycom\Base\Models\Product\ProductTag;
use Phycom\Base\Models\User;

use Phycom\Base\Models\Vendor;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use Yii;

/**
 * Class ProductForm
 * @package Phycom\Backend\Models\Product
 *
 * @property-read Product $product
 * @property-read array $allCategories
 * @property-read array $productTags
 * @property-read array $allTags
 * @property-read array $allVendors
 * @property-read ProductAttachmentForm $attachmentForm
 * @property-read ProductTranslationForm $translationForm
 * @property-read ProductVariantForm $variantForm
 * @property-read ProductParamForm $paramForm
 * @property-read ProductPricingForm $pricingForm
 * @property-read ProductPricingOptionsForm $pricingOptionsForm
 */
class ProductForm extends Model
{
	use ModelTrait;

	public $sku;

	public $status;

	public $categories = [];

	public $tags = [];

	public $vendors = [];

	/**
	 * @var Product
	 */
	protected $product;
    /**
     * @var User
     */
	protected $user;
    /**
     * @var ProductAttachmentForm
     */
	protected $attachmentForm;
    /**
     * @var ProductTranslationForm
     */
	protected $translationForm;
    /**
     * @var ProductVariantForm
     */
	protected $variantForm;
    /**
     * @var ProductParamForm
     */
	protected $paramForm;
    /**
     * @var ProductPricingForm
     */
	protected $pricingForm;
    /**
     * @var ProductPricingOptionsForm
     */
	protected $pricingOptionsForm;

	/**
	 * ProductForm constructor.
	 *
	 * @param Product|null $product
     * @param User|null $user
	 * @param array $config
	 */
	public function __construct(Product $product = null, User $user = null, array $config = [])
	{
		$this->product = $product ?? Yii::$app->modelFactory->getProduct();
		$this->user = $user;
		parent::__construct($config);
	}

	public function rules()
	{
		return [
			[['sku'], 'required'],
			[['sku', 'status'], 'string', 'max' => 255],
			['status', 'in', 'range' => ProductStatus::all()],
			[['categories', 'tags', 'vendors'], 'safe'],
			['categories', 'validateCategories'],
			['tags', 'in', 'range' => Yii::$app->modelFactory->getProductTag()::allTags(), 'allowArray' => true]
		];
	}

	public function attributeLabels()
	{
        return [
            'sku'        => $this->product->getAttributeLabel('sku'),
            'status'     => $this->product->getAttributeLabel('status'),
            'categories' => Yii::t('phycom/backend/main', 'Product categories'),
            'tags'       => Yii::t('phycom/backend/main', 'Product tags'),
            'vendors'    => Yii::t('phycom/backend/main', 'Vendors')
        ];
	}

	public function init()
	{
		parent::init();

		if ($this->product->isNewRecord) {
			$this->product->loadDefaultValues();
		}

		$this->sku = $this->product->sku;
		$this->status = (string)$this->product->status;
		$this->categories = $this->getProductCategories();
		$this->tags = $this->getProductTags();
		$this->vendors = $this->getProductVendors();
	}

    /**
     * @return Product
     */
	public function getProduct()
    {
        return $this->product;
    }

    /**
     * @return ProductAttachmentForm
     */
	public function getAttachmentForm()
    {
        return $this->attachmentForm ?: $this->attachmentForm = new ProductAttachmentForm($this->product);
    }

    /**
     * @return ProductTranslationForm
     */
    public function getTranslationForm()
    {
        return $this->translationForm ?: $this->translationForm = new ProductTranslationForm($this->product);
    }

    /**
     * @return ProductParamForm
     */
    public function getParamForm()
    {
        return $this->paramForm ?: $this->paramForm = new ProductParamForm($this->product, $this->user);
    }

    /**
     * @return ProductVariantForm
     */
    public function getVariantForm()
    {
        return $this->variantForm ?: $this->variantForm = new ProductVariantForm($this->product, $this->user);
    }

    /**
     * @return ProductPricingForm
     */
    public function getPricingForm()
    {
        return $this->pricingForm ?: $this->pricingForm = new ProductPricingForm($this->product);
    }

    /**
     * @return ProductPricingOptionsForm
     */
    public function getPricingOptionsForm()
    {
        return $this->pricingOptionsForm ?: $this->pricingOptionsForm = new ProductPricingOptionsForm($this->product);
    }

	public function getProductCategories(): array
	{
		$model = new SearchProductCategory();
		$model->language = Yii::$app->lang->current;

		$dataProvider = $model->search();
		$dataProvider->query->andWhere(['c.id' => ArrayHelper::getColumn($this->product->categories, 'id')]);

		return ArrayHelper::map($dataProvider->getModels(), 'id', 'title');
	}

	public function getAllCategories(): array
	{
        return (new ProductCategoryLabel())->generateLabels();
	}

	public function getProductTags(): array
	{
		return ArrayHelper::map($this->product->visibleTags, 'value', 'label');
	}

    public function getProductVendors(): array
    {
        return ArrayHelper::map($this->product->vendors, 'id', 'name');
    }

    public function getAllVendors(): array
    {
        return ArrayHelper::map(Vendor::find()->where(['not', ['status' => [VendorStatus::DELETED]]])->all(), 'id', 'name');
    }

	public function getAllTags(): array
	{
		return ArrayHelper::map(Yii::$app->modelFactory->getProductTag()::visible(), 'value', 'label');
	}

	public function validateCategories($attribute)
	{
		foreach ($this->$attribute as $key => $categoryId) {
			if (!$category = ProductCategory::findOne($categoryId)) {
				$this->addError($attribute, Yii::t('phycom/backend/error', 'Category {id} not found', ['id' => $categoryId]));
			}
		}
	}

    /**
     * @param array|null $attributes
     * @return array
     * @throws yii\base\InvalidConfigException
     */
	public function exportFormAttributes(array $attributes = null)
    {
        $result = [];
        $formName = $this->formName();
        foreach ($this->getAttributes($attributes) as $attribute => $value) {

            if (!empty($value) && is_array($value)) {
                if ($attribute === 'categories') {
                    foreach (ArrayHelper::filter($this->getAllCategories(), $value) as $id => $title) {
                        $result[$formName . "[$attribute][$id]"] = (string)$title;
                    }
                    continue;
                }
                if ($attribute === 'tags') {
                    foreach (ArrayHelper::filter($this->getAllTags(), $value) as $id => $label) {
                        $result[$formName . "[$attribute][$id]"] = (string)$label;
                    }
                    continue;
                }
                if ($attribute === 'vendors') {
                    foreach (ArrayHelper::filter($this->getAllVendors(), $value) as $id => $name) {
                        $result[$formName . "[$attribute][$id]"] = (string)$name;
                    }
                    continue;
                }
            }

            $result[$formName . "[$attribute]"] = (string) $value;
        }
        return $result;
    }

	public function update()
	{
		if ($this->validate()) {

			$transaction = Yii::$app->db->beginTransaction();
			try {

				$this->product->sku = $this->sku;
				$this->product->status = new ProductStatus($this->status);
				$this->product->vendor_id = Yii::$app->user->vendor->id;

                if ($this->user && $this->product->isNewRecord) {
                    $this->product->created_by = $this->user->id;
                }

				if (!$this->product->save()) {
					return $this->rollback($transaction, $this->product->errors);
				}

				$this->updateCategories($transaction);
				$this->updateTags($transaction);
				$this->updateVendors($transaction);

				$transaction->commit();
				return true;

			} catch (\Exception $e) {
				$transaction->rollBack();
				throw $e;
			}
		}
		return false;
	}


	protected function updateCategories(yii\db\Transaction $transaction)
	{
		// remove category relations
		$deleteCondition = ['and', ['product_id' => $this->product->id]];
		if (!empty($this->categories)) {
			$deleteCondition[] = ['not in', 'category_id', $this->categories];
		}
		Yii::$app->db->createCommand()->delete(ProductCategoryProductRelation::tableName(), $deleteCondition)->execute();

		if ($this->categories) {
			// reassign categories
			foreach ($this->categories as $categoryId) {

				$categoryRelation = ProductCategoryProductRelation::findOne(['product_id' => $this->product->id, 'category_id' => $categoryId]);

				if (!$categoryRelation) {
					$categoryRelation = new ProductCategoryProductRelation();
					$categoryRelation->category_id = (int)$categoryId;
					$categoryRelation->product_id = $this->product->id;

					if (!$categoryRelation->save()) {
						return $this->rollback($transaction, $categoryRelation->errors);
					}
				}
			}
		}
	}


	protected function updateTags(yii\db\Transaction $transaction)
	{
		// remove existing product tags
		$deleteCondition = ['and', ['product_id' => $this->product->id]];
		if (!empty($this->tags)) {
			$deleteCondition[] = ['not in', 'value', $this->tags];
		}
		Yii::$app->db->createCommand()->delete(ProductTag::tableName(), $deleteCondition)->execute();

		if ($this->tags) {
			// reassign categories
			foreach ($this->tags as $tag) {
				$productTag = Yii::$app->modelFactory->getProductTag()::findOne(['product_id' => $this->product->id, 'value' => $tag]);
				if (!$productTag) {
					$productTag = Yii::$app->modelFactory->getProductTag();
					$productTag->value = $tag;
					$productTag->product_id = $this->product->id;
					$productTag->created_by = Yii::$app->user->id;

					if (!$productTag->save()) {
						return $this->rollback($transaction, $productTag->errors);
					}
				}
			}
		}
	}


    protected function updateVendors(yii\db\Transaction $transaction)
    {
        // remove vendor relations
        $deleteCondition = ['and', ['product_id' => $this->product->id]];
        if (!empty($this->vendors)) {
            $deleteCondition[] = ['not in', 'vendor_id', $this->vendors];
        }

        Yii::$app->db->createCommand()->delete(ProductVendorRelation::tableName(), $deleteCondition)->execute();

        if ($this->vendors) {
            // reassign vendors
            foreach ($this->vendors as $vendorId) {

                $vendorRelation = ProductVendorRelation::findOne(['product_id' => $this->product->id, 'vendor_id' => $vendorId]);

                if (!$vendorRelation) {
                    $vendorRelation = new ProductVendorRelation();
                    $vendorRelation->vendor_id = (int)$vendorId;
                    $vendorRelation->product_id = $this->product->id;

                    if (!$vendorRelation->save()) {
                        return $this->rollback($transaction, $vendorRelation->errors);
                    }
                }
            }
        }
    }
}
