<?php

namespace Phycom\Backend\Models\Product;

use Phycom\Base\Components\ActiveQuery;
use Phycom\Base\Models\Product\ProductVariant;
use Phycom\Base\Models\Traits\SearchQueryFilter;
use Phycom\Base\Interfaces\SearchModelInterface;

use yii\data\ActiveDataProvider;
use yii\data\Sort;

/**
 * Class SearchProductVariant
 * @package Phycom\Backend\Models\Product
 */
class SearchProductVariant extends ProductVariant implements SearchModelInterface
{
	use SearchQueryFilter;

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     * @return ActiveDataProvider
     * @throws \yii\base\Exception
     */
	public function search(array $params = [])
	{
		$query = $this->createSearchQuery();
        $dataProvider = new ActiveDataProvider([
            'query'      => $query,
            'sort'       => ['defaultOrder' => ['created_at' => SORT_ASC]],
            'pagination' => ['pageSize' => 40],
        ]);

		$this->sort($dataProvider->sort);
		return $dataProvider;
	}

	protected function sort(Sort $sort)
	{

	}

    /**
     * @return ActiveQuery
     * @throws \yii\base\Exception
     */
	protected function createSearchQuery()
	{
		$query = static::find();
		$query->select(['pv.*']);
		$query->from(['pv' => static::tableName()]);
        $query->andWhere(['pv.product_id' => $this->product_id]);

		return $query;
	}
}
