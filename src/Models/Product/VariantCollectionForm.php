<?php

namespace Phycom\Backend\Models\Product;


use Phycom\Backend\Models\ModelCollectionForm;
use Phycom\Base\Models\Product\Variant;
use Phycom\Base\Models\Setting;

use Yii;

/**
 * @method VariantForm createModel(array $attributes = [])
 * @method VariantForm[] getModels()
 * @property VariantForm[] $models
 */
class VariantCollectionForm extends ModelCollectionForm
{
    public array $productVariantConfig = [];

    /**
     * @var Variant[]
     */
    protected array $productVariants = [];

    /**
     * @var string
     */
    protected $productVariantEnabledPrefix = 'productVariantEnabled__';


    protected function loadProductVariants(): void
    {
        $this->productVariants = Yii::$app->modelFactory->getVariant()::findAll();
    }

    public function getModelDefinition(): array
    {
        return [
            'class' => VariantForm::class
        ];
    }

    protected function loadSavedModels(): array
    {
        $this->loadProductVariants();
        $prefix = $this->productVariantEnabledPrefix;
        $variantsEnabled = Setting::findByKeyPrefix($prefix, fn($key) => explode($prefix, $key)[1]);
        $models = [];

        foreach ($this->productVariants as $productVariant) {

            $isEnabled = array_key_exists($productVariant->name, $variantsEnabled) && $variantsEnabled[$productVariant->name];

            $model = $this->createModel(['isEnabled' => $isEnabled]);
            $model->setVariant($productVariant);

            $models[$productVariant->name] = $model;
        }

        return $models;
    }

    /**
     * @return bool
     */
    public function update(): bool
    {
        if (!$this->validate()) {
            return false;
        }
        foreach ($this->getModels() as $model) {
            $key = $this->productVariantEnabledPrefix . $model->getVariant()->name;
            $value = $model->isEnabled ? '1' : '0';
            Setting::set($key, $value);
        }
        return true;
    }
}
