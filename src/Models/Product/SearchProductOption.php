<?php

namespace Phycom\Backend\Models\Product;

use Phycom\Base\Components\ActiveQuery;
use Phycom\Base\Models\Traits\SearchQueryFilter;
use Phycom\Base\Interfaces\SearchModelInterface;
use Phycom\Base\Models\Product\ProductVariant;

use yii\data\ActiveDataProvider;
use yii\data\Sort;
use yii;

/**
 * Class SearchProductOption
 * @package Phycom\Backend\Models\Product
 */
class SearchProductOption extends ProductVariant implements SearchModelInterface
{
	use SearchQueryFilter;


	/**
	 * Creates data provider instance with search query applied
	 * @param array $params
	 * @return ActiveDataProvider
	 */
	public function search(array $params = [])
	{
		$query = $this->createSearchQuery();
		$dataProvider = new ActiveDataProvider([
			'query' => $query,
			'sort' => ['defaultOrder' => ['created_at' => SORT_DESC]],
			'pagination' => [
				'pageSize' => 40
			],
		]);

		$this->sort($dataProvider->sort);
		return $dataProvider;
	}

	protected function sort(Sort $sort)
	{

	}

	/**
	 * @return ActiveQuery
	 */
	protected function createSearchQuery()
	{
		$query = static::find();
		$query->select(['o.*']);
		$query->from(['o' => static::tableName()]);

		return $query;
	}
}
