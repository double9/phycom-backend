<?php

namespace Phycom\Backend\Models\Product;


use Phycom\Base\Models\Product\Product;
use Phycom\Base\Models\Product\ProductParam;
use Phycom\Base\Models\User;

use yii\db\ActiveRecordInterface;

/**
 * Class ProductParamForm
 * @package Phycom\Backend\Models\Product
 *
 * @property ProductParam[] $models
 */
class ProductParamForm extends ProductModelCollectionForm
{
    /**
     * @var User
     */
	protected $user;

	/**
	 * ProductParamForm constructor.
     *
	 * @param Product $product
     * @param User|null $user
	 * @param array $config
	 */
	public function __construct(Product $product, User $user = null, array $config = [])
	{
		$this->user = $user;
		parent::__construct($product, $config);
	}

    /**
     * @return string|ProductParam
     */
    public function getModelClassName()
    {
        return ProductParam::class;
    }

    /**
     * @return array|ProductParam[]
     */
    protected function loadSavedModels()
    {
        return $this->product->isNewRecord ? [] : $this->product->params;
    }

    /**
     * @param ActiveRecordInterface|ProductParam $model
     * @param mixed $key
     * @return bool
     */
    protected function beforeSaveModel(ActiveRecordInterface $model, $key)
    {
        if ($model->isNewRecord && $existingModel = $this->findModelByCondition(['name' => $model->name, 'product_id' => $this->product->id])) {
            $existingModel->delete();
        }
        if ($this->user) {
            if ($model->isNewRecord) {
                $model->created_by = $this->user->id;
            }
            $model->updated_by = $this->user->id;
        }
        return parent::beforeSaveModel($model, $key);
    }
}
