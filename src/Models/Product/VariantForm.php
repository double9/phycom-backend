<?php

namespace Phycom\Backend\Models\Product;

use Phycom\Backend\Models\StrictModelAttributes;
use Phycom\Base\Models\Product\Variant;

use yii\base\InvalidValueException;
use yii\base\InvalidConfigException;
use yii\base\Model;
use Yii;

/**
 * @property Variant $variant
 */
class VariantForm extends Model
{
    use StrictModelAttributes;

    /**
     * @var bool
     */
    public bool $isEnabled = true;

    /**
     * @var string|null
     */
    public ?string $label;

    /**
     * @var Variant|null
     */
    protected ?Variant $variant;


    public function __construct(Variant $variant = null, $config = [])
    {
        $this->label = $variant?->label;
        $this->variant = $variant;
        parent::__construct($config);
    }

    /**
     * @param Variant|string $value
     * @throws InvalidConfigException
     */
    public function setVariant(Variant|string $value): void
    {
        if ($value instanceof Variant) {
            $this->variant = $value;
            return;
        } else if (is_string($value) && $variant = Yii::$app->modelFactory->getVariant()::findByName($value)) {
            $this->variant = $variant;
        }
        throw new InvalidValueException('Invalid variant');
    }


    public function getVariant(): ?Variant
    {
        return $this->variant;
    }


    public function rules()
    {
        return [
            [['isEnabled'], 'boolean', 'trueValue' => true, 'falseValue' => false, 'strict' => true],
            [['variant'], 'safe']
        ];
    }

    public function attributeLabels()
    {
        return [
            'isEnabled' => Yii::t('phycom/backend/settings', 'Is Enabled'),
            'variant'   => Yii::t('phycom/backend/product', 'Variant'),
        ];
    }
}
