<?php

namespace Phycom\Backend\Models;

use yii;

/**
 * Class AddressFieldForm
 * @package Phycom\Backend\Models
 *
 * @property-read null $model
 */
class AddressFieldForm extends \Phycom\Base\Models\AddressForm
{
    public function init()
    {
        parent::init();
        $this->country = Yii::$app->country->defaultCountry;
    }

    public function getModel()
    {
        return null;
    }

    public function update()
    {

    }
}
