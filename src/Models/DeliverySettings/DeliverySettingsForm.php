<?php

namespace Phycom\Backend\Models\DeliverySettings;


use Phycom\Base\Models\Traits\ModelTrait;

use Phycom\Base\Modules\Delivery\Models\Settings;
use Phycom\Base\Modules\Delivery\Module as DeliveryModule;

use yii\base\Model;
use Yii;

/**
 * Class DeliverySettingsForm
 *
 * @package Phycom\Backend\Models\DeliverySettings
 */
class DeliverySettingsForm extends Model
{
    use ModelTrait;

    /**
     * @var string|null
     */
    public ?string $nonDeliveryDays = null;


    public function init()
    {
        parent::init();
        $this->loadNonDeliveryDays();
    }

    /**
     * @return array[]
     */
    public function rules()
    {
        return [
            [['nonDeliveryDays'], 'string']
        ];
    }

    public function attributeHints()
    {
        return [
            'nonDeliveryDays' => Yii::t('phycom/backend/delivery', 'Here you can mark days when all delivery is suspended')
        ];
    }

    /**
     * @return bool
     * @throws \yii\base\InvalidConfigException
     */
    public function update(): bool
    {
        if ($this->validate()) {
            /**
             * @var DeliveryModule $deliveryModule
             */
            $deliveryModule = Yii::$app->getModule('delivery');
            /**
             * @var Settings $settingsForm
             */
            $settingsForm = $deliveryModule->getSettings();
            $settingsForm->nonDeliveryDays = $this->parseNonDeliveryDays();
            return $settingsForm->save();
        }
        return false;
    }

    protected function loadNonDeliveryDays()
    {
        /**
         * @var DeliveryModule $deliveryModule
         */
        $deliveryModule = Yii::$app->getModule('delivery');
        $this->nonDeliveryDays = implode(', ', $deliveryModule->nonDeliveryDays);
    }


    /**
     * @return array
     */
    protected function parseNonDeliveryDays(): array
    {
        return !empty($this->nonDeliveryDays)
            ? array_map('trim', explode(',', $this->nonDeliveryDays))
            : [];
    }
}
