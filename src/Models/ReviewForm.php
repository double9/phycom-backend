<?php

namespace Phycom\Backend\Models;


use Phycom\Base\Models\Review;
use Phycom\Base\Models\Traits\ModelTrait;
use yii\base\Model;

/**
 * Class ReviewForm
 * @package Phycom\Backend\Models
 */
class ReviewForm extends Model
{
    use ModelTrait;

    public $status;

    protected $review;

    public function __construct(Review $review, array $config = [])
    {
        $this->review = $review;
        parent::__construct($config);
    }


}
