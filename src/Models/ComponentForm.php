<?php

namespace Phycom\Backend\Models;

use Phycom\Base\Interfaces\PersistableConfigurationInterface;
use Phycom\Base\Interfaces\PhycomComponentInterface;
use Phycom\Base\Models\Traits\ModelTrait;

use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use Yii;


class ComponentForm extends Model
{
    use ModelTrait;

    public ?string $config = null;

    /**
     * @var PhycomComponentInterface|PersistableConfigurationInterface
     */
    protected PhycomComponentInterface $component;

    /**
     * @param PhycomComponentInterface $component
     * @param $config
     */
    public function __construct(PhycomComponentInterface $component, $config = [])
    {
        $this->component = $component;
        parent::__construct($config);
    }

    public function init()
    {
        parent::init();
        $this->config = $this->component->serialize();
    }

    public function rules()
    {
        return [
            [['config'], 'safe']
        ];
    }

    /**
     * @return PhycomComponentInterface
     */
    public function getComponent(): PhycomComponentInterface
    {
        return $this->component;
    }

    public function load($data, $formName = null)
    {
        $currentConfig = Json::decode($this->config);
        $result = parent::load($data, $formName);

        if ($result) {
            // perserve null values
            $config = Json::decode($this->config);
            foreach ($currentConfig as $key => $value) {
                if (!array_key_exists($key, $config)) {
                    $config[$key] = null;
                }
            }
            $this->config = Json::encode($config);
        }

        return $result;
    }

    /**
     * @return bool
     */
    public function save(): bool
    {
        if ($this->validate()) {
            $config = Json::decode($this->config);
            foreach ($config as $property => $value) {
                if (property_exists($this->component, $property)) {
                    $this->component->$property = $value;
                }
            }
            return $this->component->persistConfig();
        }
        return false;
    }
}