<?php

namespace Phycom\Backend\Models\Schedule;

use Phycom\Backend\Models\ModelCollectionForm;


/**
 * Class ScheduleWithTimeRanges
 *
 * @package Phycom\Backend\Models\Schedule
 *
 * @property TimeRange[] $models
 * @method TimeRange createModel(array $attributes = [])
 *
 */
abstract class ScheduleWithTimeRanges extends ModelCollectionForm
{
    /**
     * @return string[]
     */
    public function getModelDefinition(): array
    {
        return [
            'class' => TimeRange::class
        ];
    }
}
