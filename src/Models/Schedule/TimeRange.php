<?php

namespace Phycom\Backend\Models\Schedule;

use yii\base\InvalidArgumentException;
use yii\base\InvalidConfigException;
use yii\base\Model;
use Yii;


/**
 * Class TimeRange
 * @package hycom\Backend\Models\Schedule
 *
 * @property-read string $key
 * @property-read string $label
 */
class TimeRange extends Model
{
    const DELIMITER = '-';

    /**
     * @var string|null
     */
    public ?string $from;

    /**
     * @var string|null
     */
    public ?string $to;

    /**
     * @var int
     */
    public int $addDays = 0;

    /**
     * @param string $key
     * @return static|object
     * @throws InvalidConfigException|InvalidArgumentException
     */
    public static function create(string $key)
    {
        $times = explode(self::DELIMITER, $key);

        if (count($times) !== 2) {
            throw new InvalidArgumentException('Invalid ' . __CLASS__ . ' definition ' . $key);
        }

        return Yii::createObject([
            'class' => static::class,
            'from'  => $times[0],
            'to'    => $times[1]
        ]);
    }

    public function rules()
    {
        return [
            [['from', 'to'], 'string']
        ];
    }

    public function attributeLabels()
    {
        return [
            'from'    => Yii::t('phycom/backend/schedule', 'From'),
            'to'      => Yii::t('phycom/backend/schedule', 'To'),
            'addDays' => Yii::t('phycom/backend/schedule', 'Days'),
        ];
    }

    /**
     * @return string
     */
    public function getKey(): string
    {
        return $this->from . self::DELIMITER . $this->to;
    }

    /**
     * @return string
     */
    public function getLabel(): string
    {
        return Yii::t('phycom/modules/delivery', '{from} to {to}', ['from' => $this->from, 'to' => $this->to]);
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->getKey();
    }
}
