<?php

namespace Phycom\Backend\Models\Comment;


use Phycom\Base\Models\Comment;
use Phycom\Base\Models\Traits\ModelTrait;
use Phycom\Base\Validators\RequiredOneValidator;

use yii\base\Model;

/**
 * Class CommentBulkUpdateForm
 * @package Phycom\Backend\Models\Comment
 */
class CommentBulkUpdateForm extends Model
{
    use ModelTrait;

    public $status;
    public $keys = [];

    public function rules()
    {
        return [
            [['keys'], 'required'],
            [['status'], RequiredOneValidator::class],
            [['keys'], 'each', 'rule' => ['integer']],
            [['status'], 'string']
        ];
    }

    public function attributeLabels()
    {
        $model = new Comment();
        return [
            'status' => $model->getAttributeLabel('status'),
        ];
    }

    public function update()
    {
        if ($this->validate()) {
            $count = 0;
            foreach (Comment::find()->where(['id' => $this->keys])->batch() as $comments) {
                /**
                 * @var Comment[] $comments
                 */
                foreach ($comments as $comment) {

                    if (!empty($this->status)) {
                        $comment->status = $this->status;
                    }

                    if ($comment->save()) {
                        $count++;
                    } else {
                        $errors = $comment->getFirstErrors();
                        foreach ($errors as $attribute => $error) {
                            $this->addError('keys', $error);
                        }
                    }
                }
            }
            return $count;
        }
        return false;
    }
}
