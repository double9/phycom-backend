<?php

namespace Phycom\Backend\Models\Comment;

use Phycom\Base\Components\ActiveQuery;
use Phycom\Base\Models\PostComment;
use Phycom\Base\Models\Product\ProductComment;
use Phycom\Base\Models\Traits\SearchQueryFilter;
use Phycom\Base\Interfaces\SearchModelInterface;
use Phycom\Base\Models\Comment;

use yii\data\ActiveDataProvider;
use yii\data\Sort;

/**
 * Class SearchComment
 * @package Phycom\Backend\Models
 */
class SearchComment extends Comment implements SearchModelInterface
{
	use SearchQueryFilter;

    public $productId;
    public $postId;

	public $createdFrom;
	public $createdTo;
	public $updatedFrom;
	public $updatedTo;

	public function rules()
	{
		return [
			[['id','productId', 'postId', 'created_by', 'approved_by', 'parent_id'], 'integer'],
			[['content', 'author_agent'], 'string'],
			[['author_name', 'author_email', 'author_ip'], 'string', 'max' => 255],
			[['created_at', 'updated_at', 'createdFrom','createdTo','updatedFrom','updatedTo','status'],'safe']
		];
	}

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     * @return ActiveDataProvider
     * @throws \yii\base\Exception
     */
	public function search(array $params = [])
	{
		$query = $this->createSearchQuery();
		$dataProvider = new ActiveDataProvider([
			'query' => $query,
			'sort' => ['defaultOrder' => ['created_at' => SORT_DESC]],
			'pagination' => [
				'pageSize' => 20
			],
		]);

		$this->sort($dataProvider->sort);

		if (!($this->load($params) && $this->validate())) {
			return $dataProvider;
		}

		$query->andFilterWhere([
			'c.id' => $this->id,
			'c.status' => (string)$this->status,
			'c.author_ip' => $this->author_ip,
			'c.author_email' => $this->author_email,
            'p.product_id' => $this->productId,
            'po.post_id' => $this->postId
		]);

		$query->filterText('c.content', $this->content);
		$query->filterText('c.author_name', $this->author_name);
		$query->filterDateRange('c.created_at', $this->createdFrom, $this->createdTo);
		$query->filterDateRange('c.updated_at', $this->updatedFrom, $this->updatedTo);
		return $dataProvider;
	}

	protected function sort(Sort $sort)
	{

	}

    /**
     * @return ActiveQuery
     * @throws \yii\base\Exception
     */
	protected function createSearchQuery()
	{
		$query = static::find();
		$query->select([
			'c.*',
            'p.product_id AS "productId"',
            'pp.post_id AS "postId"'
		]);
		$query->from(['c' => Comment::tableName()]);
        $query->leftJoin(['p' => ProductComment::tableName()], 'p.comment_id = c.id');
        $query->leftJoin(['pp' => PostComment::tableName()], 'pp.comment_id = c.id');
		return $query;
	}
}
