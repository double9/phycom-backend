<?php

namespace Phycom\Backend\Models;

use Phycom\Base\Components\ActiveQuery;
use Phycom\Base\Models\Traits\SearchQueryFilter;
use Phycom\Base\Interfaces\SearchModelInterface;
use Phycom\Base\Models\Attributes\DiscountRuleStatus;
use Phycom\Base\Models\DiscountRule;

use yii\data\ActiveDataProvider;
use yii\data\Sort;
use Yii;

/**
 * Class SearchDiscountCard
 * @package Phycom\Backend\Models
 */
class SearchDiscountRule extends DiscountRule implements SearchModelInterface
{
	use SearchQueryFilter;

	public $createdFrom;
	public $createdTo;
	public $updatedFrom;
	public $updatedTo;
	public $expiresFrom;
	public $expiresTo;
	public $birthdayFrom;
	public $birthdayTo;

	public function rules()
	{
		return [
			[['shop_id', 'used', 'created_by'], 'integer'],
			[['discount_rate', 'birthday_discount_rate'], 'number'],
			[[
				'birthday',
				'expires_at',
				'created_at',
				'updated_at',
				'createdFrom',
				'createdTo',
				'updatedFrom',
				'updatedTo',
				'expiresFrom',
				'expiresTo',
				'birthdayFrom',
				'birthdayTo',
				'type',
				'status'
			], 'safe'],
			[['code', 'name'], 'string', 'max' => 255],

		];
	}

	/**
	 * Creates data provider instance with search query applied
	 * @param array $params
	 * @return ActiveDataProvider
	 */
	public function search(array $params = [])
	{
		$query = $this->createSearchQuery();
		$dataProvider = new ActiveDataProvider([
			'query' => $query,
			'sort' => ['defaultOrder' => ['created_at' => SORT_DESC]],
			'pagination' => [
				'pageSize' => 20
			],
		]);

		$this->sort($dataProvider->sort);

		if (!($this->load($params) && $this->validate())) {
			return $dataProvider;
		}

		$query->andFilterWhere([
			'd.id' => $this->id,
			'd.status' => (string)$this->status,
			'd.type' => (string)$this->type,
			'd.shop_id' => $this->shop_id,
			'd.discount_rate' => $this->discount_rate,
			'd.birthday_discount_rate' => $this->birthday_discount_rate
		]);

        $query->andFilterWhere(['ilike', 'd.code', $this->code]);
		$query->filterText('d.name', $this->name);
		$query->filterDateRange('d.created_at', $this->createdFrom, $this->createdTo);
		$query->filterDateRange('d.updated_at', $this->updatedFrom, $this->updatedTo);
		$query->filterDateRange('d.expires_at', $this->expiresFrom, $this->expiresTo);
		$query->filterDateRange('d.birthday', $this->birthdayFrom, $this->birthdayTo);
		return $dataProvider;
	}

	protected function sort(Sort $sort)
	{

	}

    /**
     * @return ActiveQuery
     * @throws \yii\base\Exception
     */
	protected function createSearchQuery()
	{
		$query = static::find();
		$query->select(['d.*']);
		$query->from(['d' => Yii::$app->modelFactory->getDiscountRule()::tableName()]);
		$query->where(['not', ['d.status' => DiscountRuleStatus::DELETED]]);
		if ($this->type) {
			$query->andWhere(['d.type' => $this->type]);
		}
		return $query;
	}
}
