<?php

namespace Phycom\Backend\Models;

use Phycom\Base\Models\Traits\ModelTrait;
use Phycom\Base\Events\ModelUpdateEvent;

use yii\base\InvalidConfigException;
use yii\base\Model;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * A Base class for a multi model form setup encapsulating a collection of models.
 *
 * Class ModelCollectionForm
 * @package Phycom\Backend\Models
 *
 * @property Model[] $models
 */
abstract class ModelCollectionForm extends Model
{
    const EVENT_AFTER_CREATE_MODEL = 'afterCreateModel';
    const EVENT_AFTER_UPDATE_MODEL_ATTRIBUTES = 'afterUpdateModelAttributes';
    const EVENT_AFTER_POPULATE_MODELS = 'afterPopulateModels';

    const TEMPLATE_MODEL_KEY = '__tpl__';
    const NEW_MODEL_KEY = 'new';

    use ModelTrait;

    /**
     * @var Model[]|null
     */
    private array|null $models = null;


    protected $clearModelValidationErrors = true;


    public function rules()
    {
        return [
            ['models', 'safe']
        ];
    }

    /**
     * The class name of the model used in $this->models collection
     * @return array
     */
    abstract public function getModelDefinition(): array;

    /**
     * @return Model[]
     */
    abstract protected function loadSavedModels(): array;


    /**
     * @return Model[]
     */
    public function getModels(): array
    {
        if (null === $this->models) {
            $this->models = $this->loadSavedModels();
            $this->trigger(self::EVENT_AFTER_POPULATE_MODELS);
        }
        return $this->models;
    }

    /**
     * @return Model[]
     */
    protected function getChangedModels(): array
    {
        $changed = [];
        foreach ($this->getModels() as $key => $model) {
            if ($this->hasChanged($model)) {
                $changed[$key] = $model;
            }
        }
        return $changed;
    }

    /**
     * @param Model $model
     * @return bool
     */
    protected function hasChanged(Model $model): bool
    {
        return true;
    }


    public function afterValidate()
    {
        $models = $this->getChangedModels();
        $valid = true;
        /* @var $model Model */
        foreach ($models as $model) {
            $valid = $model->validate(null, $this->clearModelValidationErrors) && $valid;
        }

        if (!$valid) {
            foreach ($models as $model) {
                if ($model->hasErrors()) {
                    $this->setErrors($model->errors, 'error');
                    break;
                }
            }
        }
        parent::afterValidate();
    }


    /**
     * @param array|null $modelAttributes
     * @return array
     * @throws InvalidConfigException
     */
    public function exportFormAttributes(array $modelAttributes = null)
    {
        $result = [];
        $formName = $this->formName();
        $index = 0;
        foreach ($this->getModels() as $modelKey => $model) {
            foreach ($model->getAttributes() as $attribute => $value) {
                if (null === $modelAttributes || in_array($attribute, $modelAttributes)) {

                    $resultKey = $formName . "[models][$index][$attribute]";
                    // for example some product params can be arrays. TODO test and make sure it will work with nested arrays
                    if (is_array($value)) {
                        foreach ($value as $arrayKey => $arrayValue) {
                            $result[$resultKey . "[$arrayKey]"] = (string) $arrayValue;
                        }
                    } else {
                        $result[$resultKey] = $this->exportModelAttribute($model, $attribute);
                    }
                }
            }

            $index++;
        }
        return $result;
    }

    /**
     * Exports single Model attribute
     *
     * @param Model $model
     * @param string $attribute
     *
     * @return mixed
     */
    protected function exportModelAttribute(Model $model, $attribute)
    {
        return (string) $model->$attribute;
    }

    /**
     * @param Model $model
     * @param string $attribute
     */
    protected function initModelAttribute(Model $model, $attribute)
    {

    }

    /**
     * @param array $models
     * @throws InvalidConfigException
     */
    public function setModels(array $models)
    {
        unset($models[static::TEMPLATE_MODEL_KEY]); // remove the hidden row used to generate rows
        $modelClassName = $this->getModelDefinition()['class'];
        $result = [];

        foreach ($models as $key => $value) {
            if (is_array($value)) {

                if (!$model = $this->getModelByKey($key)) {
                    $model = $this->createModel();
                }

                if ($this->clearModelValidationErrors) {
                    $model->clearErrors();
                    $this->clearModelValidationErrors = false;
                }

                $model->setAttributes($value);
                $this->afterUpdateModelAttributes($model, $value);
                $result[$key] = $model;

            } elseif ($value instanceof $modelClassName) {
                $result[$key] = $value;
            }
        }
        $this->models = $result;
    }

    /**
     * Called after model attributes are updated from user input
     *
     * @param Model $model
     * @param array $attributes
     */
    protected function afterUpdateModelAttributes(Model $model, array $attributes = [])
    {
        foreach ($attributes as $attribute => $value) {
            $this->initModelAttribute($model, $attribute);
        }
        $this->trigger(self::EVENT_AFTER_UPDATE_MODEL_ATTRIBUTES, new ModelUpdateEvent(['model' => $model, 'attributes' => $attributes]));
    }

    /**
     * @param mixed $key
     * @return Model|null
     */
    protected function findModelByKey($key): ?Model
    {
        return null;
    }

    /**
     * Creates a new model
     *
     * @param array $attributes
     * @return Model
     * @throws InvalidConfigException
     */
    protected function createModel(array $attributes = []): Model
    {
        /**
         * @var Model|object $model
         */
        $model = Yii::createObject($this->getModelDefinition());

        if (!empty($attributes)) {
            $model->attributes = $attributes;
        }

        $this->afterCreateModel($model);
        return $model;
    }

    /**
     * @param Model $model
     */
    protected function afterCreateModel(Model $model)
    {
        $this->trigger(self::EVENT_AFTER_CREATE_MODEL, new ModelUpdateEvent(['model' => $model]));
    }

    /**
     * Finds the model from storage by key
     *
     * @param $key
     * @return Model|null
     */
    protected function getModelByKey($key): ?Model
    {
        $model = $key && strpos($key, static::NEW_MODEL_KEY) === false ? $this->findModelByKey($key) : false;
        if (!$model) {
            $model = $this->getModels()[$key] ?? null;
        }
        return $model;
    }
}
