<?php

namespace Phycom\Backend\Models;

use Phycom\Base\Models\User;
use Phycom\Base\Models\AddressForm;

/**
 * Class UserAddressForm
 * @package Phycom\Backend\Models
 *
 * @property User $model
 */
class UserAddressForm extends AddressForm
{
	protected $user;

	public function __construct(User $user, array $config = [])
	{
		$this->user = $user;
		parent::__construct($config);
	}

	public function getModel()
	{
		return $this->user;
	}
}
