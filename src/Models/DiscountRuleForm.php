<?php

namespace Phycom\Backend\Models;

use Phycom\Backend\Models\Product\SearchProductCategory;
use Phycom\Backend\Helpers\ProductCategoryLabel;

use Phycom\Base\Helpers\f;
use Phycom\Base\Models\Attributes\DiscountRuleStatus;
use Phycom\Base\Models\Traits\ModelTrait;
use Phycom\Base\Models\Attributes\DiscountRuleType;
use Phycom\Base\Models\DiscountRule;
use Phycom\Base\Models\DiscountRuleCategory;
use Phycom\Base\Models\User;

use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use yii\base\Model;
use Yii;

/**
 * Class ClientCardForm
 * @package Phycom\Backend\model
 *
 * @property-read User $user
 * @property-read array $timezones
 * @property-read array $languages
 * @property-read array $allCategories
 *
 * @property-read DiscountRule $discountRule
 */
class DiscountRuleForm extends Model
{
    const DEFAULT_EXPIRES_IN = 'P1Y';
    const DEFAULT_RATE = '5.00';

	use ModelTrait;

	public string $code;
	public string $name;
	public string $personalCode;
	public string $rate;
	public string $expires;
    public string $birthday;
	public string $birthdayRate;
	public array $categories = [];
	public int $count = 1;

	/**
	 * @var DiscountRule
	 */
	protected DiscountRule $model;

	public function __construct(DiscountRule $discountCard, array $config = [])
	{
		$this->model = $discountCard;
		parent::__construct($config);
	}

    public function init()
    {
        parent::init();

        if ($this->model->isNewRecord) {
            $this->model->loadDefaultValues();
        }

        $this->code = $this->model->code ?: '';
        $this->name = $this->model->name ?: '';
        $this->personalCode = $this->model->personal_code ?: '';
        $this->rate = $this->model->discount_rate ? bcmul($this->model->discount_rate, '100', 2) : static::DEFAULT_RATE;
        $this->expires = $this->getExpireDate();
        $this->birthday = $this->model->birthday ? f::date($this->model->birthday) : '';
        $this->birthdayRate = $this->model->birthday_discount_rate ? bcmul($this->model->birthday_discount_rate, '100', 2) : $this->rate;
        $this->categories = $this->getProductCategories();
    }


	public function rules()
	{
		return [
		    [['name'], 'required', 'when' => function ($model, $attribute) {
                /**
                 * @var DiscountRuleForm $model
                 */
                return $model->discountRule->type->is(DiscountRuleType::CLIENT_CARD);
            }],
            ['code', 'validateUniqueCode', 'when' => function ($model, $attribute) {
                /**
                 * @var DiscountRuleForm $model
                 */
                return $model->getDiscountRule()->isNewRecord || $model->code !== $model->getDiscountRule()->code;
            }],
		    [['rate'], 'required'],
		    [['rate', 'birthdayRate'], 'number', 'max' => 100, 'min' => 0],
            [['code', 'name', 'personalCode'], 'string', 'max' => 255],
            [['birthday', 'expires'], 'date'],
            [['categories'], 'safe'],
            [['count'], 'integer'],
            [['categories'], 'validateCategories'],
        ];
	}

    public function attributeLabels()
    {
        return [
            'code'         => $this->model->getAttributeLabel('code'),
            'name'         => $this->model->getAttributeLabel('name'),
            'personalCode' => $this->model->getAttributeLabel('personal_code'),
            'rate'         => $this->model->getAttributeLabel('discount_rate'),
            'expires'      => $this->model->getAttributeLabel('expires_at'),
            'birthday'     => $this->model->getAttributeLabel('birthday'),
            'birthdayRate' => $this->model->getAttributeLabel('birthday_discount_rate'),
            'categories'   => Yii::t('phycom/backend/product', 'Product categories'),
            'count'        => Yii::t('phycom/backend/discount-rule', 'Count'),
        ];
    }


    public function attributeHints()
    {
        return [
            'code'         => Yii::t('phycom/backend/discount-rule', 'Unique code used when purchasing. Automatically generated if left empty.'),
            'count'        => Yii::t('phycom/backend/discount-rule', 'Number of records to generate'),
            'personalCode' => Yii::t('phycom/backend/discount-rule', 'National Personal Identification Number'),
            'rate'         => Yii::t('phycom/backend/discount-rule', 'Discount rate %'),
            'expires'      => Yii::t('phycom/backend/discount-rule', 'Default expiry time is {interval}', ['interval' => f::humanInterval(new \DateInterval(static::DEFAULT_EXPIRES_IN))]),
            'birthday'     => Yii::t('phycom/backend/discount-rule', 'Birthday date can be left blank when personal code is used'),
            'birthdayRate' => Yii::t('phycom/backend/discount-rule', 'Discount rate % on birthday week'),
            'categories'   => Yii::t('phycom/backend/discount-rule', 'Specify which product categories can be discounted. If empty all categories apply'),
        ];
    }

    public function getDiscountRule()
    {
        return $this->model;
    }

    public function getExpireDate()
    {
        $dateTime = $this->model->expires_at ?: (new \DateTime())->add(new \DateInterval(static::DEFAULT_EXPIRES_IN));
        return f::date($dateTime);
    }

    public function getProductCategories()
    {
        $model = new SearchProductCategory();
        $model->language = Yii::$app->lang->current;
        // $model->vendorId = Yii::$app->vendor->id;

        $dataProvider = $model->search();
        $dataProvider->query->andWhere(['c.id' => ArrayHelper::getColumn($this->model->categories, 'product_category_id')]);

        return ArrayHelper::map($dataProvider->getModels(), 'id', 'title');
    }

    public function getAllCategories()
    {
        return (new ProductCategoryLabel())->generateLabels();
    }

    public function validateUniqueCode($attribute)
    {
        $exists = DiscountRule::find()
            ->where(['code' => $this->$attribute])
            ->andWhere(['not in', 'status', [DiscountRuleStatus::DELETED]])
            ->one();

        if ($exists) {
            $this->addError($attribute, Yii::t('phycom/backend/error', '{attribute} "{value}" is already in use', [
                'attribute' => $this->attributeLabels()[$attribute],
                'value' => $this->$attribute
            ]));
        }
    }

    public function validateCategories($attribute)
    {
        foreach ($this->$attribute as $key => $categoryId) {
            if (!Yii::$app->modelFactory->getProductCategory()::findOne(['id' => $categoryId])) {
                $this->addError($attribute, Yii::t('phycom/backend/error', 'Category {id} was not found', ['id' => $categoryId]));
            }
        }
    }

    public function setAttributes($values, $safeOnly = true)
    {
        if (is_array($values) && isset($values['categories']) && empty($values['categories']) ) {
            $values['categories'] = [];
        }
        parent::setAttributes($values, $safeOnly);
    }

    /**
	 * @return int|bool
	 * @throws \Exception
	 */
	public function update()
	{
		if ($this->validate()) {
			$transaction = Yii::$app->db->beginTransaction();
			try {

                $this->model->code = $this->code ?: $this->model->generateCode();
                $this->model->name = $this->name;
                $this->model->personal_code = $this->personalCode;
                $this->model->discount_rate = bcdiv($this->rate, '100', 2);
                if ($this->birthday) {
                    $this->model->birthday = new \DateTime($this->birthday);
                    $this->model->birthday_discount_rate = $this->birthdayRate
                        ? bcdiv($this->birthdayRate, '100', 2)
                        : $this->model->discount_rate;
                }
                if ($this->expires) {
                    $this->model->expires_at = new \DateTime($this->expires);
                }
                if (!$this->model->vendor_id) {
                    $this->model->vendor_id = Yii::$app->vendor->id;
                }
                if ($this->model->isNewRecord) {
                    $this->model->used = 0;
                    $this->model->created_by = Yii::$app->user->id;
                }
                if (!$this->model->save()) {
                    return $this->rollback($transaction, $this->model->errors);
                }
                $this->updateCategories($transaction, $this->model->id);
                $transaction->commit();
                return true;

			} catch (\Exception $e) {
				$transaction->rollBack();
				throw $e;
			}
		}
		return false;
	}

	public function create()
    {
        if ($this->validate()) {
            $model = null;
            for ($i=0; $i < $this->count; $i++) {
                $model = $this->createModel();
            }
            $this->model = $model;
            return $i;
        }
        return false;
    }

    protected function createModel()
    {
        $transaction = Yii::$app->db->beginTransaction();
        try {
            $model = Yii::$app->modelFactory->getDiscountRule();
            $model->attributes = $this->model->attributes;

            $model->code = ($this->code && $this->count === 1) ? $this->code : $model->generateCode();
            $model->name = $this->name;
            $model->personal_code = $this->personalCode;
            $model->discount_rate = bcdiv($this->rate, '100', 2);
            if ($this->birthday) {
                $model->birthday = new \DateTime($this->birthday);
                $model->birthday_discount_rate = $this->birthdayRate
                    ? bcdiv($this->birthdayRate, '100', 2)
                    : $this->model->discount_rate;
            }
            if (!$model->vendor_id) {
                $model->vendor_id = Yii::$app->vendor->id;
            }
            if ($this->expires) {
                $model->expires_at = new \DateTime($this->expires);
            }
            $model->used = 0;
            $model->created_by = Yii::$app->user->id;

            if (!$model->save()) {
                return $this->rollback($transaction, $model->errors);
            }
            $this->updateCategories($transaction, $model->id);

            $transaction->commit();
            return $model;

        } catch (\Exception $e) {
            $transaction->rollBack();
            throw $e;
        }
    }


    protected function updateCategories(yii\db\Transaction $transaction, $modelId)
    {
        // remove category relations
        $deleteCondition = ['and', ['discount_rule_id' => $modelId]];
        if (!empty($this->categories)) {
            $deleteCondition[] = ['not in', 'product_category_id', $this->categories];
        }
        Yii::$app->db->createCommand()->delete(DiscountRuleCategory::tableName(), $deleteCondition)->execute();

        if ($this->categories) {

            // reassign categories
            foreach ($this->categories as $categoryId) {

                $categoryRelation = DiscountRuleCategory::findOne(['discount_rule_id' => $modelId, 'product_category_id' => $categoryId]);

                if (!$categoryRelation) {
                    $categoryRelation = new DiscountRuleCategory();
                    $categoryRelation->product_category_id = (int)$categoryId;
                    $categoryRelation->discount_rule_id = $modelId;

                    if (!$categoryRelation->save()) {
                        return $this->rollback($transaction, $categoryRelation->errors);
                    }
                }
            }
        }
    }
}
