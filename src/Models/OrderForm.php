<?php

namespace Phycom\Backend\Models;


use Phycom\Base\Models\Attributes\OrderStatus;
use Phycom\Base\Models\Traits\ModelTrait;
use Phycom\Base\Models\Order;

use yii\base\Model;
use yii;

/**
 * Class OrderForm
 * @package Phycom\Backend\Models
 *
 * @property-read Order $order
 */
class OrderForm extends Model
{
	use ModelTrait;

    /**
     * @var string
     */
	public $promotionCode;

    /**
     * @var string
     */
	public $status;

    /**
     * @var Order|null
     */
	protected $order;

	public function __construct(Order $order = null, array $config = [])
	{
		$this->order = $order ?: Yii::$app->modelFactory->getOrder();
		parent::__construct($config);
	}

	public function rules()
    {
        return [
            ['status', 'in', 'range' => OrderStatus::all()],
        ];
    }

    public function getOrder()
	{
		return $this->order;
	}

    /**
     * @return array
     * @throws yii\base\InvalidConfigException
     */
	public function getUpdatableStatuses(): array
    {
        if ($this->order->status->in([OrderStatus::CLOSED, OrderStatus::CANCELED, OrderStatus::DELETED])) {
            return [];
        }

        $exclude = [
            OrderStatus::PENDING_PAYMENT => [
                OrderStatus::NEW
            ],
            OrderStatus::PAYMENT_COMPLETE => [
                OrderStatus::NEW,
                OrderStatus::PENDING_PAYMENT
            ],
            OrderStatus::PENDING => [
                OrderStatus::NEW,
                OrderStatus::PENDING_PAYMENT,
                OrderStatus::PAYMENT_COMPLETE
            ],
            OrderStatus::PROCESSING => [
                OrderStatus::NEW,
                OrderStatus::PENDING_PAYMENT,
                OrderStatus::PAYMENT_COMPLETE,
                OrderStatus::PENDING
            ],
            OrderStatus::PROCESSING_COMPLETE => [
                OrderStatus::NEW,
                OrderStatus::PENDING_PAYMENT,
                OrderStatus::PAYMENT_COMPLETE,
                OrderStatus::PROCESSING
            ],
            OrderStatus::SHIPPED => [
                OrderStatus::NEW,
                OrderStatus::EXPIRED,
                OrderStatus::PENDING_PAYMENT,
                OrderStatus::PAYMENT_COMPLETE,
                OrderStatus::PROCESSING,
                OrderStatus::PROCESSING_COMPLETE,
                OrderStatus::PARTIALLY_SHIPPED
            ],
            OrderStatus::PARTIALLY_SHIPPED => [
                OrderStatus::NEW,
                OrderStatus::EXPIRED,
                OrderStatus::PENDING_PAYMENT,
                OrderStatus::PAYMENT_COMPLETE,
                OrderStatus::PROCESSING,
                OrderStatus::PROCESSING_COMPLETE
            ],
            OrderStatus::ON_HOLD => [
                OrderStatus::NEW
            ]
        ];

        $all = OrderStatus::all();
        $status = (string)$this->order->status;
        if (isset($exclude[$status])) {
            $statuses = array_diff($all, $exclude[$status]);
        } else {
            $statuses = $all;
        }

        $result = [];
        foreach ($statuses as $item) {
            if ($status !== $item) {
                $result[$item] = OrderStatus::create($item)->label;
            }
        }

        return $result;
    }


    public function update(): bool
    {
        $this->order->status = $this->status;
        return $this->order->save();
    }

}
