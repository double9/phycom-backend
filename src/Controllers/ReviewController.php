<?php

namespace Phycom\Backend\Controllers;


use Phycom\Backend\Models\Review\ReviewBulkUpdateForm;
use Phycom\Backend\Models\Review\SearchReview;
use Phycom\Base\Helpers\FlashMsg;
use Phycom\Base\Models\Attributes\ReviewStatus;
use Phycom\Base\Models\Review;

use yii\web\NotFoundHttpException;
use yii;

/**
 * Class ReviewController
 * @package Phycom\Backend\Controllers
 */
class ReviewController extends BaseController
{
    /**
     * @param yii\base\Action $action
     * @return bool
     * @throws NotFoundHttpException
     */
    public function beforeAction($action)
    {
        $this->checkEnabled(Yii::$app->reviews);
        return parent::beforeAction($action);
    }

    /**
     * @return string
     * @throws yii\base\Exception
     * @throws yii\web\ForbiddenHttpException
     */
	public function actionIndex()
	{
        $this->checkPermission('search_reviews');

		$model = new SearchReview();
		$dataProvider = $model->search(Yii::$app->request->get());

        $bulkUpdateModel = new ReviewBulkUpdateForm();
        if ($bulkUpdateModel->load(Yii::$app->request->post())) {
            if ($bulkUpdateModel->validate() && ($count = $bulkUpdateModel->update())) {
                FlashMsg::success(Yii::t('phycom/backend/main', '{count} items successfully updated', ['count' => $count]));
            } else {
                FlashMsg::error($bulkUpdateModel->errors);
            }
        }

		return $this->render('index', ['model' => $model, 'dataProvider' => $dataProvider]);
	}

    /**
     * @param int $id
     * @return string
     * @throws NotFoundHttpException
     * @throws yii\web\ForbiddenHttpException
     */
	public function actionEdit($id)
	{
		$this->checkPermission('update_review');

		$review = $this->findReview($id);

        if ($review->load(Yii::$app->request->post())) {
            if ($review->save(true, ['status'])) {
                FlashMsg::success(Yii::t('phycom/backend/review', 'Review {id} successfully updated', ['id' => $review->id]));
            } else {
                FlashMsg::error($review->errors);
            }
        }
		return $this->render('edit', ['model' => $review]);
	}

    /**
     * @param int $id
     * @return yii\web\Response
     * @throws NotFoundHttpException
     * @throws \Throwable
     * @throws yii\db\StaleObjectException
     * @throws yii\web\ForbiddenHttpException
     */
    public function actionDelete($id)
    {
        $this->checkPermission('delete_review');

        $review = $this->findReview($id);
        $review->status = ReviewStatus::DELETED;

        if ($review->update()) {
            FlashMsg::success(Yii::t('phycom/backend/review', 'Review {id} successfully deleted', ['id' => $id]));
        } else {
            FlashMsg::error($review->errors);
        }
        return $this->redirect(Yii::$app->request->referrer);
    }

	/**
	 * @param int $id
	 * @return Review
	 * @throws NotFoundHttpException
	 */
	protected function findReview($id)
	{
		$review = Review::findOne($id);
		if (!$review) {
			throw new NotFoundHttpException('The requested page does not exist.');
		}
		return $review;
	}
}
