<?php

namespace Phycom\Backend\Controllers;

use Phycom\Backend\Models\SearchAll;
use yii;

class SearchController extends BaseController
{
	public $defaultAction = 'search';


	public function actionSearch($q)
	{
		$this->checkPermission('search');

		$model = new SearchAll();
        $model->language = Yii::$app->lang->current;
		$model->q = Yii::$app->request->get('q', '');
		$model->type = Yii::$app->request->get('type', null);
		$dataProvider = $model->search();

		return $this->render('/search/search-results', ['dataProvider' => $dataProvider, 'model' => $model]);
	}
}
