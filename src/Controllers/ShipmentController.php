<?php

namespace Phycom\Backend\Controllers;

use Phycom\Backend\Models\SearchShipment;
use Phycom\Backend\Models\ShipmentForm;
use Phycom\Base\Helpers\FlashMsg;
use Phycom\Base\Models\Attributes\ShipmentStatus;
use Phycom\Base\Models\Shipment;

use yii\web\NotFoundHttpException;
use yii\helpers\Url;
use yii;

/**
 * Class ShipmentController
 * @package Phycom\Backend\Controllers
 */
class ShipmentController extends BaseController
{
    /**
     * @param yii\base\Action $action
     * @return bool
     * @throws NotFoundHttpException
     */
    public function beforeAction($action)
    {
        $this->checkEnabled([Yii::$app->commerce, Yii::$app->commerce->delivery]);
        return parent::beforeAction($action);
    }

    /**
     * @return string
     * @throws yii\web\ForbiddenHttpException
     */
	public function actionIndex()
	{
		$this->checkPermission('search_shipments');

		$model = new SearchShipment();
		$dataProvider = $model->search(Yii::$app->request->get());

		return $this->render('index', ['model' => $model, 'dataProvider' => $dataProvider]);
	}

    /**
     * @return string
     * @throws yii\web\ForbiddenHttpException
     */
	public function actionAdd()
	{
		$this->checkPermission('create_shipment');
		$model = new ShipmentForm();

		return $this->render('add', ['model' => $model]);
	}

    /**
     * @param $id
     * @return string
     * @throws NotFoundHttpException
     * @throws yii\web\ForbiddenHttpException
     */
	public function actionEdit($id)
	{
		$this->checkPermission('update_shipment');
		$shipment = $this->findShipment($id);
		$model = new ShipmentForm($shipment);

		return $this->render('edit', ['model' => $model]);
	}

    /**
     * @param $id
     * @param $status
     * @return yii\web\Response
     * @throws NotFoundHttpException
     * @throws yii\web\ForbiddenHttpException
     */
	public function actionUpdateStatus($id, $status)
	{
		$this->checkPermission('update_shipment');
		$shipment = $this->findShipment($id);

		$shipment->status = $status;

		if ($status === ShipmentStatus::DISPATCHED) {
			$shipment->shipped_at = new \DateTime();
		}

		if ($shipment->save()) {
			FlashMsg::success(Yii::t('phycom/backend/shipment', 'Shipment status successfully changed'));
		} else {
		    foreach ($shipment->errors as $error) {
                FlashMsg::error($error);
            }
		}
		return $this->redirect(Url::toRoute(['/shipment/edit', 'id' => $id]));
	}

    /**
     * @param $id
     * @return string|yii\web\Response
     * @throws NotFoundHttpException
     * @throws yii\web\ForbiddenHttpException
     */
	public function actionPostageLabel($id)
    {
        $this->checkPermission('update_shipment');
        $shipment = $this->findShipment($id);

        if (!$shipment->deliveryMethod->hasPostageLabel()) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        if (!$shipment->postage_label) {
            if ($shipment->createPostageLabel()) {
                FlashMsg::success(Yii::t('phycom/backend/shipment', 'Postage label successfully created'));
                if (!empty($shipment->messages)) {
                    foreach ($shipment->messages as $message) {
                        FlashMsg::info($message);
                    }
                }
            }
        }
        if ($shipment->hasErrors()) {
            foreach ($shipment->errors as $error) {
                FlashMsg::error($error);
            }
            return $this->redirect(Yii::$app->request->referrer);
        }
        return $this->render('label', ['shipment' => $shipment]);
    }

	/**
	 * @param int $id
	 * @return Shipment
	 * @throws NotFoundHttpException
	 */
	protected function findShipment($id)
	{
		$shipment = Shipment::findOne($id);
		if (!$shipment) {
			throw new NotFoundHttpException('The requested page does not exist.');
		}
		return $shipment;
	}
}
