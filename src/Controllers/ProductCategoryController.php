<?php

namespace Phycom\Backend\Controllers;


use Phycom\Backend\Models\Product\ProductCategoryForm;
use Phycom\Backend\Models\Product\SearchProductCategory;
use Phycom\Base\Helpers\FlashMsg;
use Phycom\Base\Models\Language;
use Phycom\Base\Models\Product\ProductCategory;
use yii\web\NotFoundHttpException;
use yii;

/**
 * Class ProductCategoryController
 * @package Phycom\Backend\Controllers
 */
class ProductCategoryController extends BaseController
{
	public $defaultAction = 'index';

    /**
     * @param string|null $lang
     * @return string
     * @throws NotFoundHttpException
     * @throws yii\web\ForbiddenHttpException
     */
	public function actionIndex($lang = null)
	{
		$this->checkPermission('search_product_categories');
		$language = $this->getLanguage($lang ?? Yii::$app->lang->default->code);
		return $this->renderMain($language);
	}

    /**
     * @param string $lang
     * @param int|null $parentId
     * @return string|yii\web\Response
     * @throws yii\web\ForbiddenHttpException
     */
	public function actionCreate($lang, $parentId = null)
	{
		$this->checkPermission('create_product_category');

		$form = new ProductCategoryForm($lang);
		if ($form->load(Yii::$app->request->post())) {
			if (!$form->update()) {
				FlashMsg::error($form->lastError);
			} else {
				FlashMsg::success(Yii::t('phycom/backend/product', 'Category successfully created'));
				return $this->redirect(['product-category/edit', 'lang' => $form->language, 'id' => $form->category->id]);
			}
		}
		return $this->renderForm($form);
	}

    /**
     * @param string $lang
     * @param int $id
     * @return string|yii\web\Response
     * @throws NotFoundHttpException
     * @throws yii\web\ForbiddenHttpException
     */
	public function actionEdit($lang, $id)
	{
		$this->checkPermission('update_product_category');
		$form = new ProductCategoryForm($lang, $this->findCategory($id));

		if ($form->load(Yii::$app->request->post())) {
			if (!$form->update()) {
				FlashMsg::error($form->lastError);
			} else {
				FlashMsg::success(Yii::t('phycom/backend/product', 'Category successfully updated'));
				return $this->redirect(['product-category/edit', 'id' => $form->category->id, 'lang' => $form->language]);
			}
		}
		return $this->renderForm($form);
	}

    /**
     * @param int $id
     * @param $target
     * @param $position
     * @throws NotFoundHttpException
     * @throws yii\web\ForbiddenHttpException
     */
	public function actionMove($id, $target, $position)
	{
		$this->checkPermission('move_product_category');
		$model = $this->findCategory($id);
		$t = $this->findCategory($target);
		switch ($position) {
			case 0:
				$model->insertBefore($t);
				break;

			case 1:
				$model->appendTo($t);
				break;

			case 2:
				$model->insertAfter($t);
				break;
		}
	}

    /**
     * @param int $id
     * @return yii\web\Response
     * @throws NotFoundHttpException
     * @throws \Throwable
     * @throws yii\db\StaleObjectException
     * @throws yii\web\ForbiddenHttpException
     */
	public function actionDelete($id)
	{
		$this->checkPermission('delete_product_category');
		$category = $this->findCategory($id);

		if ($category->deleteRecursive()) {
			FlashMsg::success(Yii::t('phycom/backend/product', 'Category {id} deleted', ['id' => $id]));
		} else {
			FlashMsg::error($category->errors);
		}
		return $this->redirect(['product-category/index']);
	}

	/**
	 * @param string $languageCode
	 * @return Language|null
	 * @throws NotFoundHttpException
	 */
	protected function getLanguage($languageCode)
	{
		if (!$language = Yii::$app->lang->get($languageCode)) {
			throw new NotFoundHttpException('The requested page does not exist.');
		}
		return $language;
	}

	/**
	 * Renders the main view with product category form
	 * @param ProductCategoryForm $form
	 * @return string
	 */
	protected function renderForm(ProductCategoryForm $form)
	{
		$model = new SearchProductCategory();
		$model->category = $form->category;
		$model->language = $form->translation->languageModel;

		$dataProvider = $model->search(Yii::$app->request->queryParams);

		return $this->render('/product/category', [
			'dataProvider' => $dataProvider,
			'model' => $model,
			'formModel' => $form
		]);
	}

	/**
	 * Renders the main view without the product category form
	 * @param Language|null $language
	 * @return string
	 */
	protected function renderMain(Language $language = null)
	{
		$model = new SearchProductCategory();
		$model->language = $language;

		$dataProvider = $model->search(Yii::$app->request->queryParams);

		return $this->render('/product/category', [
			'dataProvider' => $dataProvider,
			'model' => $model,
			'formModel' => null
		]);
	}


	/**
	 * @param int $id
	 * @return ProductCategory
	 * @throws NotFoundHttpException
	 */
	protected function findCategory($id)
	{
		$category = ProductCategory::findOne($id);
		if (!$category) {
			throw new NotFoundHttpException('The requested page does not exist.');
		}
		return $category;
	}
}
