<?php

namespace Phycom\Backend\Controllers;


use Phycom\Backend\Models\DeliveryArea\DeliveryAreaForm;

use Phycom\Base\Helpers\FlashMsg;
use Phycom\Base\Modules\Delivery\Models\DeliveryArea;

use yii\base\InvalidConfigException;
use yii\helpers\Url;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\base\Exception;
use Yii;

/**
 * Class DeliveryAreaController
 * @package Phycom\Backend\Controllers
 */
class DeliveryAreaController extends BaseController
{
    public $activeTab = 'info';

    /**
     * @param yii\base\Action $action
     * @return bool
     * @throws NotFoundHttpException
     */
    public function beforeAction($action)
    {
        $this->checkEnabled([Yii::$app->commerce, Yii::$app->commerce->delivery]);
        return parent::beforeAction($action);
    }


    /**
     * @param DeliveryAreaForm $model
     * @return array
     */
    public function getSubmenuItems(DeliveryAreaForm $model): array
    {
        $items = [
            'info' => [
                'label' => '<span class="fas fa-info" style="margin: 0 18px 0 4px;"></span>' . Yii::t('phycom/backend/delivery-area', 'Info'),
                'view' => 'partials/info',
                'params' => ['model' => $model]
            ]
        ];
        if (!$model->deliveryArea->isNewRecord) {

            if (Yii::$app->user->can('update_delivery_area_weekly_schedule')) {
                $items['weekly-schedule'] = [
                    'label'   => '<span class="far fa-calendar" style="margin-right: 15px;"></span>' . Yii::t('phycom/backend/delivery-area', 'Weekly schedule'),
                    'view'    => 'partials/schedule-weekly',
                    'params'  => ['model' => $model->getWeeklyScheduleForm()],
                    'visible' => Yii::$app->commerce->delivery->isEnabled()
                ];
            }
            if (Yii::$app->user->can('update_delivery_area_daily_schedule')) {
                $items['daily-schedule'] = [
                    'label'   => '<span class="far fa-calendar" style="margin-right: 15px;"></span>' . Yii::t('phycom/backend/delivery-area', 'Daily schedule'),
                    'view'    => 'partials/schedule-daily',
                    'params'  => ['model' => $model->getDailyScheduleForm()],
                    'visible' => Yii::$app->commerce->delivery->isEnabled()
                ];
            }
            if (Yii::$app->user->can('update_delivery_area_settings')) {
                $items['settings'] = [
                    'label'   => '<span class="fas fa-cogs" style="margin-right: 10px;"></span>' . Yii::t('phycom/backend/delivery-area', 'Settings'),
                    'view'    => 'partials/settings',
                    'params'  => ['model' => $model->getSettingsForm()],
                    'visible' => Yii::$app->commerce->delivery->isEnabled()
                ];
            }
        }
        return $items;
    }


    /**
     * @return string
     * @throws ForbiddenHttpException|Exception
     */
	public function actionIndex()
	{
		$this->checkPermission('search_delivery_areas');

		$model = Yii::$app->modelFactory->getDeliveryAreaSearch();
		$dataProvider = $model->search(Yii::$app->request->get());


        $bulkUpdateModel = Yii::$app->modelFactory->getDeliveryAreaBulkUpdateForm();
        if ($bulkUpdateModel->load(Yii::$app->request->post())) {
            if ($bulkUpdateModel->validate() && ($count = $bulkUpdateModel->update())) {
                FlashMsg::success(Yii::t('phycom/backend/main', '{count} items successfully updated', ['count' => $count]));
            } else {
                FlashMsg::error($bulkUpdateModel->errors);
            }
        }

		return $this->render('index', ['model' => $model, 'dataProvider' => $dataProvider]);
	}

    /**
     * @return string
     * @throws ForbiddenHttpException|InvalidConfigException
     */
	public function actionAdd()
	{
		$this->checkPermission('create_delivery_area');
		$model = Yii::$app->modelFactory->getDeliveryAreaForm();

		if ($model->load(Yii::$app->request->post()) && $model->validate()) {
		    if ($model->save()) {
                FlashMsg::success(Yii::t('phycom/backend/delivery-area', 'Delivery area {id} successfully created', ['id' => $model->deliveryArea->id]));
                return $this->redirect(Url::toRoute(['delivery-area/edit', 'id' => $model->deliveryArea->id]));
            } else {
                FlashMsg::error($model->errors);
            }
        }

        return $this->render('edit', ['items' => $this->getSubmenuItems($model), 'active' => 'info', 'model' => $model]);
	}

    /**
     * @param int $id
     * @return string
     * @throws NotFoundHttpException
     * @throws ForbiddenHttpException|InvalidConfigException
     */
	public function actionEdit($id)
	{
		$this->checkPermission('update_delivery_area');
		$area = $this->findArea($id);
        $model = Yii::$app->modelFactory->getDeliveryAreaForm($area);

        $this->checkActiveTab();
        $successMsg = false;

        if ($model->load(Yii::$app->request->post())) {
            $successMsg = true;
            if (!$model->save()) {
                FlashMsg::error($model->errors);
                $successMsg = false;
            }
        }

        if (Yii::$app->user->can('update_delivery_area_weekly_schedule') && $model->getWeeklyScheduleForm()->load(Yii::$app->request->post())) {
            $successMsg = true;
            if (!$model->getWeeklyScheduleForm()->save()) {
                FlashMsg::error($model->getWeeklyScheduleForm()->hasErrors()
                    ? $model->getWeeklyScheduleForm()->errors
                    : Yii::t('phycom/backend/delivery-area', 'Error updating weekly schedule'));
                $successMsg = false;
            }
        }

        if (Yii::$app->user->can('update_delivery_area_daily_schedule') && $model->getDailyScheduleForm()->load(Yii::$app->request->post())) {
            $successMsg = true;
            if (!$model->getDailyScheduleForm()->save()) {
                FlashMsg::error($model->getDailyScheduleForm()->hasErrors()
                    ? $model->getDailyScheduleForm()->errors
                    : Yii::t('phycom/backend/delivery-area', 'Error updating daily schedule'));
                $successMsg = false;
            }
        }

        if (Yii::$app->user->can('update_delivery_area_settings') && $model->getSettingsForm()->load(Yii::$app->request->post())) {
            $successMsg = true;
            if (!$model->getSettingsForm()->save()) {
                FlashMsg::error($model->getSettingsForm()->hasErrors()
                    ? $model->getSettingsForm()->errors
                    : Yii::t('phycom/backend/delivery-area', 'Error updating delivery area settings'));
                $successMsg = false;
            }
        }

        if ($successMsg) {
            FlashMsg::success(Yii::t('phycom/backend/delivery-area', 'Delivery area {id} successfully updated', ['id' => $area->id]));
        }

        return $this->render('edit', ['items' => $this->getSubmenuItems($model), 'active' => $this->activeTab, 'model' => $model]);
	}

    /**
     * @param $id
     * @return yii\web\Response
     * @throws ForbiddenHttpException
     * @throws NotFoundHttpException
     * @throws \Throwable
     * @throws yii\db\StaleObjectException
     */
    public function actionDelete($id)
    {
        $this->checkPermission('delete_delivery_area');
        $area = $this->findArea($id);

        if ($area->delete()) {
            FlashMsg::success(Yii::t('phycom/backend/delivery-area', 'Delivery area {id} deleted', ['id' => $id]));
        } else {
            FlashMsg::error($area->errors);
        }
        return $this->redirect(Yii::$app->request->referrer);
    }


	/**
	 * @param int $id
	 * @return DeliveryArea
	 * @throws NotFoundHttpException
	 */
	protected function findArea($id)
	{
		$shipment = DeliveryArea::findOne($id);
		if (!$shipment) {
			throw new NotFoundHttpException('The requested page does not exist.');
		}
		return $shipment;
	}

    protected function checkActiveTab()
    {
        if ($tab = Yii::$app->request->post('tab')) {
            $this->activeTab = $tab;
        }
    }
}
