<?php

namespace Phycom\Backend\Controllers;


use yii2tech\filestorage\DownloadAction;

use yii\web\NotFoundHttpException;
use yii;

/**
 * Class FileDownloadAction
 * @package Phycom\Backend\Controllers
 */
class FileDownloadAction extends DownloadAction
{
    public $inline = true;

    public function run($bucket, $filename)
    {
        try {
            return parent::run($bucket, $filename);
        } catch (\Exception $e) {
            Yii::error($e->getMessage() . ' - ' . $e->getFile() . ':' . $e->getLine(), __METHOD__);
            Yii::error($e->getTraceAsString(), __METHOD__);
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
