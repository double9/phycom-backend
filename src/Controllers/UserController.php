<?php

namespace Phycom\Backend\Controllers;

use Phycom\Backend\Models\SearchUser;
use Phycom\Backend\Models\SearchUserActivity;
use Phycom\Backend\Models\SearchUserInvitation;
use Phycom\Backend\Models\UserAddressForm;
use Phycom\Backend\Models\UserInvitationForm;
use Phycom\Backend\Models\UserProfileForm;
use Phycom\Backend\Models\UserProfileImageForm;
use Phycom\Base\Helpers\FlashMsg;
use Phycom\Base\Models\Address;
use Phycom\Base\Models\Attributes\UserStatus;
use Phycom\Base\Models\User;

use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\web\UploadedFile;
use yii\helpers\Json;
use yii;

/**
 * Class UserController
 * @package Phycom\Backend\Controllers
 */
class UserController extends BaseController
{
    /**
     * @return string
     * @throws yii\base\Exception
     * @throws yii\web\ForbiddenHttpException
     */
	public function actionIndex()
	{
		$this->checkPermission('search_users');
		$model = new SearchUser();

		if (Yii::$app->user->can('search_unregistered_users')) {
		    $model->showUnregistered = true;
        }

		$dataProvider = $model->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'model'        => $model
        ]);
	}

    /**
     * @return string
     * @throws yii\web\ForbiddenHttpException
     * @throws yii\base\InvalidConfigException
     * @throws \Exception
     */
	public function actionInvite()
	{
		$this->checkPermission('invite_backend_users');
		$searchModel = new SearchUserInvitation();
        /**
         * @var UserInvitationForm|object $form
         */
        $form = Yii::createObject(UserInvitationForm::class, [null]);
		if ($form->load(Yii::$app->request->post())) {
			$form->create() ?
				FlashMsg::success(Yii::t('phycom/backend/user', 'Invitation message was queued')) :
				FlashMsg::error($form->errors);
		}
        return $this->render('invite', [
            'model'       => $form,
            'searchModel' => $searchModel
        ]);
	}

    /**
     * @param int|null $id - user id
     * @return array|mixed|null
     * @throws NotFoundHttpException
     * @throws yii\web\ForbiddenHttpException
     */
	public function actionUpdateSettings(int $id = null)
    {
        $this->ajaxOnly(Response::FORMAT_JSON);
        $userId = $id ?: Yii::$app->user->id;

        $this->checkPermission('update_user', $userId);
        $user = $this->findUser($userId);

        $settings = Json::decode(Yii::$app->request->rawBody);

        $userSettings = $user->settings;
        $userSettings->load($settings, '');

        if (!$userSettings->validate()) {
            return ['errors' => $userSettings->errors];
        }
        if (!$user->save()) {
            return ['errors' => $user->errors];
        }
        return [];
    }

    /**
     * @return string
     * @throws NotFoundHttpException
     * @throws yii\base\Exception
     * @throws yii\web\ForbiddenHttpException
     */
	public function actionPendingInvitations()
	{
		$this->checkPermission('invite_backend_users');
		$this->ajaxOnly();

		$searchModel = new SearchUserInvitation();
		$dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		return $this->renderPartial('partials/invitations', ['dataProvider' => $dataProvider]);
	}

    /**
     * @param int $id - user id
     * @return array
     * @throws NotFoundHttpException
     * @throws yii\web\ForbiddenHttpException
     * @throws yii\base\InvalidConfigException
     * @throws \Exception
     */
	public function actionResendInvitation($id)
	{
		$this->checkPermission('invite_backend_users');
		$this->ajaxOnly(Response::FORMAT_JSON);
        /**
         * @var UserInvitationForm|object $form
         */
		$form = Yii::createObject(UserInvitationForm::class, [$this->findUser($id)]);

		$successMessage = Yii::t('phycom/backend/user', 'Invitation successfully sent');
		return $form->resendInvitation() ? ['message' => $successMessage] : ['error' => $form->lastError];
	}

    /**
     * @param int $id - user id
     * @return array
     * @throws NotFoundHttpException
     * @throws \Throwable
     * @throws yii\db\StaleObjectException
     * @throws yii\web\ForbiddenHttpException
     */
	public function actionRemoveInvitation($id)
    {
        $this->checkPermission('remove_pending_users');
        $this->ajaxOnly(Response::FORMAT_JSON);

        $user = $this->findUser($id);

        if ((string)$user->status === UserStatus::PENDING) {
            return $user->delete()
                ? ['message' => Yii::t('phycom/backend/user', 'User invitation {id} removed', ['id' => $user->id])]
                : ['error' => array_values($user->firstErrors)[0]];
        }
        return ['error' => Yii::t('phycom/backend/user', 'User {id} is not pending registration', ['id' => $user->id])];
    }

    /**
     * @param int $id - user id
     * @return Response
     * @throws NotFoundHttpException
     * @throws yii\web\ForbiddenHttpException
     */
    public function actionBlock($id)
    {
        $this->checkPermission('update_user', $id);

        $user = $this->findUser($id);
        $user->status = UserStatus::BLOCKED;

        if ($user->save()) {
            FlashMsg::success(Yii::t('phycom/backend/user', 'User {id} successfully blocked', ['id' => $id]));
        } else {
            FlashMsg::error($user->errors);
        }
        return $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * @param $id - user id
     * @return string
     * @throws NotFoundHttpException
     * @throws \Throwable
     * @throws yii\db\StaleObjectException
     * @throws yii\web\ForbiddenHttpException
     */
	public function actionProfile($id)
	{
		$this->checkPermission('update_user', $id);
		$user = $this->findUser($id);
		$profileForm = new UserProfileForm($user);
		$profileImageForm = new UserProfileImageForm($user);
		$addressForm = new UserAddressForm($user);

		if ($profileForm->load(Yii::$app->request->post())) {
			$profileForm->update() ?
				FlashMsg::success(Yii::t('phycom/backend/user', 'User {user_id} profile successfully updated', ['user_id' => $user->id])) :
				FlashMsg::error($profileForm->errors);
		}

		if ($addressForm->load(Yii::$app->request->post())) {
			$addressForm->update() ?
				FlashMsg::success(Yii::t('phycom/backend/user', 'User {user_id} address successfully saved', ['user_id' => $user->id])) :
				FlashMsg::error($addressForm->errors);
		}

        if ($user->meta->load(Yii::$app->request->post())) {
		    if (!$user->meta->validate()) {
                FlashMsg::error($user->meta->errors);
            } else {
                $user->update() ?
                    FlashMsg::success(Yii::t('phycom/backend/user', 'User {user_id} meta successfully updated', ['user_id' => $user->id])) :
                    FlashMsg::error($user->errors);
            }
        }

		return $this->render('profile', [
			'user' => $user,
			'profileForm' => $profileForm,
			'profileImageForm' => $profileImageForm,
			'addressForm' => $addressForm,
			'activity' => new SearchUserActivity($id)
		]);
	}

    /**
     * @param int $id - user id
     * @return array
     * @throws NotFoundHttpException
     * @throws yii\web\ForbiddenHttpException
     */
	public function actionUploadAvatar($id)
	{
		$this->checkPermission('change_avatar', $id);
		if (Yii::$app->request->isPost) {

			Yii::$app->response->format = Response::FORMAT_JSON;
			$user = $this->findUser($id);
			$model = new UserProfileImageForm($user);
			$model->avatar = UploadedFile::getInstance($model, 'avatar');
			$image = $model->upload();

			return $image ? [
				'initialPreviewThumbnails' => [
					'<img src="' . $image . '" alt="' . $user->fullName . ' Avatar" title="' . $model->avatar->name . '"">',
				],
				'append' => false
			] : ['error' => $model->lastError];
		}
		throw new NotFoundHttpException('The requested page does not exist.');
	}

    /**
     * @param $id
     * @return void
     * @throws \Throwable
     * @throws yii\db\StaleObjectException
     * @throws yii\web\ForbiddenHttpException
     */
    public function actionDeleteAddress($id): Response
    {
        $userId = Yii::$app->user->id;
        $this->checkPermission('update_user', $userId);

        if (!$address = Address::findOne(['id' => $id])) {
            throw new yii\base\InvalidCallException("Address $id was not found" );
        }

        if ($address->delete()) {
            FlashMsg::success(Yii::t(
                'phycom/backend/user',
                'User {user_id} address successfully deleted',
                ['user_id' => $userId])
            );
        } else {
            FlashMsg::error(Yii::t('phycom/backend/user', 'Error deleting address'));
        }
        return $this->redirect(Yii::$app->request->getReferrer());
    }

    /**
     * Renders the user activity table partial
     *
     * @param int $id - user id
     * @return string
     * @throws NotFoundHttpException
     * @throws yii\web\ForbiddenHttpException
     */
	public function actionActivity($id)
	{
		$this->checkPermission('search_user_activity');
		$this->ajaxOnly();

		$searchModel = new SearchUserActivity($id);
		$dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		return $this->renderPartial('partials/user-activity', ['dataProvider' => $dataProvider]);
	}

    /**
     * @return string
     * @throws yii\web\ForbiddenHttpException
     */
	public function actionPermissions()
	{
		$this->checkPermission('view_permissions');
		return $this->render('permissions');
	}

	/**
	 * @param $id - user id
	 * @return User
	 * @throws NotFoundHttpException
	 */
	protected function findUser($id)
	{
		$user = Yii::$app->modelFactory->getUser()::findOne($id);
		if (!$user) {
			throw new NotFoundHttpException('The requested page does not exist.');
		}
		return $user;
	}
}
