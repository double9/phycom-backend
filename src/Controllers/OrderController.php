<?php

namespace Phycom\Backend\Controllers;

use Phycom\Backend\Models\OrderForm;
use Phycom\Backend\Models\SearchOrder;

use Phycom\Base\Helpers\FlashMsg;
use Phycom\Base\Models\Statistics\SearchOrderStatistics;
use Phycom\Base\Models\Order;

use yii\web\NotFoundHttpException;
use Yii;


/**
 * Class OrderController
 * @package Phycom\Backend\Controllers
 */
class OrderController extends BaseController
{
    /**
     * @param \yii\base\Action $action
     * @return bool
     * @throws NotFoundHttpException
     */
    public function beforeAction($action)
    {
        $this->checkEnabled(Yii::$app->commerce);
        return parent::beforeAction($action);
    }

    /**
     * @return string
     * @throws \yii\base\Exception
     * @throws \yii\web\ForbiddenHttpException
     */
	public function actionIndex()
	{
		$this->checkPermission('search_orders');

		$model = Yii::$app->modelFactory->getSearchOrder();
		$dataProvider = $model->search(Yii::$app->request->get());

		return $this->render('index', ['model' => $model, 'dataProvider' => $dataProvider]);
	}

    /**
     * @return string
     * @throws \yii\base\Exception
     * @throws \yii\web\ForbiddenHttpException
     */
    public function actionRecent()
    {
        $this->checkPermission('search_orders');
        $this->ajaxOnly();

        $model = Yii::$app->modelFactory->getSearchOrder();
        $dataProvider = $model->recent(Yii::$app->request->get());

        return $this->renderPartial('partials/recent-orders', ['dataProvider' => $dataProvider]);
    }

    /**
     * @return string
     * @throws \yii\web\ForbiddenHttpException
     */
	public function actionAdd()
	{
		$this->checkPermission('create_order');
		$model = Yii::$app->modelFactory->getOrderForm();

		return $this->render('add', ['model' => $model]);
	}

    /**
     * @param $id
     * @return string
     * @throws NotFoundHttpException
     * @throws \yii\web\ForbiddenHttpException
     */
	public function actionEdit($id)
	{
		$this->checkPermission('update_order');
		$order = $this->findOrder($id);
		$model = Yii::$app->modelFactory->getOrderForm($order);

		if ($model->load(Yii::$app->request->post())) {
		    if ($model->validate() && $model->update()) {
		        FlashMsg::success(Yii::t('phycom/backend/order', 'Order {id} successfully updated', ['id' => $order->id]));
            } else {
		        FlashMsg::error($model->errors);
            }
        }

		return $this->render('edit', ['model' => $model, 'order' => $order]);
	}

    /**
     * @param $id
     * @return string
     * @throws NotFoundHttpException
     * @throws \yii\web\ForbiddenHttpException
     */
    public function actionLabel($id)
    {
        $this->checkPermission('view_order');
        $this->layout = 'print';
        return $this->render('label', ['order' => $this->findOrder($id)]);
    }

    /**
     * @return array|\yii\web\Response
     * @throws NotFoundHttpException
     * @throws \yii\base\Exception
     * @throws \yii\web\ForbiddenHttpException
     */
	public function actionStatistics()
    {
        $this->checkPermission('search_orders');
        $this->ajaxOnly();

        $model = new SearchOrderStatistics();
        $model->load(Yii::$app->request->get(), '');

        if ($model->validate()) {
            return $this->asJson($model->search()->allModels);
        }
        return $this->asJson(['error' => $model->firstErrors]);
    }

	/**
	 * @param $id
	 * @return Order
	 * @throws NotFoundHttpException
	 */
	protected function findOrder($id)
	{
		$order = Yii::$app->modelFactory->getOrder()::findOne($id);
		if (!$order) {
			throw new NotFoundHttpException('The requested page does not exist.');
		}
		return $order;
	}

}
