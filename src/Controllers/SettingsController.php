<?php

namespace Phycom\Backend\Controllers;


use Phycom\Backend\Models\ComponentForm;
use Phycom\Backend\Models\Product\InventorySettingForm;
use Phycom\Backend\Models\Product\VariantCollectionForm;
use Phycom\Backend\Models\Product\ParamCollectionForm;
use Phycom\Backend\Models\Vendor\MainVendorForm;
use Phycom\Base\Components\PhycomComponent;
use Phycom\Base\Helpers\FlashMsg;
use Phycom\Base\Interfaces\ModuleInterface;
use Phycom\Base\Interfaces\PersistableConfigurationInterface;
use Phycom\Base\Interfaces\PhycomComponentInterface;
use Phycom\Base\Models\Vendor;

use yii\base\InvalidConfigException;
use yii\helpers\Inflector;
use yii\web\ForbiddenHttpException;
use Yii;

/**
 * Class SettingsController
 * @package Phycom\Backend\Controllers
 *
 * @property-read array $submenuItems
 */
class SettingsController extends BaseController
{
    public $activeTab = 'vendor';

    /**
     * @var string[]
     */
    protected $excludedModules = [
        'delivery'
    ];

    /**
     * @return array[]
     * @throws InvalidConfigException|ForbiddenHttpException
     */
	public function getSubmenuItems(): array
	{
        $items = [];

        foreach (Yii::$app->components as $id => $value) {
            $component = Yii::$app->{$id};
            if ($component instanceof PersistableConfigurationInterface && $component instanceof PhycomComponent) {
                $items[$id] = [
                    'label'   => $component->getLabel(),
                    'view'    => '/settings/component',
                    'params'  => ['model' => $this->getComponentForm($id, $component)]
                ];
            }
        }

        if (Yii::$app->commerce->variants->isEnabled()) {
            $items['variants'] = [
                'label'   => Yii::t('phycom/backend/product', 'Variants'),
                'view'    => '/product/variants',
                'params'  => ['model' => $this->getVariantCollectionForm()]
            ];
        }

        if (Yii::$app->commerce->params->isEnabled()) {
            $items['params'] = [
                'label'   => Yii::t('phycom/backend/product', 'Params'),
                'view'    => '/product/params',
                'params'  => ['model' => $this->getParamCollectionForm()]
            ];
        }

        $items['vendor'] = [
            'label'  => Yii::t('phycom/backend/vendor', 'Vendor'),
            'view'   => '/vendor/edit-main',
            'params' => ['model' => $this->getVendorForm(Yii::$app->vendor)]
        ];

        foreach (Yii::$app->getModules() as $id => $moduleConfig) {

            if (in_array($id, $this->excludedModules)) {
                continue;
            }
            $module = Yii::$app->getModule($id);

            if ($module instanceof ModuleInterface) {
                $settings = $module->getAllSettings();
                if (!empty($settings)) {
                    $component = $module->getComponent();
                    if ($component && !$component->isEnabled()) {
                        continue;
                    }
                    if (!Yii::$app->user->can('update_' . Inflector::camel2id($id, '_') . '_settings')) {
                        continue;
                    }
                    $items[$module->id] = [
                        'label' => $module->getLabel(),
                        'view' => '/settings/' . ($module->getBackendSettingsView() ?: 'module'),
                        'params' => [
                            'module' => $module,
                            'models' => $this->getModuleSettings($module)
                        ],
                        'formCollection' => true
                    ];
                }
            }
        }

        return $items;
	}

    /**
     * @return string
     * @throws InvalidConfigException|ForbiddenHttpException
     */
	public function actionIndex()
	{
	    $this->checkPermission('update_global_settings');
        return $this->render('index', ['items' => $this->getSubmenuItems(), 'active' => $this->activeTab]);
	}


    /**
     * @param ModuleInterface $module
     * @return array
     * @throws InvalidConfigException|ForbiddenHttpException
     */
    protected function getModuleSettings(ModuleInterface $module): array
    {
        $models = [];

        $modelUpdate = false;
        $err = 0;

        foreach ($module->getAllSettings() as $id => $subModule) {

            if ($subModule->useSettings) {

                $model = $subModule->getSettings();
                if ($model->load(Yii::$app->request->post(), $model->formName())) {

                    $this->checkPermission('update_' . Inflector::camel2id($module->id, '_') . '_settings');
                    $this->activeTab = $module->id;
                    $modelUpdate = true;

                    if (!$model->save()) {
                        $err++;
                        FlashMsg::error(Yii::t('phycom/backend/settings', 'Error saving {form}', ['form' => $model->formLabel()]));
                    }
                }
                $models[] = $model;
            }
        }
        if ($modelUpdate && Yii::$app->request->isPost && !$err) {
            FlashMsg::success(Yii::t('phycom/backend/settings', '{module} settings successfully updated', ['module' => $module->getLabel()]));
        }
        return $models;
    }

    /**
     * @return ComponentForm
     */
    protected function getComponentForm($id, PhycomComponentInterface $component): ComponentForm
    {
        $model = Yii::$app->modelFactory->getComponentForm($component);
        if ($model->load(Yii::$app->request->post())) {
            $this->activeTab = $id;
            if ($model->save()) {
                FlashMsg::success(Yii::t('phycom/backend/settings', '{component} settings successfully updated', ['component' => $component->getLabel()]));
            } else {
                FlashMsg::error($model->errors);
            }
        }
        return $model;
    }

    /**
     * @return VariantCollectionForm
     * @throws ForbiddenHttpException
     */
    protected function getVariantCollectionForm(): VariantCollectionForm
    {
        $model = Yii::$app->modelFactory->getVariantCollectionForm();
        if ($model->load(Yii::$app->request->post())) {
            $this->checkPermission('update_product_variant');
            $this->activeTab = 'variants';

            if ($model->update()) {
                FlashMsg::success(Yii::t('phycom/backend/main', 'Settings successfully updated'));
            } else {
                FlashMsg::error($model->errors);
            }
        }
        return $model;
    }

    /**
     * @return ParamCollectionForm
     * @throws ForbiddenHttpException
     */
    protected function getParamCollectionForm(): ParamCollectionForm
    {
        $model = Yii::$app->modelFactory->getParamCollectionForm();
        if ($model->load(Yii::$app->request->post())) {
            $this->checkPermission('update_product_param');
            $this->activeTab = 'params';

            if ($model->update()) {
                FlashMsg::success(Yii::t('phycom/backend/main', 'Settings successfully updated'));
            } else {
                FlashMsg::error($model->errors);
            }
        }
        return $model;
    }

    /**
     * @param Vendor|null $vendor
     * @return MainVendorForm
     * @throws ForbiddenHttpException
     * @throws \Exception
     */
    protected function getVendorForm(Vendor $vendor = null): MainVendorForm
    {
        $model = Yii::$app->modelFactory->getMainVendorForm($vendor);
        if ($model->load(Yii::$app->request->post())) {
            $this->checkPermission('update_vendor');
            $this->activeTab = 'vendor';

            if ($model->load(Yii::$app->request->post())) {
                if ($model->update()) {
                    FlashMsg::success(Yii::t('phycom/backend/shop', 'Vendor successfully updated'));
                } else {
                    Yii::error($model->errors, __METHOD__);
                    FlashMsg::error($model->hasGlobalErrors() ? $model->globalErrors : Yii::t('phycom/backend/shop', 'Error updating vendor'));
                }
            }
        }
        return $model;
    }
}
