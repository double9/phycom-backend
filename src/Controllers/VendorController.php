<?php

namespace Phycom\Backend\Controllers;

use Phycom\Backend\Models\Vendor\VendorForm;

use Phycom\Base\Helpers\FlashMsg;
use Phycom\Base\Models\Vendor;

use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\helpers\Url;
use yii\web\Response;
use Yii;

/**
 * Class VendorController
 * @package Phycom\Backend\Controllers
 */
class VendorController extends BaseController
{
    public $activeTab = 'info';

    /**
     * @param VendorForm $model
     * @return array
     */
    public function getSubmenuItems(VendorForm $model)
    {
        $items = [
            'info' => [
                'label'  => '<span class="fas fa-info" style="margin: 0 14px 0 4px;"></span>' . Yii::t('phycom/backend/vendor', 'Vendor info'),
                'view'   => 'partials/info',
                'params' => ['model' => $model]
            ]
        ];
        return $items;
    }
    /**
     * @return string
     * @throws ForbiddenHttpException|\yii\base\Exception
     */
    public function actionIndex()
    {
        $this->checkPermission('search_vendors');

        $model = Yii::$app->modelFactory->getVendorSearch();
        $model->main = false;
        $dataProvider = $model->search(Yii::$app->request->get());

        return $this->render('index', ['model' => $model, 'dataProvider' => $dataProvider]);
    }

    /**
     * @param $id
     * @return string
     * @throws NotFoundHttpException
     * @throws ForbiddenHttpException
     */
	public function actionEdit($id)
	{
		$this->checkPermission('update_vendor');
		$vendor = $this->findVendor($id);
		$model = Yii::$app->modelFactory->getVendorForm($vendor);

		if ($model->load(Yii::$app->request->post())) {
		    if ($model->update()) {
		        FlashMsg::success(Yii::t('phycom/backend/vendor', '{type} info successfully updated', ['type' => $model->vendor->type->label]));
            } else {
                FlashMsg::error($model->hasErrors() ? $model->errors : Yii::t('phycom/backend/shop', 'Error updating vendor'));
            }
        }
        return $this->render('edit', ['items' => $this->getSubmenuItems($model), 'active' => $this->activeTab, 'model' => $model]);
	}

    /**
     * @return string|yii\web\Response
     * @throws ForbiddenHttpException
     */
	public function actionAdd()
	{
		$this->checkPermission('create_vendor');
        $model = Yii::$app->modelFactory->getVendorForm();

        if ($model->load(Yii::$app->request->post())) {
            if ($model->update()) {
                FlashMsg::success(Yii::t('phycom/backend/vendor', '{type} {id} successfully created', ['type' => $model->vendor->type->label, 'id' => $model->vendor->id]));
                return $this->redirect(Url::toRoute(['vendor/edit', 'id' => $model->vendor->id]));
            } else {
                FlashMsg::error($model->hasErrors() ? $model->errors : Yii::t('phycom/backend/shop', 'Error creating new vendor'));
            }
        }
        return $this->render('add', ['items' => $this->getSubmenuItems($model), 'active' => 'info', 'model' => $model]);
	}

    /**
     * @param $id - vendor id
     * @return Response
     * @throws NotFoundHttpException
     * @throws \Throwable
     * @throws yii\db\StaleObjectException
     * @throws yii\web\ForbiddenHttpException
     */
    public function actionDelete($id)
    {
        $this->checkPermission('delete_vendor');
        $vendor = $this->findVendor($id);

        if ($vendor->delete()) {
            FlashMsg::success(Yii::t('phycom/backend/vendor', '{type} {id} successfully deleted', ['type' => $vendor->type->label, 'id' => $id]));
        } else {
            FlashMsg::error($vendor->errors);
        }
        return $this->redirect(Yii::$app->request->referrer);
    }

	/**
	 * @param int $id
	 * @return Vendor
	 * @throws NotFoundHttpException
	 */
	protected function findVendor($id): Vendor
	{
		$vendor = Vendor::findOne($id);
		if (!$vendor) {
			throw new NotFoundHttpException('The requested page does not exist.');
		}
		return $vendor;
	}
}
