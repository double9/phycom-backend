<?php

namespace Phycom\Backend\Controllers;


use Phycom\Backend\Models\DeliverySettings\DeliverySettingsForm;

use Phycom\Base\Helpers\FlashMsg;
use Phycom\Base\Interfaces\ModuleInterface;
use Phycom\Base\Modules\Delivery\Module as DeliveryModule;

use yii\base\InvalidConfigException;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use Yii;

/**
 * Class DeliverySettingsController
 *
 * @package Phycom\Backend\Controllers
 */
class DeliverySettingsController extends BaseController
{
    /**
     * @var string
     */
    public string $activeTab = 'methods';

    /**
     * @param yii\base\Action $action
     * @return bool
     * @throws NotFoundHttpException
     */
    public function beforeAction($action)
    {
        $this->checkEnabled([Yii::$app->commerce, Yii::$app->commerce->delivery]);
        return parent::beforeAction($action);
    }

    /**
     * @param DeliverySettingsForm $model
     * @return array
     * @throws ForbiddenHttpException
     */
    public function getSubmenuItems(DeliverySettingsForm $model): array
    {
        $items = [];

        /**
         * @var DeliveryModule $deliveryModule
         */
        if ($deliveryModule = Yii::$app->getModule('delivery')) {

            if ($deliveryModule->isEnabled() && Yii::$app->user->can('update_delivery_settings')) {

                $items['methods'] = [
                    'label'          => '<span class="fas fa-truck" style="margin-right: 10px;"></span>' . Yii::t('phycom/backend/delivery', 'Delivery methods'),
                    'view'           => '/delivery-settings/partials/methods',
                    'params'         => ['models' => $this->getModuleSettings($deliveryModule->getAllMethods(), 'methods')],
                    'formCollection' => true
                ];

                $commonSettings = $this->getModuleSettings([$deliveryModule], 'common');
                if (!empty($commonSettings)) {
                    $items['common'] = [
                        'label'          => '<span class="fas fa-cogs" style="margin-right: 10px;"></span>' . Yii::t('phycom/backend/delivery', 'Common Settings'),
                        'view'           => '/delivery-settings/partials/common',
                        'params'         => ['model' => $commonSettings[0]],
                        'formCollection' => true
                    ];
                }
            }
        }

        if (Yii::$app->user->can('update_non_delivery_days')) {
            $items['non-delivery-days'] = [
                'label'   => '<span class="far fa-calendar" style="margin-right: 15px;"></span>' . Yii::t('phycom/backend/delivery', 'Non delivery days'),
                'view'    => 'partials/non-delivery-days',
                'params'  => ['model' => $model],
                'visible' => Yii::$app->commerce->delivery->isEnabled()
            ];
        }

        return $items;
    }

    /**
     * @return string
     * @throws ForbiddenHttpException|InvalidConfigException
     */
	public function actionIndex()
	{
		$this->checkPermission('update_delivery_settings');
        $model = Yii::$app->modelFactory->getDeliverySettingsForm();

        $this->checkActiveTab();

        if (Yii::$app->user->can('update_non_delivery_days') && $model->load(Yii::$app->request->post())) {
            if ($model->update()) {
                FlashMsg::success(Yii::t('phycom/backend/delivery', 'Delivery settings successfully updated'));
            } else {
                FlashMsg::error($model->errors);
            }
        }

        return $this->render('index', ['items' => $this->getSubmenuItems($model), 'active' => $this->activeTab, 'model' => $model]);
	}


    protected function checkActiveTab()
    {
        if ($tab = Yii::$app->request->post('tab')) {
            $this->activeTab = $tab;
        }
    }

    /**
     * @param ModuleInterface[] $modules
     * @param string $activeTab
     * @return array
     * @throws ForbiddenHttpException
     */
    protected function getModuleSettings(array $modules, string $activeTab): array
    {
        $models = [];
        $modelUpdate = false;
        $err = 0;

        foreach ($modules as $id => $module) {

            if ($module->useSettings) {
                $model = $module->getSettings();
                if ($model->load(Yii::$app->request->post(), $model->formName())) {

                    $this->checkPermission('update_delivery_settings');
                    $this->activeTab = $activeTab;
                    $modelUpdate = true;

                    if (!$model->save()) {
                        $err++;
                        FlashMsg::error(Yii::t('phycom/backend/settings', 'Error saving {form}', ['form' => $model->formLabel()]));
                    }
                }
                $models[] = $model;
            }
        }
        if ($modelUpdate && Yii::$app->request->isPost && !$err) {
            FlashMsg::success(Yii::t('phycom/backend/delivery', 'Delivery settings successfully updated'));
        }
        return $models;
    }
}
