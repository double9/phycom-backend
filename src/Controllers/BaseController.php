<?php

namespace Phycom\Backend\Controllers;


use Phycom\Backend\Helpers\UserLanguage;

use Phycom\Base\Models\Traits\ComponentTrait;
use Phycom\Base\Models\Traits\WebControllerTrait;
use Phycom\Base\Models\UserActivity;

use yii\filters\AccessControl;
use yii\web\Controller;
use yii;

/**
 * This is the base controller class that every backend controller should extend.
 *
 * Class BaseController
 * @package Phycom\Backend\Controllers
 */
class BaseController extends Controller
{
	use WebControllerTrait;
	use ComponentTrait;

    const AFTER_SAVE_NEW = 'new';
    const AFTER_SAVE_DUPLICATE = 'duplicate';
    const AFTER_SAVE_CLOSE = 'close';

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                    	'allow' => true,
	                    'roles' => ['use_backend']
                    ], // Only users with "use_backend" permission should be able to access the admin area.
                    ['allow' => false]
                ],
            ],
        ];
    }

    public function beforeAction($action)
    {
        if ($action->id == 'error') {
            $this->layout = 'main-login.php';
        }
    	Yii::$app->user->identity && Yii::createObject(UserLanguage::class, [Yii::$app->user->identity])->check();
	    return parent::beforeAction($action);
    }

	public function afterAction($action, $result)
	{
		$result = parent::afterAction($action, $result);
		$routesNotRecorded = ['site/status', 'file/download'];
		$routesRecorded = ['user/upload-avatar'];

		if (!Yii::$app->user->isGuest && (!Yii::$app->request->isAjax || (in_array($action->controller->route, $routesRecorded)) && !in_array($action->controller->route, $routesNotRecorded))) {
			UserActivity::logEffectiveUserUrl();
		}
		return $result;
	}

    /**
     * @param mixed $component
     * @throws yii\web\NotFoundHttpException
     */
	protected function checkEnabled($component)
    {
        if (!$this->isComponentEnabled($component)) {
            throw new yii\web\NotFoundHttpException('The requested page does not exist.');
        }
    }
}
