<?php

namespace Phycom\Backend\Controllers;

use Phycom\Base\Models\MessageTemplate;

use yii\web\ForbiddenHttpException;
use yii\helpers\ArrayHelper;
use Yii;

/**
 * Class TemplateController
 * @package Phycom\Backend\Controllers
 */
class TemplateController extends BaseController
{
    /**
     * @return string
     * @throws ForbiddenHttpException
     */
	public function actionIndex()
	{
		$this->checkPermission('search_message_templates');
		return $this->render('index');
	}

    /**
     * @param $id
     * @return string
     * @throws ForbiddenHttpException
     */
	public function actionView($id)
	{
		$this->checkPermission('view_message_template');
		return $this->render('view', ['model' => Yii::$app->modelFactory->getMessageTemplate()::getTemplate($id)]);
	}

    /**
     * @param $id
     * @return string
     * @throws ForbiddenHttpException
     * @throws \Exception
     */
	public function actionRender($id)
	{
		$this->checkPermission('view_message_template');

		$template = Yii::$app->modelFactory->getMessageTemplate()::getTemplate($id);
		$params = $template->generateDummyParameters();
		$params = ArrayHelper::merge($params, $this->getTemplateParams($template));

		return $template->render($params);
	}

    /**
     * @param MessageTemplate $template
     * @return array
     */
	protected function getTemplateParams(MessageTemplate $template): array
    {
        return [];
    }
}
