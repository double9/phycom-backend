<?php

namespace Phycom\Backend\Controllers;

use Phycom\Backend\Models\Shop\ShopForm;

use Phycom\Base\Helpers\FlashMsg;
use Phycom\Base\Models\Attributes\PostType;
use Phycom\Base\Models\Shop;

use yii\web\NotFoundHttpException;
use yii\helpers\Url;
use yii\web\Response;
use yii;

/**
 * Class ShopController
 * @package Phycom\Backend\Controllers
 */
class ShopController extends BaseController
{
    public $activeTab = 'info';

    /**
     * @param yii\base\Action $action
     * @return bool
     * @throws NotFoundHttpException
     */
    public function beforeAction($action)
    {
        $this->checkEnabled([Yii::$app->commerce]);
        return parent::beforeAction($action);
    }

    /**
     * @param ShopForm $model
     * @return array
     */
    public function getSubmenuItems(ShopForm $model)
    {
        $items = [
            'info' => [
                'label' => '<span class="fas fa-info" style="margin: 0 14px 0 4px;"></span>' . Yii::t('phycom/backend/shop', 'Shop info'),
                'view' => 'partials/info',
                'params' => ['model' => $model]
            ]
        ];
        if (!$model->shop->isNewRecord) {
            $items['open'] = [
                'label'   => '<span class="far fa-calendar" style="margin-right: 10px;"></span>' . Yii::t('phycom/backend/shop', 'Opening hours'),
                'view'    => 'partials/open-weekdays',
                'params'  => ['formModel' => $model->openForm],
                'visible' => Yii::$app->commerce->shopOpen->isEnabled()
            ];
            $items['closed'] = [
                'label'   => '<span class="far fa-calendar" style="margin-right: 10px;"></span>' . Yii::t('phycom/backend/shop', 'Closed schedule'),
                'view'    => 'partials/closed-schedule',
                'params'  => ['model' => $model->closedForm],
                'visible' => Yii::$app->commerce->shopClosed->isEnabled()
            ];
            $items['supply'] = [
                'label'   => '<span class="fas fa-truck" style="margin-right: 10px;"></span>' . Yii::t('phycom/backend/shop', 'Supply days'),
                'view'    => 'partials/supply-weekdays',
                'params'  => ['formModel' => $model->supplyForm],
                'visible' => Yii::$app->commerce->shopSupply->isEnabled()
            ];
            $items['content'] = [
                'label'          => '<span class="far fa-sticky-note" style="margin: 0 10px 0 1px;"></span>' . Yii::t('phycom/backend/shop', 'Content'),
                'view'           => 'partials/content',
                'params'         => ['model' => $model->contentForm],
                'formCollection' => true,
                'formSubmitAjax' => true,
                'formAction'     => $model->contentForm->post->isNewRecord
                    ? Url::toRoute(['/post/add', 'type' => PostType::SHOP])
                    : Url::toRoute(['/post/edit', 'id' => $model->contentForm->post->id]),
                'visible'        => Yii::$app->commerce->shopContent->isEnabled()
            ];
        }
        return $items;
    }

    /**
     * @return string
     * @throws yii\base\Exception
     * @throws yii\web\ForbiddenHttpException
     */
	public function actionIndex()
	{
		$this->checkPermission('search_shops');

		$model = Yii::$app->modelFactory->getShopSearch();
		$dataProvider = $model->search(Yii::$app->request->get());

		return $this->render('index', ['model' => $model, 'dataProvider' => $dataProvider]);
	}

    /**
     * @param $id
     * @return string
     * @throws NotFoundHttpException
     * @throws \Throwable
     * @throws yii\db\Exception
     * @throws yii\web\ForbiddenHttpException
     */
	public function actionEdit($id)
	{
		$this->checkPermission('update_shop');
		$shop = $this->findShop($id);
		$model = Yii::$app->modelFactory->getShopForm($shop);

		$this->checkActiveTab();

		if ($model->load(Yii::$app->request->post())) {
		    if ($model->update()) {
		        FlashMsg::success(Yii::t('phycom/backend/shop', 'Shop info successfully updated'));
            } else {
                FlashMsg::error($model->hasErrors() ? $model->errors : Yii::t('phycom/backend/shop', 'Error updating shop'));
            }
        }

        if ($model->closedForm->load(Yii::$app->request->post())) {
            if ($model->closedForm->save()) {
                FlashMsg::success(Yii::t('phycom/backend/shop', 'Closed schedule successfully updated'));
            } else {
                FlashMsg::error($model->closedForm->hasErrors() ? $model->closedForm->errors : Yii::t('phycom/backend/shop', 'Error updating schedule'));
            }
        }

        if ($model->openForm->load(Yii::$app->request->post())) {
            if ($model->openForm->save()) {
                FlashMsg::success(Yii::t('phycom/backend/shop', 'Opening hours successfully updated'));
            } else {
                FlashMsg::error($model->openForm->hasErrors() ? $model->openForm->errors : Yii::t('phycom/backend/shop', 'Error updating schedule'));
            }
        }

        if ($model->supplyForm->load(Yii::$app->request->post())) {
            if ($model->supplyForm->save()) {
                FlashMsg::success(Yii::t('phycom/backend/shop', 'Supply dates successfully updated'));
            } else {
                FlashMsg::error($model->supplyForm->hasErrors() ? $model->supplyForm->errors : Yii::t('phycom/backend/shop', 'Error updating schedule'));
            }
        }
        return $this->render('edit', ['items' => $this->getSubmenuItems($model), 'active' => $this->activeTab, 'model' => $model]);
	}

    /**
     * @return string|yii\web\Response
     * @throws yii\web\ForbiddenHttpException
     */
	public function actionAdd()
	{
		$this->checkPermission('create_shop');
        $model = Yii::$app->modelFactory->getShopForm();

        $this->checkActiveTab();

        if ($model->load(Yii::$app->request->post())) {
            if ($model->update()) {
                FlashMsg::success(Yii::t('phycom/backend/shop', 'Shop info successfully updated'));
                return $this->redirect(Url::toRoute(['shop/edit', 'id' => $model->shop->id]));
            } else {
                FlashMsg::error($model->hasErrors() ? $model->errors : Yii::t('phycom/backend/shop', 'Error updating shop'));
            }
        }
		return $this->render('add', ['items' => $this->getSubmenuItems($model), 'active' => 'info', 'model' => $model]);
	}


    /**
     * @param $id - shop id
     * @return Response
     * @throws NotFoundHttpException
     * @throws \Throwable
     * @throws yii\db\StaleObjectException
     * @throws yii\web\ForbiddenHttpException
     */
    public function actionDelete($id)
    {
        $this->checkPermission('delete_shop');
        $shop = $this->findShop($id);

        if ($shop->delete()) {
            FlashMsg::success(Yii::t('phycom/backend/shop', '{type} {id} successfully deleted', ['type' => $shop->type->label, 'id' => $id]));
        } else {
            FlashMsg::error($shop->errors);
        }
        return $this->redirect(Yii::$app->request->referrer);
    }


	protected function checkActiveTab()
    {
        if ($tab = Yii::$app->request->post('tab')) {
            $this->activeTab = $tab;
        }
    }

	/**
	 * @param int $id
	 * @return Shop
	 * @throws NotFoundHttpException
	 */
	protected function findShop($id)
	{
		$shop = Shop::findOne($id);
		if (!$shop) {
			throw new NotFoundHttpException('The requested page does not exist.');
		}
		return $shop;
	}
}
