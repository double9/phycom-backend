<?php

namespace Phycom\Backend\Components;

use Phycom\Base\Interfaces\PhycomComponentInterface;
use Phycom\Base\Models\PostAttachment;
use Phycom\Base\Models\Product\Product;
use Phycom\Base\Models\Product\ProductAttachment;
use Phycom\Base\Models\Product\ProductCategory;
use Phycom\Base\Models\Product\Variant;
use Phycom\Base\Models\Product\Param;
use Phycom\Base\Models\Shop;
use Phycom\Base\Models\User;
use Phycom\Base\Models\Vendor;
use Phycom\Base\Modules\Delivery\Models\DeliveryArea;

/**
 * Class ModelFactory
 *
 * @package Phycom\Backend\Components
 *
 *
 * ### Product ###
 * @method \Phycom\Backend\Models\Product\ProductPrice getProductPrice(array $config = [])
 * @method \Phycom\Backend\Models\Product\SearchProduct getSearchProduct(array $config = [])
 * @method \Phycom\Backend\Models\Product\ProductAttachmentForm getProductAttachmentForm(Product $product, array $config = [])
 * @method \Phycom\Backend\Models\Product\ProductAttachmentOptionsForm getProductAttachmentOptionsForm(ProductAttachment $attachment, array $config = [])
 * @method \Phycom\Backend\Models\Product\ProductBulkUpdateForm getProductBulkUpdateForm(array $config = [])
 * @method \Phycom\Backend\Models\Product\ProductCategoryForm getProductCategoryForm($languageCode, ProductCategory $category = null, array $config = [])
 * @method \Phycom\Backend\Models\Product\ProductForm getProductForm(Product $product = null, User $user = null, array $config = [])
 * @method \Phycom\Backend\Models\Product\ProductModelCollectionForm getProductModelCollectionForm(Product $product,  array $config = [])
 * @method \Phycom\Backend\Models\Product\ProductParamForm getProductParamForm(Product $product, User $user = null, array $config = [])
 * @method \Phycom\Backend\Models\Product\ProductPricingForm getProductPricingForm(array $config = [])
 * @method \Phycom\Backend\Models\Product\ProductPricingOptionsForm getProductPricingOptionsForm(Product $product = null, array $config = [])
 * @method \Phycom\Backend\Models\Product\ProductTranslationForm getProductTranslationForm(Product $product, array $config = [])
 * @method \Phycom\Backend\Models\Product\ProductVariantForm getProductVariantForm(Product $product, User $user = null, array $config = [])
 * @method \Phycom\Backend\Models\Product\SearchProductCategory getSearchProductCategory(array $config = [])
 * @method \Phycom\Backend\Models\Product\SearchProductOption getSearchProductOption(array $config = [])
 * @method \Phycom\Backend\Models\Product\SearchProductParam getSearchProductParam(array $config = [])
 * @method \Phycom\Backend\Models\Product\ParamForm getParamForm(Param $variant, array $config = [])
 * @method \Phycom\Backend\Models\Product\ParamCollectionForm getParamCollectionForm(array $config = [])
 * @method \Phycom\Backend\Models\Product\SearchProductPrice getSearchProductPrice(array $config = [])
 * @method \Phycom\Backend\Models\Product\SearchProductVariant getSearchProductVariant(array $config = [])
 * @method \Phycom\Backend\Models\Product\VariantForm getVariantForm(Variant $variant, array $config = [])
 * @method \Phycom\Backend\Models\Product\VariantCollectionForm getVariantCollectionForm(array $config = [])
 *
 *
 * ### Order ###
 * @method \Phycom\Backend\Models\SearchOrder getSearchOrder(array $config = [])
 *
 *
 * ### Post ###
 * @method \Phycom\Backend\Models\PostAttachmentOptionsForm getPostAttachmentOptionsForm(PostAttachment $attachment, array $config = [])
 *
 *
 * ### Delivery Area ###
 * @method \Phycom\Backend\Models\DeliveryArea\BulkUpdateForm getDeliveryAreaBulkUpdateForm(array $config = [])
 * @method \Phycom\Backend\Models\DeliveryArea\DeliveryAreaForm getDeliveryAreaForm(DeliveryArea $deliveryArea = null, array $config = [])
 * @method \Phycom\Backend\Models\DeliveryArea\Search getDeliveryAreaSearch(array $config = [])
 * @method \Phycom\Backend\Models\DeliveryArea\AddressForm getDeliveryAreaAddressForm(DeliveryArea $deliveryArea, array $config = [])
 * @method \Phycom\Backend\Models\DeliveryArea\DailyScheduleForm getDeliveryAreaDailyScheduleForm(DeliveryArea $deliveryArea, array $config = [])
 * @method \Phycom\Backend\Models\DeliveryArea\WeeklyScheduleForm getDeliveryAreaWeeklyScheduleForm(DeliveryArea $deliveryArea, array $config = [])
 * @method \Phycom\Backend\Models\DeliveryArea\SettingsForm getDeliveryAreaSettingsForm(DeliveryArea $deliveryArea, array $config = [])
 *
 *
 * ### Delivery Settings ###
 * @method \Phycom\Backend\Models\DeliverySettings\DeliverySettingsForm getDeliverySettingsForm(array $config = [])
 *
 *
 * ### Vendor ###
 * @method \Phycom\Backend\Models\Vendor\VendorForm getVendorForm(Vendor $vendor = null, array $config = [])
 * @method \Phycom\Backend\Models\Vendor\MainVendorForm getMainVendorForm(Vendor $vendor = null, array $config = [])
 * @method \Phycom\Backend\Models\Vendor\Search getVendorSearch(array $config = [])
 * @method \Phycom\Backend\Models\Vendor\AddressForm getVendorAddressForm(Vendor $vendor, array $config = [])
 *
 *
 * ### Shop ###
 * @method \Phycom\Backend\Models\Shop\ShopForm getShopForm(Shop $shop = null, array $config = [])
 * @method \Phycom\Backend\Models\Shop\Search getShopSearch(array $config = [])
 * @method \Phycom\Backend\Models\Shop\AddressForm getShopAddressForm(Shop $shop, array $config = [])
 * @method \Phycom\Backend\Models\Shop\OpenScheduleCollectionForm getShopOpenScheduleCollectionForm(Shop $shop, array $config = [])
 * @method \Phycom\Backend\Models\Shop\ClosedScheduleForm getShopClosedScheduleForm(Shop $shop, array $config = [])
 * @method \Phycom\Backend\Models\Shop\SupplyScheduleCollectionForm getShopSupplyScheduleCollectionForm(Shop $shop, array $config = [])
 *
 *
 * ### Var ###
 * @method \Phycom\Backend\Models\SearchInvoice getSearchInvoice(array $config = [])
 * @method \Phycom\Backend\Models\OrderForm getOrderForm(array $config = [])
 * @method \Phycom\Backend\Models\AddressFieldForm getAddressFieldForm(array $config = [])
 * @method \Phycom\Backend\Models\ChangePasswordForm getChangePasswordForm(array $config = [])
 * @method \Phycom\Backend\Models\ComponentForm getComponentForm(PhycomComponentInterface $component, array $config = [])
 */
class ModelFactory extends \Phycom\Base\Components\ModelFactory
{
    protected array $definitions = [

        ### Product ###
        'getProductPrice'                 => 'Backend\Models\Product\ProductPrice',
        'getSearchProduct'                => 'Backend\Models\Product\SearchProduct',
        'getProductAttachmentForm'        => 'Backend\Models\Product\ProductAttachmentForm',
        'getProductAttachmentOptionsForm' => 'Backend\Models\Product\ProductAttachmentOptionsForm',
        'getProductBulkUpdateForm'        => 'Backend\Models\Product\ProductBulkUpdateForm',
        'getProductCategoryForm'          => 'Backend\Models\Product\ProductCategoryForm',
        'getProductForm'                  => 'Backend\Models\Product\ProductForm',
        'getProductModelCollectionForm'   => 'Backend\Models\Product\ProductModelCollectionForm',
        'getProductParamForm'             => 'Backend\Models\Product\ProductParamForm',
        'getProductPricingForm'           => 'Backend\Models\Product\ProductPricingForm',
        'getProductPricingOptionsForm'    => 'Backend\Models\Product\ProductPricingOptionsForm',
        'getProductTranslationForm'       => 'Backend\Models\Product\ProductTranslationForm',
        'getProductVariantForm'           => 'Backend\Models\Product\ProductVariantForm',
        'getSearchProductCategory'        => 'Backend\Models\Product\SearchProductCategory',
        'getSearchProductOption'          => 'Backend\Models\Product\SearchProductOption',
        'getSearchProductParam'           => 'Backend\Models\Product\SearchProductParam',
        'getParamForm'                    => 'Backend\Models\Product\ParamForm',
        'getParamCollectionForm'          => 'Backend\Models\Product\ParamCollectionForm',
        'getSearchProductPrice'           => 'Backend\Models\Product\SearchProductPrice',
        'getSearchProductVariant'         => 'Backend\Models\Product\SearchProductVariant',
        'getVariantForm'                  => 'Backend\Models\Product\VariantForm',
        'getVariantCollectionForm'        => 'Backend\Models\Product\VariantCollectionForm',

        ### Order ###
        'getSearchOrder'                  => 'Backend\Models\SearchOrder',

        ### Post ###
        'getPostAttachmentOptionsForm'    => 'Backend\Models\PostAttachmentOptionsForm',


        ### Review ###
        'getReviewBulkUpdateForm' => 'Backend\Models\Review\ReviewBulkUpdateForm',
        'getSearchReview'         => 'Backend\Models\Review\SearchReview',


        ### Comment ###
        'getCommentBulkUpdateForm' => 'Backend\Models\Comment\CommentBulkUpdateForm',
        'getSearchComment'         => 'Backend\Models\Comment\SearchComment',


        ### Delivery Area ###
        'getDeliveryAreaForm'               => 'Backend\Models\DeliveryArea\DeliveryAreaForm',
        'getDeliveryAreaAddressForm'        => 'Backend\Models\DeliveryArea\AddressForm',
        'getDeliveryAreaSearch'             => 'Backend\Models\DeliveryArea\Search',
        'getDeliveryAreaBulkUpdateForm'     => 'Backend\Models\DeliveryArea\BulkUpdateForm',
        'getDeliveryAreaDailyScheduleForm'  => 'Backend\Models\DeliveryArea\DailyScheduleForm',
        'getDeliveryAreaWeeklyScheduleForm' => 'Backend\Models\DeliveryArea\WeeklyScheduleForm',
        'getDeliveryAreaSettingsForm'       => 'Backend\Models\DeliveryArea\SettingsForm',

        ### Delivery Settings ###
        'getDeliverySettingsForm'           => 'Backend\Models\DeliverySettings\DeliverySettingsForm',


        ### Vendor ###
        'getVendorForm'        => 'Backend\Models\Vendor\VendorForm',
        'getMainVendorForm'    => 'Backend\Models\Vendor\MainVendorForm',
        'getVendorSearch'      => 'Backend\Models\Vendor\Search',
        'getVendorAddressForm' => 'Backend\Models\Vendor\AddressForm',


        ### Shop ###
        'getShopForm'                         => 'Backend\Models\Shop\ShopForm',
        'getShopSearch'                       => 'Backend\Models\Shop\Search',
        'getShopAddressForm'                  => 'Backend\Models\Shop\AddressForm',
        'getShopOpenScheduleCollectionForm'   => 'Backend\Models\Shop\OpenScheduleCollectionForm',
        'getShopClosedScheduleForm'           => 'Backend\Models\Shop\ClosedScheduleForm',
        'getShopSupplyScheduleCollectionForm' => 'Backend\Models\Shop\SupplyScheduleCollectionForm',


        ### Var ###
        'getSearchInvoice'      => 'Backend\Models\SearchInvoice',
        'getOrderForm'          => 'Backend\Models\OrderForm',
        'getAddressFieldForm'   => 'Backend\Models\AddressFieldForm',
        'getChangePasswordForm' => 'Backend\Models\ChangePasswordForm',
        'getComponentForm'      => 'Backend\Models\ComponentForm'
    ];
}
