<?php

namespace Phycom\Backend\Components\Commerce;

use Phycom\Base\Interfaces\CommerceComponentInterface;

use yii\base\BaseObject;

/**
 * Class Sale
 *
 * @package Phycom\Backend\Components\Commerce
 */
class Sale extends BaseObject implements CommerceComponentInterface
{
    public bool $enabled = true;

    public function isEnabled(): bool
    {
        return $this->enabled;
    }

    public function getName(): string
    {
        return 'sale';
    }

    public function getLabel(): string
    {
        return Yii::t('backend/components', 'Sale');
    }

    public function getJsonSchema(): array
    {
        return [];
    }
}
