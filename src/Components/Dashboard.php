<?php

namespace Phycom\Backend\Components;

use Phycom\Base\Components\PhycomComponent;
use Phycom\Base\Interfaces\PhycomComponentInterface;

/**
 * Class Dashboard
 *
 * @package Phycom\Backend\Components
 */
class Dashboard extends PhycomComponent implements PhycomComponentInterface
{

}
