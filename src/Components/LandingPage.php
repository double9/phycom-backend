<?php

namespace Phycom\Backend\Components;

use Phycom\Base\Components\PhycomComponent;
use Phycom\Base\Interfaces\PhycomComponentInterface;

/**
 * Class LandingPage
 *
 * @package Phycom\Backend\Components
 */
class LandingPage extends PhycomComponent implements PhycomComponentInterface
{
    /**
     * @var bool - determines if multiple landing pages can be set published or only one.
     */
    public bool $onlyOnePublished = true;
}
