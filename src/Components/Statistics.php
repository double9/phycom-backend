<?php

namespace Phycom\Backend\Components;

use Phycom\Base\Components\PhycomComponent;
use Phycom\Base\Interfaces\PhycomComponentInterface;

/**
 * Class Statistics
 *
 * @package Phycom\Backend\Components
 */
class Statistics extends PhycomComponent implements PhycomComponentInterface
{

}
